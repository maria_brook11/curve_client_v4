var PRODUCTION = false;

var dashboard_css_files = [
			'Javascript/Plugins/Jquery/jquery-multi-select/css/multi-select.css',
			'Javascript/Plugins/Jquery/jqvmap/jqvmap.css',
			'Javascript/Plugins/Jquery/jquery-notific8/jquery.notific8.min.css',
			'Javascript/Plugins/UI/datatables/media/css/jquery.dataTables.min.css',
			'Javascript/Plugins/Jquery/jquery.qtip.custom/jquery.qtip.min.css',
			'Javascript/Plugins/Bootstrap/bootstrap-select/bootstrap-select.min.css',
			'Javascript/Plugins/Bootstrap/bootstrap-daterangepicker/daterangepicker-bs3.css',
			'Javascript/Plugins/Bootstrap/bootstrap-tour/bootstrap-tour.min.css',
			'Javascript/Plugins/UI/select2/select2.css',
			'Javascript/Plugins/UI/ammap/ammap.css',
			'Styles/Pages/Dashboard/insights.css',
			'Styles/Pages/Dashboard/counters.css',
			'Styles/Pages/Dashboard/journey.css',
			'Styles/Pages/Dashboard/create.css',
			'Styles/Pages/Dashboard/curve.css',
			'Styles/Pages/Dashboard/status.css',
			'Styles/Pages/Dashboard/engCenter.css',   
			'Styles/Pages/Dashboard/comparable_knob.css',
			'Styles/Pages/Dashboard/graphs.css',
			'Styles/Pages/Dashboard/rzslider.css',
			'Styles/Pages/Dashboard/single_customer.css',
			'Styles/Pages/audience.css',
			'Styles/Pages/Dashboard/go.css'
		];

var login_css_files = [
			'Javascript/Plugins/UI/select2/select2.css',
			'Styles/Pages/login.css',
			'Styles/Libraries/bootstrap-tour.min.css',
			'Styles/Global/curve.css'
		];

var index_css_files = [
			'Styles/Libraries/bootstrap.min.css',
			'Styles/Plugins/font-awesome/css/font-awesome.min.css',
			'Styles/Libraries/simple-line-icons/simple-line-icons.min.css',
			'Styles/Libraries/bootstrap-switch.min.css',
			'Styles/Libraries/components.css',
			'Styles/Libraries/plugins.css',
			'Styles/Libraries/layout.css',
			'Styles/Libraries/default.css',
			'Styles/Global/custom.css',
			'Styles/Global/tooltip.css',
			'Styles/Pages/onboarding.css',
			'Styles/Pages/Dashboard/funnel.css',
			'Styles/Global/responsive.css',
			'Styles/Global/introjscurve.css'
		];
		
var dashboard_js_files = [
		
		'Javascript/Plugins/Jquery/jquery-notific8/jquery.notific8.min.js', 
		'Javascript/Dashboards/dashboards.notifications.js', 
		'Javascript/Plugins/Jquery/jquery.sparkline.min.js', 
		'Javascript/Libraries/Jquery/flot/tooltips.js', 
		'Javascript/Plugins/Jquery/jquery.knob.min.js', 
		'Javascript/Plugins/UI/moment.js', 
		'Javascript/Plugins/UI/canvasjs.min.js',
		'Javascript/Plugins/Bootstrap/bootstrap-daterangepicker/moment.min.js', 
		'Javascript/Plugins/Bootstrap/bootstrap-daterangepicker/daterangepicker.js', 
		'Javascript/Libraries/Bootstrap/bootstrap-datepicker.min.js', 
		'Javascript/Libraries/Bootstrap/Markdown/bootstrap-markdown.js',  
		'Javascript/Libraries/Bootstrap/Markdown/to-markdown.js',  
		'Javascript/Libraries/Bootstrap/Markdown/markdown.js',  
		'Javascript/Plugins/UI/datatables/media/js/jquery.dataTables.min.js', 
		'Javascript/Plugins/UI/datatables/extensions/TableTools/js/dataTables.tableTools.min.js', 
		'Javascript/Plugins/UI/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js', 
		'Javascript/Plugins/UI/datatables/extensions/Scroller/js/dataTables.scroller.min.js', 
		'Javascript/Plugins/UI/datatables/plugins/bootstrap/dataTables.bootstrap.js', 
		'Javascript/Plugins/UI/select2/select2.min.js', 
		'Javascript/Dashboards/dashboards.tables.js', 
		'Javascript/Plugins/Jquery/jquery.easypiechart.min.js', 
		'Javascript/Dashboards/uj.data.js', 
		'Javascript/Plugins/UI/jit.js', 
		'Javascript/Dashboards/dashboards.multiline.js', 
		'Javascript/Dashboards/dashboards.create.graph.js', 
		'Javascript/Dashboards/dashboards.funnel.ui.js', 
		'Javascript/Dashboards/dashboards.funnel.painter.js', 
		'Javascript/Dashboards/dashboards.cohort.factory.js', 
		'Javascript/Dashboards/campaign.js', 
		'Javascript/Dashboards/cohort.email.js', 
		'Javascript/Dashboards/cohort.push.js', 
		'Javascript/Dashboards/cohort.sms.js', 
		'Javascript/Application/Email/emailValidatorManager.js', 
		'Javascript/Dashboards/dashboards.user.search.js', 
		'Javascript/Dashboards/dashboards.js', 
		'Javascript/Dashboards/piechartUtils.js',
		'Javascript/Dashboards/ajaxManager.js', 
		'Javascript/Dashboards/painter.js', 
		'Javascript/Dashboards/data_filtering.js', 
		'Javascript/Dashboards/global.js', 
		'Javascript/Dashboards/dashboards.data.js', 
		'Javascript/Dashboards/dashboards_prev.js',
		'Javascript/Dashboards/audience.js',
		'Javascript/Dashboards/services/go.js',
		'Javascript/Dashboards/diagram.js'
		
		
		];
		
var index_js_files = [
		'Javascript/Libraries/Jquery/jquery-1.11.0.min.js',
		'Javascript/Libraries/Jquery/jquery-migrate-1.2.1.min.js',
		'Javascript/Libraries/Jquery/jquery-ui-1.10.3.custom.min.js',
		'Javascript/Libraries/Jquery/jquery.slimscroll.min.js',
		'Javascript/Libraries/Jquery/jquery.blockui.min.js',
		'Javascript/Libraries/Jquery/jquery.cokie.min.js',
		'Javascript/Plugins/Jquery/jquery.form.min.js',//************
		'Javascript/Plugins/Jquery/clipboard.min.js',
		'Javascript/Plugins/Jquery/ammap/ammap.js', 
		'Javascript/Plugins/Jquery/ammap/worldLow.js',
		'Javascript/Libraries/Angular/angular.min.js',
        'Javascript/Libraries/Angular/angular-material.min.js',
		'Javascript/Libraries/Angular/angular-route.min.js',
        'Javascript/Libraries/Angular/angular-aria.min.js',
		'Javascript/Libraries/Angular/angular-cookies.min.js',
		'Javascript/Libraries/Angular/angular-animate.min.js',
        'Javascript/Libraries/Angular/angular-animate.js',
		'Javascript/Libraries/Angular/angular-uploader.min.js',
		'Javascript/Libraries/Angular/angular-sanitize.min.js',
        /*'Javascript/Libraries/Angular/angular-carousel.js',*/
        'Javascript/Libraries/Angular/angular-touch.js',
		'Javascript/Libraries/Angular/Chart11.js',
		/* 'Javascript/Libraries/Angular/utils.js',
		'Javascript/Libraries/Angular/Chart.bundle.js', */
		'Javascript/Libraries/Angular/angular-chart.js',
		'Javascript/Libraries/Bootstrap/bootstrap.min.js',
		'Javascript/Libraries/Bootstrap/bootstrap-hover-dropdown.min.js',
		'Javascript/Libraries/Bootstrap/bootstrap-switch.min.js',
		'Javascript/Libraries/Bootstrap/metronic.min.js',//************
		'Javascript/Libraries/Bootstrap/layout.min.js',//************
		'Javascript/Libraries/Bootstrap/quick-sidebar.min.js',//************
		'Javascript/Plugins/Jquery/flot/jquery.flot.min.js',
		'Javascript/Plugins/Jquery/flot/jquery.flot.time.js',
		'Javascript/Plugins/Jquery/flot/jquery.flot.resize.min.js',
		'Javascript/Plugins/Jquery/flot/jquery.flot.pie.min.js',
		'Javascript/Plugins/Jquery/flot/jquery.flot.stack.min.js',
		'Javascript/Plugins/Jquery/flot/jquery.flot.crosshair.min.js',
		'Javascript/Plugins/Jquery/flot/jquery.flot.categories.min.js',
		'Javascript/Plugins/UI/toArrayFilter.js',
		'Javascript/Global/general.js',
		'Javascript/Global/constants.js',
		'Javascript/Global/storage.js',
		'Javascript/Global/md5.min.js',
		'Javascript/Global/curveUtils.js',
		'Javascript/Global/APICalls.js',
		'Javascript/Global/alerts.js',
		'Javascript/Global/settings.external.apis.js',
		'Javascript/Global/gamemode.js',
		'Javascript/Global/toppanel.js',
		'Javascript/Global/permissions.js',
		'Javascript/Global/intro.js',
		'Javascript/Global/angular-intro.js',
		'Javascript/Global/main.ctrl.js',
		'Javascript/Global/admin.ctrl.js',
		'Javascript/Global/tooltip.js',
		'Javascript/Login/login.ctrl.js',
		'Javascript/Application/apps.ctrl.js',
		'Javascript/Dashboards/dashbrd.ctrl.js',
		'Javascript/Extern/externCtrl.js',
		'Javascript/Extern/externCharts.js',
		'Javascript/Extern/externMap.js',
		'Javascript/Extern/smallTable.js',
		'Javascript/Extern/funnel.js',
		'Javascript/Onboarding/onboardingCtrl.js',
		'Javascript/Application/applications.js',
		'Javascript/Application/curve.constants.js',
		'Javascript/Application/global.painter.js',
		'Javascript/Libraries/Bootstrap/bootstrap-tour.min.js',
		'Javascript/Application/Tutorials/tutorial.texts.en.js',
		'Javascript/Application/Tutorials/tutorial.factory.js',
		'Javascript/Application/Tutorials/tutorial.manager.js',
		//'Javascript/plugins/Bootstrap/bootstrap-markdown/js/bootstrap-markdown.min.js',//************
		//'Javascript/plugins/Bootstrap/bootstrap-markdown/js/to-markdown.min.js',//************
		//'Javascript/plugins/Bootstrap/bootstrap-markdown/js/markdown.min.js',//************
		'Javascript/Application/Email/sendEmail.js',
		'Javascript/Application/Email/emailValidatorManager.js',
		//'Javascript/Libraries/Jquery/ZeroClipboard.js',//************
		'Javascript/Dashboards/directives/graph.map.js',
		'Javascript/Dashboards/directives/graph.SparkLine.js',
		'Javascript/Dashboards/directives/graph.Charts.js',
		'Javascript/Dashboards/directives/smallTable.js',
		'Javascript/Dashboards/directives/slimScroll.js',
		'Javascript/Dashboards/directives/funnel.js',
		'Javascript/Dashboards/directives/funnelPreview.js',
		'Javascript/Dashboards/directives/counterGroup.js',
		'Javascript/Dashboards/directives/multiselectDropdown.js',
		'Javascript/Dashboards/directives/goDiagram.js',
		'Javascript/Dashboards/datepicker.js',
		'Javascript/Dashboards/directives/compar.graph.js',
		'Javascript/Dashboards/directives/comparable_knob.js',
		'Javascript/Dashboards/directives/rzSlider.js',
		'Javascript/Dashboards/directives/canvasLine.js',
        'Javascript/Dashboards/directives/mobile-swipe.js',

		'Javascript/Dashboards/services/services.js',
		'Javascript/Dashboards/services/sequence.data.convertor.js',
		'Javascript/Dashboards/services/diagramValidation.js',
		'Javascript/Plugins/Jquery/ngclipboard.min.js'
		];

var login_js_files = [
		'Javascript/Libraries/Jquery/jquery.validate.min.js', 
		'Javascript/Plugins/UI/select2/select2.min.js', 
		'Javascript/Login/logout.js', 
		'Javascript/Login/login.global.js',  
		'Javascript/Login/login.handler.js', 
		'Javascript/Login/register.trial.handler.js', 
		'Javascript/Login/start.trial.handler.js', 
		'Javascript/Login/resetPassword.js', 
		'Javascript/Libraries/Bootstrap/Markdown/bootstrap-markdown.js',  
		'Javascript/Libraries/Bootstrap/Markdown/to-markdown.js',  
		'Javascript/Libraries/Bootstrap/Markdown/markdown.js'
		];

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var minifycss = require('gulp-minify-css');
var concat = require('gulp-concat');
var gutil = require('gulp-util');
var wrap = require('gulp-wrap');
var strip = require('gulp-strip-comments');


	
// Internal Libraries
gulp.task('index_libs', function()
	{
		return gulp.src(index_js_files)
		.pipe( strip() )
		.pipe(wrap('//########### <%= file.path %>\n<%= contents %>'))
		.pipe( concat('Javascript/index_libs.js') )
		//.pipe( uglify() )
		.pipe(gulp.dest('dist'));
	});
	
// Fonts Libraries
gulp.task('fonts_copy', function()
	{
		return gulp.src(['Styles/fonts/**/*.*', 'Styles/Plugins/font-awesome/fonts/**/*.*'])
		.pipe(gulp.dest('dist/fonts'));
	});
	
	// Includes Libraries
gulp.task('includes', function()
	{
		return gulp.src(['includes/**/*.*'])
		.pipe(gulp.dest('dist/includes'));
	});
	
// Fonts Libraries
gulp.task('fonts_copy_2', function()
	{
		return gulp.src(['Styles/Libraries/simple-line-icons/fonts/**/*.*'])
		.pipe(gulp.dest('dist/Styles/fonts'));
	});	
// Login Libraries
gulp.task('login_libs', function()
	{	
		var resp = gulp.src(login_js_files)
		.pipe(wrap('//########### <%= file.path %>\n<%= contents %>'))
		.pipe( concat('Javascript/login_libs.js') );
		
		
		if(PRODUCTION != null && PRODUCTION)
			resp = resp.pipe( uglify() );
		
		resp = resp.pipe(gulp.dest('dist'));
		return resp;
	});

// Dashboards Libraries
gulp.task('dashboards_libs', function()
	{
		var resp = gulp.src(dashboard_js_files)
		.pipe(wrap('//########### <%= file.path %>\n<%= contents %>'))
		.pipe( concat('Javascript/dashboards_libs.js') );
		
		
		if(PRODUCTION != null && PRODUCTION)
			resp = resp.pipe( uglify() );
		
		resp = resp.pipe(gulp.dest('dist'));
		return resp;
	});	


// HTML Copy
gulp.task('index_html_copy', function()
	{
		return gulp.src(['index.html'])
		.pipe(gulp.dest('dist'));
	});
	
// HTML Copy
gulp.task('html_copy', function()
	{
		return gulp.src(['Templates/**/*.html', 'v/*.html'])
		.pipe(gulp.dest('dist/Templates'));
	});
	
// Images Copy
gulp.task('images_copy', function()
	{
		return gulp.src(['Images/**/*.*'])
		.pipe(gulp.dest('dist/Images'));
	});
	
// Other Bitmaps Copy
gulp.task('bitmaps_copy', function()
	{
		return gulp.src(['Javascript/**/*.gif', 'Javascript/**/*.png', 'Javascript/**/*.gif', 'Javascript/**/*.svg' ])
		.pipe(gulp.dest('dist/Javascript'));
	});	
	
// Minify CSS
gulp.task('index_styles', function()
	{
		var resp = gulp.src(index_css_files)
		.pipe(wrap('/*########### <%= file.path %>*/\n<%= contents %>'))
		.pipe( concat('Styles/index_styles.css') );
		
		if(PRODUCTION != null && PRODUCTION)
			resp = resp.pipe(minifycss({compatibility: 'ie8'}));
		
		resp = resp.pipe(gulp.dest('dist'));
		return resp;
		
		
	});
	
// Minify CSS
gulp.task('login_styles', function()
	{
		var resp = gulp.src(login_css_files)
		.pipe(wrap('/*########### <%= file.path %>*/\n<%= contents %>'))
		.pipe( concat('Styles/login_styles.css') );
		
		if(PRODUCTION != null && PRODUCTION)
			resp = resp.pipe(minifycss({compatibility: 'ie8'}));
		
		resp = resp.pipe(gulp.dest('dist'));
		return resp;
	});	
	
// Minify CSS
gulp.task('dashboards_styles', function()
	{
		var resp = gulp.src(dashboard_css_files)
		.pipe(wrap('/*########### <%= file.path %>*/\n<%= contents %>'))
		.pipe( concat('Styles/dashboards_styles.css') );
		
		if(PRODUCTION != null && PRODUCTION)
			resp = resp.pipe(minifycss({compatibility: 'ie8'}));
		
		resp = resp.pipe(gulp.dest('dist'));
		return resp;
	});	

gulp.task('default', function()
	{
		gulp.watch(dashboard_css_files, ['dashboards_styles']);
		gulp.watch(login_css_files, ['login_styles']);
		gulp.watch(index_css_files, ['index_styles']);
		
		gulp.watch(dashboard_js_files, ['dashboards_libs']);
		gulp.watch(index_js_files, ['index_libs']);
		gulp.watch(login_js_files, ['login_libs']);
		
		gulp.watch(['Images/**/*.*'], ['images_copy']);
		gulp.watch(['Templates/**/*.html'], ['html_copy']);
		gulp.watch(['index.html'], ['index_html_copy']);
	});
	
//gulp.start('default');
gulp.start([
			//'default',
			// Styles tasks
			'index_styles', 
			'dashboards_styles', 
			'login_styles', 
			
			// Scripts
			'index_libs' , 
			'login_libs', 
			'dashboards_libs', 
			
			// Copy
			'images_copy', 
			'bitmaps_copy',
			'fonts_copy',
			'includes',
			'fonts_copy_2',
			'html_copy', 
			'index_html_copy']);