var Crv = function(){}
Crv.curveinstance = null;

// *************** STATIC *********************

Crv.DEBUG = false;
Crv.MAX_CONFIG_RETRY = 10;

try{
	if(CrvOverrideDebugMode != null)
	{
		Crv.DEBUG = CrvOverrideDebugMode;
	}
}catch(s){}


Crv.defer = function (method) 
{
    if (window.jQuery)
        method();
    else
        setTimeout(function() { Crv.defer(method) }, 5);
}
	
Crv.i = function(t, c)
{
	Crv.curveinstance = Curve.init(t, c);
	return Crv.curveinstance;
}

Crv.setSuperProperty = function(k, v)
{
	if(Crv.curveinstance == null)
		Crv.curveinstance = new Curve();
	
	Crv.curveinstance.setSuperProperty(k, v);
}

Crv.setSessionProperty = function(k, v)
{
	if(Crv.curveinstance == null)
		Crv.curveinstance = new Curve();
	
	Crv.curveinstance.setSessionProperty(k, v);
}


Crv.clk = function(c, i)
{
	if(Crv.curveinstance != null)
		Crv.curveinstance.setupClicks(c, i);
	else
		console.log("Curve not initialized! Please call Crv.clk([], []); only after you've called Crv.i(token, config)");
}

Crv.err = function(w)
{
	if(Crv.curveinstance != null)
		Crv.curveinstance.setupGlobalErrors(w);
	else
		console.log("Curve not initialized! Please call Crv.err(window); only after you've called Crv.i(token, config)");
}

Crv.screen = function(sourceScreen, targetScreen, sourceVisitTimeSecs, type, props){Crv.curveinstance.screen(sourceScreen, targetScreen, sourceVisitTimeSecs, type, props);};
Crv.click = function(buttonID, state, xx, yy, screenName, props){Crv.curveinstance.click(buttonID, state, xx, yy, screenName, props);};
Crv.event = function(name, category, action, label, value, props){Crv.curveinstance.customEvent(category, action, label, value, name, props);};
Crv.user = function(firstName, lastName, email, locale, gender, socialNwType, socialNwID, phone, age, birthdate, country, city, push_token, props){Crv.curveinstance.user(firstName, lastName, email, locale, gender, socialNwType, socialNwID, phone, age, birthdate, country, city, push_token, props);};
Crv.ad = function(adsProviderID, adsProviderName, adPlatform, adType, adEvent, props){Crv.curveinstance.ad(adsProviderID, adsProviderName, adPlatform, adType, adEvent, props);};
Crv.social = function(socialNetworkType, socialNetworkID, eventType, param1, param2, props){Crv.curveinstance.social(socialNetworkType, socialNetworkID, eventType, param1, param2, props);};
Crv.error = function(errorDecription, stackTrace, props){Crv.curveinstance.error(errorDecription, stackTrace, props);};
Crv.app = function(eventType, props){Crv.curveinstance.appLifeCycle(eventType, props);};
Crv.transaction = function(transactionType, transactionSubType, virtualCurrency1Delta, virtualCurrency1Balance, virtualCurrency2Delta, virtualCurrency2Balance, sourcePlatform, sourceTranID, transactionId, originalTransactionID, receiptSignature, realMoneyValue, errorMessage, props)
					{
						Crv.curveinstance.transaction(transactionType, transactionSubType, virtualCurrency1Delta, virtualCurrency1Balance, virtualCurrency2Delta, virtualCurrency2Balance, sourcePlatform, sourceTranID, transactionId, originalTransactionID, receiptSignature, realMoneyValue, errorMessage, props);
					};

Crv.regPlugin = function(p)
{
	if(Crv.curveinstance == null)
	{
		if(Crv.plgns == null)
			Crv.plgns = [];
	
		Crv.plgns.push(p);
	}
	else
	{
		p.init();
	}
	
}

// *************** STATIC *********************

// **********************************************

var Curve = function()
{
	console.log("Curve started. V." + Curve.VERSION);
};

// ************* STATIC FIELDS ******************

Curve.VERSION = '1.3.2';
Curve.CURRENT_PLATFORM = 'web';
// Curve.TRACK_URL = "https://api.curve.tech:8443/API/";
Curve.TRACK_URL = "https://api.curve.tech:8443/API/";
Curve.CONFIG_SERVLET = "GetGameConfig";
Curve.ADS_CONFIG_SERVLET = "GetAdsProvidersWaterfall";
Curve.CLICK_SERVLET = "Clicks";
Curve.TRANSACTION_SERVLET = "Transactions";
Curve.APPLICATION_SERVLET = "AppLifeCycle";
Curve.SESSION_DATA_SERVLET = "SessionData";
Curve.TRAFFIC_SERVLET = "TrafficSources";
Curve.SOCIAL_SERVLET = "Socials";
Curve.ADS_SERVLET = "Advertisements";
Curve.USERS_SERVLET = "Users";
Curve.SCREENS_SERVLET = "Screens";
Curve.CUSTOM_EVENTS_SERVLET = "Custom";
Curve.ERROR_SERVLET = "Errors";

// ************
Curve.SESSION_IDLE_TIMEOUT_MS = 1000*60*30; // 30 Minutes
Curve.TIME_TO_SEND_FAST = 1000;
Curve.TIME_TO_SEND_SLOW = 10000;
Curve.BATCH_SIZE_FAST = 1;
Curve.BATCH_SIZE_SLOW = 10;

Curve.ValueLimitChars = 15;
Curve.MaxJSONLimitChars = 10000;

Crv.starttime = new Date().getTime();
Crv.pageloadtimesec = null;

Crv.updatePageLoaded = function(){jQuery(document).ready(function(){Crv.pageloadtimesec = (new Date().getTime()-Crv.starttime)/1000;});};
Crv.defer(Crv.updatePageLoaded);

// ************************ COOKIES ******************************
// State
Crv.CURVE_STATE_COOKIE = 'curve_state_cookie';
Crv.CURVE_LAST_EVENT_COOKIE = 'curve_lastevent_cookie';
//Screens
Crv.PREV_SCREEN_COOKIE = 'curve_prevsecreen_cookie';
Crv.PREV_SCREEN_NAME_URL_COOKIE = 'curve_prevsecreenurl_cookie';
Crv.PREV_SCREEN_TIME_COOKIE = 'curve_prevsecreen_time_cookie';
// Session Identifiers
Crv.CURVE_SESSION_COOKIE_KEY = 'curve_session_cookie';
Crv.CURVE_HARDWARE_COOKIE_KEY = 'curve_hw_cookie';
Crv.CURVE_USERID_COOKIE_KEY = 'curve_userid_cookie';
Crv.CURVE_USERID_COOKIE_OBJ_KEY = 'curve_userid_cookie_obj';
// ************************ COOKIES ******************************

Curve.init = function(token, config)
{
	if(Crv.curveinstance == null)
		Crv.curveinstance = new Curve();
	
	Crv.curveinstance.init(token, config);
	return Crv.curveinstance;
}

Curve.saveValue = function(val)
{
	if(!isNaN(val))
		return val;
	if(val == "true" || val == "false")
		return val;
	
	return "\"" + val + "\"";
}

Curve.stringifyToJSON = function(map)
{
	var ans = "[";

	for(key in map)
	{
		var next = map[key];
		var counter = 0;
		for(k in next)
		{
			if(counter == 0)
				ans += "{";	
			
			counter++;
			ans += "\"" + k + "\":" + Curve.saveValue(next[k]) + ","; 			
		}
		
		if(counter > 0)
		{
			ans = ans.substr(0, ans.length-1);
			ans += "},";
		}
	}

	if(ans.length != 1) ans = ans.substr(0, ans.length-1);				

	ans += "]";

	return ans;
};

Curve.limitValueOfObject = function (val)
{
	if (val == null)
		return null;
	if (typeof val === 'string' || val instanceof String)
	{
		if (val.length > Curve.ValueLimitChars)
		{
			return val.substr(0, Curve.ValueLimitChars);
		}
		else
			return val;
	}
	else if (typeof val === 'boolean' || val instanceof Boolean || typeof val === 'number' || val instanceof Number || typeof val === 'function' || val instanceof Function)
	{
		return val;
	}
	else
	{
		$.each(val, function(key, value){
			val[key] = Curve.limitValueOfObject(value);
		});
		
		return val;
	}
};
	
Curve.truncateJson = function (obj)
{
	var str = JSON.stringify(obj);
	if (str.length > Curve.MaxJSONLimitChars)
	{
		var newObj = {};
		$.each(obj, function(key, value){

			// console.log("CHECK obj-key:" + key);
			newObj[key] = Curve.limitValueOfObject(value);

		});
		
		
		var str2 = JSON.stringify(newObj);
		console.log("AFTER: " + str2.length);
		return newObj;
	}
	else
		return obj;
};

Curve.prototype = {
	
	constructor: Curve,
	
	// ************* CLASS MEMBERS ******************
	
	_servletQueue: {},
	_timeoutConfig: 0,
	_superProps: null,
	_sessionProps: null,
	_interval: -1,
	_printout: null,
	_config: {},
	_pageMetadata: null,
	_queue: [],
	_appStartTime: 0,
	_token: "abc",
	_ready: false,
	_initialized: false,
	_window: null,
	_trackedClasses_local: [],
	_trackedClasses_server: [],
	_trackedIDs_local: [],
	_trackedIDs_server: [],
	_window: null,
	_lastEvent: -1,
		
	// ************* CLASS MEMBERS ******************
	
	init: function(token_, configData_) 
	{
		this._token = token_;
		UserIDGenerator.getInstance().setToken(this._token);
			
		if(configData_ != null)
		{
			if(configData_.superProps != null)
			{
				if(this._superProps == null)
					this._superProps = configData_.superProps;
				else
				{
					for(k in configData_.superProps)
						this.setSuperProperty(k, configData_.superProps[k])
				}				
			}
			
			this._pageMetadata = configData_.pageMetadata;
			
			if(configData_.userID != null) 
				UserIDGenerator.getInstance().setUserID(configData_.userID);

			if(configData_.printToConsole != null && configData_.printToConsole == true) 
				this.setPrintout(function(str){console.log(str);});
		}
		else
		{
		//	this.setPrintout(function(str){console.log(str);});
		}
		
		this.getBatchLoader(Curve.CLICK_SERVLET, false);
		this.getBatchLoader(Curve.SCREENS_SERVLET, false);
		this.getBatchLoader(Curve.CUSTOM_EVENTS_SERVLET, false);

		this._initialized = true;
		this.config();
		
		if(Crv.plgns != null)
		{
			for(i=0;i<Crv.plgns.length;i++)
			{
				var plg = Crv.plgns[i];
				if(plg != null) plg.init();
			}
		}
	},
	
	setSuperProperty: function(key, value)
	{
		if(key == null || key == "") return;
		
		if(this._superProps == null)
			this._superProps = {};
		
		this._superProps[key] = value;
	},
	
	setSessionProperty: function(key, value)
	{
		if(key == null || key == "") return;
		
		if(this._sessionProps == null)
			this._sessionProps = {};
		
		this._sessionProps[key] = value;
	},
	
	setupClicks: function(classes, ids) 
	{
		if(classes == null)
			classes = [];
		if(ids == null)
			ids = [];
		this._trackedClasses_local = classes;
		this._trackedIDs_local = ids;
		
		document.onclick = this.clickListener;
	},
	
	clickListener: function(e)
	{
		var el;
		
		if(e == null)
			el = event.srcElement;
		else
			el = e.target;
		
		if(el != null && Crv.curveinstance.reportClickClassOrID(el.className, el.id))
		{
			var state = CurveEnum.CLICK;
			if(el.tagName != null && el.tagName.toLowerCase() == "a")
				state = CurveEnum.LINK;
			
			Crv.curveinstance.reportElementClicked(el, state, e.screenX, e.screenY);	
		}
	},
	
	reportElementClicked(el, state, screenX, screenY)
	{
		var ID = el.id != null && el.id != "" ? el.id : null;
		var buttonID = ID != null ? ID : el.className;
		
		var screenName = encodeURI(window.location.toString());
		
		//alert("Reporting click: " + buttonID + " state: " + state + " xx: " + screenX + " yy: " + screenY + " screenName: " + screenName);
		Crv.click(buttonID, state, screenX, screenY, screenName);
	},
	
	reportClickClassOrID(cls, id)
	{
		var cur = null;
		if(cls != null && cls != "")
		{
			var clsReg;
			cur = null;
			for(i=0;i<this._trackedClasses_local.length;i++)
			{
				cur = this._trackedClasses_local[i];
				clsReg = new RegExp("(^|\\s)"+cur+"($|\\s)")
				if(clsReg.test(cls))
					return true;
			}
			
			cur = null;
			for(i=0;i<this._trackedClasses_server.length;i++)
			{
				cur = this._trackedClasses_server[i];
				clsReg = new RegExp("(^|\\s)"+cur+"($|\\s)")
				if(clsReg.test(cls))
					return true;
			}
		}
		
		if(id != null && id != "")
		{
			var idReg;
			cur = null;
			for(i=0;i<this._trackedIDs_local.length;i++)
			{
				cur = this._trackedIDs_local[i];
				idReg = new RegExp("(^|\\s)"+cur+"($|\\s)")
				if(idReg.test(id))
					return true;
			}
			
			cur = null;
			for(i=0;i<this._trackedIDs_server.length;i++)
			{
				cur = this._trackedIDs_server[i];
				idReg = new RegExp("(^|\\s)"+cur+"($|\\s)")
				if(idReg.test(id))
					return true;
			}
		}
		
		return false;
	},
	
	setupGlobalErrors: function(win) 
	{
		this._window = win;
		this.errorSetupComplete();
	},
	
	errorSetupComplete: function(win) 
	{
		if(this._window != null && this._ready)
			this._window.onerror = this.globalErrorCought;
	},
	
	saveState: function() 
	{
		var state = {};
		state.token = this._token;
		state.appStartTime = this._appStartTime;
		state.config = JSON.stringify(this._config);
		state.interval = this._interval;
		
		Crv.setSessionCookie(Crv.CURVE_STATE_COOKIE, JSON.stringify(state));
	},
	
	hasState: function() 
	{
		var stateStr = Crv.getCookie(Crv.CURVE_STATE_COOKIE);
		
		if(stateStr != null && stateStr.length > 2)
		{
			try
			{
				var state = JSON.parse(stateStr);
				if(state != null) 
					return this.validateState(state);
			}
			catch(e)
			{
				console.log("Cought Error!! " + e);
			}
		}
		
		return false;
	},
	
	validateState: function(state) 
	{
		try
		{	
			if(state == null)
				return false;
			if(state.token == null || this._token != state.token)
				return false;
			if(state.appStartTime == null || isNaN(parseInt(state.appStartTime)) || parseInt(state.appStartTime) <= 0)
				return false;
			if(state.config == null || state.config.length < 2 || JSON.parse(state.config) == null)
				return false;
		}
		catch(e)
		{
			console.log("Cought Error!! " + e);
			return false;
		}
		
		return true;
	},
	
	loadState: function()
	{
		var stateStr = Crv.getCookie(Crv.CURVE_STATE_COOKIE);
		
		if(stateStr != null && stateStr.length > 2)
		{
			try
			{
				var state = JSON.parse(stateStr)
				if(state != null && this.validateState(state)) 
				{
					this._appStartTime = state.appStartTime;
					this._config = JSON.parse(state.config);
					this._interval = state.interval;
				}
			}
			catch(e)
			{
				console.log('Error: ' + e);
			}
		}
	},
	
	setPrintout: function(printout_) 
	{
		this._printout = printout_;
	},
	
	getPrintout: function() 
	{
		return this._printout ;
	},
	
	config: function()
	{
		if(this.hasState())
		{
			this.loadState();
			this.purgeQueue();
			CurveDriver.collectData();
			this.trackScreen();
		}
		else
		{
			//var uri = Curve.TRACK_URL + Curve.CONFIG_SERVLET + "?Token=" + this._token;
			var uri = Curve.TRACK_URL + Curve.CONFIG_SERVLET;
			this.loadURL(uri, this, {Token: this._token});
		}
	},
	
	purgeQueue: function()
	{
		this._ready = true;
		
		for(i = 0 ; i < this._queue.length ; i++)
		{
			var d = this._queue[i];
			if(d != null)
			{
				var data = d.obj;
				if(data != null)
				{
					var time = (this._config == null ? 0 : this._config.CurrentServerTimeStamp) + (new Date().getTime() - this._appStartTime);
					data.EventTimeLong = time+'';
				}

				this.apiUrl(d.servlet, data);
			}		
		}
	},

	// ***********************************************************************************************************
	
	isReady: function()
	{ 
		return this._initialized; 
	},
	
	getBatchLoader: function(servlet, fast)
	{
		if(this._servletQueue[servlet] == null)
			this._servletQueue[servlet] = this.getNewLoader(servlet, true);

		return this._servletQueue[servlet];
	},
	
	getNewLoader: function(servlet, fast)
	{
		var batch = fast ? Curve.BATCH_SIZE_FAST : Curve.BATCH_SIZE_SLOW;
		var interval = fast ? Curve.TIME_TO_SEND_FAST : ( this._config != null ? this._config.PingIntervalSecs * 1000 : Curve.TIME_TO_SEND_SLOW );
		var loader = new URLBatchLoader(servlet, this._token, interval, batch, this);
		loader.setPrintout(this); 
		return loader;
	},
	
	apiUrl: function(servlet, data)
	{
		if(!this._ready) 
		{
			this._queue.push({servlet: servlet, data: data});
			return;
		}

		if(!this.reportEnabled(servlet)) return;
		
		var batch = this.getBatchLoader(servlet, true);
		batch.submitt(data);
	},
	
	reportEnabled: function(servlet)
	{
		var conf = this.getFromConfig(servlet);
		return conf == null ? true : conf;
	},
	
	getFromConfig: function(servlet)
	{
		if(servlet == Curve.CLICK_SERVLET)
			return this._config.ClicksReportsEnabled;
		if(servlet == Curve.TRANSACTION_SERVLET)
			return this._config.TransactionsReportsEnabled;
		if(servlet == Curve.APPLICATION_SERVLET)
			return this._config.ApplicationsReportsEnabled;
		if(servlet == Curve.SESSION_DATA_SERVLET)
			return true;
		if(servlet == Curve.TRAFFIC_SERVLET)
			return true;
		if(servlet == Curve.SOCIAL_SERVLET)
			return this._config.SocialsReportsEnabled;
		if(servlet == Curve.USERS_SERVLET)
			return this._config.UsersReportsEnabled;
		if(servlet == Curve.SCREENS_SERVLET)
			return this._config.ScreensReportsEnabled;
		if(servlet == Curve.ADS_SERVLET)
			return this._config.AdsReportsEnabled;
		if(servlet == Curve.CUSTOM_EVENTS_SERVLET)
			return this._config.CustomEventsReportsEnabled;
		if(servlet == Curve.ERROR_SERVLET)
			return this._config.ErrorReportsEnabled;
		
		return null;
	},
	
	setToken: function(value)
	{
		if(this._token == value) return;

		this._token = value;
		this.updateToken();
	},
	
	updateToken: function()
	{
		for(servlet in this._servletQueue)
		{
			var loader = this._servletQueue[servlet];
			if(loader != null)
				loader.setToken(_token);
		}
	},
		
	onError: function(e)
	{
		this._timeoutConfig++;
		
		if(this._timeoutConfig <= Crv.MAX_CONFIG_RETRY)
		{	
			console.log("Curve config request's connection timed out, Retry " + this._timeoutConfig + "...")
			//var uri = Curve.TRACK_URL + Curve.CONFIG_SERVLET + "?Token=" + this._token;
			var uri = Curve.TRACK_URL + Curve.CONFIG_SERVLET;
			this.loadURL(uri, this, {Token: this._token});
		}
		else
		{
			this.print("unable to fetch config from server. Session not recorded.");
			//this._config = new ReportConfiguration();
			//this._appStartTime = new Date().getTime();
			//this.purgeQueue();
		}
	},
	
	//onSuccess: function(str)
	onSuccess: function(data)
	{
		this.print('onSuccess ' + data);
		this._config = ReportConfiguration.fromObject(data);
		this._appStartTime = new Date().getTime();
		this.purgeQueue();
		CurveDriver.collectData();
		this.trackApplication();
		this.trackScreen();
		this.saveState();
		this.errorSetupComplete();
	},
	
	globalErrorCought: function(errorMsg, url, lineNumber, column, stack)
	{
		if(Crv.curveinstance != null)
			Crv.curveinstance.error(encodeURI(errorMsg), encodeURI(stack+""));
	},
	
	loadURL: function(uri, handler, data)
	{
		var loader = new URLTimeoutLoader(uri, 1000, 3, handler, data);
		loader.load();
		// }catch(Exception e){if(handler != null) handler.onError(e);} //TODO

		this.print("Fetching " +  uri);
	},
	
	trackApplication: function()
	{
		this.appLifeCycle(CurveEnum.APP_START);
		this.sessionData(CurveData.HardwareID, CurveData.OpSys, CurveData.Browser, CurveData.DeviceWidth, CurveData.DeviceHeight);
		this.detectTraffic();
	},
	
	resetSession: function()
	{
		SessionIDGenerator.getInstance().resetSession();
		
		this.print("Resetting session. New session id: '" +SessionIDGenerator.getInstance().getSessionID() + "'");
		this.appLifeCycle(CurveEnum.APP_START);
		this.sessionData(CurveData.HardwareID, CurveData.OpSys, CurveData.Browser, CurveData.DeviceWidth, CurveData.DeviceHeight);
		this.forceTrackCurrentScreen();
	},
	
	detectTraffic: function()
	{
		var isSameUrl = function(ref)
		{
			try
			{
				var hostnm = window.location.host;
				if(hostnm != null)
				{
					return ref.indexOf(hostnm) != -1;
				}
			}
			catch(e){}
			
			return false;
		};
		
		var source = Crv.getParameterByName("utm_source");
		var medium = Crv.getParameterByName("utm_medium");
		var campaign = Crv.getParameterByName("utm_campaign");
		var term = Crv.getParameterByName("utm_term");
		var content = Crv.getParameterByName("utm_content");
		var gclid = Crv.getParameterByName("gclid");
		var referrer = null;

		try{ referrer = document.referrer; } catch(s){}
		
		var uri = "";
		try{ uri = encodeURI(window.location.href); } catch(s){}
		
		if(source != null && medium != null && campaign != null && source != "" && medium != "" && campaign != "")
		{
			this.traffic("campaign", source, medium, campaign, term, content, referrer, {campaign_type: "utm", url: uri});
		}
		else if(gclid != null && gclid != "")
		{
			this.traffic("adwords", null, null, null, null, null, gclid, {campaign_type: "utm", url: uri});
		}
		else if(referrer != null && referrer != "" && !isSameUrl(referrer))
		{
			this.traffic("referrer", null, null, null, null, null, referrer, {url: uri});
		}
		else
		{
			this.traffic("organic", null, null, null, null, null, null, {url: uri});
		}
	},
	
	trackScreen: function()
	{
		window.onunload = CurveDriver.unload;
		
		var target = encodeURI(window.location.pathname);
						
		if(this._pageMetadata != null && this._pageMetadata.name != null && this._pageMetadata.name != "")
			target = this._pageMetadata.name;
		
		if(CurveDriver.PREVIOUS_SCREEN_URL != window.location.pathname)
			this.trackCurrentScreen(target);
	},
	
	trackCurrentScreen: function(target)
	{
		if(Crv.pageloadtimesec != null)
		{
			this.screen(CurveDriver.PREVIOUS_SCREEN, target, 
			Math.floor(CurveDriver.PREVIOUS_SCREEN_TIME*.01)*.1, 
			CurveEnum.SC_SCREEN, {screen_load_time_sec: Crv.pageloadtimesec});
		}
		else
		{
			jQuery(document).ready(
				function()
				{
					if(Crv.curveinstance != null)
					{
						Crv.pageloadtimesec = (new Date().getTime()-Crv.starttime)/1000;
						Crv.curveinstance.screen(CurveDriver.PREVIOUS_SCREEN, target, 
						Math.floor(CurveDriver.PREVIOUS_SCREEN_TIME*.01)*.1, 
						CurveEnum.SC_SCREEN, {screen_load_time_sec: Crv.pageloadtimesec});
					}
				});
		}
	},
	
	forceTrackCurrentScreen: function()
	{
		console.log("")
		var target = encodeURI(window.location.pathname);
						
		if(this._pageMetadata != null && this._pageMetadata.name != null && this._pageMetadata.name != "")
			target = this._pageMetadata.name;
		
		Crv.pageloadtimesec = 0;
		this.trackCurrentScreen(target);
	},
	
	print: function(s)
	{
		if(this.getPrintout() != null) 
			this.getPrintout()("[Curve]>> " + s);
		
		if(Crv.DEBUG == true)
			console.log("[Curve]>> " + s);
	},

	// ******************* EXTERNAL API *******************************************
	
	getData: function(props)
	{
		var now = new Date();
		
		if(this._lastEvent == -1)
		{
			var lstEvCk = Crv.getCookie(Crv.CURVE_LAST_EVENT_COOKIE);;
			if(lstEvCk != null)
			{
				try{					
					this._lastEvent = parseInt(lstEvCk);
				}
				catch(e){ this._lastEvent =  -1;}
			}
		}
		if(this._lastEvent != -1 && this._lastEvent + Curve.SESSION_IDLE_TIMEOUT_MS < now.getTime() )
		{
			var timeout = -1*(this._lastEvent + Curve.SESSION_IDLE_TIMEOUT_MS - now.getTime())/1000/60;
			this.print("Session timed out " + timeout + " minutes ago.");
			this._lastEvent = -1;
			Crv.setSessionCookie(Crv.CURVE_LAST_EVENT_COOKIE, this._lastEvent);
			this.resetSession();
		}
		
		
		this._lastEvent = now.getTime();
		Crv.setSessionCookie(Crv.CURVE_LAST_EVENT_COOKIE, this._lastEvent);
		
		var data = {};
		
		if(this._superProps != null)
		{
			for(key in this._superProps)
			{
				if(props == null) props = {};
				props[key] = Crv.esc_quot(this._superProps[key]);
			}
		}
		
		var usrdID = UserIDGenerator.getInstance().getUserID();
		var sysUsrdID = UserIDGenerator.getInstance().getSysUserID();
		
		if(usrdID != null && usrdID != "")
			data.UserID = usrdID;

		if(sysUsrdID == null)
			sysUsrdID = "";
		
		if(props == null) props = {};
		props.system_userid = sysUsrdID;
		
		data.SessionID = SessionIDGenerator.getInstance().getSessionID();
		var utcTimestamp = Date.UTC(now.getUTCFullYear(),now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
		var time = (this._config != null ? this._config.CurrentServerTimeStamp : 0) + (utcTimestamp - this._appStartTime);
		data.EventTimeLong = time+'';
		
		if(props != null && props != "" && props != "undefined")
		{
			try
			{
				props = Curve.truncateJson(props);
				data.Props = Crv.rev_esc_quot(JSON.stringify(props).replace(new RegExp("\"", 'g'), "\\\""));
			}
			catch(e)
			{
				console.log("Error on setting props: ", e);
				data.Props = null;
				delete data.Props;
			}
		}
			
		
		return data;
	},

	flush: function()
	{
		for(key in this._servletQueue)
		{
			var batchLoader = this._servletQueue[key];
			if(batchLoader != null) batchLoader.flush();
		}
	},

	social: function(socialNetworkType, socialNetworkID, eventType, param1, param2, props)
	{
		var data = this.getData(props);
		if(socialNetworkType != null && socialNetworkType != '') data.SocialNwType = socialNetworkType+'';
		if(socialNetworkID != null && socialNetworkID != '') data.SocialNwID = socialNetworkID+'';
		if(eventType != null && eventType != '') data.EventType = eventType+'';
		if(param1 != null && param1 != '') data.Param1 = param1+'';
		if(param2 != null && param2 != '') data.Param2 = param2+'';

		this.apiUrl(Curve.SOCIAL_SERVLET, data);
	},
	
	traffic: function(trafficsource, source, medium, campaign, term, content, referrer, props)
	{
		var data = this.getData(props);
		if(trafficsource != null && trafficsource != '') data.TrafficSource = trafficsource+'';
		if(source != null && source != '') data.Source = source+'';
		if(medium != null && medium != '') data.Medium = medium+'';
		if(campaign != null && campaign != '') data.Campaign = campaign+'';
		if(term != null && term != '') data.Term = term+'';
		if(content != null && content != '') data.Content = content+'';
		if(referrer != null && referrer != '') data.Referrer = referrer+'';

		this.apiUrl(Curve.TRAFFIC_SERVLET, data);
	},

	error: function(errorDecription, stackTrace, props)
	{
		var data = this.getData(props);
		if(stackTrace != null && stackTrace != '') data.StackTrace = stackTrace+'';
		if(errorDecription != null && errorDecription != '') data.ErrorDesc = errorDecription+'';
		if(CurveData.OpSys != null && CurveData.OpSys != '') data.OpSys = CurveData.OpSys+'';
		if(CurveData.Browser != null && CurveData.Browser != '') data.Browser = CurveData.Browser+'';
		if(CurveData.HardwareID != null && CurveData.HardwareID != '') data.HardwareID = CurveData.HardwareID+'';
		if(CurveData.BatteryLevel != -1) data.BatteryLevel = CurveData.BatteryLevel+'';
		
		if(CurveData.AndroidVersion != null && CurveData.AndroidVersion != '') data.AndroidVersion = CurveData.AndroidVersion+'';
		if(CurveData.AppVersion != null && CurveData.AppVersion != '') data.AppVersion = CurveData.AppVersion+'';
		if(CurveData.DeviceModel != null && CurveData.DeviceModel != '') data.DeviceModel = CurveData.DeviceModel+'';
		
		data.SourceScreen = encodeURI(window.location.pathname);
		data.Platform = Curve.CURRENT_PLATFORM+'';
		
		//var url = Curve.TRACK_URL + Curve.ERROR_SERVLET + "?Token=" + this._token + "&Data=" + Curve.stringifyToJSON([data]);
		//Crv.curveAjaxCommand(url, null);
		var url = Curve.TRACK_URL + Curve.ERROR_SERVLET;
		Crv.curveAjaxCommandPost(url, null, {Token: this._token, Data: Curve.stringifyToJSON([data])});
	},

	ad: function(adsProviderID, adsProviderName, adPlatform, adType, adEvent, props)
	{
		var data = this.getData(props);
		if(adsProviderID != null && adsProviderID != '') data.AdsProviderID = adsProviderID+'';
		if(adsProviderName != null && adsProviderName != '') data.AdsProviderName = adsProviderName+'';
		if(adType != null && adType != '') data.AdType = adType+'';
		if(adEvent != null && adEvent != '') data.AdEvent = adEvent+'';
		if(adPlatform != null && adPlatform != '') data.Platform = adPlatform+'';

		this.apiUrl(Curve.ADS_SERVLET, data);
	},

	user: function(firstName, lastName, email, locale, gender, socialNwType, socialNwID, phone, age, birthdate, country, city, push_token, props)
	{
		var data = this.getData(props);
		data.HardwareID = HardwareIDGenerator.getInstance().getHardwareID();
		if(firstName != null && firstName != '') data.FirstName = firstName+'';
		if(lastName != null && lastName != '') data.LastName = lastName+'';
		if(email != null && email != '') data.Email = email+'';
		if(locale != null && locale != '') data.Locale = locale+'';
		if(gender != null && gender != '') data.Gender = gender+'';
		if(socialNwType != null && socialNwType != '') data.SocialNwType = socialNwType+'';
		if(socialNwID != null && socialNwID != '') data.SocialNwID = socialNwID+'';
		if(phone != null && phone != '') data.Phone = phone+'';
		if(push_token != null && push_token != '') data.PushToken = push_token+'';
		if(age != null && age != '') data.Age = age+'';
		if(birthdate != null && birthdate != '') data.BirthDate = birthdate+'';
		if(country != null && country != '') data.Country = country+'';
		if(city != null && city != '') data.City = city+'';

		this.apiUrl(Curve.USERS_SERVLET, data);
	},

	appLifeCycle: function(eventType, props)
	{
		var data = this.getData(props);
		
		if(eventType != null && eventType != '') data.EventType = eventType+'';		
		this.apiUrl(Curve.APPLICATION_SERVLET, data);
	},

	sessionData: function(hardwareID, opSys, browser, deviceWidth, deviceHeight, props)
	{
		if(props == null)
			props = {};
		
		if(this._sessionProps != null)
		{
			for(key in this._sessionProps)
			{
				if(props == null) props = {};
				props[key] = Crv.esc_quot(this._sessionProps[key]);
			}
		}
				
		props.baseurl = encodeURI(window.location.origin);
		var data = this.getData(props);
		data.Platform = Curve.CURRENT_PLATFORM;
		data.SDKVer = Curve.VERSION;
		data.ScalingFactor =  '1';
		
		if(hardwareID != null && hardwareID != '') data.HardwareID = hardwareID+'';
		if(opSys != null && opSys != '') data.OpSys = opSys+'';
		if(browser != null && browser != '') data.Browser = browser+'';
		if(deviceWidth != null && deviceWidth != '') data.DeviceWidth = deviceWidth+'';
		if(deviceHeight != null && deviceHeight != '') data.DeviceHeight = deviceHeight+'';
		if(data.HardwareID != null || data.HardwareID != '') data.HardwareID = data.UserID+'';
		
		this.apiUrl(Curve.SESSION_DATA_SERVLET, data);
	},

	transaction: function(transactionType, transactionSubType, 
						   virtualCurrency1Delta, virtualCurrency1Balance, 
						   virtualCurrency2Delta, virtualCurrency2Balance, 
						   sourcePlatform, sourceTranID,
						   transactionId, originalTransactionID, receiptSignature, 
						   realMoneyValue, errorMessage, props)
	{
		// if(this._config != null && !this._config.TransactionsReportsEnabled) return;

		var data = this.getData(props);
		if(transactionType != null && transactionType != '') data.TranType = transactionType+'';
		if(transactionSubType != null && transactionSubType != '') data.TranSubType = transactionSubType+'';
		if(virtualCurrency1Delta != null && virtualCurrency1Delta != '') data.VirtualCur1Delta = virtualCurrency1Delta+'';
		if(virtualCurrency1Balance != null && virtualCurrency1Balance != '') data.VirtualCur1Balance = virtualCurrency1Balance+'';
		if(virtualCurrency2Delta != null && virtualCurrency2Delta != '') data.VirtualCur2Delta = virtualCurrency2Delta+'';
		if(virtualCurrency2Balance != null && virtualCurrency2Balance != '') data.VirtualCur2Balance = virtualCurrency2Balance+'';
		if(sourcePlatform != null && sourcePlatform != '') data.SourcePlatform = sourcePlatform+'';
		if(sourceTranID != null && sourceTranID != '') data.SourceTranID = sourceTranID+'';
		if(realMoneyValue != null && realMoneyValue != '') data.RealMoneyValue = realMoneyValue+'';
		if(transactionId != null && transactionId != '') data.TransactionId = transactionId+'';
		if(originalTransactionID != null && originalTransactionID != '') data.OriginalTransactionID = originalTransactionID+'';
		if(receiptSignature != null && receiptSignature != '') data.ReceiptSignature = receiptSignature+'';		
		if(errorMessage != null && errorMessage != '') data.ErrorMessage = errorMessage+'';
		

		this.apiUrl(Curve.TRANSACTION_SERVLET, data);
	},
	
	click: function(buttonID, state, xx, yy, screenName, props)
	{
		// if(this._config != null && !this._config.ClicksReportsEnabled) return;
		
		if (buttonID==null || buttonID==undefined || buttonID=='')
			return;
		
		var data = this.getData(props);
		data.ButtonID = buttonID+'';
		data.XCoord = Math.floor(xx)+'';
		data.YCoord = Math.floor(yy)+'';
		data.ButtonState = state+'';
		data.ScreenID = screenName+'';

		this.apiUrl(Curve.CLICK_SERVLET, data);
	},

	screen: function(sourceScreen, targetScreen, sourceVisitTimeSecs, type, props)
	{
		if(this._pageMetadata != null)
		{
			for(key in this._pageMetadata)
			{
				if(this._pageMetadata[key] == null) continue;
				if(props == null) props = {};
				props[key] = Crv.esc_quot(this._pageMetadata[key]);
			}
		}
		
		var data = this.getData(props);
		data.TargetScreenID = targetScreen+'';
		data.SourceScreenID = sourceScreen+'';
		data.SourceVisitTimeSecs = sourceVisitTimeSecs+'';
		data.Type = type+'';
		
		this.apiUrl(Curve.SCREENS_SERVLET, data);
	},

	customEvent: function(category, action, label, value, name, props)
	{
		var data = this.getData(props);
		data.Category = category+'';
		data.Action = action+'';
		data.Label = label+'';
		data.Value = value+'';
		data.Name = name+'';

		this.apiUrl(Curve.CUSTOM_EVENTS_SERVLET, data);
	}
}

// *********************** ReportConfiguration *************************

var ReportConfiguration = function()
{
	var now = new Date;
	this.CurrentServerTimeStamp = Date.UTC(now.getUTCFullYear(),now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
};

ReportConfiguration.prototype = {
	
	constructor: ReportConfiguration,
	
	PingIntervalSecs: 10,
	CurrentServerTimeStamp: 0,
	
	ScreensReportsEnabled: true,
	ApplicationsReportsEnabled: true,
	ErrorReportsEnabled: true,
	AdsReportsEnabled: true,
	UsersReportsEnabled: true,
	ClicksReportsEnabled: true,
	TransactionsReportsEnabled: true,
	SocialsReportsEnabled: true,
	CustomEventsReportsEnabled: true
}

ReportConfiguration.fromObject = function(o)
{
	var r = new ReportConfiguration();
	
	try 
	{
		if(o.hasOwnProperty("ScreensReportsEnabled")) r.ScreensReportsEnabled = o.ScreensReportsEnabled;
		if(o.hasOwnProperty("ApplicationsReportsEnabled")) r.ApplicationsReportsEnabled = o.ApplicationsReportsEnabled;
		if(o.hasOwnProperty("SocialsReportsEnabled")) r.SocialsReportsEnabled = o.SocialsReportsEnabled;
		if(o.hasOwnProperty("CurrentServerTimeStamp")) r.CurrentServerTimeStamp = o.CurrentServerTimeStamp;
		if(o.hasOwnProperty("UsersReportsEnabled")) r.UsersReportsEnabled = o.UsersReportsEnabled;
		if(o.hasOwnProperty("AdsReportsEnabled")) r.AdsReportsEnabled = o.AdsReportsEnabled;
		if(o.hasOwnProperty("TransactionsReportsEnabled")) r.TransactionsReportsEnabled = o.TransactionsReportsEnabled;
		if(o.hasOwnProperty("ClicksReportsEnabled")) r.ClicksReportsEnabled = o.ClicksReportsEnabled;
		if(o.hasOwnProperty("CustomEventsReportsEnabled")) r.CustomEventsReportsEnabled = o.CustomEventsReportsEnabled;		
		if(o.hasOwnProperty("ErrorReportsEnabled")) r.ErrorReportsEnabled = o.ErrorReportsEnabled;		
		if(o.hasOwnProperty("PingIntervalSecs")) r.PingIntervalSecs = o.PingIntervalSecs;
	}
	catch (err)
	{
		console.log("Error on parsing config: ", err);
	}
	
	return r;
}

// *********************** SessionIDGenerator *************************

var SessionIDGenerator = function(){};

SessionIDGenerator.prototype = {
	
	constructor: SessionIDGenerator,
	
	_sessionID: null,
	
	getSessionID: function()
	{
		if(this._sessionID == null)
			this._sessionID = Crv.getCookie(Crv.CURVE_SESSION_COOKIE_KEY);
		
		if(this._sessionID == null)
		{
			this._sessionID = Crv.generateUUID();
			Crv.setSessionCookie(Crv.CURVE_SESSION_COOKIE_KEY, this._sessionID);
		}
		
		return this._sessionID;
	},
	
	resetSession: function()
	{
		this._sessionID = Crv.generateUUID();
		Crv.setSessionCookie(Crv.CURVE_SESSION_COOKIE_KEY, this._sessionID);
	}
};

SessionIDGenerator._i = null;
SessionIDGenerator.getInstance = function()
{
	if(SessionIDGenerator._i == null)
		SessionIDGenerator._i = new SessionIDGenerator();
	
	return SessionIDGenerator._i;
};

// *********************** HardwareIDGenerator *************************

var HardwareIDGenerator = function(){};

HardwareIDGenerator.prototype = {
	
	constructor: HardwareIDGenerator,
	
	_idProvider: null,
	_hwID: null,
	
	getGingeeHardwareID: function()
	{	
		// fetch provider uid
		if(this._hwID == null && this._idProvider != null)
			this._hwID = this._idProvider.create();
				
		if(this._hwID == '')
			this._hwID = null;
			
		if(this._hwID == null)
			this._hwID = Crv.getCookie(Crv.CURVE_HARDWARE_COOKIE_KEY);
		
		if(this._hwID == null)
		{
			this._hwID = Crv.generateUUID();
			Crv.setCookie(Crv.CURVE_HARDWARE_COOKIE_KEY, this._hwID);
		}
		
		return this._hwID;
	},
	
	setIdProvider: function(value)
	{ 
		this._idProvider = value; 
	},
	
	getHardwareID: function()
	{
		return this.getGingeeHardwareID();
	},
	
	setHardwareID: function(value)
	{
		this._hwID = value;
	}
}

// SINGLETON 

HardwareIDGenerator.instance = null;

HardwareIDGenerator.getInstance = function()
{
	if(HardwareIDGenerator.instance == null)
		HardwareIDGenerator.instance = new HardwareIDGenerator();
	
	return HardwareIDGenerator.instance;
};

// *********************** UserIDGenerator *****************************

var UserIDGenerator = function()
{
	this._userIDsByToken = Crv.getCookie(Crv.CURVE_USERID_COOKIE_OBJ_KEY);
};

UserIDGenerator.prototype = {
	
	constructor: UserIDGenerator,
	
	_userID: null,
	_userIDsByToken: null,
	_sysUserId: null,
	_savedUserid: null,
	_useridProvider: null,
	_token: null,
	
	setToken: function(t)
	{
		this._token = t;
	},
	
	getSysUserID: function()
	{
		return this._sysUserId;
	},
	
	getUserID: function()
	{		
		if(this._userIDsByToken != null && this._userIDsByToken != "" && this._token != null && this._token != '')
		{
			try{
				var ids = JSON.parse(this._userIDsByToken);
				if(ids != null)
					this._userID = ids[this._token];
			}
			catch(e){}
		}
		if(this._userID != null && this._userID != '')
			return this._userID;
		if(this._savedUserid != null && this._savedUserid != '')
			this._savedUserid = Crv.getCookie(Crv.CURVE_USERID_COOKIE_KEY);
		
		if(this._useridProvider != null)
			this._userID = this._useridProvider.create();
		
		if(this._userID == null && this._savedUserid != null && this._savedUserid != '')
			this._userID = this._savedUserid;
		
		if(this._userID == null || this._userID == '')
			this._userID = HardwareIDGenerator.getInstance().getHardwareID();
		
		if(this._savedUserid == null || this._savedUserid == '')
		{
			this._savedUserid = this._userID;
			this.updateToUserIDs(this._userID);
		}			
		
		return this._userID;		
	},
	
	setUseridProvider: function(value)
	{ 
		this._useridProvider = value;
	},
	
	setUserID: function(value)
	{ 
		this._sysUserId = value;
		if(this._userID == null)
		{
			this._userID = value; 
			_savedUserid = null;
			this.updateToUserIDs(this._userID);
		}
	},
	
	updateToUserIDs: function(value)
	{
		if(this._token == null || this._token == '')
			return;
		
		var idByToken = Crv.getCookie(Crv.CURVE_USERID_COOKIE_OBJ_KEY);
		var ids = null;
		try{
			ids = JSON.parse(this._userIDsByToken);
		}catch(e){}
		
		if(ids == null)
			ids = {};
		
		ids[this._token] = value;
		Crv.setCookie(Crv.CURVE_USERID_COOKIE_OBJ_KEY, JSON.stringify(ids));
	}
}

// SINGLETON 

UserIDGenerator.instance = null;

UserIDGenerator.getInstance = function()
{
	if(UserIDGenerator.instance == null)
		UserIDGenerator.instance = new UserIDGenerator();
	
	return UserIDGenerator.instance;
};

// ********************** URLTimeoutLoader *******************

var URLTimeoutLoader = function(url, timeout, numRetries, handler, data)
{
	this._url = url;
	this._timeout = timeout;
	this._numRetries = numRetries;
	this._handler = handler;
	this._data = data;
}

URLTimeoutLoader.prototype = {
	
	constructor: URLTimeoutLoader,
	
	_url: '',
	_timeout: -1,
	_numRetries: 0,
	_printer: null,
	_handler: null,
	_data: null,
	
	load: function()
	{
		this.assertData();
		this.retry();
	},
	
	interrupt: function()
	{
		this.killTimer();
		this.killLoader();
	},
	
	killLoader: function() 
	{
		if(this._connection != null)
			this._connection.disconnect();
	},
	
	killTimer: function() 
	{
		if(this._timer == null) return;
		this._timer.stop();
	},

	retry: function()
	{
		if(this._numRetries == -1 || this._numRetries > 0)
		{
			if(this._numRetries != -1) 
				this._numRetries--;
			
			this.execute();
		}
		else
			this.onError(null);
	},
	
	execute: function()
	{
		this.setupTimer();
		//Crv.curveAjaxCommand(this._url, this);
		Crv.curveAjaxCommandPost(this._url, this, this._data);
	},
	
	print: function(str) 
	{
		if(this.getPrinter() != null)
			this.getPrinter().print("[URLTimeoutLoader] >>" + str);
	},
	
	getRetriesLeft: function()
	{ 
		return this._numRetries; 
	},
	
	onError: function(e) 
	{
		this.interrupt();
		this.print("Error Loading url '" + this._url + "'" + (e != null ? " " + e : ""));		
		if(this._handler != null)
			this._handler.onError(e);
	},
	
	onComplete: function(resp) 
	{
		this.interrupt();
		this.print("Completed Loading. Response: '" + this._url + "'");
		if(this._handler != null)
			this._handler.onSuccess(resp);
	},
	
	setupTimer: function() 
	{
		if(this._timer != null) return;
		this._timer = new TaskTimer(this._timeout, this);
	},
	
	run: function() 
	{
		this.retry();
	},
	
	assertData: function()
	{
		if(this._url == null || this._url == "") console.log("Cant load with an empty URLRequest");
		if(this._timeout <= 0) console.log("Timeout must be greater than 0, given is: '" + this._timeout + "'");
	},
	
	getPrinter: function() 
	{
		return this._printer;
	},
	
	setPrinter: function(printer) 
	{
		this._printer = printer;
	}
}

// ********************* TaskTimer ************************************

var TaskTimer = function(delay, listener)
{
	this._delay = delay;
	this._listener = listener;
}

TaskTimer.prototype = {
	
	constructor: TaskTimer,
	
	_delay: 0,
	_timer: -1,
	_listener: null,
	
	start: function()
	{
		this.stop();
		this._timer = setTimeout(run, _delay);
	},

	stop: function()
	{
		if(this._timer != -1)
			clearTimeout(this._timer);
		
		this._timer = -1;
	},

	run: function() 
	{
		if(this._listener != null)
			this._listener.run();
	},
	
	destroy: function() 
	{
		this.stop();
		this._listener = null;
	}
}

// ********************** URLBatchLoader ********************

var URLBatchLoader = function(servlet, token, batchTime , batchSize, curve)
{
	this._curve = curve;
	this._token = token;
	this._batchSize = batchSize;
	this._batchTime = batchTime;
	this._servlet = servlet;
	this._queue = [];
	this.loadFromCookie();
	this.initTimer();
}
URLBatchLoader.cookiename = "URLBatchLoader.";

URLBatchLoader.prototype = {
	
	constructor: URLBatchLoader,
	
	_curve: null,
	_token: "",
	_batchSize: -1,
	_batchTime: -1,
	_servlet: "",
	_queue: [],
	
	initTimer: function()
	{
		if(this._batchSize < 2) return;
		if(!this.validateTimer()) return;

		this.killTimer();

		this._timer = new TaskTimer(this._batchTime, this);
		this._timer.start();
	},
	
	run: function()
	{
		this.flush();
	},
	
	validateTimer: function()
	{
		return this._batchTime > 20;
	},
	
	killTimer: function()
	{
		if(this._timer == null) return;

		this._timer.stop();
		this._timer.destroy();
		this._timer = null;
	},
	
	submitt: function(data)
	{
		this._queue.push(data);
		if(this._timer == null || this._queue.length >= this._batchSize) 
		{
			this.initTimer();
			this.flush(); 
			this.purgeCookie();
		}
		else
			this.updateCookie();
		
	},
	
	updateCookie: function()
	{
		var cname = URLBatchLoader.cookiename + this._token + "." + this._servlet;
		var cvalue = JSON.stringify(this._queue);
		var exyears = 1/365;
		Crv.setCookieWithExp(cname, cvalue, exyears);
	},
	
	purgeCookie: function()
	{
		var cname = URLBatchLoader.cookiename + this._token + "." + this._servlet;
		Crv.removeCookie(cname);
	},
	
	loadFromCookie: function()
	{
		var cname = URLBatchLoader.cookiename + this._token + "." + this._servlet;
		var queue = Crv.getCookie(cname);
		if(queue != null)
		{
			if(this._queue == null)
				this._queue = [];
			this._queue = this._queue.concat(JSON.parse(queue));
		}
		
		if(this._queue != null && this._queue.length > 0) 
		{
			this.initTimer();
			this.flush(); 
		}
	},
	
	flush: function()
	{
		if(this._queue == null || this._queue.length == 0) return;

		var queue = this._queue.slice(0);
		this._queue = [];

		if(queue.length > 0)
		{
			var parseddata = Curve.stringifyToJSON(queue);
			
			if(parseddata != null && parseddata != "[]")
			{
				var uri = this.apiUrl();
				var data = {Token: this._token, Data: parseddata};
				this.send(uri, data);
			}
		}
	},
	
	send: function(uri, data)
	{
		var loader = new URLTimeoutLoader(uri, 3000, 3, null, data);
		loader.setPrinter(this._printer);
		
		loader.load();
	},
	
	apiUrl: function()
	{
		var url = Curve.TRACK_URL + this._servlet;// + "?Token=" + this._token + "&Data=" + Curve.stringifyToJSON(obj);
		return url;
	},
	
	getToken: function()
	{
		return this._token;
	},
	
	setToken: function(value)
	{
		this._token = value;
	},
	
	setPrintout: function(printer) 
	{
		this._printer = printer;
	},
	
	getPrinter: function()
	{
		return this._printer;
	},
	
	setPrinter: function(_printer)
	{
		this._printer = _printer;
	}
}

// **************** CurveData *********************************

var CurveData = function(){};

CurveData.AndroidVersion = '';
CurveData.OpSys = '';
CurveData.Browser = '';
CurveData.HardwareID = '';
CurveData.AppPackage = '';
CurveData.AppVersion = '';
CurveData.DeviceModel = '';
CurveData.DeviceWidth = '';
CurveData.BatteryLevel = -1;

// **************** CurveData *********************************


// **************** CurveEnum *********************************

var CurveEnum = function(){};

CurveEnum.FACEBOOK = 'facebook';
CurveEnum.TWITTER = 'twitter';
CurveEnum.DOUBLE_CLICK = 'DoubleClick';
CurveEnum.ON = 'On';
CurveEnum.OFF = 'Off';
CurveEnum.CLICK = 'Click';
CurveEnum.LINK = 'Link';

// ............ Screen Types
CurveEnum.SC_POPUP = 'popup';
CurveEnum.SC_ERROR = 'error';
CurveEnum.SC_SCREEN = 'screen';
CurveEnum.SC_VIRTUAL = 'virtual';

CurveEnum.APP_PING = 'AppPing';
CurveEnum.APP_CLOSE = 'AppClose';
CurveEnum.APP_START = 'AppStart';

// ........... FACEBOOK ....................

CurveEnum.FB_COMPLETE_LOGIN = 'Login';
CurveEnum.FB_FAILED_LOGIN = 'FailedLogin';
CurveEnum.FB_INVITE_NON_USERS = 'Invite';
CurveEnum.FB_POST_ON_WALL = 'PostOnWall';
CurveEnum.FB_POST_ON_PAGE_WALL = 'PostOnPageWall';

CurveEnum.FB_POST_VIDEO = 'PostVideo';
CurveEnum.FB_POST_IMAGE = 'PostImage';
CurveEnum.FB_LOGOUT = 'Logout';

CurveEnum.FB_FQL_QUERY = 'FQL_QUERY';
CurveEnum.FB_GET_APP_USERS = 'GET_APP_USERS';
CurveEnum.FB_FRIEND_LIST = 'FRIEND_LIST';
CurveEnum.FB_API_CALL = 'API_CALL';


// **************** CurveEnum *********************************

// **************** HELPER FUNCTIONS **********************

Crv.generateUUID = function()
{
    var d = new Date().getTime();
    if(window.performance && typeof window.performance.now === "function"){
        d += performance.now();; //use high-precision timer if available
    }
	
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
}

// ********************** AJAX ******************************

Crv.curveAjaxCommand = function (url_, target_)
{
	var ajax = jQuery.ajax({
						url: url_,
						type: 'GET',
						dataType: 'text',
						success: function(response) 
						{
							if(target_ != null && target_.onComplete != null)
								target_.onComplete(Crv.parseAjaxCurveResponse(response));
						},
						fail: function(response) 
						{
							// console.log('fail!');
							if(target_ != null && target_.onError != null)
								target_.onError(Crv.parseAjaxCurveResponse(response));
						}
					});
					
	return ajax;
}

Crv.curveAjaxCommandPost = function (url_, target_, data_)
{
	var ajax = jQuery.ajax({
						url: url_,
						type: 'POST',
						dataType: 'json',
						data: data_,
						success: function(response) 
						{
							//console.log("SUCCESS", response);
							if(target_ != null && target_.onComplete != null)
								target_.onComplete(response);
						},
						fail: function(response) 
						{
							// console.log('fail!');
							if(target_ != null && target_.onError != null)
								target_.onError(response);
						},
						error: function(response) 
						{
							// console.log('fail!');
							if(target_ != null && target_.onError != null)
								target_.onError(response);
						}
					});
					
	return ajax;
}

Crv.parseAjaxCurveResponse = function(data)
{
	if(data.substr(0, "parseCurveResponse(".length) == "parseCurveResponse(")
	{
		data = data.substr("parseCurveResponse(".length);
		data = data.substr(0, data.length-3);
	}
	return data;
}

Crv.parseAjaxPostCurveResponse = function(data)
{
	if(typeof data == "string" && data.substr(0, "parseCurveResponse(".length) == "parseCurveResponse(")
	{
		data = data.substr("parseCurveResponse(".length);
		data = data.substr(0, data.length-3);
	}
	return JSON.stringify(data);
}

// ********************** COOKIES ****************************************************

Crv.COOKIE_EXP = 2;// 2 years

Crv.setCookie = function(cname, cvalue) 
{
   Crv.setCookieWithExp(cname, cvalue, Crv.COOKIE_EXP);
}

Crv.setSessionCookie = function(cname, cvalue) 
{
    document.cookie = cname + "=" + cvalue + ";";
}

Crv.setCookieWithExp = function(cname, cvalue, exyears)
{
    var d = new Date();
    d.setTime(d.getTime() + (exyears*365*24*60*60*1000));
    var expires = "expires="+d.toUTCString() + ";";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

Crv.removeCookie = function(cname)
{
    var expires = "expires=Thu, 01 Jan 1970 00:00:00 UTC;";
    document.cookie = cname + "= ''; " + expires;
}

Crv.getCookie = function(cname) 
{
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
	
    return null;
}

// ********************** BROWSER ******************************

var CurveDriver = function(){};
CurveDriver.BROWSER = null;
CurveDriver.PREVIOUS_SCREEN = null;
CurveDriver.PREVIOUS_SCREEN_URL = null;
CurveDriver.PREVIOUS_SCREEN_TIME = 0;
CurveDriver.START_TIME = -1;
CurveDriver.DATA_READY = false;

Crv.setupStartTime = function()
{
	CurveDriver.START_TIME = new Date().getTime();
	
	var prvScreen = Crv.getCookie(Crv.PREV_SCREEN_COOKIE);
	var prvScrTime = Crv.getCookie(Crv.PREV_SCREEN_TIME_COOKIE);
	if(prvScrTime == null)
		prvScrTime = 0;
	
	if(window.location.pathname == prvScreen)
		CurveDriver.START_TIME -= prvScrTime;	
}

Crv.setupStartTime();

CurveDriver.getBrowser = function()
{
	if(CurveDriver.BROWSER == null)
		CurveDriver.BROWSER = Crv.collectBrowserData();
	
	return CurveDriver.BROWSER.browser;
}

CurveDriver.getFullVersion = function()
{
	if(CurveDriver.BROWSER == null)
		CurveDriver.BROWSER = Crv.collectBrowserData();
	
	return CurveDriver.BROWSER.fullVersion;
}

CurveDriver.getOpSys = function()
{
	if(CurveDriver.BROWSER == null)
		CurveDriver.BROWSER = Crv.collectBrowserData();
	
	return CurveDriver.BROWSER.opsys;
}

CurveDriver.getVersion = function()
{
	if(CurveDriver.BROWSER == null)
		CurveDriver.BROWSER = Crv.collectBrowserData();
	
	return CurveDriver.BROWSER.version;
}

CurveDriver.unload = function()
{
	var time = new Date().getTime() - CurveDriver.START_TIME;
	Crv.setSessionCookie(Crv.PREV_SCREEN_TIME_COOKIE, time);
}

CurveDriver.collectData = function()
{
	CurveData.DeviceWidth = jQuery(document).width();
	CurveData.DeviceHeight = jQuery(document).height();
	CurveData.Browser = CurveDriver.getBrowser() + " " + CurveDriver.getFullVersion();
	CurveData.AndroidVersion = CurveDriver.getFullVersion();
	CurveData.HardwareID = '';
	CurveData.DeviceModel = '';
	CurveData.OpSys = CurveDriver.getOpSys();
	
	
	if(CurveDriver.DATA_READY) return;
	CurveDriver.DATA_READY = true;
	
	// Update Currenc Screen
	CurveDriver.PREVIOUS_SCREEN = Crv.getCookie(Crv.PREV_SCREEN_COOKIE);
	CurveDriver.PREVIOUS_SCREEN_URL = Crv.getCookie(Crv.PREV_SCREEN_NAME_URL_COOKIE);
	CurveDriver.PREVIOUS_SCREEN_TIME = Crv.getCookie(Crv.PREV_SCREEN_TIME_COOKIE);
	if(CurveDriver.PREVIOUS_SCREEN_TIME == null)
		CurveDriver.PREVIOUS_SCREEN_TIME = 0;
	
	var target = window.location.pathname;
					
	if(this._pageMetadata != null && this._pageMetadata.name != null && this._pageMetadata.name != "")
	{
		target = this._pageMetadata.name;
	}
	
	Crv.removeCookie(Crv.PREV_SCREEN_TIME_COOKIE);
	Crv.setSessionCookie(Crv.PREV_SCREEN_COOKIE, target);
	Crv.setSessionCookie(Crv.PREV_SCREEN_NAME_URL_COOKIE, window.location.pathname);
}

Crv.collectBrowserData = function()
{
	var nVer = navigator.appVersion;
	var nAgt = navigator.userAgent;
	var browserName  = navigator.appName;
	var fullVersion  = ''+parseFloat(navigator.appVersion); 
	var majorVersion = parseInt(navigator.appVersion,10);
	var nameOffset,verOffset,ix;

	// In Opera 15+, the true version is after "OPR/" 
	if ((verOffset=nAgt.indexOf("OPR/"))!=-1) {
	 browserName = "Opera";
	 fullVersion = nAgt.substring(verOffset+4);
	}
	// In older Opera, the true version is after "Opera" or after "Version"
	else if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
	 browserName = "Opera";
	 fullVersion = nAgt.substring(verOffset+6);
	 if ((verOffset=nAgt.indexOf("Version"))!=-1) 
	   fullVersion = nAgt.substring(verOffset+8);
	}
	// In MSIE, the true version is after "MSIE" in userAgent
	else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
	 browserName = "Microsoft Internet Explorer";
	 fullVersion = nAgt.substring(verOffset+5);
	}
	// In Chrome, the true version is after "Chrome" 
	else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
	 browserName = "Chrome";
	 fullVersion = nAgt.substring(verOffset+7);
	}
	// In Safari, the true version is after "Safari" or after "Version" 
	else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
	 browserName = "Safari";
	 fullVersion = nAgt.substring(verOffset+7);
	 if ((verOffset=nAgt.indexOf("Version"))!=-1) 
	   fullVersion = nAgt.substring(verOffset+8);
	}
	// In Firefox, the true version is after "Firefox" 
	else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
	 browserName = "Firefox";
	 fullVersion = nAgt.substring(verOffset+8);
	}
	// In most other browsers, "name/version" is at the end of userAgent 
	else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) < 
			  (verOffset=nAgt.lastIndexOf('/')) ) 
	{
	 browserName = nAgt.substring(nameOffset,verOffset);
	 fullVersion = nAgt.substring(verOffset+1);
	 if (browserName.toLowerCase()==browserName.toUpperCase()) {
	  browserName = navigator.appName;
	 }
	}
	// trim the fullVersion string at semicolon/space if present
	if ((ix=fullVersion.indexOf(";"))!=-1)
	   fullVersion=fullVersion.substring(0,ix);
	if ((ix=fullVersion.indexOf(" "))!=-1)
	   fullVersion=fullVersion.substring(0,ix);

	majorVersion = parseInt(''+fullVersion,10);
	if (isNaN(majorVersion)) {
	 fullVersion  = ''+parseFloat(navigator.appVersion); 
	 majorVersion = parseInt(navigator.appVersion,10);
	}
	
	return {browser: browserName,
			version: majorVersion,
			opsys: window.navigator.oscpu,
			fullVersion:fullVersion};
}

Crv.esc_quot = function(text)
{
    return text.replace("\"", "@@@$$@@@");
}

Crv.rev_esc_quot = function(text)
{
    return text.replace("@@@$$@@@", "\\\"");
}

Crv.getParameterByName = function(name) 
{
    var url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
		
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

// *************************** Translators ***************************************************