function getStatusGraphOptions(type)
{
	var options = {};
	
	
	var colors = ["#41b7e9", "#e94174",
		"#41e9b4", "#41e96c", "#9841e9",
		"#4141e9", "#4188e9",
		"#41c7e9"];
	
	
	options.colors = colors;//["#06789e", "#88ce5c", "#d1e05c", "#afe1c1", "#b4d6e2"];
	
	if(type.toLowerCase() == "piechart")
	{
		options.series = 
		{
			"pie": {
				combine: {"color": "#999", "threshold": 0},
				"innerRadius": 0.6,
				"radius": 1,
				"show": true,
				"label": {"radius": 2/3, "show": false, "threshold": 0.075}
			}
		};
		
		options.legend = {"show": true};
	}
	// Bars
	else if(type.toLowerCase() == "barchart")
	{
		options.series = 
		{
			"bars": {
				"show": true
			}
		};
		
		options.bars = 
		{
			"align": "right",
			"barWidth": 82800000,
			"lineWidth": 0,
			"shadowSize": 0
		};
		
		options.grid = {
			"borderColor": "#eee",
			"borderWidth": 1,
			"clickable": true,
			"hoverable": true,
			"labelMargin": 10,
			"tickColor": "#eee"
		};
		
		options.xaxis = {"tickLength": 0, ticks: []};
		options.yaxis = {"tickLength": 0, ticks: []};
		options.legend = {"backgroundColor": "#eee", "labelBoxBorderColor": "none"};
	}
	// Bars
	else if(type.toLowerCase() == "basicchart")
	{
		options.series = 
		{
			"bars": {
				"show": true
			}
		};
		
		options.grid = {
			"borderColor": "#eee",
			"borderWidth": 1,
			"clickable": true,
			"hoverable": true,
			"labelMargin": 10,
			"tickColor": "#eee"
		};
		
		options.xaxis = {"tickLength": 0, ticks: []};
		options.yaxis = {"tickLength": 0, ticks: []};
		options.legend = {"backgroundColor": "#eee", "labelBoxBorderColor": "none"};
	}
	// Lines
	else if(type.toLowerCase() == "trackingchart")
	{
		options.series = 
		{
			"lines": {
				"fill": true,
				"lineWidth": 2,
				"show": true,
				"opacity": 0.5,
				"fillColor": 
				{
					"colors": [{"opacity": 0.5}, {"opacity": 0.5}]
				}
			},
			"curvedLines":
			{
			   "apply": true,
			   "active": true,
			   "monotonicFit": true
			}
		};
		
		options.grid = {
			"borderColor": "#eee",
			"borderWidth": 1,
			"hoverable": false,
			"tickColor": "#eee",
			"margin": 1000
		};
		
		
		options.xaxis = {"tickLength": 1, tickColor: "#ff0000"};
		options.yaxis = {"tickLength": 1, minTickSize: 65, ticks: [[-65, "Low"],[65,"High"]]};
		options.legend = 
		{
			labelFormatter: function(label, series)
			{
				//var onoff = series.lines.show? "(on) " : "(off) ";
				return '<a href="#" onClick="togglePlot(\''+label+'\'); return false;">'+ /*onoff + */label + '</a>';
			}
		};
		
	}
	
	
	return options;
}