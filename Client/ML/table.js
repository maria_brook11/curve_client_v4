function getHead(titles)
{
	var resp = '<thead><tr>';
	resp += '<th>Segment</th>';  
	resp += '<th>Create Audience</th>';    
	for(var i = 0; i < titles.length ; i++)
		resp += '<th>' + titles[i].split("_").join(" ") + '</th>';    
	
    resp += '</tr></thead>';
	return resp;
}

function getBody(clusters, aliases)
{
	var resp = '<tbody>';
	
	for(var i = 0; i < clusters.length ; i++)
	{
		resp += '<tr>';
		var cluster = clusters[i];
		resp += '<td>' + aliases[i] + '</td>';
		resp += '<td><button>+</button></td>';
		
		for(var j = 0; j < cluster.length ; j++)
			resp += '<td>' + Math.floor(cluster[j].magnitude*100)/100 + '</td>';
		
		resp += '</tr>';
	}
		
	
    resp += '</tbody>';
	return resp;
}

function getTable(titles, values, aliases)
{
	var resp = '<table id="set_as_data_table" class="table-hover">' + getHead(titles) + getBody(values, aliases) + '</table>';
	return resp;
}