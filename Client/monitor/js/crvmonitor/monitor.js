var CrvMonitor = {};

CrvMonitor.servers = [
						// Main Servers
						{name: "curve", desc:"API Server", url:["52.1.71.61"], port:8080, testservlet:"/Curve/Test"},
						{name: "aggregation", desc:"Aggregation Server", url:["52.23.108.236"], port:8080, testservlet:"/aggregation/Test"},
						{name: "notification", desc:"Notification Server", url:["52.7.51.17"], port:8080, testservlet:"/Notifications/Test"},
						//{name: "track", desc:"Redirect Server", url:[""]},
						
						// Data Staging
						{name: "api", desc:"API Server", url:["api.curve.tech"], port:8080, testservlet: "/API/Test"},
						{name: "api.curve", desc:"API.Curve Server", url:["api.curve.tech"], port:8080, testservlet: "/API/Test"},
						{name: "k2s3", desc:"Kinesis To S3", url:["54.236.252.176"], port:7171, testservlet: "/API/1.1/?command=test", statusservlet: "/API/1.1/?command=status"},
						//{name: "dsc", desc:"Data Staging Consumer", url:["54.85.133.27"], port:7171, testservlet: "/API/1.1/?command=test", statusservlet: "/API/1.1/?command=status"},
						{name: "s3tr", desc:"S3 To Redshift Manager", url:["54.172.182.201"], port:7171, testservlet: "/API/1.1/?command=test", statusservlet: "/API/1.1/?command=status"},
						//{name: "s3trna", desc:"S3 To Redshift Manager (NEW)", url:["52.90.144.110"], port:7171, testservlet: "/API/1.1/?command=test", statusservlet: "/API/1.1/?command=status"},
						{name: "rs2sql", desc:"Redshift To MySQL Manager", url:["52.202.80.192"], port:7171, testservlet: "/API/1.1/?command=test", statusservlet: "/API/1.1/?command=status"}
					];
										
CrvMonitor.divid_to_url = {};

CrvMonitor.getServerStatusDiv = function(name, desc, status, serverstatus_div_id)
{
	return '<div class="serverstatus"><span id="' + serverstatus_div_id + '"class="base_status_span serverstatus_' + status + '">' + status.toUpperCase() + '</span><span class="base_status_span serverstatus_name">' + name.toUpperCase() + '</span><span class="base_status_span serverstatus_desc">' + desc + '</span></div>'
}

CrvMonitor.onAjaxStatusSuccess = function(data, servername, serverdesc)
{
	jQuery(document).ready(
					function()
					{
						if(servername == "s3tr")
						{
							var resp = JSON.parse(data);
							console.log(resp);
							
							var start = CrvMonitor.getDate(resp.start_date);
							var last = CrvMonitor.getDate(resp.last_read);
							var server_time = CrvMonitor.getDate(resp.current_server_gmt_time);
							var timeDiff = Math.abs(server_time.getTime() - last.getTime())/1000;
							
							$("#s3tr").append("<div>Last read was " + CrvMonitor.getDifferenceDiv(timeDiff) + " ago</div>");
							$("#s3tr").append("<div>Avg read rate: " + resp.avg_read_rate + "</div>");
							$("#s3tr").append("<div>Total consumed: " + CrvMonitor.formatNumber(resp.total_records_consumed) + "</div>");
							$("#s3tr").append("<div>Server started: " + CrvMonitor.rebuildDate(resp.start_date) + "</div>");
							$("#s3tr").append("<div>Server version: " + resp.version + "</div>");
							$("#s3tr").append("<div>Server up time: " + CrvMonitor.getDifferenceDivNoColor(resp.up_time_sec) + "</div>");
							
							$("#s3tr").append("<div class='folder_reads_title'>VACUUM:</div>");
							$("#s3tr").append("<div>In vacuum: " + resp.system_in_vacuum + "</div>");
							$("#s3tr").append("<div>Next vacuum: " + CrvMonitor.getDifferenceDivNoColor(resp.next_vacuum_in_ms/1000) + "</div>");
							$("#s3tr").append("<div>Last vacuum: " + resp.last_vacuum + "</div>");
							$("#s3tr").append("<div title='"+resp.last_vacuum_query+"'>Last vacuum query: " + resp.last_vacuum_query.substr(0, 20)+"..." + "</div>");
							$("#s3tr").append("<div>Last vacuum length: " + CrvMonitor.getDifferenceDivNoColor(resp.last_vacuum_length_sec) + "</div>");
							$("#s3tr").append("<div>Avg vacuum length: " + CrvMonitor.getDifferenceDivNoColor(resp.avg_vacuum_length_sec) + "</div>");
							$("#s3tr").append("<div>Max vacuum length: " + CrvMonitor.getDifferenceDivNoColor(resp.max_vacuum_length_sec) + "</div>");
							$("#s3tr").append("<div>Number of vacuums: " + resp.num_vacuums + "</div>");
							
							$("#s3tr").append("<div class='folder_reads_title'>FOLDER READS: " + CrvMonitor.parseFolders(server_time, JSON.parse(resp.last_read_by_folders)) + "</div>");
						}
						
						if(servername == "s3trna")
						{
							var resp = JSON.parse(data);
							console.log(resp);
							
							var start = CrvMonitor.getDate(resp.start_date);
							var last = CrvMonitor.getDate(resp.last_read);
							var server_time = CrvMonitor.getDate(resp.current_server_gmt_time);
							var timeDiff = Math.abs(server_time.getTime() - last.getTime())/1000;
							
							$("#s3trna").append("<div>Last read was " + CrvMonitor.getDifferenceDiv(timeDiff) + " ago</div>");
							$("#s3trna").append("<div>Avg read rate: " + resp.avg_read_rate + "</div>");
							$("#s3trna").append("<div>Total consumed: " + CrvMonitor.formatNumber(resp.total_records_consumed) + "</div>");
							$("#s3trna").append("<div>Server started: " + CrvMonitor.rebuildDate(resp.start_date) + "</div>");
							$("#s3trna").append("<div>Server version: " + resp.version + "</div>");
							$("#s3trna").append("<div>Server up time: " + CrvMonitor.getDifferenceDivNoColor(resp.up_time_sec) + "</div>");
							
							$("#s3trna").append("<div class='folder_reads_title'>VACUUM:</div>");
							$("#s3trna").append("<div>In vacuum: " + resp.system_in_vacuum + "</div>");
							$("#s3trna").append("<div>Next vacuum: " + CrvMonitor.getDifferenceDivNoColor(resp.next_vacuum_in_ms/1000) + "</div>");
							$("#s3trna").append("<div>Last vacuum: " + resp.last_vacuum + "</div>");
							if(resp.last_vacuum_query != null)
								$("#s3trna").append("<div title='"+resp.last_vacuum_query+"'>Last vacuum query: " + resp.last_vacuum_query.substr(0, 20)+"..." + "</div>");
							$("#s3trna").append("<div>Last vacuum length: " + CrvMonitor.getDifferenceDivNoColor(resp.last_vacuum_length_sec) + "</div>");
							$("#s3trna").append("<div>Avg vacuum length: " + CrvMonitor.getDifferenceDivNoColor(resp.avg_vacuum_length_sec) + "</div>");
							$("#s3trna").append("<div>Max vacuum length: " + CrvMonitor.getDifferenceDivNoColor(resp.max_vacuum_length_sec) + "</div>");
							$("#s3trna").append("<div>Number of vacuums: " + resp.num_vacuums + "</div>");
							
							$("#s3trna").append("<div class='folder_reads_title'>FOLDER READS: " + CrvMonitor.parseFolders(server_time, JSON.parse(resp.last_read_by_folders)) + "</div>");
						}
						
						if(servername == "k2s3")
						{
							var resp = JSON.parse(data);
							var start = CrvMonitor.getDate(resp.start_date);
							var last = CrvMonitor.getDate(resp.last_read);
							var server_time = CrvMonitor.getDate(resp.current_server_gmt_time);
							var timeDiff = last == 'Never' ? 'Never' : Math.abs(server_time.getTime() - last.getTime())/1000;
							
							$("#k2s3").append("<div>Last read was " + CrvMonitor.getDifferenceDiv(timeDiff) + " ago</div>");
							$("#k2s3").append("<div>Server up time: " + CrvMonitor.getDifferenceDivNoColor(resp.up_time_sec) + "</div>");
							$("#k2s3").append("<div>[Elastic Search] Total inserted: " + CrvMonitor.formatNumber(resp.total_records_consumed_elastic) + "</div>");
							$("#k2s3").append("<div>[S3] Total inserted: " + CrvMonitor.formatNumber(resp.total_records_consumed_s3) + "</div>");
							$("#k2s3").append("<div>[S3] Running queries: " + resp.s3_pending_events + "</div>");
							$("#k2s3").append("<div>[S3] Avg insert rate: " + resp.s3_insertion_rate + " / Sec</div>");
							$("#k2s3").append("<div>[Kinesis] Avg read rate: " + resp.avg_read_rate + "</div>");
							$("#k2s3").append("<div>[Kinesis] Total consumed: " + CrvMonitor.formatNumber(resp.total_records_consumed) + "</div>");
							$("#k2s3").append("<div>Server started: " + CrvMonitor.rebuildDate(resp.start_date) + "</div>");
							$("#k2s3").append("<div>Server version: " + resp.version + "</div>");
							$("#k2s3").append("<div>Server Name: " + resp.name + "</div>");
							$("#k2s3").append("<div>Server is master: " + resp.is_master + "</div>");
							$("#k2s3").append("<div>Server id: " + resp.id + "</div>");
							$("#k2s3").append("<div>Kinesis Stream ID: " + resp.kinesis_stream_id + "</div>");
						}
						
						if(servername == "k2rs")
						{
							var resp = JSON.parse(data);
							
							var start = CrvMonitor.getDate(resp.start_date);
							var last = CrvMonitor.getDate(resp.last_read);
							var server_time = CrvMonitor.getDate(resp.current_server_gmt_time);
							var timeDiff = last == 'Never' ? 'Never' : Math.abs(server_time.getTime() - last.getTime())/1000;
							var next_vacuum = Math.abs(resp.next_vacuum - server_time.getTime());
							
							$("#k2rs").append("<div>Last read was " + CrvMonitor.getDifferenceDiv(timeDiff) + " ago</div>");
							$("#k2rs").append("<div>Server up time: " + CrvMonitor.getDifferenceDivNoColor(resp.up_time_sec) + "</div>");
							$("#k2rs").append("<div>[RedShift] Total inserted: " + CrvMonitor.formatNumber(resp.total_records_inserted_to_redshift) + "</div>");
							$("#k2rs").append("<div>[RedShift] Running queries: " + resp.running_queries_on_redshift + "</div>");
							$("#k2rs").append("<div>[RedShift] Avg insert rate: " + resp.redshift__insertion_rate + " / Sec</div>");
							$("#k2rs").append("<div>[RedShift] Next Vacuum: " + CrvMonitor.getConvertedTimeDiv(next_vacuum) + " | " + next_vacuum + "</div>");
							$("#k2rs").append("<div>[RedShift] Total vacuums: " + resp.num_vacuums + "</div>");
							$("#k2rs").append("<div>[RedShift] avg vacuum length: " + resp.last_vacuum_length_sec + " Sec</div>");
							$("#k2rs").append("<div>[RedShift] last vacuum length: " + resp.avg_vacuum_length_sec + " Sec</div>");
							$("#k2rs").append("<div>[RedShift] Avg read rate: " + resp.redshift__insertion_rate + " / Sec</div>");
							$("#k2rs").append("<div>[Kinesis] Avg read rate: " + resp.avg_read_rate + "</div>");
							$("#k2rs").append("<div>[Kinesis] Total consumed: " + CrvMonitor.formatNumber(resp.total_records_consumed) + "</div>");
							$("#k2rs").append("<div>[RedShift] Last Vacuum: " + resp.last_vacuum + "</div>");
							$("#k2rs").append("<div>[RedShift] max vacuum length: " + resp.max_vacuum_length_sec + " Sec</div>");
							$("#k2rs").append("<div>Server started: " + CrvMonitor.rebuildDate(resp.start_date) + "</div>");
							$("#k2rs").append("<div>Server version: " + resp.version + "</div>");
							$("#k2rs").append("<div>Server is master: " + resp.is_master + "</div>");
							$("#k2rs").append("<div>Server id: " + resp.id + "</div>");
						}
						
						if(servername == "dsc")
						{
							var resp = JSON.parse(data);
							
							var start = CrvMonitor.getDate(resp.start_date);
							var last = CrvMonitor.getDate(resp.last_read);
							var server_time = CrvMonitor.getDate(resp.current_server_gmt_time);
							var timeDiff = Math.abs(server_time.getTime() - last.getTime())/1000;
							
							$("#dsc").append("<div>Last read was " + CrvMonitor.getDifferenceDiv(timeDiff) + " ago</div>");
							$("#dsc").append("<div>Avg read rate: " + resp.avg_read_rate + "</div>");
							$("#dsc").append("<div>Total consumed: " + CrvMonitor.formatNumber(resp.total_records_consumed) + "</div>");
							$("#dsc").append("<div>Server started: " + CrvMonitor.rebuildDate(resp.start_date) + "</div>");
							$("#dsc").append("<div>Server version: " + resp.version + "</div>");
							$("#dsc").append("<div>Server is master: " + resp.is_master + "</div>");
							$("#dsc").append("<div>Server id: " + resp.id + "</div>");
							$("#dsc").append("<div>Server up time: " + CrvMonitor.getDifferenceDivNoColor(resp.up_time_sec) + "</div>");
						}
						
						if(servername == "rs2sql")
						{
							var resp = JSON.parse(data);
							
							var start = CrvMonitor.getDate(resp.start_date);
							var last = CrvMonitor.getDate(resp.last_read);
							var server_time = CrvMonitor.getDate(resp.current_server_gmt_time);
							var timeDiff = Math.abs(server_time.getTime() - last.getTime())/1000;
							
							$("#rs2sql").append("<div>Last read was " + CrvMonitor.getDifferenceDiv(timeDiff, 60) + " ago</div>");
							$("#rs2sql").append("<div>Max read time: " + CrvMonitor.getDifferenceDivNoColor(resp.max_read_time_ms/1000) + "</div>");
							$("#rs2sql").append("<div>Min read time: " + CrvMonitor.getDifferenceDivNoColor(resp.min_read_time_ms/1000) + "</div>");
							$("#rs2sql").append("<div>Avg read time: " + CrvMonitor.getDifferenceDivNoColor(resp.avg_read_time_ms/1000) + "</div>");
							$("#rs2sql").append("<div>Server started: " + CrvMonitor.rebuildDate(resp.start_date) + "</div>");
							$("#rs2sql").append("<div>Read cycles: " + resp.total_read_cycles + "</div>");
							$("#rs2sql").append("<div>Process Running: " + resp.process_running + "</div>");
							$("#rs2sql").append("<div>Process tables left: " + resp.process_tables_left + "</div>");
							$("#rs2sql").append("<div>Server up time: " + CrvMonitor.getDifferenceDivNoColor(resp.up_time_sec) + "</div>");
						}
					});
	//console.log(servername + " was loaded successfully." + data);
}

CrvMonitor.formatNumber = function(num)
{
	return num.replace(/./g, 
		function(c, i, a) {
			return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
		});
}

CrvMonitor.rebuildDate = function(timestring)
{
	var dates = timestring.split(" ");
	var dates_part = dates[0].split("-");
	return dates_part[2] + "/" + dates_part[1]  + "/" + dates_part[0] + " " + dates[1];
}

CrvMonitor.parseFolders = function(server_time, last_read_by_folders)
{
	var folders = "";
	for(var foldername in last_read_by_folders)
	{
		var last = CrvMonitor.getDate(last_read_by_folders[foldername]);
		var timeDiff = Math.abs(server_time.getTime() - last.getTime())/1000;
		folders += "<div class='folder_read'>" + foldername.substr(0, 1).toUpperCase()  + foldername.substr(1).toLowerCase() + " : " + CrvMonitor.getDifferenceDiv(timeDiff) + "</div>";
	}
	
	return folders;
}

CrvMonitor.getDate = function(timestring)
{
	try
	{
		var dates = timestring.split(" ");
		var time = dates[1].split(":");
		var d = new Date(dates[0]);
		d.setHours(time[0], time[1], time[2]);
		return d;
	}catch(s){}
	
	return timestring;
}

CrvMonitor.getDifferenceDiv = function(timeDiff, max_min_bad)
{
	if(max_min_bad == null)
		max_min_bad = 6;
	var timediff_units = "Seconds";
	var class_name = "good";
	
	
	if (timeDiff == null)
	{
		return "<span class='time_diff_bad'>Never</span>";
	}
	
	if(timeDiff > 60)
	{
		timediff_units = "Minutes";
		timeDiff = timeDiff / 60;
		if(timeDiff > max_min_bad)
			class_name = "bad";
	}
	
	if(timeDiff > 60)
	{
		timediff_units = "Hours";
		timeDiff = timeDiff / 60;
		class_name = "bad";
	}
	
	//timeDiff = Math.floor(timeDiff * 100) / 100; 
	return "<span class='time_diff_" + class_name + "'>" + CrvMonitor.convertToTimeUnits(timeDiff, timediff_units) + " " + timediff_units + "</span>";
}

CrvMonitor.getDifferenceDivNoColor = function(timeDiff)
{
	try{
		var timediff_units = "Seconds";
		
		if(timeDiff > 60)
		{
			timediff_units = "Minutes";
			timeDiff = timeDiff / 60;
		
			if(timeDiff > 60)
			{
				timediff_units = "Hours";
				timeDiff = timeDiff / 60;
		
				if(timeDiff > 24)
				{
					timediff_units = "Days";
					timeDiff = timeDiff / 24;
					
					
					if(timeDiff > 30)
					{
						timediff_units = "Months";
						timeDiff = timeDiff / 30;
		
						if(timeDiff > 12)
						{
							timediff_units = "Years";
							timeDiff = timeDiff / 12;
						}
					}
				}
			}
		}
		
		timeDiff = Math.floor(timeDiff * 100) / 100; 
		return "<span class='time_diff'>" + CrvMonitor.convertToTimeUnits(timeDiff, timediff_units) + " " + timediff_units + "</span>";
	}
	catch(s){}
	
	return timeDiff;
}

CrvMonitor.convertToTimeUnits = function(time, units)
{
	if(units == "Hours" || units == "Minutes")
	{
		var first = Math.floor(time);
		var sec = Math.floor((time-first)*60);
		if(sec < 10)
			sec = "0"+sec;
		if(first < 10)
			first = "0"+first;
		return first + ":" + sec;
	}
	
	return Math.floor(time * 100) / 100;
}

CrvMonitor.onAjaxSuccess = function(data, servername, serverdesc, serverstatus_div_id)
{
	jQuery(document).ready(
					function()
					{
						if(serverstatus_div_id != null)
						{
							$("#" + serverstatus_div_id).empty();
							$("#" + serverstatus_div_id)[0].innerHTML = "OK";
							$("#" + serverstatus_div_id).addClass("serverstatus_ok");
						}
						else
							$("#servers_status").append(CrvMonitor.getServerStatusDiv(servername, serverdesc, "ok"));
					});
	//console.log(servername + " was loaded successfully." + data);
}

CrvMonitor.onAjaxFail = function(data, servername, serverdesc, serverstatus_div_id)
{
	//console.log(servername + " failed loading." + data);
	jQuery(document).ready(
					function()
					{
						if(serverstatus_div_id != null)
						{
							$("#" + serverstatus_div_id).empty();
							$("#" + serverstatus_div_id)[0].innerHTML = "FAIL";
							$("#" + serverstatus_div_id).addClass("serverstatus_fail");
						}
						else
							$("#servers_status").append(CrvMonitor.getServerStatusDiv(servername, serverdesc, "fail"));
					});
}
CrvMonitor.divindex = 0;
CrvMonitor.getServerStatuses = function()
{
	var managers = [];
	var ln = CrvMonitor.servers.length;
	var loadingdiv = "";
	for(var i = 0 ; i < ln ; i++)
	{
		var server = CrvMonitor.servers[i];
		var urls = server.url;
		var port = server.port;
		var testservlet = server.testservlet;
		var stts = server.statusservlet;
		var ln_uri = urls.length;
		for(var j = 0 ; j < ln_uri ; j++)
		{
			var serverstatus_div_id = "monitor_" + CrvMonitor.divindex++;
			CrvMonitor.divid_to_url[urls[j]] = serverstatus_div_id;
			var uri = "http://" + urls[j] + ":" + port + testservlet;
			loadingdiv += CrvMonitor.getServerStatusDiv(server.name, server.desc, "loading", serverstatus_div_id);

			managers.push(new AjaxManager(uri, CrvMonitor.onAjaxSuccess, CrvMonitor.onAjaxFail, server.name, server.desc, serverstatus_div_id));
			
			if(stts != null)
			{ 
				var uristts = "http://" + urls[j] + ":" + port + stts;
				managers.push(new AjaxManager(uristts, CrvMonitor.onAjaxStatusSuccess, CrvMonitor.onAjaxFail, server.name, server.desc, serverstatus_div_id));
			}
		}
	}
	
	jQuery(document).ready
	(
		function()
		{
			$("#servers_status").append(loadingdiv);
			for(var k = 0 ; k < managers.length ; k++)
				managers[k].load();
		});
	
}

var AjaxManager = function(uri_, onsuccess_, onfail_, name_, desc_, serverstatus_div_id_)
{
	this.complete = function(data,status,xhr)
	{
		if(status == "success")
		{
			if(this.onsuccess != null)
				this.onsuccess(data, this.name, this.desc, this.serverstatus_div_id);
		}
		else
			if(this.onfail != null)
				this.onfail(data, this.name, this.desc, this.serverstatus_div_id);
		
	}.bind(this);
	
	this.fail = function(xhr, status, error)
	{
		if(this.onfail != null)
			this.onfail(error, this.name, this.desc, this.serverstatus_div_id);
		
	}.bind(this);
	
	this.serverstatus_div_id = serverstatus_div_id_;
	this.uri = uri_;
	this.onsuccess = onsuccess_;
	this.onfail = onfail_;
	this.name = name_;
	this.desc = desc_;
};

AjaxManager.prototype = 
{
	uri: "",
	name: "",
	desc: "",
	onsuccess: null,
	onfail: null,
	
	load: function()
	{
		//$.get(this.uri, this.complete);
		 $.ajax({
						url: this.uri,
						type: "GET",
						dataType: 'text',
						success:  this.complete,
						error:  this.fail
					});
	}	
}

CrvMonitor.getServerStatuses();
