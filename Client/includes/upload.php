<?php

header("Access-Control-Allow-Origin: *");


function uploadFile($file, $appname, $path, $relativePath)
{
	$size = $file["size"];
	if($size == 0) return;
	
	$name = $file["name"];
	$type = $file["type"];
	$allowedExts = array("gif", "jpeg", "jpg", "png", "bmp");
	$temp = explode(".", $name);
	$extension = end($temp);
	$maxFileSize = 500000;
	
	if (
	   ($type == "image/gif")
	|| ($type == "image/jpeg")
	|| ($type == "image/jpg")
	|| ($type == "image/pjpeg")
	|| ($type == "image/bmp")
	|| ($type == "image/x-png")
	|| ($type == "image/png"))
	{
		if(in_array($extension, $allowedExts))
		{
			if($size < $maxFileSize)
			{
			  if ($file["error"] > 0) 
			  {
				$message = "Return Code: " . $file["error"] . "<br>";
				echo '{"status": "fail", "path": "", "message":"'.$message.'"}';
			  } 
			  else 
			  {				
				move_uploaded_file($file["tmp_name"], $path.$appname.".".$extension);
				echo '{"status": "success", "path": "'.$relativePath.'/'.$appname.".".$extension.'", "message":""}';
			  }
			}
			else
			{
				$message = "File '".$name."' is too big! file size is: ".($size / 1024)." and max file size is:".($maxFileSize / 1024)."<br>";
				echo '{"status": "fail", "path": "", "message":"'.$message.'"}';
			}
		}
		else
		{
			$message = "File '".$name."'. Invalid file - un allowed extension"."<br>";
			echo '{"status": "fail", "path": "", "message":"'.$message.'"}';
		}
	} 
	else 
	{
	  $message = "Invalid file"."<br>";
	  echo '{"status": "fail", "path": "", "message":"'.$message.'"}';
	}
}


//***************** START HTML ************


$relativePath = "/upload/".$_POST["CustomerId"];

$dir = realpath(dirname(__FILE__)).$relativePath;

$savepath = $dir."/";

if (!file_exists($savepath))
{
	mkdir($savepath , 0777, true);
}

chmod($savepath, 0777);

uploadfile($_FILES["file1"], $_POST["AppToken"], $savepath, $relativePath);
?>