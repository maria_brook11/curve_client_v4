function LoginHandlFunction($scope,$rootScope,$http,$location,$cookies,$cookieStore){

	function handleLogin()
	{
		$('.login-form').validate(
		{
			errorElement: 'span', //default input error message container
			errorClass: 'help-block', // default input error message class
			focusInvalid: false, // do not focus the last invalid input
			rules: {
				email: {
					required: true,
					email: true
				},
				password: {
					required: true
				},
				remember: {
					required: false
				}
			},

			messages: {
				email: {
					required: "Email is required."
				},
				password: {
					required: "Password is required."
				}
			},

			invalidHandler: function (event, validator) { //display error alert on form submit   
				$('.alert-danger', $('.login-form')).show();
			},

			highlight: function (element) { // hightlight error inputs
				$(element)
					.closest('.form-group').addClass('has-error'); // set error class to the control group
			},

			success: function (label) {
				label.closest('.form-group').removeClass('has-error');
				label.remove();
			},

			errorPlacement: function (error, element) {
				error.insertAfter(element.closest('.input-icon'));
			},

		});

	}

	function validRegistrationForm()
	{
		$('.register-form').validate({
				errorElement: 'span', //default input error message container
				errorClass: 'help-block', // default input error message class
				focusInvalid: false, // do not focus the last invalid input
				rules: {
					email: {
						required: true,
						email: true
					},
					password: {
						required: true
					},
					remember: {
						required: false
					},
					rpassword: {
						equalTo: "#registerPassword"
					},	                
				},

				messages: {
					email: {
						required: "Email is required."
					},
					password: {
						required: "Password is required."
					}
				},

				invalidHandler: function (event, validator) { //display error alert on form submit
					$('.alert-danger', $('.register-form')).show();
				},

				highlight: function (element) { // hightlight error inputs
					$(element)
						.closest('.form-group').addClass('has-error'); // set error class to the control group
				},

				success: function (label) {
					label.closest('.form-group').removeClass('has-error');
					label.remove();
				},

				errorPlacement: function (error, element) {
					error.insertAfter(element.closest('.input-icon'));
				},

			});

	}

	function resetPasswordFormValidation() 
	{
		$('.forget-form').validate(
		{
			errorElement: 'span', //default input error message container
			errorClass: 'help-block', // default input error message class
			focusInvalid: false, // do not focus the last invalid input
			rules: {
				email: {
					required: true,
					email: true
				}                
			},

			messages: {
				email: {
					required: "Email is required."
				}
			},

			invalidHandler: function (event, validator) { //display error alert on form submit
				$('.alert-danger', $('.forget-form')).show();
			},

			highlight: function (element) { // hightlight error inputs
				$(element)
					.closest('.form-group').addClass('has-error'); // set error class to the control group
			},

			success: function (label) {
				label.closest('.form-group').removeClass('has-error');
				label.remove();
			},

			errorPlacement: function (error, element) {
				error.insertAfter(element.closest('.input-icon'));
			},

		});
	}

	function failedResetRequest(data)
	{
        $scope.$apply(function () {
            $scope.loginPage.textError = data;
        });

		$('.loading-page').hide();
		/*$scope.show_error("Error " + data, "Something went wrong! please try again at a later time or contact support.");*/
	}

	function completeResetRequest(data)
	{
        $scope.$apply(function () {
            $scope.loginPage.textError = "Success! Please check your email for forther instructions";
        });
		$('.loading-page').hide();
		/*$scope.show_success("Success", "Please check your email for forther instructions");*/
	}

	$scope.completeLogin = function(data)
	{
		$scope.cacheLoginData(data);
		$scope.gotoApps();
	}

	$scope.loginFailed = function(data)
	{
        $scope.$apply(function () {
            $scope.loginPage.textError = data;
        });

        console.log(data);
		$('.loading-page').hide();

		$scope.deleteCache("customerID");
	}

	$scope.doLogin = function(e)
	{
		if(e != null) e.preventDefault();
		var email = $('input#LoginEmail').val();
		var password = $('input#LoginPassword').val();
		var rememberMe = $('input#LoginRememberMe').attr('checked');
		handleLogin();
		if ($('.login-form').validate().form())
		{
			login(email, password, rememberMe);
		}
		else
		{
            $scope.loginPage.textError = '';
			console.log("Form login **NOT** valid");
		}
	}

	function login(em, pass, remember)
	{	//$rootScope.Token = '';
		//$('.loading-page').show();
		$scope.curvePostAPI('Login', {Email: em, Password: window.md5(pass), RememberMe: remember}, $scope.completeLogin, $scope.loginFailed);
	}
		
		 
		if (!navigator.cookieEnabled)
		{
			alert("ERROR : Cookies Disabled. This site requires 'cookies' to be turned on in order to work properly.");
			return;
		}
		
		$('form input#LoginRememberMe').change(function()
		{
			 if($(this).attr('checked')){
				  $(this).val('true');
			 }else{
				  $(this).val('false');
			 }
		});	

	$scope.forgetFun = function(e)
		{
			var email = $('input#resetEmail').val();
			resetPasswordFormValidation();
			if ($('.forget-form').validate().form())
			{
				$('.loading-page').show();
				$scope.curvePostAPI('SendResetPasswordEmail', {Email: email}, completeResetRequest, failedResetRequest);
			}
			else $scope.loginPage.textError = '';
		};
			
}