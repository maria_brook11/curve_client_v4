app.controller('LoginCtrl', function($scope,$rootScope,$http,$location,TConst,URLConst,$timeout,$interval) {
	StartTrialHandlFunction($scope,$rootScope,$http,$location);
	LoginHandlFunction($scope,$rootScope,$http,$location);
	LoginGlobalFunction($scope,$rootScope,$http,$location);
	RegTrialHandlFunction($scope,$rootScope,$http,$location);
	ResetPasswFunction($scope,$rootScope,$http,$location);
				 
	$rootScope.loginView = true;
	$scope.showForm = 0;
	$scope.showRegStatus = 0;
    $scope.loginPage = {'textError' : '','showError' : false};

	//login + forget
	if($rootScope.loginTpl == 0){
		$scope.showFormFun = function(n){
            $scope.loginPage = {'textError' : '','showError' : false};
			$scope.showForm = n;
			$rootScope.viewForm = n;
		};
		$scope.curveAPI('IsUserAuthenticated', null, authVerifyComplete, authVerifyFail, false, true);	
		function authVerifyComplete(data){ $scope.completeLogin(data); };
		function authVerifyFail(data){ 
		$('.loading-page').hide();
		};
		
		var intervLF = $interval(function ()
			{
				if ($("#LoginEmail").is(":visible"))
				{
					//$timeout(function () {
						document.getElementById("LoginEmail").focus();
					//}, 3000);
					$interval.cancel(intervLF);
					
				}
			}, 300);
		
		
	}
	
	//registration
	if($rootScope.loginTpl == 1){
		$rootScope.viewForm = 2;
		$scope.showRegStatusFun = function(n){
			$scope.showRegStatus = n;
			//$scope.$apply();
		};
		$scope.RegisterFun();
	}
	
	//start trial + invintation
	if($rootScope.loginTpl == 2){
		$rootScope.viewForm = 3;
		$scope.checkParamST();
	}
	
	//reset password
	if($rootScope.loginTpl == 3){
		$rootScope.viewForm = 4;
		$('.loading-page').hide();
		$scope.checkParamRP();
	}

	
});