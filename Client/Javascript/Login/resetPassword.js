function ResetPasswFunction($scope,$rootScope,$http,$location){

	$scope.checkParamRP = function(){
		
		$scope.urlParameters = $location.search();

	};

	$scope.resPaswSubmitFun = function(e) 
	{
		if(e != null) e.preventDefault();
		$('.loading-page').show();
		var password = $('input#resetPassword').val();
		resetFormValidation();
		if ($('.reset-form').validate().form())
			doApiCall(password);
		else 
			$('.loading-page').hide();
	}

	function doApiCall(password)
	{
		var data = {Token:$scope.urlParameters.Token, Email:$scope.urlParameters.Email, NewPassword:window.md5(password)};
		$scope.curvePostAPI('ResetPassword', data, onCompleteChangePassword, onError);
	}

	function onError(data) 
	{
		$('.loading-page').hide();
	}

	function resetFormValidation() 
	{
		$('.reset-form').validate(
		{
			errorElement: 'span', //default input error message container
			errorClass: 'help-block', // default input error message class
			focusInvalid: false, // do not focus the last invalid input
			rules: {
				email: {
					required: true,
					email: true
				},
				password: {
					required: true
				},
				rpassword: {
					equalTo: "#resetPassword"
				}
			},

			messages: {
				email: {
					required: "Email is required."
				},
				password: {
					required: "Password is required."
				}
			},

			invalidHandler: function (event, validator) { //display error alert on form submit   
				$('.alert-danger', $('.reset-form')).show();
			},

			highlight: function (element) { // hightlight error inputs
				$(element)
					.closest('.form-group').addClass('has-error'); // set error class to the control group
			},

			success: function (label) {
				label.closest('.form-group').removeClass('has-error');
				label.remove();
			},

			errorPlacement: function (error, element) {
				error.insertAfter(element.closest('.input-icon'));
			},

			submitHandler: function (form) {
				
			}
		});
	}	


	function onCompleteChangePassword(data)
	{
		$scope.cacheLoginData(data);
		$scope.gotoApps();
	}

}