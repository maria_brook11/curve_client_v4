function RegTrialHandlFunction($scope,$rootScope,$http,$location){
	
	$scope.validateComplete = function(d)
	{
		if(d == null || d == -1)
		{
			$scope.coupon.Status = 'X';
			$("#trial_length").html("14");
		}
		else
		{
			$scope.coupon.Status = 'V';
			$("#trial_length").html(d);
		}
	}
	
	$scope.validateFail = function(d)
	{
		$scope.coupon.Status = 'X';
	}
	
	$scope.checkCouponFun = function(){
		if(!$scope.coupon.Value.length || $scope.coupon.Value === ""){
			$scope.coupon.Status = 'N';
		}else{
			
			$scope.coupon.Status = 'L';
			$scope.curveAPI('GetCuponLength', {'Cupon': $scope.coupon.Value}, $scope.validateComplete, $scope.validateFail);
		}
		
	}
	
	$scope.coupon = {'Status':'N','Value':''};
	var urlParameters = $location.search();
	
	var Cupons = {};

	if(urlParameters != null)
	{
		var cupon_from_url = decodeURIComponent(urlParameters.Coupon);
		if(cupon_from_url != 'undefined' && cupon_from_url !== ""){
			$scope.coupon.Value = cupon_from_url;
			$scope.checkCouponFun();
		}
	}

	
	
	/* Cupons.setStatus = function(stts)
	{
		$("#cupon_loading").hide();
		$("#cupon_check").hide();
		$("#cupon_fail").hide();
							
		if(stts == "V")
			$("#cupon_check").show();
		else if(stts == "X")
			$("#cupon_fail").show();
		else if(stts == "L")
			$("#cupon_loading").show();
	}

 */


	

/* 	Cupons.validateCupon = function(cpn)
	{
		Cupons.setStatus('L');
		$scope.curveAPI('GetCuponLength', {'Cupon': cpn}, Cupons.validateComplete, Cupons.validateFail);
	}

	Cupons.checkCupon = function()
	{
		//setTimeout(function () { Cupons.checkCupon();console.log('check cupon') }, 1000);
			
		if(Cupons.last_cupon == null && Cupons.cupon_from_url != null && Cupons.cupon_from_url != '')
		{
			Cupons.last_cupon = Cupons.cupon_from_url;
			Cupons.validateCupon(Cupons.last_cupon);
		}
		else
		{
			var cpn = $("input#cupon").val();
			if(cpn != "" && cpn != Cupons.last_cupon)
			{
				Cupons.last_cupon = cpn;
				Cupons.validateCupon(Cupons.last_cupon);
			}
			else if(cpn == "")
			{
				$("#trial_length")[0].innerHTML = "14";
				Cupons.setStatus('N');
			}
		}
		
	}
 //Cupons.checkCupon(); */
	$scope.RegisterFun = function(){
		
		$("#cupon_loading").hide();
		$("#cupon_check").hide();
		$("#cupon_fail").hide();
		//Cupons.setStatus('N');
		$('.loading-page').hide();
		$("#page-content").show();
		
		//Cupons.checkCupon();
		
		/* if(Cupons.cupon_from_url != null && Cupons.cupon_from_url != '')
			$("input#cupon").val(Cupons.cupon_from_url);
		 */
	}

	$scope.regSubmit = function(e) 
			{	
				e.preventDefault();
				resetFormValidation();

				if ($('.reset-form').validate().form()) 
					callRegisterAPI();
			}

	function resetFormValidation() 
	{
		$('.reset-form').validate({
				errorElement: 'span', //default input error message container
				errorClass: 'help-block', // default input error message class
				focusInvalid: false, // do not focus the last invalid input
				rules: {
					email: {
						required: true,
						email: true
					},
					companyname: {
						required: true
					},
					name: {
						required: true
					}
				},

				messages: {
					email: {
						required: "Email is required."
					},
					companyname: {
						required: "Company Name is required."
					},
					companyname: {
						required: "Name is required."
					}
				},

				invalidHandler: function (event, validator) { //display error alert on form submit   
					$('.alert-danger', $('.reset-form')).show();
				},

				highlight: function (element) { // hightlight error inputs
					$(element)
						.closest('.form-group').addClass('has-error'); // set error class to the control group
				},

				success: function (label) {
					label.closest('.form-group').removeClass('has-error');
					label.remove();
				},

				errorPlacement: function (error, element) {
					error.insertAfter(element.closest('.input-icon'));
				},

				submitHandler: function (form) {
					
				}
			});

			$('.reset-form input').keypress(function (e) {
				if (e.which == 13) 
				{
					if ($('.reset-form').validate().form())
						callRegisterAPI();
					
					return false;
				}
			});
	}

	function callRegisterAPI()
	{
		$('.loading-page').show();
		$("#page-content").hide();
		var email = $("input#userEmail").val();
		var companyName = $("input#companyName").val();
		var name = $("input#userName").val();
		var cupon = $("input#cupon").val();
		var data = {Email:email, CompanyName: companyName};
		
		if(name != null && name != '' && name != 'undefined')
			data.Name = name;
		
		if(cupon != null && cupon != '' && cupon != 'undefined')
			data.Cupon = cupon;
		
		Crv.event("RegisterTrial", 
					(data.Cupon != null ? "Cupon" : "Normal"), 
					"register", 
					email, 
					null, 
					{"cupon": cupon, "name": name, "email": email, "companyName": companyName});
					
		$scope.curvePostAPI('RegisterTrial', data, $scope.onCompleteSendingData, onFailedSendingData, false);
	}

	$scope.onCompleteSendingData = function(data)
	{	console.log(data)
		$('.loading-page').hide();
		$scope.showRegStatusFun(1);
	}

	function onFailedSendingData(data)
	{
		$('.loading-page').hide();
		$scope.show_error(data, 'Please retry.');
	}

}