window.onload = function() {jQuery('input[type=text]').blur();};
function LoginGlobalFunction($scope,$rootScope,$http,$location){
	
	var QueryString = function () 
	{
		var query_string = {};
		var query = window.location.search.substring(1);
		query = query;
		var vars = query.split("&");
		
		for (var i=0;i<vars.length;i++) 
		{
			var pair = vars[i].split("=");
			
			if (typeof query_string[pair[0]] === "undefined")
			{
				query_string[pair[0]] = decodeURIComponent(pair[1]);
			} 
			else if (typeof query_string[pair[0]] === "string") 
			{
				var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
				query_string[pair[0]] = arr;
			}
			else 
			{
				query_string[pair[0]].push(decodeURIComponent(pair[1]));
			}
		}
		
		return query_string;
	};

	var msg = new QueryString();
	if(msg.m != null && msg.m != ''){alert(msg.m);}
	
	
	$scope.cacheLoginData = function(data)
	{	
	
		$scope.storeToCache("expiration", data.userData.expiration);
		$scope.storeToCache("trial", data.userData.trial);
		$scope.storeToCache("userid", data.userData.UserID);
		$scope.storeToCache("email", data.userData.Email);
		$scope.storeToCache("lastLogin", data.userData.LastLogin);
		$scope.storeToCache("userImage", data.userData.ImageURL);
		$scope.storeToCache("globalFirstLogin", data.userData.firstlogin);
		$scope.storeToCache("userFirstName", data.userData.FirstName);
		$scope.storeToCache("userLastName", data.userData.LastName);
		setFirstLogin(true);	

		if($rootScope.production){
			
			$scope.curveReport();
		}
	
	}
	
	$scope.curveReport = function(){
		
		var UserID = $scope.loadFromStorage('email');
		var config = {userID: UserID, 
					  printToConsole: console.log};
		Crv.i('CURV-58W5SZP5XIM', config);
	};
	
	function setFirstLogin(isFirstLogin)
	{
		$scope.storeToCache("firstLogin", isFirstLogin);
	}

	function isFirstLogin()
	{
		var resp = $scope.loadFromCache("firstLogin");
		setFirstLogin(false);
		return resp;
	}

	$scope.gotoApps = function()
	{
		$scope.applicationsStart();
	}


}