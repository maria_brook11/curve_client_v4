function StartTrialHandlFunction($scope,$rootScope,$http,$location){

	$scope.mName = 'Sir/Madam';
	$scope.statusParam = true;
	$scope.userParam = {};
		
	$scope.checkParamST = function(){
		
		var urlParameters = $location.search();
		
		if($rootScope.startStatus == 0){
			
			if(urlParameters == null)
				{
					$scope.statusParam = false;
					startForm($scope.statusParam);
				}
			else
				{
					
					$scope.userParam.name = decodeURIComponent(urlParameters.name);
					$scope.userParam.email = decodeURIComponent(urlParameters.email);
					$scope.userParam.companyName = decodeURIComponent(urlParameters.companyName);
					$scope.userParam.token = urlParameters.token;
					
					if($scope.userParam.companyName == null || $scope.userParam.companyName == "" || $scope.userParam.companyName.length < 3)
					{
						alert("Invalid link, redirecting you. [1001]");
						$scope.statusParam = false;
					}
					
					if($scope.userParam.email == null || $scope.userParam.email == "" || $scope.userParam.email.length < 3)
					{
						alert("Invalid link, redirecting you. [1002]");
						$scope.statusParam = false;
					}
					
					if($scope.userParam.token == null || $scope.userParam.token == "" || $scope.userParam.token.length < 3)
					{
						alert("Invalid link, redirecting you. [1003]");
						$scope.statusParam = false;
					}
					
					startForm($scope.statusParam);
					
				}
		}else{
			
			$scope.userParam.email = decodeURIComponent(urlParameters.Email);
			$scope.userParam.companyName = decodeURIComponent(urlParameters.CompanyName);
			startForm($scope.statusParam);
		}
	}

	function startForm(e){
		
		if(e){
			if($rootScope.startStatus == 0){
				if($scope.userParam.name != 'undefined' && $scope.userParam.name != null && $scope.userParam.name.length > 0)
				{
					$scope.mName = $scope.userParam.name;	
				}
			}
			$('.loading-page').hide();
			$("#page-content").show();
			
		}else{
			$location.path('/');
		}

	}

	$scope.startSubmitFun = function(e){
		e.preventDefault();
		resetFormValidation();

		if ($('.reset-form').validate().form()) 
		{
			sendAPICall();
		}
	};

	function resetFormValidation() 
	{
		$('.reset-form').validate({
				errorElement: 'span', //default input error message container
				errorClass: 'help-block', // default input error message class
				focusInvalid: false, // do not focus the last invalid input
				rules: {
					email: {
						required: true,
						email: true
					},
					firstName: {
						minlength: 3,
						maxlength: 15,
						required: true
					},
					lastName: {
						minlength: 3,
						maxlength: 15,
						required: true
					},
					password: {
						required: true
					},
					rpassword: {
						equalTo: "#passwordSelect"
					}
				},

				messages: {
					email: {
						required: "Email is required."
					},
					firstName: {
						required: "First name is required."
					},
					lastName: {
						required: "Last name is required."
					},
					password: {
						required: "Password is required."
					}
				},

				invalidHandler: function (event, validator) { //display error alert on form submit   
					$('.alert-danger', $('.reset-form')).show();
				},

				highlight: function (element) { // hightlight error inputs
					$(element)
						.closest('.form-group').addClass('has-error'); // set error class to the control group
				},

				success: function (label) {
					label.closest('.form-group').removeClass('has-error');
					label.remove();
				},

				errorPlacement: function (error, element) {
					error.insertAfter(element.closest('.input-icon'));
				},

				submitHandler: function (form) {
					
				}
			});

			$('.reset-form input').keypress(function (e) {
				if (e.which == 13) 
				{
					if ($('.reset-form').validate().form())
						sendAPICall();
					
					return false;
				}
			});
	}

	function sendAPICall()
	{	
		$('.loading-page').show();
		$("#page-content").hide();
		var password = $('input#passwordSelect').val();
		var firstName = $('input#firstName').val();
		var lastName = $('input#lastName').val();
		
		if($rootScope.startStatus == 0){
			
			var data = {Email:$scope.userParam.email, Password:window.md5(password), CompanyName: $scope.userParam.companyName, Token:$scope.userParam.token, FirstName:firstName, LastName:lastName, RememberMe:false};
			$scope.curvePostAPI('RegisterUserFromTrial', data, onCompleteUseInvitation, onFailedUseInvitation);

		}else{
			
			var data = {Email:$scope.userParam.email, Password:window.md5(password), FirstName:firstName, LastName:lastName, RememberMe:false};
			$scope.curvePostAPI('RegisterUser', data, onCompleteUseInvitation, onFailedUseInvitation);
		
		}
	}

	function onCompleteUseInvitation(data)
	{
		$('.loading-page').hide();
		$("#page-content").show();
		$scope.cacheLoginData(data);
		$scope.gotoApps();
	}

	function onFailedUseInvitation(data)
	{
		$('.loading-page').hide();
		$("#page-content").show();
		$scope.show_error('Using invitation failed', data);
	}

}