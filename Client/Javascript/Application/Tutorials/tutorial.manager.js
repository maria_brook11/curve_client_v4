function TutorlManagerFunction($scope,$rootScope,$http,$location,TConst,$timeout){
$scope.currTutorial = {"className" : "", "currName":""};
$scope.tutorialIntro = {'data' : ''};
$scope.tutorialGetStarted = function(){
	
	if($rootScope.Token != 'CURV-FXEH1VYSXGW'){
		$scope.tutorialAddEvent('ftue1Start1Key', $scope.startFtueTutorial);
		$scope.iconClicked('Demo', 'CURV-FXEH1VYSXGW', $rootScope.protocol+'://prod.curve.tech/includes/upload/bb373e0d-c67b-4fe2-ae33-c81a8cedd103/CURV-FXEH1VYSXGW.jpg', true);
	
	}else{

			//$scope.setTabFun(data.Dashboards[i].ID, data.Dashboards[i].Name);

			$timeout(function () {$(".tutorial_topbar_item_1").trigger( "click" );$scope.startFtueTutorial(100);}, 200);
				
	}
	
}
$scope.startFtueTutorial = function(time)
{
	var militime = 1000;
	if (time != null)
		militime = time;
	$scope.tutorialRemoveEvent("ftue1Start1Key");
	$timeout(function () {$("#ftue1Start1").trigger( "click" );	}, militime);

}
$scope.introOptions = 
{
		steps:[],
		showStepNumbers: false,
        exitOnOverlayClick: true,
        exitOnEsc:true,
        nextLabel: '<span>NEXT</span>',
        prevLabel: '<span>PREVIOUS</span>',
        skipLabel: 'EXIT',
        doneLabel: 'EXIT',
		showBullets: false,
		disableInteraction: true,
		hidePrev:true,
		hideNext:true,
}

$scope.goToTopAndOverflowHidden = function()
{
	window.scrollTo(0, 0);
	document.querySelector('body').setAttribute('style', 'overflow: hidden !important');
}

$scope.goToTopAndOverflowAuto = function()
{
	window.scrollTo(0, 0);
	document.querySelector('body').style.overflow="auto";
}

$scope.autoOverflow = function()
{
	document.querySelector('body').style.overflow="auto";
}

$scope.afterTutorial = function()
{
	var bgColorH = $scope.cssApp.bgColor.slice(0,-1);
	if ($scope.skip === true)
	{
		$scope.skip = false;
		$scope.exitTutorial();
		return;
	}
	//console.log("afterTutorial");
	if (document.querySelector('.page-sidebar-wrapper') != null)
		document.querySelector('.page-sidebar-wrapper').style.position = 'fixed';
	if (document.querySelector('.navbar-fixed-top') != null)
	{
		document.querySelector('.navbar-fixed-top').setAttribute('style', 'z-index: 1001');
		document.querySelector('.navbar-fixed-top').style.position = 'fixed';
		document.querySelector('.navbar-fixed-top').style.background = bgColorH
		
	}
	
	if (document.querySelector('#myModalNewApp') != null)
		document.querySelector('#myModalNewApp').style.position = 'fixed';
	if (document.querySelector('#myModalNewApp2') != null)
		document.querySelector('#myModalNewApp2').style.position = 'fixed';	
		if (document.querySelector('.tutorial_advanced_view') != null)
		document.querySelector('.tabsBox').style.position = 'fixed';	
		
				
	if (document.querySelector('.tutorial_upper_panel') != null)
		document.querySelector('.tutorial_upper_panel').setAttribute('style', 'z-index: 3');
	if (document.querySelector('.tutorial_advanced_view') != null)
		document.querySelector('.tutorial_advanced_view').setAttribute('style', 'z-index: 0');
	if (document.querySelector('.tutorial_tools') != null)
		document.querySelector('.tutorial_tools').style.visibility = 'visible';
	if (document.querySelector('.tutorial_engagement') != null)
		document.querySelector('.tutorial_engagement').style.visibility = 'visible';
	
	if (document.querySelector('.tutorial_new_app') != null)
	{
		$('.tutorial_new_app').on('hover', null);
		$('.tutorial_new_app').off('hover', null);
	}
	
	if (document.querySelector('.tutorial_app_settings') != null)
	{
		$('.tutorial_app_settings').on('hover', null);
		$('.tutorial_app_settings').off('hover', null);
	}
	
	if (document.querySelector('.tutorial_learn_btn') != null)
				{
					document.querySelector('.tutorial_learn_btn').classList.remove("open");
					$('.tutorial_learn_btn').on('hide.bs.dropdown', null);	
					$('.tutorial_learn_btn').off('hide.bs.dropdown', null);
				}
				
	if (document.querySelector('.tutorial_dropdown_btn') != null)
	{
		document.querySelector('.tutorial_dropdown_btn').classList.remove("open");
		$('.tutorial_dropdown_btn').on('hide.bs.dropdown', null);	
		$('.tutorial_dropdown_btn').off('hide.bs.dropdown', null);
	}
	
	if (document.querySelector('.introjs-skipbutton') != null)
	{
		$('.introjs-skipbutton').on('click', null);
	}
	if (document.querySelector('.tutorial_new_app_close') != null)
	{
		document.querySelector('.tutorial_new_app_close').setAttribute('style', 'display: block');
	}
		
	if (document.querySelector('.tutorial_new_app_discard') != null)
	{
		document.querySelector('.tutorial_new_app_discard').setAttribute('style', 'visibility: visible');
	}		
	if (document.querySelector('.tutorial_insights_new') != null){
		document.querySelector('.tutorial_insights_new').style.display = 'inline';
	}
	/* if (document.querySelector('.tutorial_engagement_send_btn') != null){
		document.querySelector('.tutorial_engagement_send_btn').style.display = 'inline';
	}
	if (document.querySelector('.tutorial_engagement_campaign_btn') != null){
		document.querySelector('.tutorial_engagement_campaign_btn').style.display = 'inline';
	}
	if (document.querySelector('.tutorial_engagement_reports_btn') != null){
		document.querySelector('.tutorial_engagement_reports_btn').style.display = 'inline';
	} */
	
	$scope.autoOverflow();
	window.scrollTo(0, 0);
	$scope.skip = false;
	
}
$scope.skip = false;
/*$scope.exitTutorial2 = function()
{
	if ($scope.currTutorial.className == $scope.currTutorial.currName)
	{
		console.log("EXIT TUTORIAL2");
	}
	
	//$scope.skip = true;

}*/
$scope.exitTutorial = function()
{
	$scope.afterTutorial();
	$scope.currTutorial.className = "";
	$scope.tutorialClearAllEvents();

}

$scope.afterStep = function()
{
	
}

////////////////////FTUE ALL/////////////////////////////
	 $scope.ftueACompletedEvent = function (scope) {
        //console.log("Completed Event called");
		$scope.afterTutorial();
		
		
    };
	

    $scope.ftueAExitEvent = function (scope) {
        //console.log("Exit Event called");
		$scope.exitTutorial();
		
    };
	$scope.getNewAppIntroText = function()
	 {
		 
		 if ($scope.appsArr !== null && $scope.appsArr.length > 1)
			return "Now lets create an application.";
		else
			return "Now lets create your first application.";
		
	 }
    $scope.ftueAChangeEvent = function (targetElement, scope) {
     
    };

    $scope.ftueABeforeChangeEvent = function (targetElement, scope) {
      
    };

    $scope.ftueAAfterChangeEvent = function (targetElement, scope) {
        
    };

  	$scope.ftueAIntroOptions = jQuery.extend(true, {}, $scope.introOptions);
	$scope.ftueAIntroOptions.steps = 
	
        [
        {
           intro: "Hey, Welcome to Curve!",
		   onchange: function(introjs)
			{
				
			},
			onbeforechange: function(introjs)
			{
				$scope.goToTopAndOverflowHidden();
				document.querySelector('.page-sidebar-wrapper').style.position = 'absolute';
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("none","block");
				$scope.afterStep();		
			}
			
        },
        {
			intro: "Curve turns your app data into actionable insights that increase customer’s lifetime value. It does so by automatically detecting growth opportunities in your data, prioritizing them and suggesting actions",
			onchange: function(introjs)
			{
				
			},
			onbeforechange: function(introjs)
			{	
				
				$scope.goToTopAndOverflowHidden();
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","block");
				$scope.afterStep();	
			}
        },
        {
           intro: "In the following interactive tutorial we will tour Curve and get an overview of its capabilities.",
			
			onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","block");
				$scope.afterStep();
			}
        }
		,
        {
            intro: "Let’s take a look at Curve’s structure.",
            onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","block");
				$scope.afterStep();
			}
        },
        {
			intro: "This is the top menu. It contains easy to use tools and dashboards.",
			element: '.tutorial_ftue_topmenu',
			position: "bottom-middle-aligned",
            onchange: function(introjs)
			{
				
			},
			onbeforechange: function(introjs)
			{
				
				document.querySelector('.tutorial_tools').style.visibility = 'visible';
				document.querySelector('.tutorial_engagement').style.visibility = 'visible';
				document.querySelector('.page-sidebar-wrapper').style.position = 'absolute';
				
				
			},
			onafterchange: function(introjs)
			{
				
				$scope.applyOnPrevNext("block","block");	
				$scope.afterStep();
			}
          
        },
		
		 {
			intro: "these are the dashboards, we will dive into them later.",
			element: '.tutorial_ftue_topmenu',
			position: "bottom-middle-aligned",
            onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				
				document.querySelector('.tutorial_tools').style.visibility = 'hidden';
				document.querySelector('.tutorial_engagement').style.visibility = 'hidden';
				document.querySelector('.page-sidebar-wrapper').style.position = 'absolute';
			},
			onafterchange: function(introjs)
			{
				
				$scope.applyOnPrevNext("block","block");
				$scope.afterStep();
			}
			
	          
        },
		{
			intro: "These are tools with which you can create and interact with your audiences as well as see higher resolutions of the data. ",
			element: '.tutorial_ftue_topmenu',
			position: "bottom-left-aligned",
            onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				
				document.querySelector('.tutorial_tools').style.visibility = 'visible';
				document.querySelector('.tutorial_engagement').style.visibility = 'visible';
				document.querySelector('.page-sidebar-wrapper').style.position = 'absolute';
			},
			onafterchange: function(introjs)
			{
			
			$scope.applyOnPrevNext("block","block");
					$timeout(function () {
				
					document.querySelector('.introjs-tooltip').style.left = '25%';			
				}, 400);
				$scope.afterStep();
			}
	  
        }
		,
		{
			
			intro: "The engagement center is where you interact with your audiences and get feedback about your interactions. Let's click on it",
			element: '.tutorial_engagement',
            onchange: function(introjs)
			{
				
			},
			onbeforechange: function(introjs)
			{
				$scope.currTutorial.className = "#ftue2Start2";
				$scope.tutorialAddEvent("ftue2Start2", function(){
					$timeout(function () {
				
					introjs.exit();

					introjs.goToStep(8).start();	
				}, 50);
					
				})
				
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","none");
								
				$timeout(function () {
				
					
					document.querySelector('.introjs-disableInteraction').classList.remove('introjs-disableInteraction');		
				}, 400);
				$scope.afterStep();
			}
				          
        },
		 {
           intro: "Great!, The engagement center is splitted into 3 tabs",
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				
				$scope.goToTopAndOverflowHidden();
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("none","block");
				$scope.afterStep();
			}
        },
		    {
           intro: "Here you can create your interactions. It uses external integrations that enable mailing, sms and push notifications.",
		   element: '.tutorial_engagement_send_btn',
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{

					$scope.goToTopAndOverflowHidden();		

			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","block");
				$scope.afterStep();
			}
        },
		{
           intro: "In the campaigns section you can create redirect campaigns that will “color” your users",
		   element: '.tutorial_engagement_campaign_btn',
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				$scope.goToTopAndOverflowHidden();
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","block");
				$scope.afterStep();
			}
        },
			    {
           intro: "Here you can see previous interactions, statistics about them and even retarget users from these interactions by their behaviour and reaction to the interaction.",
		   element: '.tutorial_engagement_reports_btn',
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{

				$scope.goToTopAndOverflowHidden();
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","block");
				$scope.afterStep();
			}
			
        },
		
		    {
           intro: "Let’s go to the dashboards themselves. Click on Status",
		   element: '.tutorial_1',
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				
				$scope.goToTopAndOverflowHidden();
				document.querySelector('.page-sidebar-wrapper').style.position = 'absolute';
				$scope.currTutorial.className = "#ftue3Start3";
				
				$scope.tutorialAddEvent("ftue3Start3", function(){introjs.exit();introjs.goToStep(13).start();})
				
				$('.introjs-skipbutton').on('click', $scope.exitTutorial2);
				
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","none");
				$timeout(function () {
				
					
					document.querySelector('.introjs-disableInteraction').classList.remove('introjs-disableInteraction');		
				}, 400);
				$scope.afterStep();
			}
		},
		 {
           intro: " The status dashboard will give you the most important actions to take as well as a general overview of your application.",
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
			
				$scope.goToTopAndOverflowHidden();
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("none","block");
				$scope.afterStep();
			}
        }
		,
		 {
           intro: "Here you can see the top actions of the day. These are recalculated daily and will be the most significant actions you can take in your app.",
		   element: ".tutorial_status_cards",
		   position: "top",
		 
		   onchange: function(introjs)
			{
				
			},
			onbeforechange: function(introjs)
			{
				
				$scope.goToTopAndOverflowHidden();
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","block");
				$scope.afterStep();
			}
        }
		,
		{
           intro: "Here you can see an overview of your application in small graphs and counters. You can add or remove graphs. "+
		   "<br>Add new graphs from our selection by clicking on the <span style='float:none;padding:2px 6px;margin-right:2px;border:0;' class='status-graph-plus'><i class='fa fa-plus' style='font-size: 12px;'></i></span>button." +
		   "<br>Remove a graph by clicking on the <div class='status_comparable_top graph_icon_close' style='float:none; display:-webkit-inline-box;margin-right:2px;'><i class='fa fa-times'></i></div>button.",
		   element: ".tutorial_status_overview",
		   position: "right",
		   
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
			
				$scope.goToTopAndOverflowAuto();
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","block");
				$scope.afterStep();
			}
        }
		,
		{
           intro: "Lets dive into a dashboard, click on Traffic",
		   element: ".tutorial_2",
		    onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				
				$('.introjs-skipbutton').on('click', $scope.exitTutorial2);
				$scope.goToTopAndOverflowHidden();
				document.querySelector('.page-sidebar-wrapper').style.position = 'absolute';
				$scope.currTutorial.className = "#ftue4Start4";
				
				$scope.tutorialAddEvent("ftue4Start4", function(){introjs.exit();introjs.goToStep(18).start();})
				
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","none");
				$timeout(function () {
				
					
					document.querySelector('.introjs-disableInteraction').classList.remove('introjs-disableInteraction');		
				}, 400);
				$scope.afterStep();
			}
        },
		 {
           intro: "This is how dashboards look like in Curve. They are splitted into a basic and advanced view",
		   element: ".tutorial_all_view",
		   position: "bottom-middle-aligned",
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				
				$scope.goToTopAndOverflowHidden();
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("none","block");
				$scope.afterStep();
			}
        },
		{
           intro: "Basic view contains 4 KPIs that compare yesterday’s metric to same one last week. You can see what is the metric today and also did it go up or down during previous week.",
		   element: ".tutorial_basic_kpi",
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				$timeout(function () {
					$scope.goToTopAndOverflowHidden();
				}, 400);
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","block");
				$scope.afterStep();
			}
        },
		{
           intro: "Here you can see the Insights. These are machine learning actions and insights that will help you be proactive to your users, create interesting audiences, interact with user segments. It is divided into 'new' tab, all the insights you did not react to yet",
		   element: ".tutorial_insights_new",
		   position: "top",
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				$timeout(function () {
				
					document.querySelector('.tutorial_insights_new').style.display = 'block';			
				}, 400);
				$scope.goToTopAndOverflowHidden();
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","block");
				$scope.afterStep();
			}
        },
		{
           intro: "and the 'Done' tab with insights you’ve already reacted and done.",
		   element: ".tutorial_insights_done",
		   position: "top",
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				
				$scope.goToTopAndOverflowHidden();
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","block");
				$scope.afterStep();
			}
        },
		{
           intro: "In the advanced tab you can see graphs related to that dashboard. Click on in it",
		   element: ".tutorial_advanced_view",
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				
				$('.introjs-skipbutton').on('click', $scope.exitTutorial2);
				$scope.goToTopAndOverflowHidden();
				//$scope.tutorialAddEvent("tutorial_advancedTab", $scope.tutorial_advancedTab);
				$scope.currTutorial.className = "#ftue5Start5";
					document.querySelector('.tabsBox').style.position = 'absolute';//kkkkkkkkkkkkkkkkkkkkkkk
				$scope.tutorialAddEvent("tutorial_advancedTab", 
				function(){
					introjs.exit();
					$timeout(function () {
				
						introjs.goToStep(22).start();			
					}, 500);
					})
				
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","none");
				$timeout(function () {
					document.querySelector('.introjs-disableInteraction').classList.remove('introjs-disableInteraction');		
				}, 100);
				$scope.afterStep();
			}
		},{
           intro: "These graphs are created dynamically for your application",
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{	
	
					$scope.goToTopAndOverflowHidden();
				
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("none","block");
				$scope.afterStep();
			}
        }, 
			{
           intro: "In these graphs you can export data, view raw data delete and give permissions to graphs that you have created.",
		   element: ".tutorial_advanced_view_graphs",
		   position: "top",
		   
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{	
				//document.querySelector('.tutorial_advanced_view_graphs .tools').classList.add('introjs-disableInteraction');
					//introJs().setOption("disableInteraction", true);
					$scope.goToTopAndOverflowAuto();
				
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","block");
				$scope.afterStep();
			}
        },
			{
           intro: "There is also some filtering options by date, country and platform.",
		   element: ".tutorial_data_filter_panel",
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				
				$scope.goToTopAndOverflowHidden();
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","block");
				$scope.afterStep();
			}
        },
		{
		   intro: "Now we want to learn about the settings page",
		  // element: ".tutorial_app_settings",
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				
				$scope.goToTopAndOverflowHidden();
				document.querySelector('.page-sidebar-wrapper').style.position = 'absolute';
				document.querySelector('.navbar-fixed-top').style.position = 'absolute';
				document.querySelector('.tutorial_upper_panel').setAttribute('style', 'z-index: -1 !important');
				document.querySelector('.tutorial_advanced_view').setAttribute('style', 'z-index: -1 !important');
				
				$timeout(function () {
					
				var element1 = document.querySelector('.tutorial_dropdown_btn');
				element1.classList.add("open");	
				
				$timeout(function () {
							
				$('.tutorial_dropdown_btn').on('hide.bs.dropdown', function () {
				return false;
				});		

					
				}, 300);
					
				}, 300);
				
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","block");
				$scope.afterStep();
				
			}
		}
		,
		{
           intro: "Click on App settings",
		   element: ".tutorial_app_settings",
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
			
				$('.introjs-skipbutton').on('click', $scope.exitTutorial2);
				$scope.goToTopAndOverflowHidden();
				$scope.currTutorial.className = "#ftue6Start6";
				//console.log($scope.currTutorial.className);
			
				$('.tutorial_app_settings').on('hover', function () {
				return false;
				});		
				
				$scope.tutorialAddEvent("ftue6Start6", function(){introjs.exit();introjs.goToStep(27).start();})
			},
			
			onafterchange: function(introjs)
			{
					$scope.applyOnPrevNext("block","none");
				$timeout(function () {
				
					
					document.querySelector('.introjs-disableInteraction').classList.remove('introjs-disableInteraction');		
				}, 100);
				$scope.afterStep();
			}
        }
        ,
		  {
           intro: "This is the settings page. It hold the application settings, you can change icon, setup external APIs see app token and much more",
		   //element: ".tutorial_basic_view",
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				if (document.querySelector('.tutorial_dropdown_btn') != null)
				{
					document.querySelector('.tutorial_dropdown_btn').classList.remove("open");
					$('.tutorial_dropdown_btn').on('hide.bs.dropdown', null);	
					$('.tutorial_dropdown_btn').off('hide.bs.dropdown', null);
				}
				$scope.goToTopAndOverflowHidden();
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("none","block");
				$scope.afterStep();
			}
        },
		{
		   intro: "Here is your application token. This will be used in order to integrate Curve SDK to your application. It identifies your app in our servers.",
		   element: ".tutorial_settings_token",
		   position: "right",
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				
				$scope.goToTopAndOverflowHidden();
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","block");
				$scope.afterStep();
			}
		},
		{
		   intro: "You can use this tab to integrate External APIs like mailing services, sms, push notifications.",
		   element: ".tutorial_external_apis",
		   
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				
				$scope.goToTopAndOverflowHidden();
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","block");
				$scope.afterStep();
			}
		},
		{
		   intro: "You can also add/remove graphs from your dashboards, grant permissions, view your own permissions.",
		   element: ".tutorial_other_apis",
		   position:"top",
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				
				$scope.goToTopAndOverflowHidden();
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","block");
				$scope.afterStep();
			}
		}
		,
		{
		   intro: "Now lets create your first application.",
		  // element: ".tutorial_app_settings",
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				
				
				$scope.goToTopAndOverflowHidden();
				//$('.navbar-fixed-top').css({'position':'absolute'});
				document.querySelector('.page-sidebar-wrapper').style.position = 'absolute';
				//document.querySelector('.tutorial_dropdown_list').style.zIndex = "2000 !important"
				
				document.querySelector('.navbar-fixed-top').style.position = 'absolute';
				document.querySelector('.tutorial_upper_panel').setAttribute('style', 'z-index: -1 !important');
								
				$timeout(function () {
					
				//document.querySelector('.tutorial_dropdown_btn')//setAttribute
				//	$(".tutorial_dropdown_btn").trigger('click');
				var element1 = document.querySelector('.tutorial_dropdown_btn');
					element1.classList.add("open");	
							$('.tutorial_dropdown_btn').on('hide.bs.dropdown', function () {
				return false;
			});		

					
			}, 300);
			},
			onafterchange: function(introjs)
			{
				$timeout(function () {
				document.querySelector('.introjs-tooltiptext').innerHTML = $scope.getNewAppIntroText();
				}, 500);
				
				$scope.applyOnPrevNext("block","block");
				$scope.afterStep();
			}
		},
			{
		   intro: "Select new App",
		   element: ".tutorial_new_app",
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				
				$('.introjs-skipbutton').on('click', $scope.exitTutorial2);
				$scope.goToTopAndOverflowHidden();
				$scope.currTutorial.className = "#ftue7Start7";	
				$scope.tutorialAddEvent("newAppPopup", function(){
					introjs.exit();
					$timeout(function () {
				
					
					introjs.goToStep(33).start();	
				}, 500);
					})
				//$scope.tutorialAddEvent("newAppPopup",$scope.ftue6NewApp );
				$('.tutorial_new_app').on('hover', function () {
				return false;
				});		
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","none");
				$timeout(function () {
				
					if (document.querySelector('.introjs-disableInteraction') != null)
					document.querySelector('.introjs-disableInteraction').classList.remove('introjs-disableInteraction');		
				}, 100);
				$scope.afterStep();
			}
		}
		,
		 {
           intro: "In this popup, lets select company, application name and click on Apply",
		   element: ".tutorial_new_app_popup",
		   position: "left",
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				
				if (document.querySelector('.tutorial_dropdown_btn') != null)
				{
					document.querySelector('.tutorial_dropdown_btn').classList.remove("open");
					$('.tutorial_dropdown_btn').on('hide.bs.dropdown', null);	
					$('.tutorial_dropdown_btn').off('hide.bs.dropdown', null);
				}
				document.querySelector('.tutorial_new_app_close').style.display = 'none';
				document.querySelector('.tutorial_new_app_discard').style.visibility = 'hidden';
				$('.introjs-skipbutton').on('click', $scope.exitTutorial2);
				$scope.goToTopAndOverflowHidden();
				document.querySelector('#myModalNewApp').style.position = 'absolute';
				//$scope.tutorialAddEvent("ftue8Start8",$scope.ftue7NewApp );
				$scope.tutorialAddEvent("ftue8Start8", function(){introjs.exit();introjs.goToStep(34).start();})
				$scope.currTutorial.className = "#ftue8Start8";	
				//console.log($scope.currTutorial);
			
			
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("none","none");
				$timeout(function () {
				
					if (document.querySelector('.introjs-disableInteraction') != null)
					document.querySelector('.introjs-disableInteraction').classList.remove('introjs-disableInteraction');		
				}, 100);
				$scope.afterStep();
				
			}
        },
		{
           intro: "Now Curve will create your application dashboards. This may take a few minutes and when this process ends, you will receive an email with application data.",
		    onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				
				$scope.goToTopAndOverflowHidden();
				
				
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("none","block");
				$scope.afterStep();
				
			}
        },
        {
           intro: "You can already copy the App Token right here In order to implement our easy to use SDK.",
		   element: ".tutorial_new_app_token",
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				
				$scope.goToTopAndOverflowHidden();
				//document.querySelector('.navbar-fixed-top').setAttribute('style', 'z-index: -1 !important');
				//document.querySelector('.tutorial_upper_panel').setAttribute('style', 'z-index: -1 !important');
				document.querySelector('#myModalNewApp2').style.position = 'absolute';
				$timeout(function () {
				var element1 = document.querySelector('.tutorial_learn_btn');
					element1.classList.add("open");	
							$('.tutorial_learn_btn').on('hide.bs.dropdown', function () {
				return false;
			});		

					
			}, 300);
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("block","block");
				$scope.afterStep();
				
			}
        },
		 {
           intro: "Instructions are available here",
		   element: ".tutorial_sdk_instructions",
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				
				$('#myModalNewApp2').modal('hide');
				$scope.goToTopAndOverflowHidden();
				//$('.navbar-fixed-top').css({'position':'absolute'});
				/*document.querySelector('.page-sidebar-wrapper').style.position = 'absolute';
				//document.querySelector('.tutorial_dropdown_list').style.zIndex = "2000 !important"
				
				document.querySelector('.navbar-fixed-top').style.position = 'absolute';
				document.querySelector('.tutorial_upper_panel').setAttribute('style', 'z-index: -1 !important');
					*/			
			
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("none","block");
				$scope.afterStep();
				
			}
        },
		 {
           intro: "This concludes our Curve overview tutorial. For more in-depth tutorials, please refer to our learning center over here with a vast variety of interactive tutorials and examples",
		   onchange: function(introjs)
			{
			},
			onbeforechange: function(introjs)
			{
				if (document.querySelector('.tutorial_learn_btn') != null)
				{
					document.querySelector('.tutorial_learn_btn').classList.remove("open");
					$('.tutorial_learn_btn').on('hide.bs.dropdown', null);	
					$('.tutorial_learn_btn').off('hide.bs.dropdown', null);
				}
				
				$scope.skip = true;
				$('.introjs-skipbutton').on('click', $scope.exitTutorial2);
				$scope.goToTopAndOverflowHidden();
				
				
				
			},
			onafterchange: function(introjs)
			{
				$scope.applyOnPrevNext("none","none");
				$scope.afterStep();
			}
        }
        ]

	 $scope.ftueAShouldAutoStart = false;

	 

	 
	
	/////////////////////////////CARDS///////////////////////////////
	$scope.cardsCompletedEvent = function (scope) {
        //console.log("Completed Event called");
    };

    $scope.cardsExitEvent = function (scope) {
        //console.log("Exit Event called");
		$scope.exitTutorial();
    };

    $scope.cardsChangeEvent = function (targetElement, scope) {
       /* console.log("Change Event called");
        console.log(targetElement);  //The target element
        console.log(this);  //The IntroJS object*/
    };

    $scope.cardsBeforeChangeEvent = function (targetElement, scope) {
        /*console.log("Before Change Event called");
        console.log(targetElement);*/
    };

    $scope.cardsAfterChangeEvent = function (targetElement, scope) {
        /*console.log("After Change Event called");
        console.log(targetElement);*/
    };

    $scope.cardsIntroOptions = {
        steps:[
        {
            intro: "Welcome to cards1."
        },
        {
			intro: "Welcome to cards2."
          //  element: document.querySelectorAll('#step2')[0],
           // intro: "<b style='color:red' >You</b> can also <em>include</em> HTML",
           // position: 'right'
        }/*,
        {
            element: '#step3',
            intro: 'More features, more fun.',
            position: 'left'
        },
        {
            element: '#step4',
            intro: "Another step.",
            position: 'bottom'
        },
        {
            element: '#step5',
            intro: 'Get it, use it.'
        }*/
        ],
        showStepNumbers: false,
        exitOnOverlayClick: true,
        exitOnEsc:true,
        nextLabel: '<strong>NEXT!</strong>',
        prevLabel: '<span style="color:green">Previous</span>',
        skipLabel: 'Exit',
        doneLabel: 'Thanks'
    };

    $scope.cardsShouldAutoStart = false;
	////////////////////////////////////////////////////////////////


		
function clearTutorialState()
{
	$scope.storeToCache(TConst.TUTORIAL_STATE_KEY, null);
	$scope.storeToCache(TConst.TUTORIAL_EVENT_KEY, null);
}

function clearTutorialEvents()
{
	$scope.storeToCache(TConst.TUTORIAL_EVENT_KEY, null);
}

function gotoTutorial(name)
{
	var url = window.location.toString();
	switch(name)
	{
		case "FTUE": 
		{	
			if (url.indexOf("applications.html") != -1)
				showTutorial(name);
			else
			{
				setTutorialAfterRedirect(name, "applications.html");
				window.location = "applications.html";
			}
			
			break;
		}
		
		case "CREATE_NEW_APP_PAGE_3": 
		{	
			if (url.indexOf("applications.html") != -1)
				showTutorial(name);
			else
			{
				setTutorialAfterRedirect(name, "applications.html");
				window.location = "applications.html";
			}
			
			break;
		}
		
		case "CREATE_NEW_APP_1": 
		{	
			if (url.indexOf("applications.html") != -1)
				showTutorial(name);
			else
			{
				setTutorialAfterRedirect(name, "applications.html");
				window.location = "applications.html";
			}
			
			break;
		}
		
		case "CREATE_NEW_GRAPH_1": 
		{	
			if (url.indexOf("applications.html") != -1)
				showTutorial(name);
			else
			{
				setTutorialAfterRedirect(name, "applications.html");
				window.location = "applications.html";
			}
			
			break;
		}
		
		case "APP_SETTINGS_1": 
		{	
			if (url.indexOf("applications.html") != -1)
				showTutorial(name);
			else
			{
				setTutorialAfterRedirect(name, "applications.html");
				window.location = "applications.html";
			}
			
			break;
		}
		
		case "FIND_TOKEN_1": 
		{	
			if (url.indexOf("applications.html") != -1)
				showTutorial(name);
			else
			{
				setTutorialAfterRedirect(name, "applications.html");
				window.location = "applications.html";
			}
			
			break;
		}
		
		case "CREATE_FUNNEL_1": 
		{	
			if (url.indexOf("applications.html") != -1)
				showTutorial(name);
			else
			{
				setTutorialAfterRedirect(name, "applications.html");
				window.location = "applications.html";
			}
			
			break;
		}
		
		case "PERMISSIONS_1": 
		{	
			if (url.indexOf("applications.html") != -1)
				showTutorial(name);
			else
			{
				setTutorialAfterRedirect(name, "applications.html");
				window.location = "applications.html";
			}
			
			break;
		}
		
		case "GRAPH_OPTIONS_1": 
		{	
			if (url.indexOf("applications.html") != -1)
				showTutorial(name);
			else
			{
				setTutorialAfterRedirect(name, "applications.html");
				window.location = "applications.html";
			}
			
			break;
		}
		
		case "FILTER_DATA_1": 
		{	
			if (url.indexOf("applications.html") != -1)
				showTutorial(name);
			else
			{
				setTutorialAfterRedirect(name, "applications.html");
				window.location = "applications.html";
			}
			
			break;
		}
			
			
				
	}
	
	if (TConst.TUTORIAL_NAMES)
	{
		for(var i = 0; i < TConst.TUTORIAL_NAMES.length ; i++)
		{
			var tut = TConst.TUTORIAL_NAMES[i];
			if(tut != null && name == tut.name)
			{
				return true;
			}
		}
	}

}

var currTour = null;
function showTutorial(name)
{
	var tour = TUTRORIALS_BY_NAME[name];
	

	if (currTour != null)
		currTour.end();
	if(tour != null)
	{
	
		tour.restart();
		tour.init(true);
		tour.start(true);
		currTour = tour;
	}
	else
	{
		$scope.storeToCache(TConst.TUTORIAL_STATE_KEY, name);
	}
}

$scope.initTutorial = function()
{
	//jQuery(document).ready(function() 
	//{
		//createFlotDiv();
		var url = window.location.toString();
		//$scope.setupTutorialLocale();
		$scope.setupTutorials(url);
		
		var tutorial = $scope.loadFromCache(TConst.TUTORIAL_STATE_KEY);
		clearTutorialState();
		
		//console.log("loaded tour " + tutorial); 
		
		if(validate(tutorial))
			showTutorial(tutorial);
	//});
}

function setTutorialAfterRedirect(tutorial, url)
{
	//console.log("setTutorialAfterRedirect", tutorial, url);
	if (validate(tutorial, url))
	{
		//console.log("after validate setTutorialAfterRedirect", tutorial, url);
		$scope.storeToCache(TConst.TUTORIAL_STATE_KEY, tutorial);
	}
}


function validate(tutorial, url)
{
	if(tutorial == null || tutorial == "")
		return false;
	
	if (url == null)
		url = window.location.toString();
			//console.log("url>>> " + url); 
	if (TConst.TUTORIAL_NAMES)
	{
		for(var i = 0; i < TConst.TUTORIAL_NAMES.length ; i++)
		{
			var tut = TConst.TUTORIAL_NAMES[i];
			//console.log("tut>>> " + tut + "," + tut.name + "," + tut.page); 
			if(tut != null && tutorial == tut.name && url.toLowerCase().indexOf(tut.page.toLowerCase()) != -1)
				{
					//console.log("return true"); 
					return true;
					}
		}
	
	}
	//console.log("return false"); 
	return false;
}

// function createFlotDiv()
// {
	// var top = document.createElement("div");
	// top.id = TUTORIAL_OBJECT_TOP;
	// top.style["width"] = '100px';
	// top.style["height"] = '100px';
	// top.style["float"] = 'left';
	// top.style["position"] = 'fixed';
	// top.style["left"] = '50%';
	// top.style["top"] = '0%';
	// appList.style["pointer-events"] = 'none';
	
	// var appList = document.createElement("div");
	// appList.id = TUTORIAL_APPLIST;
	// appList.style["width"] = '200px';
	// appList.style["height"] = '30%';
	// appList.style["float"] = 'left';
	// appList.style["position"] = 'absolute';
	// appList.style["left"] = '40%';
	// appList.style["top"] = '25%';
	// appList.style["pointer-events"] = 'none';
	
	
			
	// $("body").append(div);
	// $("body").append(top);
	// $("body").append(appList);
// }


$scope.tutorialRemoveEvent = function (key)
{	
	if (TConst.TUTORIAL_EVENTS != null && TConst.TUTORIAL_EVENTS[key] != null)
		TConst.TUTORIAL_EVENTS[key]	 = null;
}

$scope.tutorialClearAllEvents = function ()
{	
	TConst.TUTORIAL_EVENTS = null;

}

$scope.tutorialAddEvent = function (key, callback)
{	
	if (TConst.TUTORIAL_EVENTS == null)
		TConst.TUTORIAL_EVENTS = {};
	//console.log("TConst.TUTORIAL_EVENTS");
	//console.log(TConst.TUTORIAL_EVENTS);
	TConst.TUTORIAL_EVENTS[key] = callback;
}

$scope.tutorialInvokeEvent = function (key)
{
	/*console.log("invokeEvent:" + key); 
	console.log("TConst.TUTORIAL_EVENTS_I");
	console.log(TConst.TUTORIAL_EVENTS);*/
	if (TConst.TUTORIAL_EVENTS != null)
	{
		var callbackFunc = TConst.TUTORIAL_EVENTS[key];
		if (callbackFunc != null)
		{
			 callbackFunc();
		}
	}
}

$scope.applyOnPrevNext = function(displayPrev, displayNext)
{
	//$timeout(function () {
				
					
	if (document.querySelector('.introjs-prevbutton') != null)
	{
		//-webkit-inline-box
		if (displayPrev === "none")
		{
			document.querySelector('.introjs-prevbutton').style.display =  "none";
		//	document.querySelector('.introjs-prevbutton').classList.add('introjs-disabled');
		//	document.querySelector('.introjs-prevbutton').disabled = true;
		}
		
			//document.querySelector('.introjs-prevbutton').style.visibility =  "hidden";
		if (displayPrev === "block")
		{
			document.querySelector('.introjs-prevbutton').style.display =  "inline-block";
			//document.querySelector('.introjs-prevbutton').classList.remove('introjs-disabled');
			//document.querySelector('.introjs-prevbutton').style.visibility =  "visible";
			//document.querySelector('.introjs-prevbutton').disabled = false;
			//'javascript:;'
		}
			   

	}
	if (document.querySelector('.introjs-nextbutton') != null)
	{
		if (displayNext === "none")
		{
			document.querySelector('.introjs-nextbutton').style.display =  "none";
			//document.querySelector('.introjs-nextbutton').classList.add('introjs-disabled');
			//document.querySelector('.introjs-nextbutton').style.visibility =  "hidden";
			//document.querySelector('.introjs-prevbutton').disabled = true;
		}
		
		if (displayNext === "block")
		{
			document.querySelector('.introjs-nextbutton').style.display =  "inline-block";
			//document.querySelector('.introjs-nextbutton').classList.remove('introjs-disabled');
			//document.querySelector('.introjs-nextbutton').style.visibility =  "visible";  
			//document.querySelector('.introjs-prevbutton').disabled = false;
		}
			 
	}
		//document.querySelector('.introjs-nextbutton').style.display = displayNext;	
	//			}, 100);
	

}

/*
function addEvent(key, tourName)
{	
	//console.log("addEvent"); 
	if (TUTORIAL_EVENTS == null)
		TUTORIAL_EVENTS = {};
	TUTORIAL_EVENTS[key] = tourName;
	$scope.storeToCache(TUTORIAL_EVENT_KEY, tourName);
}

function addPageEvent(key, tourName)
{
	//console.log("addEventPage"); 
	$scope.storeToCache(TUTORIAL_STATE_KEY, tourName);
}
*/
var clickedOnApply = false;
/*
function invokeEvent(key)
{
	//console.log("invokeEvent:" + key); 
	////console.log("hideBackdrop");$('.tour-backdrop').hide();
	var tutorial = $scope.loadFromCache(TUTORIAL_EVENT_KEY);
	
	if (key == "newAppPopupApply")
		clickedOnApply = true;
	else
		clickedOnApply = false;
	if (tutorial != null)
	{
		var tour = TUTRORIALS_BY_NAME[tutorial];
	
		//console.log("invokeEvent: tour " + tour + " , " + tutorial); 
		if(tour != null)
		{
			tour.end();
			clearTutorialState();
		}
		
		if(currTour != null)
		{
			currTour.end();
		}
		
		if (TUTORIAL_EVENTS != null)
		{
			var tourName = TUTORIAL_EVENTS[key];
			if (tourName != null)
			{
				 showTutorial(tourName);
			}
		}
	}
}*/

function clearAll()
{
	clearTutorialState();
	if (currTour)
	{
		//console.log("force end currTour", currTour);
		currTour.end();
	}
	currTour = null;
}

var allowForceClose = true;
function forceCloseTutorialOnClosePopup()
{
	//console.log("FORCE");
	
	if (allowForceClose)
	{
			clearAll();
			TConst.TUTORIAL_EVENTS = null;
			
	}
			
}

function endTourCompletly()
{
	//console.log("endTourCompletly");
	allowForceClose = true;
	clearTutorialState();
	currTour = null;
	TConst.TUTORIAL_EVENTS = null;
				
}

function dismissPopupOnTutorial()
{
	//console.log("dismissPopupOnTutorial");
	var tutorialEvent = $scope.loadFromCache(TConst.TUTORIAL_EVENT_KEY);
	var tutorialState = $scope.loadFromCache(TConst.TUTORIAL_STATE_KEY);
	if (clickedOnApply == false && (tutorialEvent != null || tutorialState != null))
	{
		var tourE = null;
		var tourS = null;
		if (tutorialEvent != null)
		 tourE = TUTRORIALS_BY_NAME[tutorialEvent];
		if (tutorialState != null)
		 tourS = TUTRORIALS_BY_NAME[tutorialState];
		 
		 if (currTour)
		 {
			//console.log("force end currTour", currTour);
			currTour.end();
		 }
		if (tourE != null){
			
			//console.log("force end tourE", tourE);
			tourE.end();
			//tourE.init();
			//
		}
		
		if (tourS != null){
			
			//console.log("force end tourS", tourS);
			tourS.end();
			//tourE.init();
			//
		}
		clearAll();
	}
	else
		{
			//console.log("ignore");
			clickedOnApply = false;
		}
}

function SDKinstructions11(type)
{	
	var userAppsArr = [];
	
	$.each($rootScope.appsArr, function (key, val){
		var temp = {'name':val.GameName,'token':val.Token};
		userAppsArr.push(temp);
	});
	
	if(type != 'def'){
		var temp = {'name' : $scope.newAppname, 'token' : newApplicationDomain.Token};
		userAppsArr.unshift(temp) 
	}else{
		var temp = {'name' : $scope.activeAppLabel , 'token' : $rootScope.Token};
	}
	
	var appSrt = JSON.stringify(userAppsArr);	
	var url = "";
	
	if(type != 'def'){
		
		if (type == "iOS"){
			url = $rootScope.mainUrl + URLConst.CurveSDKConstants.IOS_SDK_INSTRUCTIONS_URL;
			
		}
		if (type == "tvOS")
			url = $rootScope.mainUrl + URLConst.CurveSDKConstants.TVOS_SDK_INSTRUCTIONS_URL;
		if (type == "Android")
			url = $rootScope.mainUrl + URLConst.CurveSDKConstants.ANDROID_SDK_INSTRUCTIONS_URL;
		if (type == "JS")
			url = $rootScope.mainUrl + URLConst.CurveSDKConstants.JS_SDK_INSTRUCTIONS_URL;
		if (type == "GTM")
			url = $rootScope.mainUrl + URLConst.CurveSDKConstants.GTM_SDK_INSTRUCTIONS_URL;
		
		url += "&AppToken=" + newApplicationDomain.Token + "&appsData=" + appSrt;
		console.log(url)
	}else{
		
		var url = $rootScope.mainUrl + URLConst.CurveSDKConstants.DEF + "&appsData=" + appSrt;
		
	}
	
	window.open(url, '_blank');
}

}