function TutorlFactoryFunction($scope,$rootScope,$http,$location,TConst){
	
var TUTRORIALS_BY_NAME = {};
var TEMPLATE_TOUR_POPUP = "<div class='popoverCurveTour'>"+
						 "<div class='arrow'></div>"+
						"<h3 class='popover-title'></h3>"+
						"<div class='popover-content'></div>"+
						"<div class='popoverCurveNavigation'>"+
							"<div class='btn-group-curve'>"+
								"<button class='btn-default-curve-tour btn-default-curve-left' data-role='prev'>« Prev</button>"+
								"<button class='btn-default-curve-tour btn-default-curve-right' data-role='next'>Next »</button>"+
							"</div>"+
							"<button class='btn-default-curve-tour' data-role='end'>End tour</button>"+
						"</div>"+
						"</div>";


$scope.setupTutorials = function(url)
{
	var tutorialAppID = TConst.TUTORIAL_DEMO_APP;
	
	if ($('#' + TConst.TUTORIAL_DEMO_APP_CURVE))
		tutorialAppID = TConst.TUTORIAL_DEMO_APP_CURVE;
	if(url.toLowerCase().indexOf("applications.html") != -1)
	{
		TUTRORIALS_BY_NAME.FTUE = 
		new Tour({
			////onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					backdrop: true,
					orphan:true,
					
					title: TConst.TutorialTexts.FTUE.title1,
					content: TConst.TutorialTexts.FTUE.text1,
					//element: "#"+TUTORIAL_OBJECT_TOP,
					//placement: "bottom"
					//onEnd: function(tour){endTourCompletly();}
					onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
				}
				,
				{
					backdrop: true,
					orphan:true,
					//element: "#"+TUTORIAL_OBJECT_TOP,
					title: TConst.TutorialTexts.FTUE.title2,
					content: TConst.TutorialTexts.FTUE.text2,
					onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
					//placement: "bottom"
				}
				,
				{
					//backdrop: true,
					backdrop: true,
					orphan:true,
					//element: "#" + TUTORIAL_APPLIST,
					content: TConst.TutorialTexts.FTUE.text3,
					onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
					//placement: "right"
				},
				{
					backdrop: true,
					element: "#"+tutorialAppID,
					content: TConst.TutorialTexts.FTUE.text4,
					placement: "bottom",
					onShown: function(tour) {$("#"+tutorialAppID).css("pointer-events", "none"); $(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
					onHidden: function(tour) { $("#"+tutorialAppID).css("pointer-events", "auto");},
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					backdrop: true,
					element: "#"+'dashboard_'+tutorialAppID,
					content: TConst.TutorialTexts.FTUE.text5,
					placement: "bottom",
					onShown: function(tour) {$("#"+'dashboard_'+tutorialAppID).css("pointer-events", "none");$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
					onHidden: function(tour) { $("#"+'dashboard_'+tutorialAppID).css("pointer-events", "auto");},
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					backdrop: true,
					element: "#"+'settings_'+tutorialAppID,
					content: TConst.TutorialTexts.FTUE.text6,
					placement: "bottom",
					onShown: function(tour) {$("#"+'settings_'+tutorialAppID).css("pointer-events", "none");$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
					onHidden: function(tour) { $("#"+'settings_'+tutorialAppID).css("pointer-events", "auto");},
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					backdrop: true,
					element: "#"+'enable_'+tutorialAppID,
					content: TConst.TutorialTexts.FTUE.text7,
					placement: "bottom",
					onShown: function(tour) {$("#"+'enable_'+tutorialAppID).css("pointer-events", "none");$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
					onHidden: function(tour) { $("#"+'enable_'+tutorialAppID).css("pointer-events", "auto");},
					//onEnd: function(tour){endTourCompletly();}
				}
				,
				{
					backdrop: true,
					element: "#"+tutorialAppID,
					content:TConst.TutorialTexts.FTUE.text8,
					placement: "bottom",
					//onShown: function(){$scope.storeToCache(TConst.TUTORIAL_STATE_KEY, "FTUE2");}
					onShown: function(){$scope.tutorialAddPageEvent("goToDashboardsFromFTUE", "FTUE2");
										$("#"+'settings_'+tutorialAppID).css("pointer-events", "none");
										$("#"+'enable_'+tutorialAppID).css("pointer-events", "none");
										$(".popover-navigation button")[2].onclick=function(){endTourCompletly();
										};},
					onHidden: function(tour) { 
										$("#"+'settings_'+tutorialAppID).css("pointer-events", "auto");
										$("#"+'enable_'+tutorialAppID).css("pointer-events", "auto");
										},
					//onEnd: function(tour){endTourCompletly();}
				}
			]
			});
			
			var tutorialNewAppID = TConst.TUTORIAL_NEW_APP_BTN;
	
			if ($('#' + TConst.TUTORIAL_NEW_APP_BTN_CURVE))
				tutorialNewAppID = TConst.TUTORIAL_NEW_APP_BTN_CURVE;
			
			
			TUTRORIALS_BY_NAME.CREATE_NEW_APP_PAGE_3 = 
			new Tour({
				keyboard: false,
				template: '<div class="popover" role="tooltip"> <div class="arrow"></div> <h3 class="popover-title" style="background-color: #76c9dc;"></h3> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-group" style="padding-bottom: 20px;"> <button class="btn btn-sm btn-default" data-role="prev"></button> <button class="btn btn-sm btn-default" data-role="next"></button> <button class="btn btn-sm btn-default" data-role="pause-resume" data-pause-text="Pause" data-resume-text="Resume">Pause</button> </div> <button class="btn btn-sm btn-default" data-role="end" style="background-color: #76c9dc !important;">Done</button> </div> </div>',
				steps: [
					{
						backdrop: true,
						title: TConst.TutorialTexts.CREATE_NEW_APP_PAGE_3.title1,
						content: TConst.TutorialTexts.CREATE_NEW_APP_PAGE_3.text1,
						element: "#tutorials_dropDown",
						delay: 500,
						placement: "bottom"
					}
				]
			});
			
			TUTRORIALS_BY_NAME.CREATE_NEW_APP_1 = 
			new Tour({
			////onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					backdrop: true,
					orphan:true,
					title: TConst.TutorialTexts.CREATE_NEW_APP_1.title1,
					content: TConst.TutorialTexts.CREATE_NEW_APP_1.text1,
					onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//element: "#"+ TConst.TUTORIAL_OBJECT_TOP,
					//placement: "bottom"
				}
				,
				{
					backdrop: true,
					element: "#"+tutorialNewAppID,
					content: TConst.TutorialTexts.CREATE_NEW_APP_1.text2,
					placement: "bottom",
					onShown: function(tour) {$scope.tutorialAddEvent("newAppPopup","CREATE_NEW_APP_2");$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
				
					//onEnd: function(tour){endTourCompletly();}
				}

			]
			});
			
			
			TUTRORIALS_BY_NAME.CREATE_NEW_APP_2 = 
			new Tour({
			//onEnd: function(tour){endTourCompletly();},
			steps: [
			
				{
					title: TConst.TutorialTexts.CREATE_NEW_APP_2.title1,
					content: TConst.TutorialTexts.CREATE_NEW_APP_2.text1,
					element: "#company_name_title",
					delay: 500,
					placement: "left",
				},
				{
					title: TConst.TutorialTexts.CREATE_NEW_APP_2.title2,
					content: TConst.TutorialTexts.CREATE_NEW_APP_2.text2,
					element: "#newApp_uploadForm",
					// delay: 500,
					placement: "left",
				}
			]
			});
			
			TUTRORIALS_BY_NAME.CREATE_NEW_APP_3 = 
			new Tour({
			//onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					title: TConst.TutorialTexts.CREATE_NEW_APP_3.title1,
					content: TConst.TutorialTexts.CREATE_NEW_APP_3.text1,
					orphan:true,
						onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
				},
				
				{
					title: TConst.TutorialTexts.CREATE_NEW_APP_3.title2,
					content: TConst.TutorialTexts.CREATE_NEW_APP_3.text2,
					orphan:true,
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					backdrop: true,
					orphan: true,
					title: TConst.TutorialTexts.FTUE2.title7,
					content: TConst.TutorialTexts.FTUE2.text9 + "<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('FTUE');\"><u>"+TConst.TutorialTexts.FTUE2.text10+"</u></a><br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('CREATE_NEW_APP_1');\"><u>"+TConst.TutorialTexts.FTUE2.text11+"</u></a><br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('APP_SETTINGS_1');\"><u>"+TConst.TutorialTexts.FTUE2.text12+"</u></a>"+"<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('PERMISSIONS_1');\"><u>"+TConst.TutorialTexts.FTUE2.text13+"</u></a>"+"<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('GRAPH_OPTIONS_1');\"><u>"+TConst.TutorialTexts.FTUE2.text14+"</u></a>"+"<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('FILTER_DATA_1');\"><u>"+TConst.TutorialTexts.FTUE2.text15+"</u></a>",
						onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
					
				}
			]
			});
			
			TUTRORIALS_BY_NAME.CREATE_NEW_GRAPH_1 = 
			new Tour({
			//onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					backdrop: true,
					element: "#"+tutorialAppID,
					content: TConst.TutorialTexts.CREATE_NEW_GRAPH_1.text1,
					placement: "bottom",
					onShown: function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};$scope.tutorialAddEvent("newGraphEnterApp","CREATE_NEW_GRAPH_2");},
					//onEnd: function(tour){endTourCompletly();}
				}
			]
			});
			
			TUTRORIALS_BY_NAME.APP_SETTINGS_1 = 
			new Tour({
			//onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					orphan:true,
					backdrop: true,
					title: TConst.TutorialTexts.APP_SETTINGS_1.title1,
					content: TConst.TutorialTexts.APP_SETTINGS_1.text1,
							onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
					
				},
				{
					backdrop: true,
					element: "#"+'settings_'+tutorialAppID,
					title: TConst.TutorialTexts.APP_SETTINGS_2.title1,
					content: TConst.TutorialTexts.APP_SETTINGS_2.text1,
					placement: "bottom",
					onShown: function(tour) {$scope.tutorialAddPageEvent("goToAppSettings","APP_SETTINGS_3");$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
					//onEnd: function(tour){endTourCompletly();}
				}
			]
			});
			
			TUTRORIALS_BY_NAME.FIND_TOKEN_1 = 
			new Tour({
			//onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					orphan:true,
					backdrop: true,
					title: TConst.TutorialTexts.FIND_TOKEN_1.title1,
					content: TConst.TutorialTexts.FIND_TOKEN_1.text1,
					onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
					
				},
				{
					backdrop: true,
					element: "#"+'settings_'+tutorialAppID,
					title: TConst.TutorialTexts.FIND_TOKEN_2.title1,
					content: TConst.TutorialTexts.FIND_TOKEN_2.text1,
					placement: "bottom",
					onShown: function(tour) {$scope.tutorialAddPageEvent("goToAppSettings","FIND_TOKEN_3");$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
					//onEnd: function(tour){endTourCompletly();}
				}
			]
			});
			/*TUTRORIALS_BY_NAME.APP_SETTINGS_2 = 
			new Tour({
			steps: [
				{
					backdrop: true,
					element: "#"+'settings_'+tutorialAppID,
					title: TConst.TutorialTexts.APP_SETTINGS_2.title1,
					content: TConst.TutorialTexts.APP_SETTINGS_2.text1,
					placement: "bottom",
					onShown: function(tour) {$scope.tutorialAddPageEvent("goToAppSettings","APP_SETTINGS_3");},
					//onEnd: function(tour){endTourCompletly();}
				}
			]
			});*/
			
			TUTRORIALS_BY_NAME.CREATE_FUNNEL_1 = 
			new Tour({
			//onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					backdrop: true,
					element: "#"+tutorialAppID,
					content: TConst.TutorialTexts.CREATE_FUNNEL_1.text1,
					placement: "bottom",
					onShown: function(tour) {$scope.tutorialAddPageEvent("goToAppFunnel","CREATE_FUNNEL_2"); $(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
					//onEnd: function(tour){endTourCompletly();}
				}
			]
			});
			
			TUTRORIALS_BY_NAME.PERMISSIONS_1 = 
			new Tour({
			//onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					orphan:true,
					backdrop:true,
					title: TConst.TutorialTexts.PERMISSIONS_1.title1,
					content: TConst.TutorialTexts.PERMISSIONS_1.text1,
					onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					////onEnd: function(tour){endTourCompletly();}
				}
				,
				{
					backdrop:true,
					element: "#managePermissionsBtn",
					placement: "right",
					title: TConst.TutorialTexts.PERMISSIONS_1.title2,
					content: TConst.TutorialTexts.PERMISSIONS_1.text2,
					onShown: function(tour) {$scope.tutorialAddPageEvent("waitNavigateToPermissions","PERMISSIONS_2");$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
					//onEnd: function(tour){endTourCompletly();}
				}
			]
			});
			
			TUTRORIALS_BY_NAME.GRAPH_OPTIONS_1 = 
			new Tour({
			//onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					orphan:true,
					backdrop:true,
					title: TConst.TutorialTexts.GRAPH_OPTIONS_1.title1,
					content: TConst.TutorialTexts.GRAPH_OPTIONS_1.text1,
					onShown: function(tour) {$('.tour-backdrop').show();$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
					////onEnd: function(tour){endTourCompletly();}
				}
				,
				{
					backdrop:true,
					element: "#"+'dashboard_'+tutorialAppID,
					placement: "bottom",
					content: TConst.TutorialTexts.GRAPH_OPTIONS_1.text2,
					onShown: function(tour) {$scope.tutorialAddPageEvent("goToDashbordsFromGraphOptions","GRAPH_OPTIONS_2");$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
					////onEnd: function(tour){endTourCompletly();}
				}
			]
			});
			
			TUTRORIALS_BY_NAME.FILTER_DATA_1 = 
			new Tour({
			//onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					orphan:true,
					backdrop:true,
					title: TConst.TutorialTexts.FILTER_DATA_1.title1,
					content: TConst.TutorialTexts.FILTER_DATA_1.text1,
								onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					////onEnd: function(tour){endTourCompletly();}
				},
				{
				
					backdrop: true,
					element: "#"+tutorialAppID,
					content: TConst.TutorialTexts.FILTER_DATA_1.text2,
					placement: "bottom",
					onShown: function(tour) {$("#"+'settings_'+tutorialAppID).css("pointer-events", "none");
										$("#"+'enable_'+tutorialAppID).css("pointer-events", "none");$scope.tutorialAddPageEvent("filterDataEnterApp","FILTER_DATA_2");$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
						onHidden: function(tour) { 
										$("#"+'settings_'+tutorialAppID).css("pointer-events", "auto");
										$("#"+'enable_'+tutorialAppID).css("pointer-events", "auto");
										},
					//onEnd: function(tour){endTourCompletly();}
														
				}
			]
			});
			
			
	}
	
	else if(url.toLowerCase().indexOf("mypermissions.html") != -1)
	{
			TUTRORIALS_BY_NAME.PERMISSIONS_4 = 
		new Tour({
		//onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					orphan:true,
					content: TConst.TutorialTexts.PERMISSIONS_4.text1,
					backdrop: true,
						onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
				},
				
				{
					orphan:true,
					content: TConst.TutorialTexts.PERMISSIONS_4.text2,
					backdrop: true,
						onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
				}
				,
				
				{
					orphan:true,
					content: TConst.TutorialTexts.PERMISSIONS_4.text3,
					backdrop: true,
						onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
				},
				
				{
					orphan:true,
					content: TConst.TutorialTexts.PERMISSIONS_4.text4,
					backdrop: true,
						onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
					
				},
				{
					orphan:true,
					content: TConst.TutorialTexts.PERMISSIONS_4.text5,
					backdrop: true,
						onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
					
				},
				{
					orphan:true,
					content: TConst.TutorialTexts.PERMISSIONS_4.text6,
					backdrop: true,
						onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
					
				},
				{
					orphan:true,
					content: TConst.TutorialTexts.PERMISSIONS_4.text7,
					backdrop: true,
						onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					backdrop: true,
					orphan: true,
					title: TConst.TutorialTexts.FTUE2.title7,
					content: TConst.TutorialTexts.FTUE2.text9 + "<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('FTUE');\"><u>"+TConst.TutorialTexts.FTUE2.text10+"</u></a><br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('CREATE_NEW_APP_1');\"><u>"+TConst.TutorialTexts.FTUE2.text11+"</u></a><br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('APP_SETTINGS_1');\"><u>"+TConst.TutorialTexts.FTUE2.text12+"</u></a>"+"<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('PERMISSIONS_1');\"><u>"+TConst.TutorialTexts.FTUE2.text13+"</u></a>"+"<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('GRAPH_OPTIONS_1');\"><u>"+TConst.TutorialTexts.FTUE2.text14+"</u></a>"+"<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('FILTER_DATA_1');\"><u>"+TConst.TutorialTexts.FTUE2.text15+"</u></a>",
					onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
					
				}	
			]
			});
	
	}
	else if(url.toLowerCase().indexOf("settings.html") != -1)
	{
		TUTRORIALS_BY_NAME.APP_SETTINGS_3 = 
		new Tour({
		//onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					backdrop: true,
					element: "#tokenHolder",
					title: TConst.TutorialTexts.APP_SETTINGS_3.title1,
					content: TConst.TutorialTexts.APP_SETTINGS_3.text1 + "<a href=\""+TConst.TutorialTexts.APP_SETTINGS_3.text1l+"\" target='_blank'>"+TConst.TutorialTexts.APP_SETTINGS_3.text2l+"</a>",
					placement: "right",
						onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					backdrop: true,
					element: "#settings_checkbox_container",
					title: TConst.TutorialTexts.APP_SETTINGS_3.title2,
					content: TConst.TutorialTexts.APP_SETTINGS_3.text2,
					placement: "right",
						onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					backdrop: true,
					element: "#graphs_and_dash",
					title: TConst.TutorialTexts.APP_SETTINGS_3.title3,
					content: TConst.TutorialTexts.APP_SETTINGS_3.text3,
					placement: "left",
					onShown: function(tour) {$("#graphs_and_dash").css("pointer-events", "none");$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
					onHidden: function(tour) { $("#graphs_and_dash").css("pointer-events", "auto");},
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					backdrop: true,
					element: "#inapp_pricing",
					title: TConst.TutorialTexts.APP_SETTINGS_3.title31,
					content: TConst.TutorialTexts.APP_SETTINGS_3.text31,
					placement: "left",
					onShown: function(tour) {$("#inapp_pricing").css("pointer-events", "none");$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
					onHidden: function(tour) { $("#inapp_pricing").css("pointer-events", "auto");},
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					backdrop: true,
					element: "#settings_img_uploadIcon",
					title: TConst.TutorialTexts.APP_SETTINGS_3.title4,
					content: TConst.TutorialTexts.APP_SETTINGS_3.text4,
					placement: "right",
					onShown: function(tour) {$("#settings_img_uploadIcon").css("pointer-events", "none");$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
					onHidden: function(tour) { $("#settings_img_uploadIcon").css("pointer-events", "auto");},
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					backdrop: true,
					element: "#applyBtnSettings",
					title: TConst.TutorialTexts.APP_SETTINGS_3.title5,
					content: TConst.TutorialTexts.APP_SETTINGS_3.text5,
					placement: "left",
					onShown: function(tour) {$("#applyBtnSettings").css("pointer-events", "none");$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
					onHidden: function(tour) { $("#applyBtnSettings").css("pointer-events", "auto");},
					//onEnd: function(tour){endTourCompletly();}
				},
				/*{
					backdrop: true,
					orphan: true,
					title: TConst.TutorialTexts.APP_SETTINGS_3.title6,
					content: TConst.TutorialTexts.APP_SETTINGS_3.text6,
					onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
					
				},*/
				{
					backdrop: true,
					orphan: true,
					title: TConst.TutorialTexts.FTUE2.title7,
					content: TConst.TutorialTexts.FTUE2.text9 + "<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('FTUE');\"><u>"+TConst.TutorialTexts.FTUE2.text10+"</u></a><br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('CREATE_NEW_APP_1');\"><u>"+TConst.TutorialTexts.FTUE2.text11+"</u></a><br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('APP_SETTINGS_1');\"><u>"+TConst.TutorialTexts.FTUE2.text12+"</u></a>"+"<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('PERMISSIONS_1');\"><u>"+TConst.TutorialTexts.FTUE2.text13+"</u></a>"+"<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('GRAPH_OPTIONS_1');\"><u>"+TConst.TutorialTexts.FTUE2.text14+"</u></a>"+"<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('FILTER_DATA_1');\"><u>"+TConst.TutorialTexts.FTUE2.text15+"</u></a>",
					onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
					
				}
				
				
			]
			});
			
		TUTRORIALS_BY_NAME.FIND_TOKEN_3 = 
		new Tour({
		//onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					backdrop: true,
					element: "#tokenHolder",
					title: TConst.TutorialTexts.APP_SETTINGS_3.title1,
					content: TConst.TutorialTexts.APP_SETTINGS_3.text1 + "<a href=\""+TConst.TutorialTexts.APP_SETTINGS_3.text1l+"\" target='_blank'>"+TConst.TutorialTexts.APP_SETTINGS_3.text2l+"</a>",
					placement: "right",
					onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
				}
			]
			});
	}
	
	else if(url.toLowerCase().indexOf("dashboard.html") != -1)
	{
		TUTRORIALS_BY_NAME.FTUE2 = 
		new Tour({
		//onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					
					orphan:true,
						backdrop: true,
					content: TConst.TutorialTexts.FTUE2.text1,
					onShown: function(tour) {
						$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};
						$('.tour-backdrop').show();
					},
									
				
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					element: "#dashboards_nav",
					title: TConst.TutorialTexts.FTUE2.title2,
					content: TConst.TutorialTexts.FTUE2.text2,
					onShown: function(tour) {$("#dashboards_nav").css("pointer-events", "none");$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
					onHidden: function(tour) { $("#dashboards_nav").css("pointer-events", "auto");},
					backdrop: true,
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					element: "#dashboard-country-select",
					title: TConst.TutorialTexts.FTUE2.title3,
					content: TConst.TutorialTexts.FTUE2.text3,
					onShown: function(tour) {$("#dashboard-country-select").css("pointer-events", "none");$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
					onHidden: function(tour) { $("#dashboard-country-select").css("pointer-events", "auto");},
					backdrop: true,
					placement: "bottom",
					//onEnd: function(tour){endTourCompletly();}
					
				},
				{
					element: "#dashboard-platform-select",
					title: TConst.TutorialTexts.FTUE2.title3,
					content: TConst.TutorialTexts.FTUE2.text4,
					backdrop: true,
					placement: "bottom",
					onShown: function(tour) {$("#dashboard-platform-select").css("pointer-events", "none");$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
					onHidden: function(tour) { $("#dashboard-platform-select").css("pointer-events", "auto");},
					//onEnd: function(tour){endTourCompletly();}
					
				},
				{
					element: "#dashboard-report-range",
					title: TConst.TutorialTexts.FTUE2.title3,
					content: TConst.TutorialTexts.FTUE2.text5,
					backdrop: true,
					placement: "bottom",
					onShown: function(tour) {$("#dashboard-report-range").css("pointer-events", "none");$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
					onHidden: function(tour) { $("#dashboard-report-range").css("pointer-events", "auto");},
					onNext:	function(tour){$("#newGraphBtn").css("position",'absolute');},
					//onEnd: function(tour){endTourCompletly();}
					
				},
				{
					element: "#newGraphBtn",
					title: TConst.TutorialTexts.FTUE2.title4,
					content: TConst.TutorialTexts.FTUE2.text6,
					placement: "left",
					backdrop: true,
					onShown: function(tour) {      $(".popover-navigation button")[2].onclick=function(){endTourCompletly();};
					    //position: absolute/fixed
													$('.tour-step-background').hide();
													$("#newGraphBtn").css("pointer-events", "none");closeOthers("", null); onNavigationClick('settings_menu');},
					onHidden: function(tour) { $("#newGraphBtn").css("pointer-events", "auto");$("#newGraphBtn").css("position",'fixed');},
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					element: "#applicationSettingsBtnFromDashboards",
					title: TConst.TutorialTexts.FTUE2.title5,
					content: TConst.TutorialTexts.FTUE2.text7,
					placement: "right",
					//backdropPadding: 25, 
					backdrop: true,
					onShown: function(tour) {$("#applicationSettingsBtnFromDashboards").css("pointer-events", "none");$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
					onHidden: function(tour) { $("#applicationSettingsBtnFromDashboards").css("pointer-events", "auto");},
					//onEnd: function(tour){endTourCompletly();}
									
				},

				{
					backdrop: true,
					element: "#user-area",
					title: TConst.TutorialTexts.FTUE2.title6,
					content: TConst.TutorialTexts.FTUE2.text8,
					placement: "bottom",
					onShown: function(tour) {$("#user-area").css("pointer-events", "none");$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
					onHidden: function(tour) { $("#user-area").css("pointer-events", "auto");},
					//onEnd: function(tour){endTourCompletly();}
				
				},
				{
					backdrop: true,
					orphan: true,
					title: TConst.TutorialTexts.FTUE2.title7,
					content: TConst.TutorialTexts.FTUE2.text9 + "<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('FTUE');\"><u>"+TConst.TutorialTexts.FTUE2.text10+"</u></a><br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('CREATE_NEW_APP_1');\"><u>"+TConst.TutorialTexts.FTUE2.text11+"</u></a><br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('APP_SETTINGS_1');\"><u>"+TConst.TutorialTexts.FTUE2.text12+"</u></a>"+"<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('PERMISSIONS_1');\"><u>"+TConst.TutorialTexts.FTUE2.text13+"</u></a>"+"<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('GRAPH_OPTIONS_1');\"><u>"+TConst.TutorialTexts.FTUE2.text14+"</u></a>"+"<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('FILTER_DATA_1');\"><u>"+TConst.TutorialTexts.FTUE2.text15+"</u></a>",
					onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
					
				}	
				
			]
			});
			
			TUTRORIALS_BY_NAME.FILTER_DATA_2 = 
			new Tour({
			//onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					backdrop: true,
					element: "#data_filter_panel",
					content: TConst.TutorialTexts.FILTER_DATA_2.text1,			
					placement: "bottom",
					onShown: function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};$("#dashboard-country-select").css("pointer-events", "none"); $("#dashboard-platform-select").css("pointer-events", "none"); $("#dashboard-report-range").css("pointer-events", "none");$("#refreshFilterDataBtn").css("pointer-events", "none");},
					onHidden: function(tour) { $("#dashboard-country-select").css("pointer-events", "auto"); $("#dashboard-platform-select").css("pointer-events", "auto"); $("#dashboard-report-range").css("pointer-events", "auto");$("#refreshFilterDataBtn").css("pointer-events", "auto");},
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					
					backdrop: true,
					element: "#dashboard-report-range",
					content: TConst.TutorialTexts.FILTER_DATA_2.text2,			
					placement: "bottom",
					//onShown: function(tour) {$scope.tutorialAddEvent("waitForFilterByDates", "FILTER_DATA_3"); $('#userPermissions').on('shown.bs.modal', function() { //$scope.tutorialInvokeEvent("waitForValidMailAndApply");});}
					onShown: function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};$scope.tutorialAddEvent("waitForFilterByDates", "FILTER_DATA_3");$("#dashboard-report-range")[0].onshow = function(){ $scope.tutorialInvokeEvent("waitForFilterByDates");};},
					//onEnd: function(tour){endTourCompletly();}
				
				}
				]
			});
			
			TUTRORIALS_BY_NAME.FILTER_DATA_3 = 
			new Tour({
			//onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					orphan:true,
					backdrop:true,
					content: TConst.TutorialTexts.FILTER_DATA_3.text1,
					onShown: function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}					
				},
				{
					
					backdrop: true,
					element: "#dashboard-platform-select",
					content: TConst.TutorialTexts.FILTER_DATA_3.text2,			
					placement: "bottom",
					onShown: function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};$scope.tutorialAddEvent("waitForFilterByPlatform", "FILTER_DATA_4"); $('#modal_platform_select').on('shown.bs.modal', function() { $scope.tutorialInvokeEvent("waitForFilterByPlatform");});},
					onHidden: function(tour) {$('.tour-backdrop').hide();},
					//onEnd: function(tour){endTourCompletly();}
					
				
				}
			]
			});
			
			TUTRORIALS_BY_NAME.FILTER_DATA_4 = 
			new Tour({
			//onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					element: "#modal_platform_select_and_apply_div",
					content: TConst.TutorialTexts.FILTER_DATA_4.text1,			
					placement: "bottom",
					
					//onEnd: function(tour){endTourCompletly();}
						onShown: function(tour) {$('#modal_platform_select').on('hidden.bs.modal', function() {allowForceClose = true; forceCloseTutorialOnClosePopup();});$(".popover-navigation button")[2].onclick=function(){endTourCompletly();}; $scope.tutorialAddEvent("waitForFilterByPlatformApply", "FILTER_DATA_5");},
					onHidden: function(tour) { $('#modal_platform_select').unbind('hidden.bs.modal'); $('#modal_platform_select').modal('hide');},
					
				}
			]
			});
			
			TUTRORIALS_BY_NAME.FILTER_DATA_5 = 
			new Tour({
			//onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					orphan:true,
					backdrop:true,
					content: TConst.TutorialTexts.FILTER_DATA_5.text1,
					onShown: function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}					
				},
				{
					element: "#dashboard-country-select",
					content: TConst.TutorialTexts.FILTER_DATA_5.text2,			
					placement: "bottom",
					backdrop:true,
					onShown: function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};$scope.tutorialAddEvent("waitForFilterByCountry", "FILTER_DATA_6"); $('#modal_country_select').on('shown.bs.modal', function() { $scope.tutorialInvokeEvent("waitForFilterByCountry");});},
					//onEnd: function(tour){endTourCompletly();}
								
				}
			]
			});
			
			TUTRORIALS_BY_NAME.FILTER_DATA_6 = 
			new Tour({
			//onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					element: "#modal_country_select_and_apply_div",
					content: TConst.TutorialTexts.FILTER_DATA_6.text1,			
					placement: "right",
					onShown: function(tour) {$('#modal_country_select').on('hidden.bs.modal', function() {allowForceClose = true; forceCloseTutorialOnClosePopup();});$(".popover-navigation button")[2].onclick=function(){endTourCompletly();}; $scope.tutorialAddEvent("waitForFilterByCountryApply", "FILTER_DATA_7");},
					onHidden: function(tour) { $('#modal_country_select').unbind('hidden.bs.modal'); $('#modal_country_select').modal('hide');},
					//onEnd: function(tour){endTourCompletly();}
				}
			]
			});
			
			TUTRORIALS_BY_NAME.FILTER_DATA_7 = 
			new Tour({
			//onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					orphan:true,
					backdrop:true,
					content: TConst.TutorialTexts.FILTER_DATA_7.text1,
					onShown: function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}					
				},
				{
					element: "#refreshFilterDataBtn",
					content: TConst.TutorialTexts.FILTER_DATA_7.text2,			
					placement: "bottom",
					backdrop:true,
					onShown: function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}					
				},
					{
					backdrop: true,
					orphan: true,
					title: TConst.TutorialTexts.FTUE2.title7,
					content: TConst.TutorialTexts.FTUE2.text9 + "<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('FTUE');\"><u>"+TConst.TutorialTexts.FTUE2.text10+"</u></a><br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('CREATE_NEW_APP_1');\"><u>"+TConst.TutorialTexts.FTUE2.text11+"</u></a><br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('APP_SETTINGS_1');\"><u>"+TConst.TutorialTexts.FTUE2.text12+"</u></a>"+"<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('PERMISSIONS_1');\"><u>"+TConst.TutorialTexts.FTUE2.text13+"</u></a>"+"<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('GRAPH_OPTIONS_1');\"><u>"+TConst.TutorialTexts.FTUE2.text14+"</u></a>"+"<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('FILTER_DATA_1');\"><u>"+TConst.TutorialTexts.FTUE2.text15+"</u></a>",
					onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
					
				}	
			]
			});
		
			TUTRORIALS_BY_NAME.CREATE_NEW_GRAPH_2 = 
			new Tour({
			//onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					backdrop: true,
					orphan: true,					
					content: TConst.TutorialTexts.CREATE_NEW_GRAPH_2.text1,
					onShown: function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};$scope.tutorialAddPageEvent("newGraphEnterApp","CREATE_NEW_GRAPH_2");},
					//onEnd: function(tour){endTourCompletly();}
				}
				,
				{
					element: "#settings_menu_btn",
					title: TConst.TutorialTexts.CREATE_NEW_GRAPH_2.title2,
					content: TConst.TutorialTexts.CREATE_NEW_GRAPH_2.text2,
					placement: "right",
					backdropPadding: 25, 
					backdrop: true,
					onShown: function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
									
				}
			]
			});			
			
			TUTRORIALS_BY_NAME.GRAPH_OPTIONS_2 = 
		new Tour({
		//onEnd: function(tour){endTourCompletly();},
			steps: [
			
				{
					backdrop: true,
					orphan: true,					
					content: TConst.TutorialTexts.GRAPH_OPTIONS_2.text0,
					onShown: function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};$('.tour-backdrop').show();},
					//onEnd: function(tour){endTourCompletly();}
				},
				{
				
					element: "#" + TConst.TUTORIAL_RAW_DATA_BTN,
					content: TConst.TutorialTexts.GRAPH_OPTIONS_2.text1,
					placement: "left",
					backdrop: true,
					onShown: function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();}; $("#show-data").on('hidden.bs.modal', function(){allowForceClose = true; forceCloseTutorialOnClosePopup();}); $scope.tutorialAddEvent("rawDataClicked","GRAPH_OPTIONS_3");},
					//onEnd: function(tour){endTourCompletly();}
					
				}
			]
			});
			
			TUTRORIALS_BY_NAME.GRAPH_OPTIONS_3 = 
		new Tour({
		//onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					delay: 500,
					element: "#graphRawDataModal",
					placement: "right",
					content: TConst.TutorialTexts.GRAPH_OPTIONS_3.text1,
					//onEnd: function(tour){endTourCompletly();}
					onShown:function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};},
					onNext: function(tour) {$("#show-data").unbind('hidden.bs.modal'); $('#show-data').modal('hide');}
				},	
				{
					element: "#" + TConst.TUTORIAL_RAW_DATA_SAVE_BTN,
					content: TConst.TutorialTexts.GRAPH_OPTIONS_3.text2,
					placement: "left",
					backdrop: true,
					onShown: function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};$("#" + TConst.TUTORIAL_RAW_DATA_SAVE_BTN).css("pointer-events", "none");},
					onHidden: function(tour) { $("#" + TConst.TUTORIAL_RAW_DATA_SAVE_BTN).css("pointer-events", "auto");},
					//onEnd: function(tour){endTourCompletly();}
				},
{
					element: "#" + TConst.TUTORIAL_RAW_DATA_EXPORT_BTN,
					content: TConst.TutorialTexts.GRAPH_OPTIONS_3.text3,
					placement: "left",
					backdrop: true,
					onShown: function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();}; $scope.tutorialAddEvent("rawDataExportClicked","GRAPH_OPTIONS_4");},
					//onEnd: function(tour){endTourCompletly();}
				}					
			
			]
			});
			
			TUTRORIALS_BY_NAME.GRAPH_OPTIONS_4 = 
		new Tour({
		//onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					orphan:true,
					content: TConst.TutorialTexts.GRAPH_OPTIONS_4.text1,
					backdrop: true,
					onShown:function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
				},	
				{
					orphan:true,
					content: TConst.TutorialTexts.GRAPH_OPTIONS_4.text2,
					backdrop: true,
					onShown:function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					orphan:true,
					content: TConst.TutorialTexts.GRAPH_OPTIONS_4.text3,
					backdrop: true,
					onShown:function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					orphan:true,
					content: TConst.TutorialTexts.GRAPH_OPTIONS_4.text4,
					backdrop: true,
					onShown:function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					orphan:true,
					content: TConst.TutorialTexts.GRAPH_OPTIONS_4.text5,
					backdrop: true,
					onShown:function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					backdrop: true,
					orphan: true,
					title: TConst.TutorialTexts.FTUE2.title7,
					content: TConst.TutorialTexts.FTUE2.text9 + "<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('FTUE');\"><u>"+TConst.TutorialTexts.FTUE2.text10+"</u></a><br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('CREATE_NEW_APP_1');\"><u>"+TConst.TutorialTexts.FTUE2.text11+"</u></a><br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('APP_SETTINGS_1');\"><u>"+TConst.TutorialTexts.FTUE2.text12+"</u></a>"+"<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('PERMISSIONS_1');\"><u>"+TConst.TutorialTexts.FTUE2.text13+"</u></a>"+"<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('GRAPH_OPTIONS_1');\"><u>"+TConst.TutorialTexts.FTUE2.text14+"</u></a>"+"<br><a class=\"tutorialLink\" href='#' onclick=\"gotoTutorial('FILTER_DATA_1');\"><u>"+TConst.TutorialTexts.FTUE2.text15+"</u></a>",
					onShown:function(tour){$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
					
				}					
			]
			});
			
	}
	else if(url.toLowerCase().indexOf("admin.html") != -1)
	{
		TUTRORIALS_BY_NAME.PERMISSIONS_2 = 
		new Tour({
		////onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					orphan:true,
					content: TConst.TutorialTexts.PERMISSIONS_2.text1,
					backdrop: true,
					onShown:function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					element: "#" + TConst.TUTORIAL_FIRST_PERMISSION_INPUT,
					title: TConst.TutorialTexts.PERMISSIONS_2.title2,
					content: TConst.TutorialTexts.PERMISSIONS_2.text2,
					onShown: function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();}; $(".input-iconFirstTutorial").css("border", "1px solid #06789e"); $scope.tutorialAddEvent("waitForValidMailAndApply", "PERMISSIONS_3");},
					onHidden: function(tour) {$(".input-iconFirstTutorial").css("border", "1px solid #ffffff");},
					backdrop: true,
					placement:"bottom",
					//onEnd: function(tour){endTourCompletly();}
				}]});
			
		TUTRORIALS_BY_NAME.PERMISSIONS_3 = 
		new Tour({
			//onEnd: function(tour){endTourCompletly();},
			steps: [
				{
					orphan:true,
					content: TConst.TutorialTexts.PERMISSIONS_3.text1,
					backdrop: true,
					onShown: function(tour) {$("#applyInviteUserBtn").css("pointer-events", "none");$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};$('#userPermissions').on('hidden.bs.modal', function () {allowForceClose = true; forceCloseTutorialOnClosePopup();});},
					onHidden: function(tour) { $("#applyInviteUserBtn").css("pointer-events", "auto");}
					//onEnd: function(tour){endTourCompletly();}
				},
				
				{
					orphan:true,
					content: TConst.TutorialTexts.PERMISSIONS_3.text2,
					backdrop: true,
					onShown:function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};$("#applyInviteUserBtn").css("pointer-events", "none");},
					onHidden: function(tour) { $("#applyInviteUserBtn").css("pointer-events", "auto");}
					//onEnd: function(tour){endTourCompletly();}
				}
				,
				
				{
					orphan:true,
					content: TConst.TutorialTexts.PERMISSIONS_3.text3,
					backdrop: true,
					onShown:function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};$("#applyInviteUserBtn").css("pointer-events", "none");},
					onHidden: function(tour) { $("#applyInviteUserBtn").css("pointer-events", "auto");}
					//onEnd: function(tour){endTourCompletly();}
				},
				
				{
					element: "#noneId",
					content: TConst.TutorialTexts.PERMISSIONS_3.text4,
					backdrop: true,
					placement: "bottom",
					onShown:function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};$("#applyInviteUserBtn").css("pointer-events", "none");},
					onHidden: function(tour) { $("#applyInviteUserBtn").css("pointer-events", "auto");}
					//onEnd: function(tour){endTourCompletly();}
					
				},
				{
					element: "#readId",
					content: TConst.TutorialTexts.PERMISSIONS_3.text5,
					backdrop: true,
					placement: "bottom",
					onShown:function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};$("#applyInviteUserBtn").css("pointer-events", "none");},
					onHidden: function(tour) { $("#applyInviteUserBtn").css("pointer-events", "auto");}
					//onEnd: function(tour){endTourCompletly();}
					
				},
				{
					element: "#writeId",
					content: TConst.TutorialTexts.PERMISSIONS_3.text6,
					backdrop: true,
					placement: "bottom",
					onShown:function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};$("#applyInviteUserBtn").css("pointer-events", "none");},
					onHidden: function(tour) { $("#applyInviteUserBtn").css("pointer-events", "auto");}
					//onEnd: function(tour){endTourCompletly();}
					
				},
				{
					element: "#adminId",
					content: TConst.TutorialTexts.PERMISSIONS_3.text7,
					backdrop: true,
					placement: "bottom",
					onShown:function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};$("#applyInviteUserBtn").css("pointer-events", "none");},
					onHidden: function(tour) { $("#applyInviteUserBtn").css("pointer-events", "auto");}
					//onEnd: function(tour){endTourCompletly();}
					
				},
				{
					element: "#applyInviteUserBtn",
					content: TConst.TutorialTexts.PERMISSIONS_3.text8,
					backdrop: true,
					placement: "bottom",
					onShown: function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};$("#applyInviteUserBtn").css("pointer-events", "none");},
					onHidden: function(tour) { $("#applyInviteUserBtn").css("pointer-events", "auto"); $('#userPermissions').unbind('hidden.bs.modal'); $('#userPermissions').modal('hide');},
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					element: "#" + TConst.TUTORIAL_FIRST_PERMISSION_PENDING,
					content: TConst.TutorialTexts.PERMISSIONS_3.text9,
					backdrop: true,
					placement: "top",
					onShown:function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}					
				},
				{
					orphan:true,
					content: TConst.TutorialTexts.PERMISSIONS_3.text10,
					backdrop: true,
					onShown:function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					element: "#" + TConst.TUTORIAL_FIRST_PERMISSION_REGISTERED,
					content: TConst.TutorialTexts.PERMISSIONS_3.text11,
					backdrop: true,
					placement: "top",
					onShown:function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
					
				},
				{
					orphan:true,
					content: TConst.TutorialTexts.PERMISSIONS_3.text12,
					backdrop: true,
					onShown:function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					orphan:true,
					content: TConst.TutorialTexts.PERMISSIONS_3.text13,
					backdrop: true,
					onShown:function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};}
					//onEnd: function(tour){endTourCompletly();}
				},
				{
					element: "#myPermissionsBtn_admin",
					content: TConst.TutorialTexts.PERMISSIONS_3.text14,
					backdrop: true,
					placement: "right",
					onShown: function(tour) {$(".popover-navigation button")[2].onclick=function(){endTourCompletly();};$scope.tutorialAddPageEvent("waitForOnMyPermissionClick", "PERMISSIONS_4");},
					//onEnd: function(tour){endTourCompletly();}
				}
			]
			});
			
			
	}
	
}

}