/*function TutorlTextsFunction($scope,$rootScope,$http,$location){
	
var TutorialTexts = {};

// ***************** FTUE ********************

$scope.setupTutorialLocale = function()
{
	TutorialTexts.FTUE = {};
	TutorialTexts.FTUE.title1 = "Welcome to CURVE";
	TutorialTexts.FTUE.text1 = "CURVE is a growth hackers tool and this tutorial will help you get started.";
	
	TutorialTexts.FTUE.title2 = "Welcome to CURVE";
	TutorialTexts.FTUE.text2 = "Let's start using CURVE";
	
	TutorialTexts.FTUE.text3 = "This is your application page. Here you will be able to view your existing apps and create new applications.";
	TutorialTexts.FTUE.text4 = "Let's investigate one of your applications. To enter the main page of your app, you need to go to dashboards you can do that by simply clicking on the app icon.";
	TutorialTexts.FTUE.text5 = "or clicking on this icon";
	TutorialTexts.FTUE.text6 = "To go to the app settings page, just use this button.";
	TutorialTexts.FTUE.text7 = "When clicking on this button you can enable or disable the reports from this application.";
	TutorialTexts.FTUE.text8 = "Now, let's investigate the data.  <b><font color='#19BAEF'>Click</font></b> on the app to continue";
	
	TutorialTexts.FTUE2 = {};
	TutorialTexts.FTUE2.text1 = "This is your application's analytics data.";
	
	TutorialTexts.FTUE2.title2 = "Dashboards";
	TutorialTexts.FTUE2.text2 = "On the left hand side you'll find your different dashboards."
	
	TutorialTexts.FTUE2.title3 = "Filters";
	TutorialTexts.FTUE2.text3 = "On the top right, you can filter your data dynamically by geo-location...";
	TutorialTexts.FTUE2.text4 = "...or by platform";
	TutorialTexts.FTUE2.text5 = "...or by date";
	
	TutorialTexts.FTUE2.title4 = "New Graph";
	TutorialTexts.FTUE2.text6 = "Click on the \"Create New Graph\" button to create new graphs.";
	
	TutorialTexts.FTUE2.title5 = "Application Settings";
	TutorialTexts.FTUE2.text7 = "You can always change the application reports by changing its settings.";
	
	TutorialTexts.FTUE2.title6 = "User Settings";
	TutorialTexts.FTUE2.text8 = "User messages and other user-related topics can be found in the User Area.";
	
	TutorialTexts.FTUE2.title7 = "Thanks for viewing this tutorial";
	TutorialTexts.FTUE2.text9 = "Related topics: ";
	TutorialTexts.FTUE2.text10 = "Get Started";
	TutorialTexts.FTUE2.text11 = "Create new app";
	TutorialTexts.FTUE2.text12 = "Edit settings";
	TutorialTexts.FTUE2.text13 = "Permissions Manager";
	TutorialTexts.FTUE2.text14 = "Graph options";
	TutorialTexts.FTUE2.text15 = "Data filtering";
	
	TutorialTexts.CREATE_NEW_APP_PAGE_3 = {};
	TutorialTexts.CREATE_NEW_APP_PAGE_3.title1 = "Thank You";
	TutorialTexts.CREATE_NEW_APP_PAGE_3.text1 = "Note you always have access to the SDK and tutorials from here.";
	
	TutorialTexts.CREATE_NEW_APP_1 = {};
	TutorialTexts.CREATE_NEW_APP_1.title1 = "How to create new app";
	TutorialTexts.CREATE_NEW_APP_1.text1 = "Let's create a new application.";
	
	TutorialTexts.CREATE_NEW_APP_1.title2 = "";
	TutorialTexts.CREATE_NEW_APP_1.text2 = "<b><font color='#19BAEF'>Click</font></b> on the \"New Application\" icon under the relevant company.";
	
	TutorialTexts.CREATE_NEW_APP_2 = {};
	TutorialTexts.CREATE_NEW_APP_2.title1 = "Choose Company";   
	TutorialTexts.CREATE_NEW_APP_2.text1 = "<b><font color='#19BAEF'>Choose the company</font></b> That the application will be assign to.";
	
	TutorialTexts.CREATE_NEW_APP_2.title2 = "Give a name";   
	TutorialTexts.CREATE_NEW_APP_2.text2 = "<b><font color='#19BAEF'>Give you application a name.</font></b> The name must consist of letters, underscore and numbers only. Once you've entered the name, <b><font color='#19BAEF'>Click on \"Apply\"</font></b>";
	
	TutorialTexts.CREATE_NEW_APP_3 = {};
	TutorialTexts.CREATE_NEW_APP_3.title1 = "";
	TutorialTexts.CREATE_NEW_APP_3.text1 = "This process takes a few seconds. Once your application has been created on our servers, an email will be sent to you with information about your application.";
	TutorialTexts.CREATE_NEW_APP_3.title2 = "";
	TutorialTexts.CREATE_NEW_APP_3.text2 = "That's all you have to do. Your application will be added to this page when it is ready.";

	
	
	TutorialTexts.CREATE_NEW_GRAPH_1 = {};
	TutorialTexts.CREATE_NEW_GRAPH_1.title1 = "Create new graph";
	TutorialTexts.CREATE_NEW_GRAPH_1.text1 = "First, you need to choose the application you want.";
	
	TutorialTexts.CREATE_NEW_GRAPH_2 = {};
	TutorialTexts.CREATE_NEW_GRAPH_2.title1 = "CREATE_NEW_GRAPH_2";
	TutorialTexts.CREATE_NEW_GRAPH_2.text1 = "Next, click on \"Settings\".";
	TutorialTexts.CREATE_NEW_GRAPH_2.title2 = "CREATE_NEW_GRAPH_2, title2";
	TutorialTexts.CREATE_NEW_GRAPH_2.text2 = "Now, go to \"Create New Graph\".";

	TutorialTexts.APP_SETTINGS_1 = {};
	TutorialTexts.APP_SETTINGS_1.title1 = "Welcome to App Settings Tutorial";
	TutorialTexts.APP_SETTINGS_1.text1 = "In this tutorial we will learn about the settings of your app";
	
	TutorialTexts.APP_SETTINGS_2 = {};
	TutorialTexts.APP_SETTINGS_2.title1 = "";
	TutorialTexts.APP_SETTINGS_2.text1 = "<b><font color='#19BAEF'>Click</font></b> on the \"Settings\" icon";
	
	TutorialTexts.APP_SETTINGS_3 = {};
	TutorialTexts.APP_SETTINGS_3.title1 = "App Token";
	TutorialTexts.APP_SETTINGS_3.text1 = "This is your Report Token. For more information regarding how to use it in your application, please click ";
	TutorialTexts.APP_SETTINGS_3.text1l = "http://www.curve.tech/getting-started/";
	TutorialTexts.APP_SETTINGS_3.text2l = "<u>here</u>";
	
	
	TutorialTexts.APP_SETTINGS_3.title2 = "Enable/Disable reporting";
	TutorialTexts.APP_SETTINGS_3.text2 = "Here you can select which events are reported. If your application does not require reporting or you wish to turn something off temporarely, use these tick boxes. Click on \"Apply\" to apply changes. Warning: applying changes impacts your data.";
	TutorialTexts.APP_SETTINGS_3.title3 = "Toggle graphs & dashboards";
	TutorialTexts.APP_SETTINGS_3.text3 = "Click on \"Setup Graphs & Dashboards\" to turn graph/dashboards display on/off. This action does not impact your data.";
	TutorialTexts.APP_SETTINGS_3.title31 = "Set inapp puchases pricings";
	TutorialTexts.APP_SETTINGS_3.text31 = "Click on \"Inapp Pricing\" to add, delete or update your app's product pricing to help curve give you a better picture of your revenues.";
	
	TutorialTexts.APP_SETTINGS_3.title4 = "Change application icon";
	TutorialTexts.APP_SETTINGS_3.text4 = "Click here to upload your application icon.";
	TutorialTexts.APP_SETTINGS_3.title5 = "Apply the changes";
	TutorialTexts.APP_SETTINGS_3.text5 = "Click on \"Apply\" to apply changes";
	TutorialTexts.APP_SETTINGS_3.title6 = "Other tutorials";
	TutorialTexts.APP_SETTINGS_3.text6 = "you can always see other tutorials";
	
	TutorialTexts.CREATE_FUNNEL_1 = {};
	TutorialTexts.CREATE_FUNNEL_1.title1 = "Welcome to create Funnel Tutorial";
	TutorialTexts.CREATE_FUNNEL_1.text1 = "First enter the app you want";
	
	TutorialTexts.CREATE_FUNNEL_2 = {};
	TutorialTexts.CREATE_FUNNEL_2.title1 = "";
	TutorialTexts.CREATE_FUNNEL_2.text1 = "Click on settings button";
	
	TutorialTexts.CREATE_FUNNEL_3 = {};
	TutorialTexts.CREATE_FUNNEL_3.title1 = "App Token";
	TutorialTexts.CREATE_FUNNEL_3.text1 = "This is your Report Token. For more information regarding how to use it in your application, please click ";
	TutorialTexts.CREATE_FUNNEL_3.text1l = "http://www.curve.tech/getting-started/";
	TutorialTexts.CREATE_FUNNEL_3.text2l = "<u>here</u>";
	
	TutorialTexts.CREATE_FUNNEL_3.title2 = "Enable/Disable reporting";
	TutorialTexts.CREATE_FUNNEL_3.text2 = "Here you can select which events are reported. If your application does not require reporting or you wish to turn something off temporarily, use these tick boxes. Warning, This impacts your data!";
	TutorialTexts.CREATE_FUNNEL_3.title3 = "Toggle graphs & dashboards";
	TutorialTexts.CREATE_FUNNEL_3.text3 = "Click on this button to turn graph/dashboards display on/off. This action does not impact your data!";
	TutorialTexts.CREATE_FUNNEL_3.title4 = "Change application icon";
	TutorialTexts.CREATE_FUNNEL_3.text4 = "Click here to upload your application icon.";
	TutorialTexts.CREATE_FUNNEL_3.title5 = "Apply the changes";
	TutorialTexts.CREATE_FUNNEL_3.text5 = "Click on \"Apply\" to apply changes";
	TutorialTexts.CREATE_FUNNEL_3.title6 = "Other tutorials";
	TutorialTexts.CREATE_FUNNEL_3.text6 = "you can always see other tutorials";
	
	
	TutorialTexts.PERMISSIONS_1 = {};
	TutorialTexts.PERMISSIONS_1.title1 = "Welcome to permissions tutorials";
	TutorialTexts.PERMISSIONS_1.text1 = "Permissions help you to decide who has permission to do what in your dashboards. Let's start by showing how to grant, modify and revoke user permissions.";
	TutorialTexts.PERMISSIONS_1.title2 = "Go to Permissions Manager";
	TutorialTexts.PERMISSIONS_1.text2 = "First, let's use the navigation bar on the left to navigate to the \"Permissions Manager\" page. <b><font color='#19BAEF'>Click now.</font></b>";
	
	TutorialTexts.PERMISSIONS_2 = {};
	TutorialTexts.PERMISSIONS_2.text1 = "This is the \"Permissions Manager\" page. Let's grant permission for a specific user.";
	TutorialTexts.PERMISSIONS_2.title2 = "Insert user name";
	TutorialTexts.PERMISSIONS_2.text2 = "In this invite box, <b><font color='#19BAEF'>enter</font></b> a VALID email address (for the time being, it can be info@curve.tech) and <b><font color='#19BAEF'>click</font></b> on the \"Invite User\" button.";

	TutorialTexts.PERMISSIONS_3 = {};
	TutorialTexts.PERMISSIONS_3.text1 = "Now you will see the \"Set Permissions\" pop-up.";
	TutorialTexts.PERMISSIONS_3.text2 = "Here you can set permissions at the company level as well as at the application level.";
	TutorialTexts.PERMISSIONS_3.text3 = "There are 4 kind of permissions: ";
	TutorialTexts.PERMISSIONS_3.text4 = "None - User will not be able to see the company / application.";
	TutorialTexts.PERMISSIONS_3.text5 = "Read - User can view the data but will not be able to modify settings or create new graphs.";
	TutorialTexts.PERMISSIONS_3.text6 = "Write - User can read, create and modify but cannot grant or revoke another user's permissions.";
	TutorialTexts.PERMISSIONS_3.text7 = "Admin - User can read, create and modify as well as grant and / or revoke another user's permissions.";
	TutorialTexts.PERMISSIONS_3.text8 = "Clicking on \"Apply\" will send the permissions for server approval.";
	TutorialTexts.PERMISSIONS_3.text9 = "Now, the user will appear under \"Pending Users\".";
	TutorialTexts.PERMISSIONS_3.text10 = "An Email will be sent to a user if they are an unregistered user, prompting them to register. If the user is already registered, they will receive a message in their \"CURVE\" inbox asking them to approve the permissions.";
	TutorialTexts.PERMISSIONS_3.text11 = "Once user permissions are approved by the user, users will appear in the \"Registered Users\" section.";
	TutorialTexts.PERMISSIONS_3.text12 = "If you are the admin, at any point, you can edit user permissions or revoke them completely.";
	TutorialTexts.PERMISSIONS_3.text13 = "To understand what you can and cannot do at the application and company level, you must understand your given permission.";
	TutorialTexts.PERMISSIONS_3.text14 = "First, let's use the navigation bar on the left to navigate to the \"My Permissions\" page.  <b><font color='#19BAEF'>Click now.</font></b>";
	
	TutorialTexts.PERMISSIONS_4 = {};
	TutorialTexts.PERMISSIONS_4.text1 = "This is your \"Permissions\" page. Here you can see which companies and applications you will be able to access and your permission level for the application(s).";
	TutorialTexts.PERMISSIONS_4.text2 = "There are 4 kinds of permissions:";
	TutorialTexts.PERMISSIONS_4.text3 = "None - User will not be able to see the company / application.";
	TutorialTexts.PERMISSIONS_4.text4 = "Read - User can view data but cannot modify settings or create new graphs.";
	TutorialTexts.PERMISSIONS_4.text5 = "Write - User can read, create and modify but cannot grant and / or revoke another user's permissions.";
	TutorialTexts.PERMISSIONS_4.text6 = "Admin - User can read, create and modify as well as grant and / or revoke another user's permissions.";
	TutorialTexts.PERMISSIONS_4.text7 = "To get permission to a game or a company, please ask the admin to grant you permissions.";

	TutorialTexts.GRAPH_OPTIONS_1 = {};
	TutorialTexts.GRAPH_OPTIONS_1.title1 = "Welcome to Graph Options Tutorial";
	TutorialTexts.GRAPH_OPTIONS_1.text1 = "\"Graph Options\" help you view / export raw data, export graphs out of CURVE, and manage & delete user-generated graphs.";
	TutorialTexts.GRAPH_OPTIONS_1.text2 = "Let's go to the dashboards. <b><font color='#19BAEF'>Click now.</font></b>";
	
	TutorialTexts.GRAPH_OPTIONS_2 = {};
	TutorialTexts.GRAPH_OPTIONS_2.text0 = "Let's see the options we have for a graph";//<i class='fa fa-pencil'></i>
	TutorialTexts.GRAPH_OPTIONS_2.text1 = "Let's <b><font color='#19BAEF'>click</font></b> on the  <i class='fa fa-database dashboard_graph_icon'></i> icon to view raw graph data.";
	
	TutorialTexts.GRAPH_OPTIONS_3 = {};
	TutorialTexts.GRAPH_OPTIONS_3.text1 = "On this pop-up, you can view the data that feeds the current graph.";
	TutorialTexts.GRAPH_OPTIONS_3.text2 = "Click on the <i class='fa fa-download dashboard_graph_icon'></i> icon to download a CSV file of the raw data to your computer.";
	TutorialTexts.GRAPH_OPTIONS_3.text3 = "Now, let's <b><font color='#19BAEF'>click</font></b> on the <i class='fa fa-share-square-o  dashboard_graph_icon'></i> icon.";
	
	TutorialTexts.GRAPH_OPTIONS_4 = {};
	TutorialTexts.GRAPH_OPTIONS_4.text1 = "The <i class='fa fa-share-square-o  dashboard_graph_icon'></i> button generates a static link that includes the graph data. ";
	TutorialTexts.GRAPH_OPTIONS_4.text2 = "You can share this link with anyone. To view this link, you don't have to be a CURVE user or even logged in. This functionality makes CURVE a great tool for data sharing.";
	TutorialTexts.GRAPH_OPTIONS_4.text3 = "When creating Custom graphs, other options will appear.";
	TutorialTexts.GRAPH_OPTIONS_4.text4 = "The \"Delete\" option <i class='fa fa-trash dashboard_graph_icon'></i> will PERMANENTLY delete the graph from dashboard. This action is not reversible.";
	TutorialTexts.GRAPH_OPTIONS_4.text5 = "Change permissions  <i class='fa fa-users dashboard_graph_icon'></i> will allow the graph creator to add or revoke user permissions.";
	
	TutorialTexts.FILTER_DATA_1 = {};
	TutorialTexts.FILTER_DATA_1.title1 = "Welcome to Filter Data Tutorial";
	TutorialTexts.FILTER_DATA_1.text1 = "CURVE enables you to manage and filter your data. Because CURVE gathers data from multiple platforms, it's sometimes necessary to filter data by various parameters. In this tutorial, you will learn about CURVE's filtering options.";
	TutorialTexts.FILTER_DATA_1.text2 = "Let's go to the dashboards. <b><font color='#19BAEF'>Click now.</font></b>";
	
	TutorialTexts.FILTER_DATA_2 = {};
	TutorialTexts.FILTER_DATA_2.text1 = "This is the filtering panel. It changes according to the dashboard. Most dashboards have this filtering panel. In the \"Single User\" dashboard, for example, you have a search panel."
	TutorialTexts.FILTER_DATA_2.text2 = "The first filtering option is filtering by date. <b><font color='#19BAEF'>Click</font></b> on it";

	
	TutorialTexts.FILTER_DATA_3 = {};
	TutorialTexts.FILTER_DATA_3.text1 = "You can use any of the given presets such as last 30 days, last 7 days, etc. You can also use \"Custom Range\" to select a specific date range.";
	TutorialTexts.FILTER_DATA_3.text2 = "You can also filter your data by platform. To do so, <b><font color='#19BAEF'>click</font></b> on the <i class='fa fa-desktop'></i> icon.";
	
	
	
	TutorialTexts.FILTER_DATA_4 = {};
	TutorialTexts.FILTER_DATA_4.text1 = "Now, <b><font color='#19BAEF'>select</font></b> one or more platforms and <b><font color='#19BAEF'>click</font></b> \"Apply & Refresh\".";
	
	
	
	TutorialTexts.FILTER_DATA_5 = {};
	TutorialTexts.FILTER_DATA_5.text1 = "Your data is now being refreshed.";
	TutorialTexts.FILTER_DATA_5.text2 = "You can also filter your data by geo-location. To do so, <b><font color='#19BAEF'>click</font></b> on the <i class='fa fa-globe'></i> icon.";
	
	TutorialTexts.FILTER_DATA_6 = {};
	TutorialTexts.FILTER_DATA_6.text1 = "Now, <b><font color='#19BAEF'>select</font></b> one or more countries from the dropdown list and <b><font color='#19BAEF'>click</font></b> \"Apply & Refresh\".";
	
	TutorialTexts.FILTER_DATA_7 = {};
	TutorialTexts.FILTER_DATA_7.text1 = "Your data is now being refreshed.";
	TutorialTexts.FILTER_DATA_7.text2 = "If you need to refresh your data manually, click on the <i class='fa fa-refresh'></i> icon.";
	
	TutorialTexts.FIND_TOKEN_1 = {};
	TutorialTexts.FIND_TOKEN_1.title1 = "How to find my app Token";
	TutorialTexts.FIND_TOKEN_1.text1 = "In this tutorial we will learn how to find my app Token.";
	
	TutorialTexts.FIND_TOKEN_2 = {};
	TutorialTexts.FIND_TOKEN_2.title1 = "";
	TutorialTexts.FIND_TOKEN_2.text1 = "<b><font color='#19BAEF'>Click</font></b> on the \"Settings\" icon";
	
	TutorialTexts.FIND_TOKEN_3 = {};
	TutorialTexts.FIND_TOKEN_3.title1 = "App Token";
	TutorialTexts.FIND_TOKEN_3.text1 = "This is your Report Token. For more information regarding how to use it in your application, please click ";
	TutorialTexts.FIND_TOKEN_3.text1l = "http://www.curve.tech/getting-started/";
	TutorialTexts.FIND_TOKEN_3.text2l = "<u>here</u>";
	
}

}	
*/