function ApplicationsFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter){

var urlParam = $location.search();

$scope.applicationsStart = function(){
	
	urlParam = $location.search();
	$rootScope.appsArr = [];
	$scope.curveAPI('GetUserCompanies', null, $scope.getGamesCallback);
}

var newApplicationDomain = 
{
};



function show_trial_text()
{
	var first = isFirstLogin();
	
	if(first)
	{
		var trial = $scope.loadFromCache("trial");
		if(trial == true)
		{
			var expiration = $scope.loadFromCache("expiration");
			var now = new Date().getTime();
			var left = expiration - now;
			
			if(left > 0)
			{
				left = left/1000; // convert to seconds
				left = left/60; // convert to min
				left = left/60; // convert to hours
				left = Math.round(left/24); // convert to days
				show_custom("Just reminding you that this is the trial version of Curve.", "Your trial will end in " + left + " days.", "Welcome Back!", 'green');
			}
		}
		
	}
}

$scope.getGamesCallback = function(data)
{ 	
	var domain = $location.absUrl();
	$scope.DefaultAppId = data.DefaultAppId;
	$.each(data.Customers, function(i, value){
			
			if(value.Games.length > 0){
				
				$.each(value.Games, function(j, app){
					if(app.ID == data.DefaultAppId){
						$rootScope.appsArr.unshift(app);
					}else{
						$rootScope.appsArr.push(app);
					}	
				});
			}
		});
		
		var demoFlag = false;
		var demo = 'Demo';
		
		

		if (urlParam.Token != null)
		{
			
			var token = urlParam.Token;
			var curApp = $filter('filter')($rootScope.appsArr,{Token: token})[0];
			var activeAppImg = curApp.Icon;
			var activeAppLabel = curApp.GameName;
			var HasWritePermission = curApp.HasWritePermission;
			
			if(curApp.Logo){
					$scope.cssApp = {"logo" : curApp.Logo,"bgColor" : curApp.BgColor,"bgImg" : curApp.BgImage};
			}else{
					$scope.cssApp = {"logo" : "Images/cssApp/logoDefault.svg","bgColor" : "#41B7E9;","bgImg" : "Images/cssApp/bgDefault.png"};		
			}
				
		}else{
			
			if($rootScope.appsArr.length > 0 && $rootScope.appsArr[0].Token){
				var token = $rootScope.appsArr[0].Token;
				var activeAppImg = $rootScope.appsArr[0].Icon;
				var activeAppLabel = $rootScope.appsArr[0].GameName;
				var HasWritePermission = $rootScope.appsArr[0].HasWritePermission;
				
				if($rootScope.appsArr[0].logo){
					$scope.cssApp = {"logo" : $rootScope.appsArr[0].Logo,"bgColor" : $rootScope.appsArr[0].BgColor,"bgImage" : $rootScope.appsArr[0].BgImage};	
					
				}else{
					$scope.cssApp = {"logo" : "Images/cssApp/logoDefault.svg","bgColor" : "#41B7E9;","bgImg" : "Images/cssApp/bgDefault.png"};		
				}
				
			}else{
				
				alert('Access expired, please contact to your system administrator');
				window.location = '/#/';
				//$('.loading-page').hide();
			}
			
			
		}
if($rootScope.appsArr.length > 0 && $rootScope.appsArr[0].Token){
	if(!demoFlag){
			
			var demoObj = {
				'divId' : TConst.TUTORIAL_DEMO_APP,
				'settingsId' : 'settings_' + TConst.TUTORIAL_DEMO_APP,
				'dashboardId' : 'dashboard_' + TConst.TUTORIAL_DEMO_APP,
				'enableId' : 'enable_' + TConst.TUTORIAL_DEMO_APP
			}
			$rootScope.appsArr[0][demo] = demoObj;	
			
			
		
			
		$scope.iconClicked(activeAppLabel, token ,activeAppImg, HasWritePermission);
	}else{
		getNoAppsDiv();
	}
	
	var companies = data.Customers;
	newApplicationDomain.companies = companies;
	var ln = companies.length;
	
	if(ln > 0){
		companies = companies.sort(function(a, b) {return a.GameName > b.GameName ? 1 :  -1});	
	}
	
	var allGames = [];
	var comp = companies[0];
	if (comp.CompanyName == "GingeeGames" && companies.length > 1)
		comp = companies[1];
	var companyName = comp.CompanyName;
	
	$scope.comNameTitle = comp.CompanyName;
	var companyIcon = comp.ImageURL;
	var companyID = comp.ID;
	
	for(var i = 0 ; i < ln ; i++)
	{
		comp = companies[i];
		allGames = allGames.concat(comp.Games);

		if (comp.CompanyName == "GingeeGames" && $scope.loadFromStorage('email') == "support@gingeegames.com")
		{
			companyName = comp.CompanyName;
			companyIcon = comp.ImageURL;
			companyID = comp.ID;
		}
	}
	
	drawCompany(companyName, companyIcon, allGames, companyID);
	
	
	setCompanyIcon(companyID);
	$scope.setCurrentCustomerID(companyID);
	$(".application_game_icon_title").empty();
	$(".application_game_icon_title").append(companyName);
	
		
		$scope.initTutorial();
		
		if($scope.isGlobalFirstLogin())
			gotoTutorial("FTUE");
}

$('.loading-page').hide();
}

$scope.modalDefaultApp = function(id,name){
	$('#setDefaultApp').modal('show');
	$scope.newDefAppID = id;
	$scope.newDefAppName = name;
};

$scope.setDefaultAppFun = function(){
	$('#setDefaultApp').modal('hide');
	$('.loading-page').show();
	var temp = {"GameId": $scope.newDefAppID };
	$scope.curveAPI('SetDefaultGameId',{'Data' : JSON.stringify(temp)}, setDefaultAppCallback99);
	$scope.applicationsStart();
};

function setDefaultAppCallback99(){
	 
};

function newCompany(companyName)
{
	$(".loading").show();
	$('.upload_form').hide();

	if(companyName == null || companyName == "")
	{
		$(".loading").hide();
		$('.upload_form').show();
		show_error('Something went wrong', 'Company name cant be empty!');
		return;
	}

	$scope.curvePostAPI('RegisterCompany', {CompanyName: companyName}, onCompanyRegistered, onCompanyRegFail);

	function onCompanyRegistered(data) { location.reload(); }

	function onCompanyRegFail(text)
	{
		$('.error_text').empty();
		$('.error_text').append('Failed :' + text);
		$(".loading").hide();
		$('.upload_form').show();
	}	
}

$scope.goToDashboard = function(AppName,Token)
{
	saveCurrentAppToLocalStorage(AppName);
	
	if(urlParam.settings != null){
					
			$scope.routerPage('dashboard','settings',urlParam.settings,Token);
	
		}else{
			$scope.routerPage('dashboard',null,null,Token);
		}
	
}

	$scope.routerPage = function(url,key,val,Token){
		
		if(key && val){
			window.location = '/#/';
			window.location = '/#/'+ url +'?Token=' + Token + '&' + key + '=' + val;
					
		}else{
			
			window.location = '/#/';
			window.location = '/#/dashboard?Token=' + Token;
			
		}
		
	}



function saveCurrentAppToLocalStorage(AppName)
{
	$scope.storeToCache('loadedGameName', AppName);
	$scope.storeToCache('companyNameByGameName', $scope.companyNameByGameName);
	$scope.storeToCache('gameIconByGameName', $scope.gameIconByGameName);
}

function getNoAppsDiv()
{
	var ans = '<div style="text-align:center; font-size: 25px;  margin-top: 50px; color: rgb(204, 204, 204);">No applications or companies currently registered. </br> please go to <a href="inbox.html">inbox</a> to approve your permissions!</div>';
	ans += '<div style="text-align: center;"><i style="font-size: 35px; margin-top: 30px; margin-right: 10px; color: rgb(204, 204, 204);" class="fa fa-share"></i><i style="font-size: 55px; margin-top: 30px; color: rgb(204, 204, 204);" class="fa fa-inbox"></i></div>'
	return ans;
}

function setCompanyIcon(customer_id)
{
	$('input#CustomerId').val(customer_id);
}

function uploadCompanyImage() 
{
	var bar = $('.bar');
	var percent = $('.percent');
	var status = $('#comp_status');
	var customer_id = $('input#CustomerId').val();
	
	/*$('form.uploadCompanyIcon').ajaxForm(
	{
	    beforeSend: function()
	    {
	        status.empty();
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
	    uploadProgress: function(event, position, total, percentComplete)
	    {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	        $('.uploadCompanyIcon').hide();
			$('.company_icon.loading').show();
	    },
	    success: function() 
	    {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
	    complete: function(xhr) 
	    {
	        status.html(xhr.responseText);
	        var response = $.parseJSON(xhr.responseText);
			
			if(response == null)
			{
				var percentVal = '0%';
				bar.width(percentVal)
				percent.html(percentVal);
	        	$('.uploadCompanyIcon').show();
				$('.company_icon.loading').hide();
				status.html("Error, did you select an image?");
				return;
			}
			
	        if(response.status == "success") 
	        {
	        	var url = "http://prod.curve.tech/includes" + response.path;
				customer_id = $('input#CustomerId').val();
	        	updateCompanyIcon(url, customer_id);
	        	status.html("Updating server.");
				
	        }
	        else 
	        {
	        	status.html("Error:" + response.message);
	        	$('.uploadCompanyIcon').show();
				$('.company_icon.loading').hide();
	        }
	    }
	});*/
}

function updateCompanyIcon(iconUrl, customer_id) 
{
	function setIconCallback()
	{
		$('#comp_status').html("Successfully uploaded.");
        location.reload();
    }
	
	function failed()
	{
		$('.loading-page').hide();
		show_error('Failed', 'Somthing went wrong');
    }
	
	$scope.curvePostAPI('SetCompanyIcon', {"CustomerID": customer_id, "Icon": iconUrl}, setIconCallback, failed);
}

$scope.companyNameByGameName = {};
$scope.gameIconByGameName = {};

var is_demo_app_first = true;


function drawCompany(companyname, companyIcon, games, customerID)
{	
	var disAppList = '';
	var numDisabled = 0;
	
	games = games.sort(function(a, b) {return a.GameName > b.GameName ? 1 :  -1});
	
	var companiesByCustomerID = $scope.loadFromStorage('customerCompanies');
	
	if(companiesByCustomerID == null)
		companiesByCustomerID = {};

	companiesByCustomerID[customerID] = {CompanyName:companyname, CompanyIcon:companyIcon};

	var gamesByCustomerID = $scope.loadFromStorage('customerGames');

	if(gamesByCustomerID == null)
		gamesByCustomerID = {};

	gamesByCustomerID[customerID] = games;
	
	$.each(games, function(i, game)
	{
		if(game.GameName != null)
		{
			if(game.Token != null && $scope.GameModeManager != null)
				$scope.GameModeManager.setGameMode(game.Token, game.Mode);
			
			if(game.Enabled)
			{  
				$scope.companyNameByGameName[game.GameName] = companyname;
				$scope.gameIconByGameName[game.GameName] = game.Icon;
				
			}
			else
			{
				$scope.companyNameByGameName[game.GameName] = companyname;
				$scope.gameIconByGameName[game.GameName] = game.Icon;
				numDisabled++;
				
			}
		}
		
	});

	$scope.CCid = customerID;

}

$scope.setCurrentCustomerID = function(currentCustomerID)
{
	
	$scope.storeToCache('currentCustomerID', currentCustomerID);
}

$scope.appStatus = function(CustomerID, GameID, Status)
{
	$scope.curveAPI('EnableGame', {CustomerID: CustomerID, GameID: GameID, Enable: Status}, onCompleteEnableGame);	
	function onCompleteEnableGame() { location.reload(); }
}


$scope.capAppName = function(name)
{	
	if(name.length > 16)
		return name.substr(0, 15)+'..';
	
	return name;
}



$scope.iconClicked = function(AppName, Token, Icon, HasWritePermission)
{	
	$('.loading-page').show();
	$scope.cssApp = {};
	
	var curApp = $filter('filter')($rootScope.appsArr,{Token: Token})[0];	
			
	if(curApp.logo){
		$scope.cssApp = {"logo" : curApp.Logo,"bgColor" : curApp.BgColor,"bgImage" : curApp.BgImage};
	}else{
		$scope.cssApp = {"logo" : "Images/cssApp/logoDefault.svg","bgColor" : "#41B7E9;","bgImg" : "Images/cssApp/bgDefault.png"};		
	}
	
	$scope.parentAdvanced = false;
	$scope.activeAppImg = {'icon':''};
	$scope.activeAppImg.icon = Icon;
	$scope.activeAppLabel = AppName;
	$scope.tutorialInvokeEvent("newGraphEnterApp");
	$scope.goToDashboard(AppName,Token);
	$scope.HasWritePermission = HasWritePermission;
		
}

var is_new_app_first = true;
function drawAddAppDiv(currentCustomerID)
{
	var id = !is_new_app_first ? "" : TConst.TUTORIAL_NEW_APP_BTN;
	is_new_app_first = false;
	
	if (currentCustomerID == '540deeb0-b833-4eef-ad22-b418378241df')
	{
		id = TConst.TUTORIAL_NEW_APP_BTN_CURVE;
	}
	
	var onClickFunc = ' onClick="setCurrentCustomerID(\'' + currentCustomerID + '\'); showNewApplication();" ';
	var resp = "";
	if (id == "")
		resp = '<div class="single_app new_app">';
	else
		resp = '<div class="single_app new_app"'+'id="'+id+'">';
		resp +='<a href="#" ' + onClickFunc + '><i class="fa fa-pencil"></i> <br /><div class="text">New App</div></a>';
		resp += '</div>';

	return resp;
}

// ****************** NEW GAME ***************************************************

$scope.createNewApplication = function()
{	
	var gn = $('input#GameName').val();
	$scope.newAppname = gn;
	if(/^[a-zA-Z0-9_ ]+$/.test(gn))
		newApp(gn);
	else
	{
		$(".new_app.loading").show();
		$('.new_app.upload_form').hide();
		alert("You can use only alphabetical or numerical characters and _.");
		$(".new_app.loading").hide();
		$('.new_app.upload_form').show();
	}
}

$scope.showNewApplication = function()
{
	$('#myModalNewApp').modal('show');
	$(".new_app.loading").hide();
	$('.new_app.upload_form').show();
	$('#please_wait_newapp').hide();
	$('#instruct_newapp').show();
	$('input#GameName').val('');
	setTimeout(function() {$('input#GameName').focus()}, 1000);
	$('.page-container').addClass("blur_content");
	$scope.tutorialInvokeEvent("newAppPopup");
	
	var allCompanies = newApplicationDomain.companies;
	allCompanies = allCompanies.sort(function(a, b) {return a.CompanyName > b.CompanyName ? 1 :  -1});
	
	var actions_code = '';
	$scope.arrCompanies = allCompanies;
	
	$('#myModalNewApp').on('hidden.bs.modal', function () {
		$('.page-container').removeClass("blur_content");
	});
}

$scope.newAppCompanySelected = function(name, currentCustomerID)
{
	$scope.setCurrentCustomerID(currentCustomerID);
	$scope.comNameTitle = name;
	
}

function newApp(GameName)
{
	$(".new_app.loading").show();
	$('.new_app.upload_form').hide();
	$('#please_wait_newapp').empty();
	$('#please_wait_newapp').append("Creating App, Please wait...");
	$('#please_wait_newapp').show();
	$('#instruct_newapp').hide();
	newApplicationDomain.GameName = GameName;
	var customID = $scope.loadFromStorage('currentCustomerID');

	if(GameName == null || GameName.length < 3)
	{
		$(".new_app.loading").hide();
		$('.new_app.upload_form').show();
		$('#please_wait_newapp').hide();
		$('#instruct_newapp').show();
		$scope.show_error('You must specify an application name', 'Application name must be 3 or more chars long. (alphabetical or numeric)', true);
		return;
	}
	
	if(customID == null)
	{
		$(".new_app.loading").hide();
		$('.new_app.upload_form').show();
		$('#please_wait_newapp').hide();
		$('#instruct_newapp').show();
		$scope.show_error('Something went wrong', 'Trying to add an app to unknown company.', true);
		return;
	}

	$scope.curvePostAPI('AddGame', {CustomerID: customID, GameName: GameName}, onGameAdded, onAddGameFail);
	$scope.tutorialInvokeEvent("newAppPopupApply");

	
}

function onGameAdded(data) 
{
	newApplicationDomain.Token = data.Token;
	var gamesByCustomerID = $scope.loadFromStorage('customerGames');
	if(gamesByCustomerID == null)
		gamesByCustomerID = {};
	data.GameName = newApplicationDomain.GameName;
	var curCustomerID = $scope.loadFromStorage('currentCustomerID');

	$scope.storeToCache('customerGames', gamesByCustomerID);
	$('.curve_app_report_token').empty();
	$('.curve_app_report_token').append(data.Token);
	$('#myModalNewApp').modal('hide');
	$('#myModalNewApp2').modal('show');
	$scope.tutorialInvokeEvent("ftue8Start8");
}

function onAddGameFail(text) 
{
	
	$(".new_app.loading").hide();
	$('#please_wait_newapp').empty();
	$('#please_wait_newapp').append("Sorry, " + $('#company_name_title').text() + ", " + text);
}

$scope.downloadSDK = function(type)
{
	if (type == "iOS")
		location.href = URLConst.CurveSDKConstants.IOS_SDK_DOWNLOAD_URL;
	if (type == "tvOS")
		location.href = URLConst.CurveSDKConstants.TVOS_SDK_DOWNLOAD_URL;
	if (type == "Android")
		location.href = URLConst.CurveSDKConstants.ANDROID_SDK_DOWNLOAD_URL;
}

$scope.SDKinstructions = function(type)
{	
	var userAppsArr = [];
	
	$.each($rootScope.appsArr, function (key, val){
		var temp = {'name':val.GameName,'token':val.Token};
		userAppsArr.push(temp);
	});
	
	if(type != 'def'){
		var temp = {'name' : $scope.newAppname, 'token' : newApplicationDomain.Token};
		userAppsArr.unshift(temp) 
	}else{
		var temp = {'name' : $scope.activeAppLabel , 'token' : $rootScope.Token};
	}
	
	var appSrt = JSON.stringify(userAppsArr);	
	var url = "";
	
	if(type != 'def'){
		
		if (type == "iOS"){
			url = $rootScope.mainUrl + URLConst.CurveSDKConstants.IOS_SDK_INSTRUCTIONS_URL;
			
		}
		if (type == "tvOS")
			url = $rootScope.mainUrl + URLConst.CurveSDKConstants.TVOS_SDK_INSTRUCTIONS_URL;
		if (type == "Android")
			url = $rootScope.mainUrl + URLConst.CurveSDKConstants.ANDROID_SDK_INSTRUCTIONS_URL;
		if (type == "JS")
			url = $rootScope.mainUrl + URLConst.CurveSDKConstants.JS_SDK_INSTRUCTIONS_URL;
		if (type == "GTM")
			url = $rootScope.mainUrl + URLConst.CurveSDKConstants.GTM_SDK_INSTRUCTIONS_URL;
		
		url += "&AppToken=" + newApplicationDomain.Token + "&appsData=" + appSrt;
		
	}else{
		
		var url = $rootScope.mainUrl + URLConst.CurveSDKConstants.DEF + "&appsData=" + appSrt;
		
	}
	
	window.open(url, '_blank');
}

$scope.sendEmailToDeveloper = function(type)
{	
	$('#myModalNewApp2').modal('hide');
	var downloadLink = "";
	var settingsLink = $rootScope.protocol+"://curve.tech/settings.html?Token=" + newApplicationDomain.Token;
	var url = "";
	if (type == "iOS")
	{
		url = URLConst.CurveSDKConstants.IOS_SDK_INSTRUCTIONS_URL;
		downloadLink = URLConst.CurveSDKConstants.IOS_SDK_DOWNLOAD_URL;
	}
	if (type == "tvOS")
	{
		url = $rootScope.mainUrl + URLConst.CurveSDKConstants.TVOS_SDK_INSTRUCTIONS_URL;
		downloadLink = URLConst.CurveSDKConstants.TVOS_SDK_DOWNLOAD_URL;
	}
	if (type == "Android")
	{
		url = $rootScope.mainUrl + URLConst.CurveSDKConstants.ANDROID_SDK_INSTRUCTIONS_URL;
		downloadLink = URLConst.CurveSDKConstants.ANDROID_SDK_DOWNLOAD_URL;
	}
	if (type == "JS")
	{
		url = $rootScope.mainUrl + URLConst.CurveSDKConstants.JS_SDK_INSTRUCTIONS_URL;
	}
	if (type == "Google Tag Manager")
	{
		url = $rootScope.mainUrl + URLConst.CurveSDKConstants.GTM_SDK_INSTRUCTIONS_URL;
	}
	
	url += "&AppToken=" + newApplicationDomain.Token;
	
	$scope.SendEmailInit("Send " + type + " instructions to Developer", "Curve " + type + " Integration", getDevEmail(newApplicationDomain.GameName, newApplicationDomain.Token, type, downloadLink, url, settingsLink));
}

function getDevEmail(gamename, token, type, downloadLink, onboardingURL, settingsLink)
{
	if (gamename == undefined)
		gamename = '';
	else
		gamename = '**' + gamename + '**';
	
	if (token == undefined)
		token = '';
	else
		token = '**' + token + '**';
	
	var sb = "";
	sb += "<html><head></head><body><div>"; 
	sb += "<div>Hi,</div><br><br>"; 
	sb += "<div>Here is the data for integrating Curve with your application " + gamename + " : </div><br><br>"; 
	sb += "<div>Reporting token for your application is: <span style=\"font-weight:bold;\">" + token + " . </span></div><br><br>";
	sb += "<div>You can find the token any time in the application settings page: " + settingsLink + " . </div><br><br>";
	if(type != "JS" && type != "Google Tag Manager") 
		sb += "<div>You can download the " + type + " SDK from: " + downloadLink + " . </div><br><br>";
	sb += "<div>Instruction on using the SDK for " + type + " can be found here: " + onboardingURL + " . </div><br><br>";
	sb += "<div>If you have any questions or feedback, our experts are just a click or a call away.</div><br><br>";
	sb += "<div>All the best, <br><br>**Roi**.</div><br><br>";
	sb += "<div>*Curve - The Answers You Need To Increase Your Users LTV.*</div>";
	sb += "</div></body></html>";
	
	return sb;
}

}
// ****************** NEW GAME ***************************************************