// requires the following div at the end of the HTML (to stay on top):
/*
<div class="modal fade" id="delete-prompt" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"style="color:#aa0000; font-size:30px;">Warnning!</h4>
			</div>
			<div class="modal-body permission_box">
				<div class="portlet-body flip-scroll">
					<h3 id="delete-prompt-title"></h3>
					<p id="delete-prompt-remark"></p>
					<div class="modal-footer" style="padding: 20px 0px 0px 0px; margin-top:20px;">
						<button id="delete-prompt-cancel" class="btn default" data-dismiss="modal" aria-hidden="true">Cancel</button>
						<button id="delete-prompt-accept" data-dismiss="modal" class="btn red">Delete</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
*/
function EmailValidFunction($scope,$rootScope,$http,$location){

	var onAccept = null;
	var onCancel = null;
	var id = -1;
	
$scope.EmailValidatorManager = function(id_, onAccept_, onCancel_, title, remark, acceptTxt, cancelTxt)
{
	// console.log("EmailValidatorManager", onAccept_, onCancel_);
	
	onAccept = onAccept_;
	onCancel = onCancel_;
	id = id_;
	
	if(title != null) $("#delete-prompt-title").html(title);
	if(remark != null) $("#delete-prompt-remark").html(title);
	if(cancelTxt != null) $("#delete-prompt-cancel").html(cancelTxt);
	if(acceptTxt != null) $("#delete-prompt-accept").html(acceptTxt);
		
	$("#delete-prompt-accept")[0].onclick =  acceptClicked;
	$("#delete-prompt-cancel")[0].onclick = cancelClicked;
	
	promptDelete();
}
	
	function acceptClicked()
	{
		// console.log("acceptClicked", onAccept, id);
		if(onAccept != null) 
			onAccept(id);
	};
	
	function cancelClicked()
	{
		// console.log("cancelClicked", onCancel);
		if(onCancel != null) 
			onCancel();
	};
	
	function promptDelete()
	{
		$("#delete-prompt").modal('show');
	};

}