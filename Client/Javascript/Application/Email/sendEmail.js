function SendEmailFunction($scope,$rootScope,$http,$location){

	 $scope.sendEmailSendEmail = function()
	{
		
		var data = validateAndPrepareData(true);
		
		if(data.error != null)
		{
			$scope.show_error("Make sure all fields are properly filled", data.error);
			return;
		}
		
		if(data.warn != null)
		{
			$scope.EmailValidatorManager("", sendWithoutValidatingEmail, null, data.warn, "If not, please make sure to fill them properly.", "Yes, Send", "Cancel");
			return;
		}
		
		$scope.curveAPI("SendEmail", {Data:JSON.stringify(data)}, onSendEmail, onErrorSendEmail);
	}; 
	
	sendWithoutValidatingEmail = function(id)
	{
		var data = validateAndPrepareData(false);
		$scope.curveAPI("SendEmail", {Data:JSON.stringify(data)},  onSendEmail, onErrorSendEmail);
		$("#cohort_email_editor").modal('hide');
	};
	
	onSendEmail = function(data)
	{
		//console.log(data);
		$scope.show_message("Success","Mail was sent succesfully ", data);
		$("#send_email_modal").modal('hide');
	};
	
	onErrorSendEmail = function(data)
	{
		console.log("ERROR", data);
		$scope.show_error("There was an Error sending the mail.", data);
		$("#send_email_modal").modal('hide');
	};
	
	$scope.SendEmailInit = function(header, emailTitle, emailBody)
	{
		
		$("#send_email_editor_textarea").markdown({
			autofocus:false,
			savable:false,
			resize:"none",
		});
		
		$("#send_email_modal").modal('show');
		$('#send_email_title')[0].innerHTML = header;
		$('#email_editor_subject')[0].value = emailTitle;
		$('#send_email_editor_textarea')[0].innerHTML = $(emailBody)[0].innerText;
		
		// clear recepient field
		$("#recepient_email").val("");
	};
	
	validateAndPrepareData = function(validate)

	{
		var emailSubject = $("#email_editor_subject").val();
		var emailContent = $(".md-preview").html();
		var emailRec = $("#recepient_email").val();
		var emailSender = $("#sender_email").val();
		
		if (emailContent == undefined)
		{
			$("#send_email_editor_textarea").data('markdown').showPreview();
			emailContent = $(".md-preview").html();
			$("#send_email_editor_textarea").data('markdown').hidePreview();
		}
		
		if(emailRec == null || emailRec == '')
		{
			return {error: "No recepient E-mail, Please enter one valid E-mail in the recepient field."};
		}
		
		if(emailSender == null || emailSender == '')
		{
			return {error: "No sender E-mail, Please enter one valid E-mail in the from field."};
		}
		
		if(validate)
		{
			if(emailSubject == null || emailSubject == "")
				return {warn: "Are you sure you want to send this email without a subject?"};
			
			if(emailContent == null || emailContent == "")
				return {warn: "Are you sure you want to send this email without a body?"};
		}
		
		return {"Title":emailSubject, "Text": emailContent, "Email": emailRec, "From": emailSender};
	};

}