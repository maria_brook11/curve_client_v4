app.controller('ExternCtrl', function ($scope, $rootScope, $http, $location, TConst, URLConst, $filter, CountryConst, InsightConst, $timeout, $interval,saveCSVService)
{
	$('.loading-page').hide();

	$scope.extUrlParam = {};
	var urlParameters = $location.search();

	$scope.extUrlParam.title = urlParameters.t;
	$scope.extUrlParam.desc = urlParameters.d;
	$scope.extUrlParam.appName = urlParameters.gn;
	$scope.extUrlParam.appImgSrc = urlParameters.icon;
	$scope.extUrlParam.graphtype = urlParameters.graphtype;
	$scope.extUrlParam.fromDate = urlParameters.dateFrom;
	$scope.extUrlParam.toDate = urlParameters.dateTo;
	var array = urlParameters.columnNames.split(",")
	$scope.extUrlParam.columnNames = array;
	var temp = urlParameters.opt;
	var tempOpt = temp.replace(/hash/g , "#");
	if(tempOpt){
		$scope.extUrlParam.options = JSON.parse(tempOpt);
	}
	var chartData = urlParameters.data;
	$scope.extUrlParam.graphData = JSON.parse(chartData);
	$scope.exportToMail = function(){
		var urlParameters = $location.url();
		$scope.url = $rootScope.protocol+"://curve.alpha.curve.tech/#";
		$scope.url += urlParameters;
		$('#export_modal').modal('show');
	}
	
	function getQueryVariable(variable)
	{
		var query = window.location.search.substring(1);
		var vars = query.split("&");
		for (var i = 0; i < vars.length; i++)
		{
			var pair = vars[i].split("=");
			if (pair[0] == variable)
			{
				return decodeURIComponent(pair[1]);
			}
		}

		return (false);
	}

	function extOptionMaker(opt, GraphType)
	{
		var options;

		if (options == null)
			options = getGraphOptions(GraphType);

		addExtraFormatting(options, GraphType);

		return options;
	}

	
$scope.parseGraphData = function(d,GraphType,ColumnNames)
{
	if(d == null || d.length == 0)
		return [];
	
	var dataTable = d;
	var graphType = GraphType;
	
	if(graphType.toLowerCase() == "funnel")
		return dataTable;
	
	if(graphType == "userjourney")
		return dataTable;
	
	var ln = ColumnNames.length;
	var resp = [];
	
	if(graphType.toLowerCase() == "sparkline")
	{
		var sparklinedata = [];
		for (var i=0; i< dataTable.length; i++)
			sparklinedata.push(dataTable[i][0]);
		
		return sparklinedata;
	}
	
	if(graphType.toLowerCase() == "countergroup")
	{
		var countergroupdata = [];
		for (var i=0; i< dataTable.length; i++)
			countergroupdata.push(dataTable[i][0]);

		return countergroupdata;
	}
	
	if (ln == 1)
	{	
		var seriesEmpty = {};
		seriesEmpty.label =  ColumnNames[0];
		seriesEmpty.data = [];
		resp.push(seriesEmpty);
		return resp;
	}
	for (var i=1; i<ln; i++)
	{ 
		var currLn = dataTable.length;
		
		var series = {};
		series.label =  ColumnNames[i];
		series.data = [];
		
		for (var j=0; j<currLn; j++)
		{
			var currData = dataTable[j];
			series.data.push([currData[0], currData[i]]);
		}

		resp.push(series);
	}

	return resp;
}



$scope.parsePieData = function(d)
{
	if(d == null ||  d.length == 0)
	{
		return [];
	}

	
	var resp = [];
	
	var ln = d.length;
	
	for (var j=0; j<ln; j++)
	{
		var currData = d[j];
		resp.push({label:currData[0], data:currData[1]});
	}
	
	return resp;
}


$scope.timeConverter = function(UNIX_timestamp)
{
	if(UNIX_timestamp <= 24)
		return UNIX_timestamp + ":00";
	
	var a = new Date(UNIX_timestamp);
	var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	var year = a.getFullYear();
	var month = months[a.getMonth()];
	var date = a.getDate();
	var time =  date + " " + month + "" + year;
	return time;
 }	



var maxPiechartObjects = 10;

$scope.truncatePiechartData = function(data)
{	
	lastColor = 0;
	if (data == null)
		return [];
	
	var ln = data.length;
	var firstColor;
	var lastColor;
	for(var j = 0 ; j < ln ; j++)
	{
		data[j].color = getColor();
		if (j == 0)
			firstColor = data[j].color;
				
		if (j == ln -1)
			lastColor = data[j].color;
	}
	
	if(ln <= maxPiechartObjects)
	{
		for(var i = 0 ; i < ln ; i++)
		{
			data[i].label = PieUtils.truncateLabel(data[i].label);
		}
		
		return data;
	}

	
	var resp = [];
	var othersObject = {label: 'others', data: 0, color:"#80c2d8"};
	for(var i = 0 ; i < ln ; i++)
	{
		if(i < maxPiechartObjects)
		{ 
			data[i].label = PieUtils.truncateLabel(data[i].label);
			resp[i] = data[i];
		}
		else
			othersObject.data = Number(othersObject.data) + Number(data[i].data);
	}

	
	resp.push(othersObject);

	return resp;
}

var colors = ["#06789e", "#50a0bb", "#19baef", "#5ecff4", "#afc9e1", "#c7d9ea", "#afe1c1", "#c9ebd5", "#88ce5c", "#abdd8d", "#d1e05c","#dfe98d"];

var lastColor = 0;
var PieUtils = {};
PieUtils.truncateLabel = function(txt)
{
	if(txt == null)
		return txt;
	
	if(txt.length > 20)
		return txt.substr(0, 20) + "...";
	
	return txt;
}

function getColor()
{
		lastColor++;
		if(lastColor == colors.length) 
		{
			lastColor = 0;
		}
		return colors[lastColor];
}
 
function compare(a, b) 
{
  if (a.data < b.data) {
    return 1;
  }
  if (a.data > b.data) {
    return -1;
  }
  
  return 0;
}



	$scope.parseMultilineData = function (data)
	{
		if (data == null || data.length == 0)
			return [];

		var dataByDate = {};
		var orderableData = [];
		var columns = [];
		var curData = data[0].data;
		var curLabel = data[0].label;
		var valData = data[1].data;
		for (var i = 0; i < curData.length; i++)
		{
			var date = curData[i][0];
			var value = curData[i][1];
			var valueLabel = valData[i][1];

			if (dataByDate[date] == null)
				dataByDate[date] = {};

			dataByDate[date][value] = valueLabel;

			if (columns.indexOf(value) == -1)
				columns.push(value);
		}

		for (k in dataByDate)
		{
			var fulldata =
			{
				date: k
			};
			var dataitem = dataByDate[k];

			for (var i = 0; i < columns.length; i++)
			{
				var cur = dataitem[columns[i]];

				if (cur == null)
					dataitem[columns[i]] = "0";
			}

			fulldata.data = dataitem;
			orderableData.push(fulldata);
		}

		function order(a, b)
		{
			if (parseInt(a.date) > parseInt(b.date))
				return 1;
			return -1;
		}

		orderableData.sort(order);
		return $scope.CurveDataParserRefactor(orderableData, columns);
	}

	$scope.CurveDataParserRefactor = function (d, c)
	{
		var colObject = [];

		for (var i = 0; i < c.length; i++)
			colObject[i] =
			{
				label: c[i],
				data: []
			};

		for (var i = 0; i < d.length; i++)
		{
			for (var j = 0; j < c.length; j++)
			{
				colObject[j].data.push([d[i].date, d[i].data[c[j]]]);
			}
		}

		return colObject;
	}


$scope.fillTable = function(el, d)
{
	var labels = [];
	var columns = [];
	
	for(var i = 0 ; i < d.length ; i++)
	{
		var col = d[i];
		
		if(col == null)
			continue;
		
		labels.push( col.label );
		var data = col.data;
		columns[i] = [];
		for(var j = 0 ; j < data.length ; j++)
		{
			columns[i].push(data[j]);
		}
	}

	// el.empty();
	// el.append($scope.getTable("title", "desc", labels, columns));
	var html = $scope.getTable("title", "desc", labels, columns);
	return html;
}

$scope.getTable = function(title, desc, labels, columns)
{
	var resp = '';
	resp += '<div class="no-footer" id="sample_1_wrapper"><div class="row"><div class="col-md-12">'
	resp += '<table aria-describedby="sample_1_info" role="grid" class="table table-striped table-bordered table-hover" id="sample_1">';
	
	// HEADER
	resp += '<thead><tr role="row">';
	
	for(var i = 0 ; i < labels.length ; i++)
	{
		resp += $scope.getTitleCol(labels[i]);
	}
	resp += '</tr></thead>';
	
	// BODY
	resp += '<tbody>';
	
	if(columns != null && columns.length > 0)
		resp += $scope.getTableRow(columns);
	
	resp += '</tbody>';
	resp += '</table></div>';

	return resp;
}

$scope.getTableRow = function(columns)
{
	var resp = '';
	
	var tableheight = columns[0].length;
	for(var j = 0 ; j < tableheight ; j++)
	{
		var oddeven = j%2 == 0 ? 'even' : 'odd';
		resp += '<tr class="' + oddeven + '" role="row">';
		for(var i = 0 ; i < columns.length ; i++)
		{
			var row = columns[i];
			try {
				resp += '<td>' + $scope.createTableRowTruncatedData(decodeURIComponent(row[j][1])) + '</td>';
			}
			catch(err) {
				resp += '<td>' + row[j][1] + '</td>';
			}
		}
	}
	
	resp += '</tr>';
	return resp;
}

$scope.createTableRowTruncatedData = function(txt)
{
	var trunc = txt;
	if(trunc.length > 30)
		trunc = trunc.substr(0, 30) + "..";
	
	return '<span title="'+txt+'">' + trunc + '</span>';
}


$scope.getTitleRowEl = function(name)
{
	return '<td>'+name+'</td>';
}

$scope.getTitleCol = function(name)
{
	return '<th aria-label="' + name + '" activate to sort column ascending" aria-sort="ascending"  colspan="1" rowspan="1" aria-controls="sample_1" tabindex="0" class="sorting_asc">'+name+'</th>';
}


$scope.formalizeTableData = function(d)
{
	var labels = [];
	var columns = [];
	for(var i = 0 ; i < d.length ; i++)
	{
		var col = d[i];
		labels.push( col.label );
		var data = col.data;
		columns[i] = [];
		for(var j = 0 ; j < data.length ; j++)
		{
			columns[i].push(data[j]);
		}
	}
	
	return {labels:labels, data:columns};
}

$scope.showAlertCopy = function(){
	alert('Link has been copied to clipboard');
}

})
