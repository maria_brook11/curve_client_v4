
app.directive('externchart',
	function ($compile) {
		return {
			restrict: 'A',
			replace:true,
			scope:true,
			link: function (scope, elem, attrs) {
				
			//console.log(scope.extUrlParam)	

			var curData = scope.extUrlParam.graphData;
			var options = scope.extUrlParam.options;
			var currBarInd = 0;	
			//console.log(options) 

		
				
	if(curData.length > 0){			
				
	if(scope.extUrlParam.graphtype == "PieChart"){

		if(options.series.pie.combine){
			options.series.pie.combine = { color: '#999', threshold: 0};
		}else{
			var combine = 'combine';
			options.series.pie.combine = { color: '#999', threshold: 0};
		}
		options.series.pie.label.formatter =
			function(label, series) 
			{
				return '<div style="font-size:8pt;text-align:center;padding:2px;color:black;">' + label + '<br/>' + series.data[0][1] + '</div>';
			}
		
		//var temp = scope.parsePieData(curData);
		//console.log(temp)
		var graphData = scope.truncatePiechartData(curData);
		//console.log(graphData)
	}else{
		
		if (curData != null && curData.length > 0 && options.bars){
				options.colors = [getNextColorOriginalBar()];
		}
		else{
				options.colors = ["#06789e","#88ce5c","#d1e05c","#afe1c1","#b4d6e2"];
		}
			


		var graphData = null;
		
		if(options.parseAsMultiline != null && (options.parseAsMultiline == "true" || options.parseAsMultiline == true))
		{	
			graphData = scope.parseGraphData(curData,scope.extUrlParam.graphtype,scope.extUrlParam.columnNames);
			graphData = scope.parseMultilineData(graphData);
		}
		else
			graphData = scope.parseGraphData(curData,scope.extUrlParam.graphtype,scope.extUrlParam.columnNames);
	}

	
	function getNextColorOriginalBar()
			{	
				var colors = ["#afc9e1", "#afe1c1","#abdd8d", "#50a0bb"];
				currBarInd++;
				if (currBarInd >= colors.length)
					currBarInd = 0;
				return colors[currBarInd];
				
			} 
			
			elem.bind("plothover", function (event, pos, item) {  
                
				var previousPoint = null;

				if(pos != null && pos.x != null)
				{
					$("#x").text(pos.x.toFixed(2));
					$("#y").text(pos.y.toFixed(2));
					angular.element('.chartTooltip').remove();
					if (item) {
						if (previousPoint != item.dataIndex) {
							previousPoint = item.dataIndex;
							
							var x = scope.timeConverter(item.datapoint[0]),
								y = item.datapoint[1].toFixed(1);
							var contents = "(" + x + " , " + y + ")";
							var screenWidth = $(window).width()/2;
							var xPos = item.pageX + 15;
							if(item.pageX > screenWidth)
								xPos = item.pageX - 100;
							var yPos = item.pageY + 5;
							var ngEl = '<div class="chartTooltip" style="position: absolute; top: '+yPos+'px; left: '+xPos+'px; border: 1px solid rgb(51, 51, 51); padding: 4px; color: rgb(255, 255, 255); border-radius: 3px; opacity: 0.8; background-color: rgb(51, 51, 51);">' + contents + '</div>';
							angular.element(document.body).append(ngEl);
						}
					} else {
						angular.element('.chartTooltip').remove();
						previousPoint = null;
					}
				}
				
				
				
				
               
            });
			
	$.plot(elem, graphData, options);
	}		
			}
			
			
		}
	}
);