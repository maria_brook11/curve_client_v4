
app.directive('exetsmalltable',
	function () {
		return {
			restrict: 'E',
			replace:true,
			scope:true,
			link: function (scope, element, attrs) {
			
			var curID = element.parent().attr("id");
			var curData = scope.extUrlParam.graphData;
			var options = {'colors' : ["#06789e","#88ce5c","#d1e05c","#afe1c1","#b4d6e2"]};	
			/* for (var i = 0; i <= scope.activeTab.dashboardActive.Graphs.length; i++) {
		
				if (scope.activeTab.dashboardActive.Graphs[i].ID == curID) {
					 
					curData = scope.activeTab.dashboardActive.Graphs[i].dataGraph;
					//console.log(curData)
					break;
				}
			} */
			
	var graphData = scope.parseGraphData(curData,scope.extUrlParam.graphtype,scope.extUrlParam.columnNames);
	var table = scope.fillTable(element, graphData, options);
	element.html(table);	
			
			
// function drawTable(type, name, description, globalItemID, ID, graphOwnerType, div_id)
// {
	// var gridColumnWidth = 'col-md-12';

	// if(type == "SmallTable") 
	 	// gridColumnWidth = 'col-md-6';

	// var graphID = '#chart_' + graphOwnerType + "_" + globalItemID + '_' + ID;
	// var containerID = 'container_' + graphOwnerType + "_" + globalItemID + '_' + ID;
	// var onClickDownload = 'onClick="saveCSV(\'' + graphID + '\', \'' + name + ' - ' + description + '\');"';
	// var onDeleteClick = 'onClick="setDeleteGraph(\'' + ID + '\', \'' + div_id + '\');"';
	// var onPermissionsClick = 'onClick="loadGraphPermissions(\'' + ID + '\');"';
	// var resp = '<div id="'+containerID+'" class="'+gridColumnWidth+'">';
	// resp += '<div id="'+containerID+'" class="portlet box blue">';
	// resp += '<div class="portlet-title">';
	// resp += '<div class="tools">';
	
	// resp += '<a href="#show-data" class="btn-circle btn-icon-only btn-default" title="Export data" alt="Export data" onClick="exportToMail(\'' + graphID + '\', \'map\', \'' + name + '\', \'' + description + '\', \'' + type + '\');" ><i class="fa fa-share-square-o  dashboard_graph_icon"></i></a>';
	
	// if(graphOwnerType != 'SystemGenerated') 
	// {
		// resp += '<a href="#graph_permissions" class="btn-circle btn-icon-only btn-default" title="Graph Permissions" alt="Delete Graph" data-toggle="modal" ' + onPermissionsClick + '"><i class="fa fa-users dashboard_graph_icon" ></i></a>';
		// resp += '<a href="#delete-graph" class="btn-circle btn-icon-only btn-default" title="Delete Graph" alt="Delete Graph" data-toggle="modal" ' + onDeleteClick + '"><i class="fa fa-trash dashboard_graph_icon"></i></a>';
	// }
	// resp += '<a href="#show-data" class="btn-circle btn-icon-only btn-default" title="Save raw data to disk" alt="Save Raw Data to Disk" ' + onClickDownload + ' ><i class="fa fa-download dashboard_graph_icon"></i></a>';
	// // resp += '<a href="#show-data" class="btn-circle btn-icon-only btn-default" title="Show raw data" alt="Show raw data" data-toggle="modal" ' + onClickFunc + ' ><i class="fa fa-database"></i></a>';
	// resp += '</div>';
							
	// resp += '<div class="caption">' + shortenGraphName(name) + '</div><div class="caption_desc">'+shortenGraphName(description)+'</div>';
	// // resp += '<div class="tools"><a href="javascript:;" class="collapse"></a><a href="#portlet-config" data-toggle="modal" class="config"></a><a href="javascript:;" class="reload"> \
	// // </a><a href="javascript:;" class="remove"></a></div>';
	// resp += '</div>';
	// resp += '<div class="portlet-body"><div id="chart_' + graphOwnerType + "_" + globalItemID + '_' + ID + '" class="chart"><div id="loader" style="background: url(Images/AjaxLoader.gif) no-repeat center center;width: 100%;height: 100%;"></div></div></div>';
	// resp += '</div>';
	// resp += '</div>';
	
	// return resp;
// }

// function saveCSV(graphID, title)
// {
	// title = title;
	// var graphdata = getFullGraphData(graphID);

	
	// if(graphdata == null)
	// {
		// $scope.show_warn("Data for this table is empty!", "You cant download data for an empty graph");
		// return;
	// }
	
	// var type = graphdata.graphType;
	// if(type == 'SmallTable' || type == 'BigTable')
	// {
		// extractTableData(graphdata.data, title);
		// return;
	// }
	
	// var rawData = type == 'PieChart' ? graphdata.data : graphdata.data[0].data;
	// var ln = rawData.length;
	
	// if(ln == 0)
	// {
		// $scope.show_warn("Data for this table is empty!", "You cant download data for an empty graph");
		// return;
	// }
	
	// var myCsv = (type == 'PieChart') ? parseCsvForPieChart(title, graphdata) : parseCsvForMultiGraph(title, graphdata);
	// var uri = 'data:text/csv;charset=utf-8,' + escape(myCsv);

	// var downloadLink = document.createElement("a");
	// downloadLink.href = uri;
	// downloadLink.download = title + "_data.csv";

	// document.body.appendChild(downloadLink);
	// downloadLink.click();
	// document.body.removeChild(downloadLink);
// }

// function extractTableData(data, title)
// {
	// var myCsv = title + "\n";
	// var d = formalizeTableData(data);
	
	// myCsv += d.labels + "\n";
	// var cols = d.data;
	// var ln = cols[0].length;
	// for(var i=0;i<ln;i++)
	// {
		// for(var j=0;j<cols.length;j++)
		// {
			// myCsv += cols[j][i][1];
			// if(j != cols.length-1) 
				// myCsv += ",";
		// }
		
		// myCsv += "\n";
	// }
		
	// window.open('data:text/csv;charset=utf-8,' + escape(myCsv));
// }



			// fillTable($(graphID), chartData, options);
			// //	storeGraphData(GraphOwnerType, GraphType, graphID, chartData);
			
			// var h = $(graphID).height;
			// $(graphID).slimScroll({position: 'right', height: h, allowPageScroll: false, disableFadeOut: true, wheelStep: '5'});
		// }
		
		
		//pie += drawTable(type, item.Name, item.Description, globalItemID, item.ID, item.graphOwnerType, div_id);
		
		
		
			}	
			
		}
	}
);