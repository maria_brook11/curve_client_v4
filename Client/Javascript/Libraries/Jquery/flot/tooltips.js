function TooltipsFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst){

// $scope.setupTooltips = function(chartID) {
	// console.log(chartID)
    // function showTooltip(x, y, contents) 
    // {
        // var screenWidth = $(window).width()/2;
        // var xPos = x + 15;
        // if(x > screenWidth)
            // xPos = x - 100;

        // $('<div id="tooltip">' + contents + '</div>').css({
            // position: 'absolute',
            // display: 'none',
            // top: y + 5,
            // left: xPos,
            // border: '1px solid #333',
            // padding: '4px',
            // color: '#fff',
            // 'border-radius': '3px',
            // 'background-color': '#333',
            // opacity: 0.80
        // }).appendTo("body").fadeIn(200);
    // }

    // var previousPoint = null;
    // $(chartID).bind("plothover", function(event, pos, item) 
	// {
		// console.log('5555555555555555555')
		// if(pos != null && pos.x != null)
		// {
			// $("#x").text(pos.x.toFixed(2));
			// $("#y").text(pos.y.toFixed(2));

			// if (item) {
				// if (previousPoint != item.dataIndex) {
					// previousPoint = item.dataIndex;

					// $("#tooltip").remove();
					// var x = $scope.timeConverter(item.datapoint[0]),
						// y = item.datapoint[1].toFixed(1);

					// showTooltip(item.pageX, item.pageY, "(" + x + " , " + y + ")");
				// }
			// } else {
				// $("#tooltip").remove();
				// previousPoint = null;
			// }
		// }
    // });
// };

$scope.showTooltip = function(x, y, contents) 
			{
				var screenWidth = $(window).width()/2;
				var xPos = x + 15;
				if(x > screenWidth)
					xPos = x - 100;

				$('<div id="tooltip">' + contents + '</div>').css({
					position: 'absolute',
					display: 'none',
					top: y + 5,
					left: xPos,
					border: '1px solid #333',
					padding: '4px',
					color: '#fff',
					'border-radius': '3px',
					'background-color': '#333',
					opacity: 0.80
				}).appendTo("body").fadeIn(200);
			}

$scope.enableTooltip = function(chartType) 
{
    if (chartType == "BasicChart" || 
	    chartType == "InteractiveChart" || 
		chartType == "BarChart" || 
		chartType == "MixedGraph" || 
		chartType == "TrackingChart") 
        return true;
	
    return false;
}


$scope.timeConverter = function(UNIX_timestamp)
{
	if(UNIX_timestamp <= 24)
		return UNIX_timestamp + ":00";
	
	var a = new Date(UNIX_timestamp);
	var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	var year = a.getFullYear();
	var month = months[a.getMonth()];
	var date = a.getDate();
	var time =  date + " " + month + "" + year;
	return time;
 }
 
}