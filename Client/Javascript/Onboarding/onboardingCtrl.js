app.controller('OnboardingCtrl', function ($scope, $rootScope, $http, $location, TConst, URLConst, $filter, CountryConst, InsightConst, $timeout, $interval)
{	
	$('.loading-page').hide();
	$scope.extUrlParam = {};
	var urlParameters = $location.search();
	$scope.URLConst = URLConst;
	
	if(urlParameters.Platform)
	{
		$scope.extUrlParam.type = urlParameters.Platform;
		$scope.extUrlParam.token = urlParameters.AppToken;
		var appsData = urlParameters.appsData;
		
		if(appsData != null)
		{
			$scope.extUrlParam.appsData = JSON.parse(appsData);
			$scope.extUrlParam.name = $scope.extUrlParam.appsData[0].name;
			$scope.extUrlParam.token = $scope.extUrlParam.appsData[0].token;
		}
	}
	
	if($scope.extUrlParam.token == null || $scope.extUrlParam.token == '')
			$scope.extUrlParam.token = "YOUR_APP_TOKEN";
	
	$scope.windowOpenFun = function(url){
		window.open(url,'_parent');
	}
})