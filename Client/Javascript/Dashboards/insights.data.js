var InsightsStartupData = [
	{
		short_text: '{"text":"Welcome to Curve insights!","label":"info"}',
		long_text: "Insights are daily actionable and informative, bite size pieces of information that will help you improve your performance and growth fast and easy! What do you need to do in order to get insights? Nothing! Just integrate Curve's SDK, let the data flow in, sit back and use your insights!",
		id: "-4",
		last_calculated: "",
		seen: "false",
		action: "None"
	},
	{
		short_text: '{"text":"Want to learn more about Curve\'s Audience system?","label":"info"}',
		long_text: "Audiences are groups of your app users that have done one or more action at a specific period of time. For example, all the users who completed a purchase more than 3 times in the past month. You can create, calculate and even take actions and export Audiences! Sounds useful? lets make our first Audience:",
		id: "-3",
		last_calculated: "",
		seen: "false",
		action: "GoToCohortsPanel"
	},
	{
		short_text: '{"text":"Meet Curve\'s automatic user segmentation","label":"user"}',
		long_text: "Curve automatically segments your paying users to easy to retarget groups. These groups are calculated for you daily and can also be exported into Audiences. If you want to read more about these groups and their characteristics, click here:",
		id: "-2",
		last_calculated: "",
		seen: "false",
		action: "ShowUserSegmentsAndCharacteristics"
	},
];

var InsightsNoSdkData = [
	{
		short_text: '{"text":"Looks like your app did not get any data yet. Lets setup Curve SDK! Its easy!","label":"info"}',
		long_text: "As it seems, you probably didn't implement our one line SDK. This is the reason all the dashboards are currently empty. let's implement the SDK, it's very easy, just follow these instructions:",
		id: "-1",
		last_calculated: "",
		seen: "false",
		action: "LinkToSdkInstructions"
	},
];

var SegmentsAnalysisData = [
	{
		name:"Best Customers",
		analysis:"These are the customers that bought recently, buy often and spend a lot. It’s likely that they will continue to do so. Since they already like you so much, consider marketing to them without price incentives to preserve your profit margin. Be sure to tell these customers about new products you carry, how to connect on social networks, and any loyalty programs or social media incentives you run."
	},
	{
		name:"Loyal Customers",
		analysis:"Anyone with a high frequency should be considered loyal. This doesn’t mean they have necessarily bought recently, or that they spent a lot, though you could define that with your R and M factors."
	},
	{
		name:"Big Spenders",
		analysis:"Big spenders have spent a lot of money over their lifetime as your customer. This might be a few number of big purchases, or many small purchases, but you know they trust you enough to invest a lot in your products. Considering marketing your most expensive products and top of the line models to this group."
	},
	{
		name:"New Spenders",
		analysis:"New Spenders are new customers that spent a lot of money on their first order(s). This is the kind of customer you want to convert into a loyal, regular customer that loves your products and brand. Be sure to welcome them and thank them for making a first purchase, and follow it up with unique incentives to come back again. Consider branding the email with a special note from the CEO, and include a survey to ask about their experience."
	},
	{
		name:"Loyal Joes",
		analysis:"Loyal Joes buy often, but don’t spend very much. Because they already like and trust you, your goal should be to increase the share of wallet you have from this customer. Send offers that require them to “Spend $100 to save $20” and “Buy 4, Get 1 Free.” These offers create high hurdles that must be cleared to gain the reward, and will increase the amount these loyal customers spend with you."
	},
	{
		name:"Lost Customers",
		analysis:"Lost Customers used to buy frequently from you, and at one point they spent a lot with you, but they’ve stopped. Now it’s time to win them back. They might be lost to a competitor; they might not have need of your products anymore, or they might have had a bad customer service experience with you. Regardless, they were an extremely valuable customer that should be approached differently."
	},
	{
		name:"Almost Lost",
		analysis:"Similar to Lost Customers. It has just been less time since they purchased. These customers might warrant more aggressive discounts so that you can win them back before it’s too late. (We all know how much less expensive it is to keep customers compared to winning new ones.)"
	},
	{
		name:"Splurgers",
		analysis:"Splurgers combine a high Monetary Value with a low Frequency, which means they’ve spent a lot of money in just a few orders. Because they have the wealth and willingness to spend a lot with you, target high priced products with good margins at this group. This group might also correspond with seasonal events or even just the typical buying cycle of your product’s wear."
	},
	{
		name:"Deadbeats",
		analysis:"These customers spent very little, bought very few times, and last ordered quite a while ago. They are unlikely to be worth much time, so put them in your general house list and consider a re-opt-in campaign."
	},
	{
		name:"Average Activity Customers",
		analysis:"These clients are characterized by an average activity. "
	},
	{
		name:"Inactive",
		analysis:"This client has not made any purchases."
	},
];