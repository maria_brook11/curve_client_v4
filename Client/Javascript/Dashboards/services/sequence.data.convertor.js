app.service('diagramParseService', function() {
	
	this.getParsedData = function (d, templates_dict, junction_actions_arr) 
	{
		
		function getTemplateIdByName(name)
		{
			for(var i = 0 ; i < templates_dict.templates.length ; i++)
			{
				var template = templates_dict.templates[i];
				if(template.name == name)
					return template.id+"";
			}
			
			return null;
		}
		
		function getJunctionActionTypeIdByDescription(description)
		{
		
			for(var i = 0 ; i < junction_actions_arr.length ; i++)
			{
				var action = junction_actions_arr[i];
				if(action.description == description)
					return action.type+"";
			}
			
			return null;
		}
		
		var target = {};

		target.interactions = [];
		target.junctions = [];
		var junctions_ids = [];
		var interactions_ids = [];
		var junction_objects_by_id = {};
		var interaction_objects_by_id = {};
		
		for(var i = 0 ; i < d.nodeDataArray.length ; i++)
		{
			var node = d.nodeDataArray[i];

			if(node.category != null && node.category == "Interaction")
			{
				interactions_ids.push(node.key+"");
				var inter = {interaction_id: getTemplateIdByName(node.text), id: node.key+"", type: "mailchimp_template"};
				interaction_objects_by_id[node.key+""] = inter;
				target.interactions.push(inter);
			}
			else if(node.category != null && node.category == "Junction")
			{
				junctions_ids.push(node.key+"");
				var timeperiod = 1000*60*60*24;	// default is days.
				
				if(node.timePeriod != null && node.timePeriod != "")
				{
					if(node.timePeriod == "Hours")
						timeperiod = 1000*60*60;
					else if(node.timePeriod == "Days")
						timeperiod = 1000*60*60*24;
					else if(node.timePeriod == "Weeks")
						timeperiod = 1000*60*60*24*7;
				}
				
				var time_number = node.number;
				if(time_number == null || isNaN(time_number))
					time_number = 1;
				
				var delay_no = time_number * timeperiod;
				
				var j = {
					junction_type: "yesno",
					junction_id: node.key+"",
					action_id: getJunctionActionTypeIdByDescription(node.text),
					yes_time_ms: 0+"",
					no_time_ms: delay_no+"",
					no_interaction: null,
					yes_interaction: null,
					no_junction_id: null,
					yes_junction_id: null,
					connected_interaction_ids: [],
					is_target: false,
					is_final: false
				};
				
				junction_objects_by_id[j.junction_id] = j;
				target.junctions.push(j);
			}
			else if(node.category != null && node.category == "Start")
			{
				junctions_ids.push(node.key+"");
				var j = {
						junction_type: "start",
						junction_id: node.key+"",
						action_id:  "start",
						yes_time_ms: 0+"",
						no_time_ms: 0+"",
						no_interaction: null,
						yes_interaction: null,
						no_junction_id: null,
						yes_junction_id: null,
						connected_interaction_ids: [],
						is_target: false,
						is_final: false
					};
					
				junction_objects_by_id[j.junction_id] = j;
				target.junctions.push(j);
			}
			else if(node.category != null && node.category == "End")
			{
				junctions_ids.push(node.key+"");
				var j = {
						junction_type: "end",
						junction_id: node.key+"",
						action_id:  "end",
						yes_time_ms: 0+"",
						no_time_ms: 0+"",
						no_interaction: null,
						yes_interaction: null,
						no_junction_id: null,
						yes_junction_id: null,
						connected_interaction_ids: [],
						is_target: true,
						is_final: true
					};
					
				junction_objects_by_id[j.junction_id] = j;
				target.junctions.push(j);
			}
		}
		
		for(var k = 0 ; k < junctions_ids.length ; k++)
		{
			var cur_junction_id = junctions_ids[k];
			var j = junction_objects_by_id[cur_junction_id];
			var isStart = j.junction_type == "start";
			var isEnd = j.junction_type == "end";
			
			if(isEnd)
				continue; // this is an end junction, it does not connect to anything.
				
			for(i = 0 ; i < d.linkDataArray.length ; i++)
			{
				var link = d.linkDataArray[i];
				// "from":-5, "to":-2, "fromPort":"B", "toPort":"L"
				
				if(link.from+"" == cur_junction_id)
				{
					if(junctions_ids.indexOf(link.to+"") == -1)
					{	
						if(isStart)
						{
							j.yes_interaction = link.to+"";
						}
						// connects to an interaction
						else if(link.fromPort == "B")
						{	
							// this is a "NO" output
							j.no_interaction = link.to+"";
						}
						else if(link.fromPort == "R")
						{
							// this is a "YES" output
							j.yes_interaction = link.to+"";
						}
					}
					else
					{
						if(isStart)
						{
							j.yes_junction_id = link.to+"";
						}
						// connects to a junction 
						else if(link.fromPort == "B")
						{	
							// this is a "NO" output
							j.no_junction_id = link.to+"";
						}
						else if(link.fromPort == "R")
						{
							// this is a "YES" output
							j.yes_junction_id = link.to+"";
						}
					}
				}
			}
		}
		
		for(var k = 0 ; k < interactions_ids.length ; k++)
		{
			var cur_interaction_id = interactions_ids[k];
			var inter = interaction_objects_by_id[cur_interaction_id];
			
			for(i = 0 ; i < d.linkDataArray.length ; i++)
			{
				var link = d.linkDataArray[i];
				// "from":-5, "to":-2, "fromPort":"B", "toPort":"L"
				
				if(link.from+"" == cur_interaction_id)
				{
					if(junctions_ids.indexOf(link.to+"") == -1)
					{
						console.log("ERROR! cant connect an interaction to another interaction!!!!");
					}
					else
					{
						inter.next_junction_id = link.to+"";
					}
				}
			}
		}
		
		
		function getConnectedInteractionIDs(junc_id, array_pointer)
		{
			for(var l = 0 ; l < d.linkDataArray.length ; l++)
			{
				var linker = d.linkDataArray[l];
				
				if(linker.to+"" == junc_id)
				{
					var from_id = linker.from + "";
					
					if(interactions_ids.indexOf(from_id) != -1)
					{
						if(array_pointer.indexOf(from_id) == -1)
							array_pointer.push(from_id);
					}
					else if(junctions_ids.indexOf(from_id) != -1)
					{
						getConnectedInteractionIDs(from_id, array_pointer);
					}
				}
			}
		}
		
		for(var k = 0 ; k < junctions_ids.length ; k++)
		{
			var cur_junction_id = junctions_ids[k];
			var j = junction_objects_by_id[cur_junction_id];
			var isStart = j.junction_type == "start";
			var isEnd = j.junction_type == "end";
			
			if(!isEnd && !isStart)
				getConnectedInteractionIDs(cur_junction_id, j.connected_interaction_ids);
		}
		//console.log(target)
		return target;
	}

})