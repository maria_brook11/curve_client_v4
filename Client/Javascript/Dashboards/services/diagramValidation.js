app.service('diagramValidationService', function() {
	this.diagramValidation = function (d) 
	{	//console.log(d)
		
			var resp = {errors: [], warnings: []};
			var interactions = d.interactions;
			var junctions = d.junctions;
		  
			var interactionids = [];
			let interactions_dictionary = {};
		  
			for (let i = 0 ; i < interactions.length ; i++)
			{
				var inter = interactions[i];
				interactionids.push(inter.id);
				interactions_dictionary[inter.id] = inter.type + " " + inter.interaction_id;
			   
				if(inter.type == null)
					resp.errors.push("Interaction '" + inter.type + " " + inter.interaction_id + "': You must specify an interaction type!" );
				else if(inter.interaction_id == null)
					resp.errors.push("Interaction must specify a template! (id: '"  + inter.id + "')" );
			}
		  
			for (let j = 0 ; j < junctions.length ; j++)
			{
				var junc = junctions[j];
		   
				if(interactionids.indexOf(junc.yes_interaction) != -1)
					interactionids.splice(interactionids.indexOf(junc.yes_interaction), 1); 
			
				if(interactionids.indexOf(junc.no_interaction) != -1)
					interactionids.splice(interactionids.indexOf(junc.no_interaction), 1); 
		   
					
				if(junc.action_id == null)
				{
					if(junc.action_id == null || junc.action_id == "null" || junc.action_id == "")
						junc_name = "";
						resp.errors.push("You must specify a junction type! (id: '"  + junc.junction_id + "')" );
				}
				else
				{	
					if(junc.connected_interaction_ids.length == 0 )
					{
						var junc_name = "";
						
						if(junc.action_id == null || junc.action_id == "null" || junc.action_id == "")
							junc_name = "No Type Selected Yet!";
						else
							junc_name = junc.action_id;
						
						if(junc.action_id != "end" && junc.action_id != "start")
							resp.errors.push("Nothing is connected to Junction '" + junc_name + "'. (id: '"  + junc.junction_id + "')" );
					}
					if( junc.no_interaction == null
						&& junc.yes_interaction == null
						&& junc.no_junction_id == null
						&& junc.yes_junction_id == null)
					{					
						
						var junc_name = "";
						
						if(junc.action_id == null || junc.action_id == "null" || junc.action_id == "")
							junc_name = "No Type Selected Yet!";
						else
							junc_name = junc.action_id;
						
						if(junc.action_id != "end")
							resp.errors.push("Junction '" + junc_name + "': Is not connected to anything. (id: '"  + junc.junction_id + "')" );
					}
				}
			}
		  
			for (let k = 0 ; k < interactionids.length ; k++)
			{
				let orphaned_interaction = interactionids[k];
				resp.warnings.push("Interaction '" + interactions_dictionary[orphaned_interaction] + "': Is not connected to anything. (id: '"  + orphaned_interaction + "')" );
			}
		  //console.log(resp)
			return resp;
		
	}
})