app.service('dateService', function() {
	
	this.getDate = function (singleDate) {
			var date_today1 = moment.utc().subtract(moment().utcOffset(), 'minutes').format('MM/DD/YYYY');
			var date_2_days_ago1 = moment.utc().subtract(2, 'days').subtract(moment().utcOffset(), 'minutes');
			if (singleDate)
			{
				var dateJson =
				{
					opens: 'left',
					showDropdowns: false,
					showWeekNumbers: true,
					timePicker: false,
					timePickerIncrement: 1,
					timePicker12Hour: true,
					buttonClasses: ['btn btn-sm'],
					applyClass: ' blue',
					cancelClass: 'default',
					singleDatePicker: true,
					format: 'MM/DD/YY',
					startDate: date_2_days_ago1,
					maxDate: date_today1,
					locale:
					{
						applyLabel: 'Apply',
						fromLabel: 'From',
						toLabel: 'To',
						customRangeLabel: 'Custom Range',
						daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
						monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
						firstDay: 1
					}
				}
			}
			else
			{

				var dateJson =
				{
					opens: 'left',
					startDate: moment.utc().subtract(29, 'days').startOf('day').subtract(moment().utcOffset(), 'minutes'),
					endDate: moment.utc().subtract(moment().utcOffset(), 'minutes'),
					minDate: '01/01/2012',
					maxDate: date_today1,
					dateLimit:
					{
						days: 120
					},
					showDropdowns: false,
					showWeekNumbers: true,
					timePicker: false,
					timePickerIncrement: 1,
					timePicker12Hour: true,
					ranges:
					{
						'Yesterday': [moment.utc().startOf('day').subtract(moment().utcOffset(), 'minutes'), moment.utc().subtract(1, 'days').hour(23).minutes(59).seconds(59).subtract(moment().utcOffset(), 'minutes')],
						'Last 7 Days': [moment.utc().subtract(6, 'days').startOf('day').subtract(moment().utcOffset(), 'minutes'), moment.utc().subtract(moment().utcOffset(), 'minutes')],
						'Last 30 Days': [moment.utc().subtract(29, 'days').startOf('day').subtract(moment().utcOffset(), 'minutes'), moment.utc().subtract(moment().utcOffset(), 'minutes')],
						'This Month': [moment.utc().startOf('month').subtract(moment().utcOffset(), 'minutes'), moment.utc().endOf('month').subtract(moment().utcOffset(), 'minutes')],
						'Last Month': [moment.utc().subtract(1, 'month').startOf('month').subtract(moment().utcOffset(), 'minutes'), moment.utc().subtract(1, 'month').endOf('month').subtract(moment().utcOffset(), 'minutes')]
					},
					buttonClasses: ['btn btn-sm'],
					applyClass: ' blue',
					cancelClass: 'default',
					format: 'MM/DD/YYYY',
					separator: ' to ',
					locale:
					{
						applyLabel: 'Apply',
						fromLabel: 'From',
						toLabel: 'To',
						customRangeLabel: 'Custom Range',
						daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
						monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
						firstDay: 1
					}
				}
			}
			
		return dateJson;
    }
});



app.service('resetService', function() {
	
    this.resetGraphDataByID = function (data,id) {
		
		angular.forEach(data.Graphs, function (key)
					{
					if(key.dataGraph && key.dataGraph != null && key.dataGraph != 'undefined'){
						if(key.dataGraph.chartData && key.dataGraph.chartData != null && key.dataGraph.chartData != 'undefined'){
							if(key.dataGraph.ReportID == id){
								if(key.GraphType != "OmniChart" && key.GraphType != "MLMagnitude" ){
										key.dataGraph.chartData.ReportDataTable.Data.Records = [];
										key.dataGraph.chartData.ReportDataTable.Data = [];
										key.dataGraph.chartData = {};
										key.responceStatus = -1;
										
									}else{
										if(key.GraphType == "OmniChart"){
											key.dataGraph.chartData.ReportDataTable.Delivery = [];
											key.dataGraph.chartData.ReportDataTable = [];
											key.dataGraph.chartData = {};
										}
										if(key.GraphType == "MLMagnitude"){
											key.dataGraph.chartData.ReportDataTable.Data = [];
											key.dataGraph.chartData.ReportDataTable = [];
											key.dataGraph.chartData = {};
										}
									}
								}
						}
					}
					}) 
					
        return data;
    }
	
	this.resetAllGraphsData = function (data) {
		
		if (data && data.Graphs)
			{		
				angular.forEach(data.Graphs, function (key)
				{
					if(key.dataGraph && key.dataGraph != null && key.dataGraph != 'undefined'){
						if(key.dataGraph.chartData && key.dataGraph.chartData != null && key.dataGraph.chartData != 'undefined'){
							if (key.dataGraph.chartData.ReportDataTable)
							{

								if(key.dataGraph.chartData.ReportDataTable.Data){
									key.dataGraph.chartData.ReportDataTable.Data.Records = [];
									key.dataGraph.chartData.ReportDataTable.Data = [];
									key.dataGraph.chartData = {};
								}else{
									key.dataGraph.chartData.ReportDataTable.Records = [];
									key.dataGraph.chartData.ReportDataTable = [];
									key.dataGraph.chartData = {};
								}
								
							}
						}
					}
				})
						
			} 
			
        return data;
    }
	
	 this.resetBarGraphData = function (data) {
		
		angular.forEach(data.Graphs, function (key)
					{
					if(key.GraphType == "BarChart" && key.dataGraph && key.dataGraph != null && key.dataGraph != 'undefined'){
						if(key.dataGraph.chartData && key.dataGraph.chartData != null && key.dataGraph.chartData != 'undefined'){
							if(key.dataGraph.chartData.ReportDataTable && key.dataGraph.chartData.ReportDataTable != null && key.dataGraph.chartData.ReportDataTable != 'undefined'){
							key.dataGraph.chartData.ReportDataTable.Records = [];
							key.dataGraph.chartData.ReportDataTable = [];
							key.dataGraph.chartData = {};	
							}
						}
					}
					}) 		
        return data;
    }
	
});

app.service('saveCSVService', function() {
	var self = this;

	this.extractTableData = function (data,title) {
		var myCsv = title + "\n";
		var d = self.formalizeTableData(data);
		myCsv += d.labels + "\n";
		var cols = d.data;
		var ln = cols[0].length;
		for(var i=0;i<ln;i++)
		{
			for(var j=0;j<cols.length;j++)
			{
				myCsv += cols[j][i][1];
				if(j != cols.length-1) 
					myCsv += ",";
			}
			
			myCsv += "\n";
		}
			
		window.open('data:text/csv;charset=utf-8,' + escape(myCsv));
	}
	
	this.formalizeTableData = function(d)
	{
		var labels = [];
		var columns = [];
		for(var i = 0 ; i < d.length ; i++)
		{
			var col = d[i];
			labels.push( col.label );
			var data = col.data;
			columns[i] = [];
			for(var j = 0 ; j < data.length ; j++)
			{
				columns[i].push(data[j]);
			}
		}
		
		return {labels:labels, data:columns};
	}

	this.parseCsvForPieChart = function (data,title,type,multi) {
		
		var type = type;

		if(multi){
			var allData = [];
				for(var i = 0; i < data.length; i ++){
					
					var tempArr = [];
					for(var j = 0; j < data[i].data.length; j ++){
					
						var date = self.convertToDate(data[i].data[j][0]);
						tempArr.push(date + ' - ' + data[i].data[j][1] )
					}
					var temp = data[i].label + ' : ' + tempArr;
					allData.push(temp)
				}
				var rawData = allData;
				
		
		}else{
			var rawData = data[0].data;
		}		
		
		var myCsv = title + "\n";
		myCsv += "Label,Value\n";
		
		for(var i = 0 ; i < rawData.length ; i++)
		{
			var currentNode = rawData[i];
			
				myCsv += self.removeCommas(currentNode) + "\n";
		}
		
		return myCsv;
	}
	
	this.convertToDate = function (d)
	{
		var monthNames = [
		  "January", "February", "March",
		  "April", "May", "June", "July",
		  "August", "September", "October",
		  "November", "December"
		];

		var date = new Date(d*1000);
		var day = date.getDate();
		var monthIndex = date.getMonth();
		var year = date.getFullYear();

		return(day + ' ' + monthNames[monthIndex] + ' ' + year);
	}
	
	this.removeCommas = function(str)
	{
		return str.toString();
	}
	
	this.parseCsvForMultiGraph = function (data,title,type) {

		var myCsv = "";
		
		var rawData = graphdata.data[0].data;
		var ln = rawData.length;
		for(var i = 0 ; i < ln ; i++)
		{
			var currentNode = rawData[i];
			
			var date = currentNode[0] > 25 ? prettyDate(currentNode[0]) : currentNode[0] + ":00";
			titlesStrings = [];
			titlesStrings = currentNode[0] > 25 ? ['Date'] : ['Hour'];
			titlesStrings.push(graphdata.data[0].label);
			var lineData = [date, currentNode[1]];
			for(var j = 1; j < graphdata.data.length ; j++)
			{
				titlesStrings.push(graphdata.data[j].label);
				lineData.push(graphdata.data[j].data[i][1])
			}
			
			titles = titlesStrings.join(",") + "\n";
			myCsv += lineData.join(",") + "\n";
		}
		
		return title + "\n" + titles + myCsv;
	}


});

// app.service('httpService', function() {
    // this.getDataFun = function (x) {
        // return x.toString(16);
    // }
// });




