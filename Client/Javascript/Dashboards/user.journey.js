var current_session_map = null;
var trending_journey_users = 0;
var untrending_journey_users = null;
var random_journey_users = null;

function parseSessionsUJ(sessions)
{
	var resp = {};
	var totalSessions = sessions.length;
	resp.total = totalSessions;
	var id_maker = 1;
	
	for(var i = 0 ; i < totalSessions ; i++)
	{
		var session = sessions[i];
		var current = resp;
		var parent_id = null;
		var parent_parent_id = null;
		var prev_time = -1;
		
		for(var j = 0 ; j < session.length ; j++)
		{
			var step = session[j];
			var isButton = step.b != null && step.b != "" ;
			var curstepid = isButton ? step.b : step.sc;
						
			var time = 0;
			
			if(prev_time != -1)
				time = step.e - prev_time;

			prev_time = step.e;
			var cur_id = "uj_stp_" + id_maker++;
			if(current[curstepid] == null)
				current[curstepid] = {id: cur_id, counter:0, children: {}, isButton: isButton, precentage: 0, parent_id: parent_id, name: curstepid, totaltime:0, avTime:0};

			current[curstepid].counter++;
			current[curstepid].totaltime += time;
			current[curstepid].avTime = current[curstepid].totaltime / (1000*current[curstepid].counter);
			current[curstepid].precentage = 100*current[curstepid].counter / totalSessions;
			current = current[curstepid].children;
			parent_id = cur_id;
		}
	}
//console.log(resp)
	return resp;
}

function devideBySessionsUJ(dt)
{
	var resp = {};
	var ssions = [];
	for(var i = 0 ; i < dt.length ; i++)
	{
		var obj = dt[i];
		var ssion = obj.se;
		if(resp[ssion] == null)
		{
			resp[ssion] = [];
			ssions.push(ssion);
		}

		resp[ssion].push(obj);
	}

	var response = [];
	for(var i = 0 ; i < ssions.length ; i++)
	{
		var ssionname = ssions[i];
		response.push(resp[ssionname]);
	}
//console.log(response)
	return response;
}
function styleNumberUJ(dt) 
{
	dt = dt * 100;
	dt = Math.round(dt);
	return dt/100;
}

function normalizeToJit(dt) 
{
	var resp = {id:0, name:"START", children:[], parent:null, total:dt.total};
	resp.children = getChildrenUJ(dt, resp);
	
	return resp;
}

function getChildrenUJ(dt, prnt)
{
	var resp = [];
	
	for(name in dt)
	{
		var obj = dt[name];
		var ch = obj.children;
		
		if(obj.name == null)
			continue;
		
		var metadata = obj.name + "__" + styleNumberUJ(obj.precentage) + "__" + styleNumberUJ(obj.avTime) + "__" + obj.isButton;
		var node = {id:obj.id, name: metadata, children:[], counter:obj.counter, parent:prnt};
		
		if(ch != null)
			node.children = getChildrenUJ(ch, node);
		
		resp.push(node)
	}
	
	return resp;
}

function getAvSessionMapUJ(dt)
{
	var sessions = devideBySessionsUJ(dt);
	return parseSessionsUJ(sessions);
}

var most_common_path = [];
var least_common_path = [];
var random_path = [];

function getRandomPath(dt)
{
	var cur = dt;
	random_path = [];
	random_path.push(cur.id);
	var id = null;
	while(cur.children != null && cur.children.length > 0)
	{
		var idx = Math.round(Math.random() * (cur.children.length-1));
		id = cur.children[idx].id;
		random_path.push(id);
		cur = cur.children[idx];
	}
	
	return id;
}

function getMostCommonPath(dt)
{
	var cur = dt;
	most_common_path.push(cur.id);
	var id = null;
	while(cur.children != null && cur.children.length > 0)
	{
		var idx = getMaxChildIndex(cur.children);
		id = cur.children[idx].id;
		most_common_path.push(id);
		cur = cur.children[idx];
	}
	
	return id;
}

function getLeastCommonPath(dt)
{
	var cur = dt;
	least_common_path.push(cur.id);
	var id = null;
	while(cur.children != null && cur.children.length > 0)
	{
		var idx = getMinChildIndex(cur.children);
		id = cur.children[idx].id;
		least_common_path.push(id);
		cur = cur.children[idx];
	}
	
	return id;
}

function getMinChildIndex(dt)
{
	var min = 10000000;
	var id = 0;
	
	for(var i = 0 ; i < dt.length ; i++)
	{
		if(min > dt[i].counter)
		{
			min = dt[i].counter;
			id = i;
		}
	}
	
	return id;
}

function getMaxChildIndex(dt)
{
	var max = -1;
	var id = 0;
	
	for(var i = 0 ; i < dt.length ; i++)
	{
		if(max < dt[i].counter)
		{
			max = dt[i].counter;
			id = i;
		}
	}
	
	return id;
}

function getUserJourneyDataJSON(data)
{
	data.sort(sortByEpochUJ);
	var avSessionMap = getAvSessionMapUJ(data);
	current_session_map = normalizeToJit(avSessionMap);
	return current_session_map;
}

function sortByEpochUJ(a, b)
{
	if(a.e > b.e)
		return 1;
	else
		return -1;
}

function drawJourney(data, div_id)
{
	if(data.length == 0)
	{
		$(div_id)[0].innerHTML = '<div style="color:#333; font-size:26px;">NO DATA</div>';
		return;
	}
	
	cur_div_id = div_id;
	var dt = getUserJourneyDataJSON(data);
	// initUserJourneyData(dt, div_id);
	
	
	initRawData(dt, div_id);
	ujHideLoading();
	mostCommonJourney();
}

function ujHideLoading()
{
	$(".uj_navigation_toolbox").show();
}

// *************** ZOOMING *************************

var cur_div_id = null;

// *************** Map clipping *********************

function getClippedMap(id)
{
	var node = searchNodeUJ(id);
	var result = getBranchUJ(node);
	
	return result;
}

function searchNodeUJ(id)
{
	return searchChildrenUJ(current_session_map, id);
}

function searchChildrenUJ(node, id)
{
	if(node.id == id)
		return node;
	
	var ch = node.children;
	if(ch != null)
	{
		var ln = node.children.length;
		for(var i = 0 ; i < ln ; i++)
		{
			var current = searchChildrenUJ(ch[i], id);
			if(current != null)
				return current;
		}
	}
	
	return null;
}

var UJ_MAX_DEPTH = 5;
function getBranchUJ(node)
{
	var result = copyChildUJ(node);
	getInnerBranch(node, UJ_MAX_DEPTH, result);
	var master_node = getParentBranch(node, UJ_MAX_DEPTH, result);
	
	return master_node;
}

function getParentBranch(node, current_depth, result)
{	
	while(current_depth > 0)
	{
		node = node.parent;
		
		if(node == null)
			return result;
		
		result.parent = copyChildUJ(node);
		result.parent.children = [result];
		result = result.parent;
		current_depth--;
	}
	
	return result;
}

function getInnerBranch(node, current_depth, result)
{
	if(current_depth < 0)
		return;
	
	node = node.children;
	if(node != null)
	{
		if(result.children == null)
			result.children = [];
		
		var ln = node.length;
		for(var i = 0 ; i < ln ; i++)
		{
			if(node[i] != null)
			{
				if(node[i] == null) continue;
				var cur = copyChildUJ(node[i]);
				getInnerBranch(node[i], current_depth-1, cur);
				result.children.push(cur);
			}
		}
	}
}

function copyChildUJ(node)
{
	if(node == null)
		return null;
	
	return {name: node.name, id:node.id, counter:node.counter};	
}
