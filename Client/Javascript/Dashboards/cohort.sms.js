function CohortSmsFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst,InsightConst,$interval){
	
	var currentStep = 1;
	var foundService = false;
	
$scope.SmsEditor = function(e,t,a)
{
	$rootScope.$broadcast('deselectAll',{});
	$("#sms_editor_textarea").markdown({
		autofocus:false,
		savable:false,
		resize:"none",
	});
	
	$scope.flagSelect = false;
	if(t != null && a != null){
		var intvGetAud = $interval(function ()
			{
				if ($scope.AudiencesArray && $scope.AudiencesArray.length > 0)
				{	
					for(var i = 0; i < $scope.AudiencesArray.length ; i ++){
						
						for(var j = 0; j < a.length ; j ++){
							
							if($scope.AudiencesArray[i].type == t && $scope.AudiencesArray[i].id == a[j].id){
								if(a[j].checked){
									
									$scope.AudiencesArray[i].add = true;
									
								}
							}
						}
					}
					$scope.flagSelect = true;
					$interval.cancel(intvGetAud);
					
				}
			}, 300);
	}else{
		$scope.flagSelect = true;
	}
		
	init();
};

	$scope.SmsEditorSendSms = function()
	{
		var data = $scope.validateAndPrepareSmsData(true);
		
		$('#sms_cohort_step_1').css('display' , 'none');
		$('#sms_cohort_step_2').css('display' , 'none');
		
		$('#sms_cohort_confirm').css('display' , 'none');
		$('#sms_cohort_next').css('display' , 'none');
		$('#sms_cohort_back').css('display' , 'none');
		
		$('#sms_main_loader').css('display' , '');
		
		if(data.error != null)
		{
			$scope.show_error("Make sure all fields are properly filled", data.error);
			return;
		}
		
		if(data.warn != null)
		{
			$scope.EmailValidatorManager("", sendWithoutValidatingSms, null, data.warn, "If not, please make sure to fill them properly.", "Yes, Send", "Cancel");
			return;
		}
		
		$scope.curveAPI("SendSmsByCohorts", {"Token": $rootScope.Token, Data:JSON.stringify(data)}, onSendSmsByCohorts, onErrorSendSmsByCohorts);
	};
	
	function sendWithoutValidatingSms(id)
	{
		var data = $scope.validateAndPrepareSmsData(false);
		$scope.curveAPI("SendSmsByCohorts", {"Token": $rootScope.Token, Data:JSON.stringify(data)}, onSendSmsByCohorts, onErrorSendSmsByCohorts);
		$("#cohort_sms_editor").modal('hide');
	};
	
	function onSendSmsByCohorts(data)
	{
		// console.log(data);
		if (data == 0)
			$scope.show_warn("Attention","Sms wasn't sent to any of the recipients, please make sure you entered the correct data.");
		else
			$scope.show_success("Success","Sms was sent succesfully to " + data + " recipients.");
		$("#cohort_sms_editor").modal('hide');
		
	};
	
	function onErrorSendSmsByCohorts(data)
	{
		console.log("ERROR", data);
		$scope.show_error("There was an Error sending the sms.", data);
		$("#cohort_sms_editor").modal('hide');
	};
	
	$scope.validateAndPrepareSmsData = function(validate, checkRecepients)
	{
		var smsContent = $(".md-preview").text();
		var ids = $scope.getCohordIDs('sms');
		var smss = getRecepientsSmss();
		var fromNumber = $("#sms_editor_sentFrom").val();
		
		if (checkRecepients)
		{
			var total_recepients = 0;
			if(ids != null)
				total_recepients += 1;
			if(smss != null)
				total_recepients += smss.length;
			
			if (total_recepients == 0)
				return {error: "You must specify at least one recepient or Audience."};
		}
		
		if (smsContent == undefined || smsContent == "")
		{
			$("#sms_editor_textarea").data('markdown').showPreview();
			smsContent = $(".md-preview").text();
			$("#sms_editor_textarea").data('markdown').hidePreview();
		}
		
		if(validate)
		{
			if(smsContent == null || smsContent == "")
				return {warn: "Are you sure you want to send this sms without a body?"};
		}
		/////////////////////////////////*  "CohortIds":ids,  555555 */////////////////////////////
		var temp = {"Text": smsContent,  "SmsNumbers": smss, "FromNumber": fromNumber};
			
			angular.forEach(ids, function (val,key) {
				temp[key] = val;
			});
		return temp;
		
	};
	
	/* function getCohordIDs()
	{
		var cohortNames = $("#sms_cohort_names_select").val();
		if(cohortNames == null)
			return [];
		
		var ans = [];
		var children = $("#sms_cohort_names_select").children();
		
		var ln = children.length;
		for (var i=0 ; i < ln ; i++)
		{
			if (cohortNames.indexOf(children[i].getAttribute('value')) != -1)
				ans.push(children[i].getAttribute('data-cohort-id'));
		}
		
		return ans;
	}; */
	
	function getRecepientsSmss()
	{
		var recepients = $("#sms_editor_recepients").val();
		
		if(recepients == null || recepients == "")
			return [];
		
		var smss = recepients.replace(/ /g,'');
		return smss != null ? smss.split(",") : [];
	};
	
	function hasSmsServiceSupport()
	{
		var data = $scope.ExternalAPIsTableData;
		
		if (data == null || data.length == 0)
			return false;
		
		var ln = data.length;
		for(var i=0 ; i < ln ; i++)
		{
			if (data[i].type == "sms")
				return true;
		}
	};
	
	function firstStep()
	{
		currentStep = 1;
		$('#cohort_sms_editor_title').html("Send Sms Notifications to groups (Cohorts)");
		$('#sms_cohort_step_1').css('display' , '');
		$('#sms_cohort_step_2').css('display' , 'none');
		
		$('#sms_cohort_confirm').css('display' , 'none');
		$('#sms_cohort_next').css('display' , '');
		$('#sms_cohort_back').css('display' , 'none');
		
		$('#sms_main_loader').css('display' , 'none');
		
		if ($scope.ExternalAPIs_is_ready && !hasSmsServiceSupport())
		{
			$scope.show_warn("No Sms service configured !", "You dont have any Sms service configured. please go to: 'Settings' > 'App Settings' > 'External APIs' to add one.");
			$("#cohort_sms_editor").modal('hide');
		}
		// else
			// $("#cohort_sms_editor").modal('show');
	};
	
	function secondStep()
	{
		currentStep = 2;
		$('#sms_cohort_step_1').css('display' , 'none');
		$('#sms_cohort_step_2').css('display' , '');
		
		$('#sms_cohort_confirm').css('display' , '');
		$('#sms_cohort_next').css('display' , 'none');
		$('#sms_cohort_back').css('display' , '');
		
		$("#sms_editor_sentFrom").val(getFromNubmerIfExist());
		
	};
	
	function init()
	{
		clearAllForms();
		firstStep();
	};
	
	function clearAllForms()
	{
		// clear cohort field
	//	$("#sms_cohort_names_select").val([]).trigger("change");
		// clear recepient field
		$("#sms_editor_recepients").val("");
		
		// clear the sms forms
		$("#sms_editor_textarea").val('');
		$(".md-preview").html('');
		$("#sms_editor_textarea").data('markdown').hidePreview();
	};
	
	$scope.SmsEditorNextStep = function()
	{
		if (currentStep == 1 && verifyFirstStep())
			secondStep();
	};
	
	$scope.SmsEditorPrevStep = function()
	{
		if (currentStep == 2)
			firstStep();
	};
	
	
	function gotCohorts()
	{
		var data = $scope.cohortData;
		var ln = data.length;
		var lines = '';
		
		//console.log("sms gotCohorts", data);
		
		for(var i=0 ; i < ln ; i++)
		{
			var cohort = data[i];
			lines+='<option data-cohort-id='+cohort.audienceid+' value="'+cohort.name+'">'+cohort.name+'</option>';
		}
		
		
		$("#sms_cohort_names_loading")[0].style.display = "none";
		/* $("#sms_cohort_names_select")[0].style.display = "";
		$("#sms_cohort_names_select").select2({ placeholder: "Audience Names", theme: "classic"});
		$("#sms_cohort_names_select").html(lines);
		$("#sms_cohort_names_select").val([]).trigger("change"); */
	};
	
	function verifyFirstStep()
	{
		var ids = $scope.getCohordIDs('sms');
		var smss = getRecepientsSmss();
		
		//console.log("verifyFirstStep", ids, smss);
		
		var total_recepients = 0;
		if(ids != null)
			total_recepients += 1;
		if(smss != null)
			total_recepients += smss.length;
		
		if (total_recepients == 0)
			$scope.show_warn("No recepients were specified!", "You must specify at least one recepient or Audience to continue.");
		
		return total_recepients != 0;	
	};
	
	function getFromNubmerIfExist()
	{
		var twilioIndex;
		var ln = $scope.ExternalAPIsTableData.length;
		for(var i=0 ; i < ln ; i++)
		{
			if ($scope.ExternalAPIsTableData[i].name == "Twilio")
			{
				twilioIndex = i; 
				break;
			}
		}
		
		var orgData = $scope.ExternalAPIsTableData[twilioIndex];
		if (orgData == null || orgData == undefined)
		{
			console.log("updateAndSaveMailingListData, Error getting Twilio data.");
			return undefined;
		}
		
		var orgMetadata = JSON.parse(orgData.metadata);
		if (orgMetadata == null || orgMetadata == undefined)
		{
			console.log("updateAndSaveMailingListData, Error getting Twilio metadata.");
			return undefined;
		}
		
		if (orgMetadata.configData == null || orgMetadata.configData == undefined)
		{
			console.log("updateAndSaveMailingListData, Error getting Twilio configData.");
			return undefined;
		}
		
		return orgMetadata.configData[2];
	};
}