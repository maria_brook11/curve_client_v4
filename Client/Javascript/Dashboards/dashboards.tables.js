function DashBrdTableFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter){

$scope.fillTable = function(el, d)
{
	var labels = [];
	var columns = [];
	
	for(var i = 0 ; i < d.length ; i++)
	{
		var col = d[i];
		
		if(col == null)
			continue;
		
		labels.push( col.label );
		var data = col.data;
		columns[i] = [];
		for(var j = 0 ; j < data.length ; j++)
		{
			columns[i].push(data[j]);
		}
	}

	// el.empty();
	// el.append($scope.getTable("title", "desc", labels, columns));
	var html = $scope.getTable("title", "desc", labels, columns);
	return html;
}

$scope.getTable = function(title, desc, labels, columns)
{
	var resp = '';
	resp += '<div class="no-footer" id="sample_1_wrapper"><div><div class="tableBox" ><section><div class="tableContainer">'
	resp += '<table aria-describedby="sample_1_info" role="grid" class="table-striped table-hover" id="sample_1">';
	
	// HEADER
	resp += '<thead><tr role="row">';
	
	for(var i = 0 ; i < labels.length ; i++)
	{
		resp += $scope.getTitleCol(labels[i]);
	}
	resp += '</tr></thead>';
	
	// BODY
	resp += '<tbody>';
	
	if(columns != null && columns.length > 0)
		resp += $scope.getTableRow(columns);
	
	resp += '</tbody>';
	resp += '</table></div></section></div>';

	return resp;
}

$scope.getTableRow = function(columns)
{
	var resp = '';
	
	var tableheight = columns[0].length;
	for(var j = 0 ; j < tableheight ; j++)
	{
		var oddeven = j%2 == 0 ? 'even' : 'odd';
		resp += '<tr class="' + oddeven + '" role="row">';
		for(var i = 0 ; i < columns.length ; i++)
		{
			var row = columns[i];
			try {
				resp += '<td>' + $scope.createTableRowTruncatedData(decodeURIComponent(row[j][1])) + '</td>';
			}
			catch(err) {
				resp += '<td>' + row[j][1] + '</td>';
			}
		}
	}
	
	resp += '</tr>';
	return resp;
}

$scope.createTableRowTruncatedData = function(txt)
{
	var trunc = txt;
	if(trunc.length > 30)
		trunc = trunc.substr(0, 30) + "..";
	
	return '<span title="'+txt+'">' + trunc + '</span>';
}


$scope.getTitleRowEl = function(name)
{
	return '<td>'+name+'</td>';
}

$scope.getTitleCol = function(name)
{
	return '<th aria-label="' + name +'" activate to sort column ascending" aria-sort="ascending" colspan="1" rowspan="1" aria-controls="sample_1" tabindex="0" class="sorting_asc">'+name+'<div>'+name+'</div></th>';
}


$scope.formalizeTableData = function(d)
{
	var labels = [];
	var columns = [];
	for(var i = 0 ; i < d.length ; i++)
	{
		var col = d[i];
		labels.push( col.label );
		var data = col.data;
		columns[i] = [];
		for(var j = 0 ; j < data.length ; j++)
		{
			columns[i].push(data[j]);
		}
	}
	
	return {labels:labels, data:columns};
}
}