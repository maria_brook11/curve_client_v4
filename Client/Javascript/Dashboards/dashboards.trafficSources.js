var TrafficSourcesDescriptor = function()
{ 
	TrafficSourcesDescriptor.instance = this; 
};

TrafficSourcesDescriptor.prototype = 
{
	CampaignSources: [],
	CampaignsContainers: [],
	
	init: function(data)
	{
		// console.log("TrafficSourcesDescriptor init", data);
		
		$scope.curveAPI('GetTrafficSources', data, this.onGetTrafficSourcesSuccess, this.onGetTrafficSourcesError);
	},
	
	addCampaignsContainer: function (cont) 
	{
		TrafficSourcesDescriptor.instance.CampaignsContainers.push(cont);
	},
	
	onGetTrafficSourcesSuccess: function(data)
	{
		// console.log("TrafficSourcesDescriptor onGetTrafficSourcesSuccess", data);
		
		// TODO: remove for test
		// data = {
			// Campaigns:[
				// {
					// "campaign": "cartrec", 
					// "visits": "490", 
					// "pages/visit": "5.95", 
					// "avg.time on site": "102:00", 
					// "% bounce rate": "8.89679700"
				// },
				// {
					// "campaign": "57697", 
					// "visits": "125", 
					// "pages/visit": "17.57", 
					// "avg.time on site": "156:55", 
					// "% bounce rate": "5.75539500"
				// },
				// {
					// "campaign": "Dresses - Exact", 
					// "visits": "41", 
					// "pages/visit": "1.00", 
					// "avg.time on site": "00:04", 
					// "% bounce rate": "8.69565200"
				// },
			// ],
			// Sources: [
				// {
					// "campaign": "cartrec", 
					// "source/medium": "bronto/email", 
					// "visits": "490", "pages/visit": "6.60", 
					// "avg.time on site": "102:00", 
					// "% bounce rate": "32.91814900"
				// },
				// {
					// "campaign": "57697", 
					// "source/medium": "AW/affiliates", 
					// "visits": "125", "pages/visit": "12.64", 
					// "avg.time on site": "156:55", 
					// "% bounce rate": "28.05755300"
				// },
				// {
					// "campaign": "Dresses - Exact", 
					// "source/medium": "bing/cpc", 
					// "visits": "37", 
					// "pages/visit": "1.18", 
					// "avg.time on site": "00:11", 
					// "% bounce rate": "40.54054000"
				// },
			// ]
		// };
		
		headerData = {
			campaignsHeader: [
				"Campaign",
				"Visits", 
				"Pages/Visit", 
				"Avg. Time on Site", 
				"% Bounce Rate",
			],
		};
		
		while (TrafficSourcesDescriptor.instance.CampaignsContainers.length > 0)
		{
			TrafficSourcesDescriptor.instance.fillCampaigns(TrafficSourcesDescriptor.instance.CampaignsContainers.pop(), headerData, data.Sources, data.Campaigns);
		}
	},
	
	fillCampaigns: function (cont, headerData, Sources, Campaigns) 
	{
		TrafficSourcesDescriptor.instance.setHeaders(cont, headerData);
		TrafficSourcesDescriptor.instance.CampaignSources = Sources;
		TrafficSourcesDescriptor.instance.setCampaigns(cont, Campaigns);
		
		$(cont + "_table_div")[0].style.display = "";
		$(cont + "_loading")[0].style.display = "none";
	},
	
	onGetTrafficSourcesError: function(data)
	{
		show_error('ERROR! ', data);
	},
	
	getCampaignLine: function(data)
	{
		var ans = "";
		ans += '<tr>';
		ans += '	<td id="campaign_' + window.md5(data["campaign"]) +'_td">';
		ans += '		<div  data-toggle="collapse" data-target=".collapse_' + window.md5(data["campaign"]) +'"style="cursor: pointer;">';
		ans += '			<i class="fa fa-plus-circle" aria-hidden="true" id="fa_' + window.md5(data["campaign"]) +'" style="display:none;margin-right:10px;"></i>\'' + data["campaign"] + '\'';
		ans += '		</div>';
		ans += '	</td>';
		ans += '	<td id="visits_' + window.md5(data["campaign"]) +'_td">' + parseInt(data["visits"]) + '</td>';
		ans += '	<td id="pages_' + window.md5(data["campaign"]) +'_td">' + data["pages/visit"] + '</td>';
		ans += '	<td id="avgTime_' + window.md5(data["campaign"]) +'_td">' + data["avg.time on site"] + '</td>';
		ans += '	<td id="bounce_' + window.md5(data["campaign"]) +'_td">' + data["% bounce rate"] + '</td>';
		ans += '</tr>';
		return ans;
	},
	
	addSourceToCampaign: function(data)
	{
		$("#fa_" + window.md5(data["campaign"]))[0].style.display = "";
		$("#campaign_" + window.md5(data["campaign"]) +"_td").append('<div class="collapse collapse_' + window.md5(data["campaign"]) +' style="padding-top: 10px; margin-left: 10%;""><span style="margin-right: 5px;color: gray;">source/medium:</span>' + data["source/medium"] + '</div>');
		$("#visits_" + window.md5(data["campaign"]) +"_td").append('<div class="collapse collapse_' + window.md5(data["campaign"]) +'" style="padding-top: 10;">' + data["visits"] + '</div>');
		$("#pages_" + window.md5(data["campaign"]) +"_td").append('<div class="collapse collapse_' + window.md5(data["campaign"]) +'" style="padding-top: 10px;">' + data["pages/visit"] + '</div>');
		$("#avgTime_" + window.md5(data["campaign"]) +"_td").append('<div class="collapse collapse_' + window.md5(data["campaign"]) +'" style="padding-top: 10px;">' + data["avg.time on site"] + '</div>');
		$("#bounce_" + window.md5(data["campaign"]) +"_td").append('<div class="collapse collapse_' + window.md5(data["campaign"]) +'" style="padding-top: 10px;">' + data["% bounce rate"] + '</div>');
	},
	
	setCampaigns: function(data)
	{
		// console.log("setCampaigns", data);
		$(cont + '_table_body').empty();
		if (data != null)
		{
			var ln = data.length;
			for(i = 0 ; i < ln ; i++)
			{
				$(cont + "_table_body").append(TrafficSourcesDescriptor.instance.getCampaignLine(data[i]));
			}
			
		}
		TrafficSourcesDescriptor.instance.setCampaignSources();
		$(cont + '_table').DataTable();
	},
	
	setCampaignSources: function()
	{
		// console.log("setCampaignSources");
		var ln = TrafficSourcesDescriptor.instance.CampaignSources.length;
		for(i = 0 ; i < ln ; i++)
		{
			var campaignSource = TrafficSourcesDescriptor.instance.CampaignSources[i];
			TrafficSourcesDescriptor.instance.addSourceToCampaign(campaignSource);
		}
	},
	
	setHeaders: function(cont, data)
	{
		
		$(cont + '_table_head').empty();
		if (data != null)
		{
			if (data.campaignsHeader != null)
			{
				var campaignsHeaderStr = "";
				campaignsHeaderStr += '<tr>';
				var ln = data.campaignsHeader.length;
				for(i = 0 ; i < ln ; i++)
				{
					campaignsHeaderStr += '	<th> ' + data.campaignsHeader[i] + ' </th>';
				}
				campaignsHeaderStr += '</tr>';
				$(cont + "_table_head").append(campaignsHeaderStr);
			}
		}
	},
}

new TrafficSourcesDescriptor();
