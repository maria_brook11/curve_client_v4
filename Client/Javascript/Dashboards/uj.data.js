function UJdataFunction($scope,$rootScope,$http,$location,$timeout){

var json_uj_data = {};
var STEP_PER_PAGE = 7;
var MAX_BUTTTONS = 7;
var selectedUJButton = '';
var selected_page_uj = 1;
var current_pages_uj = [];
var num_pages_uj = 0;
var labelType, useGradients, nativeTextSupport, animate;
var nodeWidth = 140;
var nodeHeight = 60;
var current_session_map = null;
var trending_journey_users = 0;
var untrending_journey_users = null;
var random_journey_users = null;
var most_common_path = [];
var least_common_path = [];
var random_path = [];
var most = null;
var least = null;
var start = null;
var st = null;
var json_data = null;
var global_div_id = null;
var cur_div_id = null;
var UJ_MAX_DEPTH = 5;
var flag = true;
$scope.drawJourneyGraph = function(data, div_id){
	if(flag){
		$timeout(function () {
			$scope.reloadCurrentTab();
		}, 3000);
		flag = false;
	}
		

	if(data.length > 0){
	
	$(div_id).css("display", "block");
	
	cur_div_id = div_id;
	var dt = getUserJourneyDataJSON(data);
	initUserJourneyData(dt, div_id);
	

	initRawData(dt, div_id);
	ujHideLoading();
	$scope.mostCommonJourney();
	}else{ 
		$(div_id).html('5555555555555555555');
		
		$(div_id+'_caption')[0].innerHTML = '';
	}
}


$scope.injectUnrealData = function(div_id)
{
	$scope.drawJourneyGraph(json_uj_data, div_id);
}

// ..................... PAGES MANAGER ................................



function updateButtonsGraphics(num)
{
	selected_page_uj = num;
	setSelectedViewUJ(num);
	centerSelectedButtonUJ(num);
	$scope.uiCenterDiv('#uj_buttons');
}

function pageSelectedUJ(num)
{
	updateButtonsGraphics(num);
	var item_id = current_pages_uj[num-1];
	selectPageItemUJ(item_id);
}

function centerSelectedButtonUJ(num)
{
	var half = Math.ceil(MAX_BUTTTONS/2);
	
	if(parseInt(num) + half > num_pages_uj)
		half = half + parseInt(num) + half - num_pages_uj;
	
	var start = num - half;
	if(start < 1)
		start = 1;
	var end = start + MAX_BUTTTONS;
	
	for(var i = 1 ; i < num_pages_uj+1 ; i++)
	{
		$('#uj_page_button_' + i)[0].style.display = (i < start || i > end) ? 'none' : 'inline-block';
	}
}

function setSelectedViewUJ(num)
{
	if(num < 1 || num > num_pages_uj) return;
	if(selectedUJButton != '')
	{
		$('#' + selectedUJButton)[0].style.background = '#fff';
	}
	
	selectedUJButton = "uj_page_button_" + num;
	$('#' + selectedUJButton)[0].style.background = '#ddd';
}

function createPageButtnUJ(num)
{
	if(num > 9)
		return '<a href="#" id="uj_page_button_'+num+'" class="uj_nav_button uj_page_button uj_navigation" onclick="pageSelectedUJ(\''+num+'\');">'+num+'</a>';

	return '<a href="#" id="uj_page_button_'+num+'" class="uj_nav_button uj_page_button uj_navigation" onclick="pageSelectedUJ(\''+num+'\');">'+num+'</a>';
}

function createPages(map)
{
	selectedUJButton = '';
	current_pages_uj = [];
	num_pages_uj = Math.ceil(map.length / STEP_PER_PAGE);
	
	var pages_code = '';	
	for(var i = 0 ; i < num_pages_uj ; i++)
	{
		var page_center = i * STEP_PER_PAGE;
		if(page_center > map.length - 1)
			page_center = map.length - 1;
		
		if(i == num_pages_uj - 1)
			current_pages_uj.push(map[map.length-1]);
		else if(i == 0)
		{
			if(STEP_PER_PAGE - 3 < map.length-1)
				current_pages_uj.push(map[STEP_PER_PAGE-3]);
			else
				current_pages_uj.push(map[map.length-1]);
		}
		else
			current_pages_uj.push(map[page_center]);
		
		pages_code += createPageButtnUJ(i+1);
	}

	$('#uj_pages')[0].innerHTML = pages_code;	
}

// NAVIGATION

function selectLastPage()
{
	pageSelectedUJ(num_pages_uj-1);
}

function selectFirstPage()
{
	pageSelectedUJ(1);
}

function selectPrevPage()
{
	if(selected_page_uj > 1)
	{
		selected_page_uj--;
		pageSelectedUJ(selected_page_uj);
	}
}

function selectNextPage()
{
	if(selected_page_uj < num_pages_uj-1)
	{
		selected_page_uj++;
		pageSelectedUJ(selected_page_uj);
	}
}

function selectPageItemUJ(id)
{
	initUserJourneyData(getClippedMap(id), global_div_id, id);
}

function formalizeTitle(st)
{
	var max = 15;
	if(st.length > max)
		return st.substr(0, max) + "..";
	
	return st;
}


function initUserJourneyData(json, div_id, clicked_id)
{	
	div_id = div_id.substr(1);
	
	$("#" + div_id).html();
	
    st = new $jit.ST(
	{
        injectInto: div_id,
        duration: 0,
        transition: $jit.Trans.Quart.easeInOut,
        //set distance between node and its children
        levelDistance: 30,
        //enable panning
        Navigation: 
		{
          enable:true,
          panning:true
        },
        Node: 
		{
            height: nodeHeight,
            width: nodeWidth,
            type: 'rectangle',
            color: '#fff',
            overridable: true
        },
        
        Edge: 
		{
            type: 'bezier',
            color: '#5cb8d8',
            lineWidth: 0.8,
            overridable: true
        },
        
        onCreateLabel: function(label, node)
		{
            label.id = node.id;
			var data = node.name.split("__");
			
			var icon = data[3] == "true" ? '<i class="fa fa-hand-pointer-o" style="margin-right:5px;color:#176f01;"></i>' : '<i class="fa fa-desktop" style="margin-right:5px;fontSize:0.7em;color:#5ecff4;"></i>';
			if(data.length > 2)
			{
			    label.innerHTML = '<div class="user_journy_container"><div style="color:#333;fontSize:1em;font-weight:bold" title="'+ data[0] +'">' + icon + formalizeTitle(data[0]) + '</div>' + 
				'<div>Users: ' + data[1] + '%</div>' + 
				'<div>Av Time: ' + data[2] + ' Sec</div></div>';
			}
			else
				label.innerHTML = '<div class="user_journy_container_start"><div style="color:#fff;fontSize:30px;font-weight:bold;font-family: "Open Sans", sans-serif;" title="'+ data[0] +'">' + formalizeTitle(data[0]) + '</div></div>';
           
			label.onclick = function()
			{
				initUserJourneyData(getClippedMap(node.id), global_div_id, node.id);
            };
			
            //set label styles
            var style = label.style;
            style.width = nodeWidth + 'px';
            style.height = nodeHeight + 'px';
            style.color = '#333';
            style.fontSize = '0.8em';
            style.textAlign= 'center';
			style["font-family"] = "Open Sans";
        },
		
        onBeforePlotLine: function(adj)
		{
            if (adj.nodeFrom.selected && adj.nodeTo.selected)
			{
                adj.data.$color = "#5ecff4";
                adj.data.$lineWidth = 4;
            }
            else 
			{
                delete adj.data.$color;
                delete adj.data.$lineWidth;
            }
        }
    });
	
    //load json data
    st.loadJSON(json);
    //compute node positions and layout
    st.compute();
    //optional: make a translation of the tree
    st.geom.translate(new $jit.Complex(-200, 0), "current");
    //emulate a click on the root node.
	
	if(clicked_id != null)
		st.onClick(clicked_id);
	else if(most == null)
		st.onClick(st.root);
	else
		st.onClick(most);
	
}

function parseSessionsUJ(sessions)
{
	var resp = {};
	var totalSessions = sessions.length;
	resp.total = totalSessions;
	var id_maker = 1;
	
	for(var i = 0 ; i < totalSessions ; i++)
	{
		var session = sessions[i];
		var current = resp;
		var parent_id = null;
		var parent_parent_id = null;
		var prev_time = -1;
		
		for(var j = 0 ; j < session.length ; j++)
		{
			var step = session[j];
			var isButton = step.b != null && step.b != "" ;
			var curstepid = isButton ? step.b : step.sc;
						
			var time = 0;
			
			if(prev_time != -1)
				time = step.e - prev_time;

			prev_time = step.e;
			var cur_id = "uj_stp_" + id_maker++;
			if(current[curstepid] == null)
				current[curstepid] = {id: cur_id, counter:0, children: {}, isButton: isButton, precentage: 0, parent_id: parent_id, name: curstepid, totaltime:0, avTime:0};

			current[curstepid].counter++;
			current[curstepid].totaltime += time;
			current[curstepid].avTime = current[curstepid].totaltime / (1000*current[curstepid].counter);
			current[curstepid].precentage = 100*current[curstepid].counter / totalSessions;
			current = current[curstepid].children;
			parent_id = cur_id;
		}
	}

	return resp;
}

function devideBySessionsUJ(dt)
{
	var resp = {};
	var ssions = [];
	for(var i = 0 ; i < dt.length ; i++)
	{
		var obj = dt[i];
		var ssion = obj.se;
		if(resp[ssion] == null)
		{
			resp[ssion] = [];
			ssions.push(ssion);
		}

		resp[ssion].push(obj);
	}

	var response = [];
	for(var i = 0 ; i < ssions.length ; i++)
	{
		var ssionname = ssions[i];
		response.push(resp[ssionname]);
	}

	return response;
}

function styleNumberUJ(dt) 
{
	dt = dt * 100;
	dt = Math.round(dt);
	return dt/100;
}

function normalizeToJit(dt) 
{
	var resp = {id:0, name:"START", children:[], parent:null, total:dt.total};
	resp.children = getChildrenUJ(dt, resp);
	
	return resp;
}

function getChildrenUJ(dt, prnt)
{
	var resp = [];
	
	for(name in dt)
	{
		var obj = dt[name];
		var ch = obj.children;
		
		if(obj.name == null)
			continue;
		
		var metadata = obj.name + "__" + styleNumberUJ(obj.precentage) + "__" + styleNumberUJ(obj.avTime) + "__" + obj.isButton;
		var node = {id:obj.id, name: metadata, children:[], counter:obj.counter, parent:prnt};
		
		if(ch != null)
			node.children = getChildrenUJ(ch, node);
		
		resp.push(node)
	}
	
	return resp;
}

function getAvSessionMapUJ(dt)
{
	var sessions = devideBySessionsUJ(dt);
	return parseSessionsUJ(sessions);
}

function getRandomPath(dt)
{
	var cur = dt;
	random_path = [];
	random_path.push(cur.id);
	var id = null;
	while(cur.children != null && cur.children.length > 0)
	{
		var idx = Math.round(Math.random() * (cur.children.length-1));
		id = cur.children[idx].id;
		random_path.push(id);
		cur = cur.children[idx];
	}
	
	return id;
}

function getMostCommonPath(dt)
{
	var cur = dt;
	most_common_path.push(cur.id);
	var id = null;
	while(cur.children != null && cur.children.length > 0)
	{
		var idx = getMaxChildIndex(cur.children);
		id = cur.children[idx].id;
		most_common_path.push(id);
		cur = cur.children[idx];
	}
	
	return id;
}

function getLeastCommonPath(dt)
{
	var cur = dt;
	least_common_path.push(cur.id);
	var id = null;
	while(cur.children != null && cur.children.length > 0)
	{
		var idx = getMinChildIndex(cur.children);
		id = cur.children[idx].id;
		least_common_path.push(id);
		cur = cur.children[idx];
	}
	
	return id;
}

function getMinChildIndex(dt)
{
	var min = 10000000;
	var id = 0;
	
	for(var i = 0 ; i < dt.length ; i++)
	{
		if(min > dt[i].counter)
		{
			min = dt[i].counter;
			id = i;
		}
	}
	
	return id;
}

function getMaxChildIndex(dt)
{
	var max = -1;
	var id = 0;
	
	for(var i = 0 ; i < dt.length ; i++)
	{
		if(max < dt[i].counter)
		{
			max = dt[i].counter;
			id = i;
		}
	}
	
	return id;
}

function getUserJourneyDataJSON(data)
{	
	data = data.sort(function(a, b){return a.e > b.e ? 1 :  -1});
	var avSessionMap = getAvSessionMapUJ(data);
	current_session_map = normalizeToJit(avSessionMap);
	return current_session_map;
}

function initRawData(json, div_id)
{
	most_common_path = [];
	least_common_path = [];
	random_path = [];
	
    most = getMostCommonPath(json);
    least = getLeastCommonPath(json);
	global_div_id = div_id;
	json_data = json;
}

$scope.mostCommonJourney = function()
{
	if(json_data == null)
	{
		alert("Data not ready!");
		return;
	}
	
	createPages(most_common_path);
	initUserJourneyData(getClippedMap(most), global_div_id, most);
	// updateButtonsGraphics(1);
	pageSelectedUJ(1);
	
	var desc_id = global_div_id + '_caption';
	$(desc_id)[0].innerHTML = "Showing: Trending Journey. Total sessions: " + json_data.total;
}

$scope.leastCommonJourney = function()
{
	if(json_data == null)
	{
		alert("Data not ready!");
		return;
	}
	
	createPages(least_common_path);
	initUserJourneyData(getClippedMap(least), global_div_id, least);
	// updateButtonsGraphics(1);
	pageSelectedUJ(1);
	
	var desc_id = global_div_id + '_caption';
	$(desc_id)[0].innerHTML = "Showing: Un-Trending Journey. Total sessions: " + json_data.total;
}

function ujHideLoading()
{
	$(".uj_navigation_toolbox").show();
}

// *************** Map clipping *********************

function getClippedMap(id)
{
	var node = searchNodeUJ(id);
	var result = getBranchUJ(node);
	
	return result;
}

function searchNodeUJ(id)
{
	return searchChildrenUJ(current_session_map, id);
}

function searchChildrenUJ(node, id)
{
	if(node.id == id)
		return node;
	
	var ch = node.children;
	if(ch != null)
	{
		var ln = node.children.length;
		for(var i = 0 ; i < ln ; i++)
		{
			var current = searchChildrenUJ(ch[i], id);
			if(current != null)
				return current;
		}
	}
	
	return null;
}

function getBranchUJ(node)
{
	var result = copyChildUJ(node);
	getInnerBranch(node, UJ_MAX_DEPTH, result);
	var master_node = getParentBranch(node, UJ_MAX_DEPTH, result);
	
	return master_node;
}

function getParentBranch(node, current_depth, result)
{	
	while(current_depth > 0)
	{
		node = node.parent;
		
		if(node == null)
			return result;
		
		result.parent = copyChildUJ(node);
		result.parent.children = [result];
		result = result.parent;
		current_depth--;
	}
	
	return result;
}

function getInnerBranch(node, current_depth, result)
{
	if(current_depth < 0)
		return;
	
	node = node.children;
	if(node != null)
	{
		if(result.children == null)
			result.children = [];
		
		var ln = node.length;
		for(var i = 0 ; i < ln ; i++)
		{
			if(node[i] != null)
			{
				if(node[i] == null) continue;
				var cur = copyChildUJ(node[i]);
				getInnerBranch(node[i], current_depth-1, cur);
				result.children.push(cur);
			}
		}
	}
}

function copyChildUJ(node)
{
	if(node == null)
		return null;
	
	return {name: node.name, id:node.id, counter:node.counter};	
}


}