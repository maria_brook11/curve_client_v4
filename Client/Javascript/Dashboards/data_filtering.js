function DataFilterFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter){


$scope.curveAPI('GetPlatformsAndCountries', {'Token': $rootScope.Token}, gotFilteringData, failedCountries);
// $scope.curveAPI('GetGamePlayers', {'Token': Token}, gotUsersData, failedCountries);

var global_users = [];
var filterby = "email";

function failedCountries(data){show_error("Failed to retrieve users data", "Please try again at a later time.");}

function gotFilteringData(data)
{
	jQuery(document).ready(function()
	{ 
		fillCountries(data.Countries);
		fillPlatforms(data.Platforms);
		
		updatePlatformName();
		updateCountryName();
	});
}
/*
function gotUsersData(data)
{
	global_users = data;
	jQuery(document).ready(function()
	{ 
		var users = data;
		var lines = '';
		var ln = users.length;
		var tags = [];
		console.log("Started parsing autocomplete");
		for(var i = 0 ; i < ln ; i++)
		{
			// var em = users[i][filterby];
			// var id = users[i]["userid"];
			// if(em != null && em != "")
				// lines+='<option value="'+id+'">'+em+'</option>';
			var val = users[i][filterby];
			if(val != null)
				tags.push(val);
		}

		// document.getElementById('users_search_input').innerHTML = lines;
		console.log("Completed parsing autocomplete");
		console.log(tags);
		$( "#users_search_input" ).autocomplete({
			source: tags
			});
	});
}
*/

function fillCountries(data)
{
	global_emails = data;
	var lines = '';
	var ln = global_emails.length;
	for(var i = 0 ; i < ln ; i++)
	{
		var em = global_emails[i].country;
		if(em != null && em != "")
			lines+='<option value="'+em+'">'+em+'</option>';
	}
	
	document.getElementById('global_countries').innerHTML = lines;
}

function fillPlatforms(data)
{
	global_emails = data;
	var lines = '';
	var ln = global_emails.length;
	for(var i = 0 ; i < ln ; i++)
	{
		var em = global_emails[i].platform;
		if(em != null && em != "")
			lines+='<option value="'+em+'">'+em+'</option>';
	}
	
	document.getElementById('global_platform').innerHTML = lines;
}

function commitPlatforms() 
{
	updatePlatformName();
	reloadCurrentTab();
	 TUTORIAL_GINGEE_FUNCTIONS.invokeEvent("waitForFilterByPlatformApply");
}

function updateCountryName() 
{
	var names = getCountries();
	
	
	if(names == "")
	{
		names = "Countries";
		$('.country-select')[0].style.backgroundColor = "#d0d0d0";
	}
	else
	{
		$('.country-select')[0].style.backgroundColor = "#575757";
	}
	
	//$('#countries_name')[0].innerHTML = names;
}

function updatePlatformName()
{
	var names = $scope.getPlatforms();
	
	//if(names.length > 9)
		//names = names.substr(0, 9) + "...";
	
	if(names == "")
	{
		//names = "Platforms";
		$('.platform-select')[0].style.backgroundColor = "#d0d0d0";
	}
	else
	{
		$('.platform-select')[0].style.backgroundColor = "#575757";
	}
	
	//$('#platform_name')[0].innerHTML = names;
}

$scope.getPlatforms = function() 
{
	var pl = $("#global_platform").val();
	
	if(pl != null)
		return pl.toString();
	
	return "";
}

function getCountries() 
{
	var cnt = $("#global_countries").val();
	
	if(cnt != null)
		return cnt.toString();
	
	return "";
}

function clearPlatforms()
{
	$("#global_platform").val([]).trigger("change");
	updatePlatformName();
}

function clearCountries()
{
	$("#global_countries").val([]).trigger("change");
	updateCountryName();
}

function onDismissCountries(){clearCountries();}
function onDismissPlatforms(){clearPlatforms();}

}