function AudienceFunction($scope,$rootScope,$http,$location,$filter,$document,$interval,$timeout)
{	
	$scope.statusAudience = {};
	$scope.statusAudience.showSpinner = true;
	$scope.behaviorsArrShow = false;


 $(document).on("show.bs.collapse", ".collapse", function (event) {
	$(this).prev().find(".arrowA").toggleClass("fa-angle-double-down fa-angle-double-up");
}).on('hide.bs.collapse', ".collapse", function (e) {
	$(this).prev().find(".arrowA").toggleClass("fa-angle-double-up fa-angle-double-down");
})	

	
//$scope.d1 = [["453", "54", "356", "565"],["453", "5654", "534", "5563"],["345", "34", "65", "4567"], ["45", "55", "554", "44"]]
  
	$scope.sortDataFun = function(d1,ll){
		
		var d2 = [];
		
		var indxData = [];
		
		for(var j = 0; j < d1[0].length; j ++){
			var temp = [];
			for(var i = 0; i < d1.length ; i ++){
				var t = parseInt(d1[i][j]);
				temp.push(t);
			}
			indxData.push(temp);
		}
		
		var indxDataArr = [];
		for(var i = 0; i < indxData.length ; i ++){
			var temp = [];
			for(var j = 0; j < indxData[i].length; j ++){
				var t = [j,indxData[i][j]];
				temp.push(t)
			}
			indxDataArr.push(temp);
		}
		
		for(var i = 0; i < indxDataArr.length ; i ++){
			
			indxDataArr[i].sort(function(a, b){return a[1]-b[1]}); 
		}

		
		for(var i = 0; i < d1.length ; i ++){
				
				var temp = {'data' : d1[i],'label' : ll[i], 'val' :[], 'ind' : 0};
				d2.push(temp);
				
		}
			
		for(var i = 0; i < indxDataArr.length; i ++){
			
			for(var j = 0; j < indxDataArr[i].length ; j ++){
				
				d2[indxDataArr[i][j][0]].val.push(j);
			}
			
		} 
		
		function getSum(total, num) {
			return total + num;
		}
		
		for(var i = 0; i < d2.length ; i ++){
			d2[i].ind = d2[i].val.reduce(getSum);
		}
		
		function sortByKey(array, key) {
			return array.sort(function(a, b) {return b[key]-a[key]}); 
		}
		
		d2 = sortByKey(d2,'ind');
		
		
		var newData = {'data' : [], 'labels' : []};
		for(var i = 0; i < d2.length ; i ++){
			newData.data.push(d2[i].data);
			newData.labels.push(d2[i].label);
		} 
		
		return newData;
	}
  
  
	$scope.newFormatDateFun = function(d){
		
		var newDate = new Date(d);
		return newDate;
	}
	
	
	$scope.openSelectedAudienceFun = function(type,subtype,id){
		
		if(type == 'audience'){
			
			$('#audience_tab').find('a').trigger('click');
			$('#audience_details_'+id+'').collapse('show');
			
			var interv = $interval(function ()
			{
				if ($('#audience_details_'+id+'').is(':visible'))
				{
					var pos = ($('#audience_details_'+id+'').offset().top) - 300;
					$("body").animate({scrollTop: pos}, "slow");
					$interval.cancel(interv);	
				}
			}, 200);
			
		}else{
			$('#smart_tab').find('a').trigger('click');
			
			
			if(type == 'behavior'){
				
				$('#tendency_'+subtype+'_tab').find('a').trigger('click');
				$('#behavior_details_'+subtype+'_'+id+'').collapse('show');
		
				 var interv = $interval(function ()
				{
					if ($('#behavior_details_'+subtype+'_'+id+'').is(':visible'))
					{
						var pos = ($('#behavior_details_'+subtype+'_'+id+'').offset().top) - 300;
						$("body").animate({scrollTop: pos}, "slow");
						$interval.cancel(interv);	
					}
				}, 200); 
			}else{
				
				$('#ranks_tab').find('a').trigger('click');
				$('#rank_details_'+type+'_'+id+'').collapse('toggle');
				var interv = $interval(function ()
					{
						if ($('#rank_details_'+type+'_'+id+'').is(':visible'))
						{
							var pos = ($('#rank_details_'+type+'_'+id+'').offset().top) - 300;
							$("body").animate({scrollTop: pos}, "slow");
							$interval.cancel(interv);	
						}
					}, 200);
			}
		}
		
	}
	
	
	$scope.checkBehavFun = function(id,bId){
		
		var flag = 0;
		var ind = 0;
		for(var k = 0; k < $scope.ranksArr.length ; k++){
			if($scope.ranksArr[k].rank.id == id){
				ind = k;
				for(var h = 0; h < $scope.ranksArr[k].behaviors.length ; h ++){
					if($scope.ranksArr[k].behaviors[h].checked){
						flag = 1;
						break;
					}
				}
			}
		}
		showButtonInRank(flag,ind);
		
	}
	
	function showButtonInRank(flag,ind){
		
			if(flag == 1){
				$scope.ranksArr[ind].check = true;
			}else{
				$scope.ranksArr[ind].check = false;
			}
		}
	
	$scope.checkRankFun = function(t,id,idR){

		var flag = 0;
		var ind1 = 0;
		var ind2 = 0;
		
		for(var j = 0 ; j < $scope.newBehaviorsArr.length ; j++){
			if($scope.newBehaviorsArr[j].subtype == t && $scope.newBehaviorsArr[j].data.length > 0){
			
				for(var b = 0 ; b < $scope.newBehaviorsArr[j].data.length ; b++){
					if($scope.newBehaviorsArr[j].data[b].behavior.behaviorid == id && $scope.newBehaviorsArr[j].data[b].ranks.length > 0){
						
						ind1 = j;
						ind2 = b;
						for(var q = 0 ; q < $scope.newBehaviorsArr[j].data[b].ranks.length ; q++){
							if($scope.newBehaviorsArr[j].data[b].ranks[q].checked){
								flag = 1;
								break;
								//$scope.newBehaviorsArr[j].check = true;
							}
						}
					}
				}
			}		
		}
		showButtonInBehav(flag,ind1,ind2);
	}
	
	function showButtonInBehav(flag,ind1,ind2){
		
			if(flag == 1){
				$scope.newBehaviorsArr[ind1].data[ind2].check = true;
			}else{
				$scope.newBehaviorsArr[ind1].data[ind2].check = false;
			}
			
		}
	
	
	$scope.goToEngnCenterFun = function(e,t,a){
		
		$scope.setTabFun(0,'Center');
		var promise = $interval(function ()
			{
				if ($('#Send').is(':visible'))
				{	
					if(e == 'mailList' || e == 'email' || e == 'mailListMaropost'){
						$('#cohort_email_editor').modal('show');
						$scope.EmailEditor(e,t,a);
						
					}
					if(e == 'push'){
						$('#cohort_push_editor').modal('show');
						$scope.PushEditor(e,t,a);
						
					}
					if(e == 'sms'){
						$('#cohort_sms_editor').modal('show');
						$scope.SmsEditor(e,t,a);
						
					}
					$interval.cancel(promise);
				}
			}, 300);
		
	}
	
	
	$document.on('click', function(event) {
        $scope.messageError = {"empty" : false, "name" : false};
		
      });
	
	$scope.getMLdataFun = function(){
		$scope.MLMagnitudeGraph = {};
		//localhost:8080/Curve/GetMLMagnitudeGraph?Token=CURV-FXEH1VYSXGW
		$scope.curveAPI('GetMLMagnitudeGraph', {'Token': $rootScope.Token}, GetMLMagnitudeGraphSuccess, GetMLMagnitudeGraphError);
	}
	function GetMLMagnitudeGraphSuccess(data){
		
		$scope.MLMagnitudeGraph = data.ReportDataTable;
		//console.log($scope.MLMagnitudeGraph)
	}
	
	function GetMLMagnitudeGraphError(data){
		console.log(data)
	}
	
	$scope.messageError = {"empty" : false, "name" : false};
	
	$scope.startAudincesFun = function(){
		$scope.abortAjaxCalls();
		$scope.curveAPI('GetBehaviorTypes', {Token: $rootScope.Token}, GetBehaviorTypesSuccess, GetBehaviorTypesError);
		$scope.curveAPI('GetAudiences', {'Token': $rootScope.Token}, GetAudiencesSuccess, GetAudiencesError);
		$scope.curveAPI('GetRanks', {Token: $rootScope.Token}, GetRanksSuccess, GetRanksError);
		$scope.curveAPI('GetBehaviors', {Token: $rootScope.Token}, GetBehaviorsSuccess, GetBehaviorsError);
		$scope.getAudienceEventsFun();
		$scope.getAudStatIntervalFun();
	}
	
	function GetBehaviorTypesSuccess(data){
		$scope.behaviorTypes = data;
		
	}
	function GetBehaviorTypesError(data){}
	function GetAudiencesSuccess(data){
		$scope.BehaviorAudienceIDs = [];
		if(data){
			$scope.audiencesArr = data;
			var calculatedShow = "calculatedShow";
			$.each($scope.audiencesArr, function (key,val){
				var n = val.name;
				val.newName = n;
				val.editName = false;
				val.calculatedShow = false;
				val.usersListShow = false;
				val.recalculetedNOW = false;
				if (val.lastcalculated != null && val.lastcalculated != undefined)
					val.lastcalculated = val.lastcalculated.split(" ")[0];
				if(val.type == "Behavior"){
					var t = JSON.parse(val.metadata);
					if(t.ID && t.ID != null){
						$scope.BehaviorAudienceIDs.push(t.ID);
					}
				}
			});
		}
		$scope.statusAudience.showSpinner = false;
		
	}

	
	function GetAudiencesError(data){
		console.log(data)
		$scope.statusAudience.showSpinner = false;
	}
	
	function GetRanksSuccess(data){
		
		$scope.ranksArr = $scope.parseData(data,'r');
		
		for(var k = 0; k < $scope.ranksArr.length ; k++){
			var predStatus = 'predStatus';
			if($scope.ranksArr[k].rank.predicted_revenue){
				var x = Math.floor( $scope.ranksArr[k].rank.predicted_revenue - $scope.ranksArr[k].rank.revenue );
				
				if(x > 0){
					$scope.ranksArr[k].rank.predStatus = 1;
				}
				if(x < 0){
					$scope.ranksArr[k].rank.predStatus = -1;
				}
				if(x == 0){
					$scope.ranksArr[k].rank.predStatus = 0;
				}
			}else{
				$scope.ranksArr[k].rank.predStatus = 0;
			}
			
			
			var showML = 'showML';
			$scope.ranksArr[k].rank.showML = false;
			$scope.ranksArr[k].rank.showCompere = false;
			$scope.ranksArr[k].rank.showCompereName = '';
			$scope.ranksArr[k].rank.showCompereData = '';
			$scope.ranksArr[k].check = false;
			for(var h = 0; h < $scope.ranksArr[k].behaviors.length ; h ++){
				var checked = "checked";
				$scope.ranksArr[k].behaviors[h].checked = false;
			}
			
		}
	
	}
	function GetRanksError(data){
		console.log(data)
	}
	
	
	$scope.kokokokokoko = {'ranks_graph' : { 'ReportDataTable' : {
					"Data": [{
							"data": {
								"ColumnNames": ["calculation_date", "size"],
								"Records": [["2017-08-14", "22354"], ["2017-08-16", "22354"], ["2017-08-21", "22354"]]
							},
							"label": "Best Customers"
						}, {
							"data": {
								"ColumnNames": ["calculation_date", "size"],
								"Records": [["2017-08-14", "28061"], ["2017-08-16", "28061"], ["2017-08-21", "28061"]]
							},
							"label": "BIG SPENDERS"
						}, {
							"data": {
								"ColumnNames": ["calculation_date", "size"],
								"Records": [["2017-08-14", "38761"], ["2017-08-16", "15687"], ["2017-08-21", "15687"]]
							},
							"label": "LOYAL CUSTOMERS"
						}, {
							"data": {
								"ColumnNames": ["calculation_date", "size"],
								"Records": [["2017-08-14", "28505"], ["2017-08-16", "15674"], ["2017-08-21", "15674"]]
							},
							"label": "LOYAL JOES "
						}, {
							"data": {
								"ColumnNames": ["calculation_date", "size"],
								"Records": [["2017-08-14", "44617"], ["2017-08-16", "22002"], ["2017-08-21", "22002"]]
							},
							"label": "DEADBEATS"
						}, {
							"data": {
								"ColumnNames": ["calculation_date", "size"],
								"Records": [["2017-08-14", "55675"], ["2017-08-16", "27906"], ["2017-08-21", "27906"]]
							},
							"label": "ALMOST LOST"
						}
					]
				}
	}}
	
	
		
/**************** new area chart ******************/	
$scope.dataChartjs = {
  
    'labels' : ["January", "February", "March", "April", "May", "June"],
	'series' : ['#B1 name', '#B1 name'],
	
    'datasets' : [
			[12, 19, 3, 5, -2, 3],
			 [7, 11, -5, 8, 3, 7]
		],

  'options' : {
	 legend: {
                display: true,
				 position: 'right'
			},
  	scales: {
    	yAxes: [{
        ticks: {
					reverse: false
        }
      }]
    }
  },
  'colors' : [ '#803690', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360']
}


$scope.colors = [
{backgroundColor: "rgba(102,153,204,0.9)",borderColor: "rgba(102,153,204, 1)",},
{backgroundColor: "rgba(95,179,179,0.9)",borderColor: "rgba(95,179,179, 1)",},
{backgroundColor: "rgba(153,199,148,0.9)", borderColor: "rgba(153,199,148, 1)",},
{backgroundColor: "rgba(250,200,99,0.9)",borderColor: "rgba(250,200,99, 1)",},
{backgroundColor: "rgba(249,145,87,0.9)",borderColor: "rgba(249,145,87, 1)",},
{backgroundColor: "rgba(236,95,103, 0.9)",borderColor: "rgba(236,95,103, 0.9)",}
            
            
          ];
 
 
/**************************************************************************************************/	
	$scope.getAreaChartData = function(Bdata,graphType){
		
		var legendShow = false;
		
		if(graphType == 'statistic'){
			legendShow = true; 
		}
		
		var dataChartjs = {
  
					'labels' : [],
					'series' : [],
					'datasets' : [],
					'options' : {
						'tooltips': { mode: 'x-axis'},
						'elements': {
							'line': {tension: 0.00000001},
							'point': {radius: 0}
						},
						'legend' : {
							'display' : legendShow,
							'position' : 'top'
						},
						'scales' : { 'yAxes' : [{'ticks' : {'reverse' : false}}] }
					},
					'colors' : [{backgroundColor: "rgba(102,153,204,0.9)",borderColor: "rgba(102,153,204, 1)"},
{backgroundColor: "rgba(95,179,179,0.9)",borderColor: "rgba(95,179,179, 1)"},
{backgroundColor: "rgba(153,199,148,0.9)", borderColor: "rgba(153,199,148, 1)"},
{backgroundColor: "rgba(250,200,99,0.9)",borderColor: "rgba(250,200,99, 1)"},
{backgroundColor: "rgba(249,145,87,0.9)",borderColor: "rgba(249,145,87, 1)"},
{backgroundColor: "rgba(236,95,103, 0.9)",borderColor: "rgba(236,95,103, 0.9)"}]
				}
	
				var d = Bdata[0];
				for(var j = 0; j < d.data.Records.length; j ++){
					
					dataChartjs.labels.push(d.data.Records[j][0]);
					
				}
				
				
				for(var i = 0; i < Bdata.length; i ++){
					var d = Bdata[i];
					dataChartjs.series.push(d.label);
					
					if(d.data.Records.length > 0){
						var t = [];
						for(var j = 0; j < d.data.Records.length; j ++){
							
								t.push(d.data.Records[j][1]);
							
						}
						dataChartjs.datasets.push(t);
					}
					
				}
				var sortedData = $scope.sortDataFun(dataChartjs.datasets,dataChartjs.series);
				dataChartjs.datasets = sortedData.data;
				dataChartjs.series = sortedData.labels;
				
		return  dataChartjs;
	}
	
	
	$scope.parseData = function(data,e){
		if(e == 'b'){
			$.each(data, function (key,val){	
				if(val.ranks_graph.ReportDataTable.Data.length > 0 && val.ranks_graph.ReportDataTable.Data[0].data.Records.length > 0){
					var dataChartjs = 'dataChartjs';
					val.ranks_graph.dataChartjs = $scope.getAreaChartData(val.ranks_graph.ReportDataTable.Data,'statistic');
				}
				if(val.attributes.ReportDataTable.Data.length > 0 && val.attributes.ReportDataTable.Data[0].data.Records.length > 0){
					var dataChartjs = 'dataChartjs';
					val.attributes.dataChartjs = $scope.getAreaChartData(val.attributes.ReportDataTable.Data,'attribut');
				}
			})
		}
		
		if(e == 'r'){
			$.each(data, function (key,val){	
				if(val.behaviors_graph.ReportDataTable.Data.length > 0 && val.behaviors_graph.ReportDataTable.Data[0].data.Records.length > 0){
					var dataChartjs = 'dataChartjs';
					val.behaviors_graph.dataChartjs = $scope.getAreaChartData(val.behaviors_graph.ReportDataTable.Data,'statistic');
				}
				if(val.attributes.ReportDataTable.Data.length > 0 && val.attributes.ReportDataTable.Data[0].data.Records.length > 0){
					var dataChartjs = 'dataChartjs';
					val.attributes.dataChartjs = $scope.getAreaChartData(val.attributes.ReportDataTable.Data,'attribut');
				}
			})
		}
		
		return data;
	}
	
	
	function GetBehaviorsSuccess(data){
		if(data.length > 0){
		for(var k = 0 ; k < data.length ; k++){
			
			var predStatus = 'predStatus';
			if(data[k].behavior && data[k].behavior.predicted_revenue){
				var x = Math.floor(data[k].behavior.predicted_revenue - data[k].behavior.revenue );
				if(x > 0){
					data[k].behavior.predStatus = 1;
				}
				if(x < 0){
					data[k].behavior.predStatus = -1;
				}
				if(x == 0){
					data[k].behavior.predStatus = 0;
				}
			}else{
				data[k].behavior.predStatus = 0;
			}
			
		}
		
		
		$scope.behaviorsArray = $scope.parseData(data,'b');
		
		$scope.newBehaviorsArr = [];
		
		var types = [data[0].behavior.subtype];
		
		
		
		for(var k = 1 ; k < data.length ; k++){
			
			if(types.indexOf(data[k].behavior.subtype) == -1){
				types.push(data[k].behavior.subtype);
			}
		}
		
		for(var a = 0 ; a < types.length ; a++){
			$scope.newBehaviorsArr.push({'subtype' : types[a] , 'data' : []})
		}
		
		for(var i = 0 ; i < data.length ; i++){
			
			var check = "check";
			data[i].check = false;
			
			for(var q = 0 ; q < data[i].ranks.length ; q++){
				
				var checked = "checked";
				data[i].ranks[q].checked = false;
			}
			
			for(var j = 0 ; j < $scope.newBehaviorsArr.length ; j++){
				if($scope.newBehaviorsArr[j].subtype == data[i].behavior.subtype){
					var showSimilars = 'showSimilars';
					data[i].showSimilars = false;
					data[i].showCompere = false;
					data[i].showCompereID = '';
					data[i].showCompereName = '';
					data[i].showCompereData = '';
					$scope.newBehaviorsArr[j].data.push(data[i]);
				}
			}		
		}
		var bType = $interval(function ()
			{
				if ($scope.behaviorTypes)
				{
					SetbehaviorTypeFun();
					$interval.cancel(bType);	
				}
			}, 200);
			
		}
		
		
	}
	function GetBehaviorsError(data){
		console.log(data)
	}
	
	 function SetbehaviorTypeFun(){
		 for(var i = 0 ; i < $scope.newBehaviorsArr.length ; i++){
			for(var j = 0 ; j < $scope.behaviorTypes.length ; j++){
				if($scope.newBehaviorsArr[i].subtype == $scope.behaviorTypes[j].typeid ){
					$scope.newBehaviorsArr[i].typeName = $scope.behaviorTypes[j].name;
					$scope.newBehaviorsArr[i].typeDescr = $scope.behaviorTypes[j].description;
					
				}
			}
			var size = 'size';
			$scope.newBehaviorsArr[i].size = 0;
			var tSum = 0;
			for(var b = 0 ; b < $scope.newBehaviorsArr[i].data.length ; b++){
				if($scope.newBehaviorsArr[i].data[b].behavior.isactive == '1'){	
					tSum += parseInt($scope.newBehaviorsArr[i].data[b].behavior.size);
				}
			}
			$scope.newBehaviorsArr[i].size = tSum;
		} 

	}
		
		
	$scope.deleteAudienceFun = function(){
		$scope.statusAudience.showSpinner = true;
		//localhost:8080/Curve/DeleteAudiences?Token=TEST-YZMF8ICL6W8&Data={"Audiences":["1","2","3"]}
		var d = {"Audiences":[$scope.statusAudience.deleteAUDid]};
		$scope.curveAPI('DeleteAudiences', {'Token': $rootScope.Token, 'Data' : JSON.stringify(d)}, DeleteAudiencesSuccess, DeleteAudiencesError);
		function DeleteAudiencesSuccess(data){
			
			for(var i = 0; i < $scope.audiencesArr.length; i ++){
				if($scope.audiencesArr[i].audienceid == $scope.statusAudience.deleteAUDid){
					$scope.audiencesArr.splice(i,1);
					break;
				}
			}
			$scope.statusAudience.deleteAUDid = '';
			$scope.statusAudience.showSpinner = false;
		}
		function DeleteAudiencesError(data){
			console.log(data)
			$scope.statusAudience.deleteAUDid = '';
			$scope.statusAudience.showSpinner = false;
		}
	}
	
	
	/***************************************** automation / calculation *************************************/

	
	$scope.calculatedShowHideFun = function(id){
		for(var i = 0; i < $scope.audiencesArr.length; i ++){
			if($scope.audiencesArr[i].audienceid == id){
				$scope.audiencesArr[i].calculatedShow = !$scope.audiencesArr[i].calculatedShow;
			}else{
				$scope.audiencesArr[i].calculatedShow = false;
			}
		}
	}
	
	
	$scope.automationModal = {'ID' : '', 'Interval': 1,'daysORmonths' : 'Days', 'Count' : 1,'Repeat' : '','StartDate' : '','repeatCheck':false,'startCheck':false};
	
	$scope.showAutomationFun = function(id){
	
		$scope.automationModal.ID = id;
		$scope.automationModal.Interval = 1;
		$scope.automationModal.daysORmonths = 'Days'
		$scope.automationModal.Count = 1;
		$scope.automationModal.Repeat = "";
		$scope.automationModal.StartDate = "";
		$scope.automationModal.repeatCheck = false;
		$scope.automationModal.startCheck = false;

		$('#automation').modal('show');
	}
	
	
	$scope.recalculetedFun = function(id){
		
		for(var i = 0; i < $scope.audiencesArr.length; i ++){
			if($scope.audiencesArr[i].audienceid == id){
				$scope.audiencesArr[i].recalculetedNOW = true;
				break;
			}
		}
		
		$scope.curveAPI('CalculateCohort', {'Token': $rootScope.Token, 'Data' : id}, CalculateCohortSuccess, CalculateCohortError);

		//Calculate
		//http://localhost:8080/Curve/CalculateCohort?Token=TEST-YZMF8ICL6W8&Data=8
	}
	function CalculateCohortSuccess(data){
		
		for(var i = 0; i < $scope.audiencesArr.length; i ++){
			if($scope.audiencesArr[i].audienceid == data[0].audienceid){
				$scope.audiencesArr[i] = data[0];
				$scope.audiencesArr[i].recalculetedNOW = false;
				break;
			}
		}
	}
	function CalculateCohortError(data){
		console.log(data)
	}
	
	$scope.deleteAutomationFun = function(id){
		//delete
        //http://localhost:8080/Curve/SwitchAudienceAutomation?Token=TEST-YZMF8ICL6W8&Data={"AudiencesOff":["6","7","8"]}
		var d = {"AudiencesOff":[""+id+""]};
		$scope.curveAPI('SwitchAudienceAutomation', {'Token': $rootScope.Token, 'Data' : JSON.stringify(d)}, SwitchAudienceAutomationSuccess, SwitchAudienceAutomationError);

	}
	function SwitchAudienceAutomationSuccess(data){
		
		for(var i = 0; i < $scope.audiencesArr.length; i ++){
			if($scope.audiencesArr[i].audienceid == data[0].audienceid){
				$scope.audiencesArr[i] = data[0];
				break;
			}
		}
	}
	function SwitchAudienceAutomationError(data){
		console.log(data)
	}
	
	$scope.AutomateAudience = function(id){
		
		if($scope.automationModal.repeatCheck){
			var count = ""+$scope.automationModal.Count+""
		}else{
			var count = "";
		}
		
		var d = {
			"AudienceId" : ""+$scope.automationModal.ID+"",
			"Active" : "1",
			"RepeatInterval" : ""+$scope.automationModal.Interval+"",
			"RepeatCount" : count, 
			"StartTime" : ""+$scope.automationModal.StartDate+""
			} 
	
		$scope.curveAPI('AutomateAudience', {'Token': $rootScope.Token, 'Data' : JSON.stringify(d)}, AutomateAudienceSuccess, AutomateAudienceError);
		$scope.automationModal.ID = id;
		$scope.automationModal.Interval = 1;
		$scope.automationModal.daysORmonths = 'Days'
		$scope.automationModal.Count = 1;
		$scope.automationModal.Repeat = "";
		$scope.automationModal.StartDate = "";
		$scope.automationModal.repeatCheck = false;
		$scope.automationModal.startCheck = false;

		$('#automation').modal('hide');
	}
	function AutomateAudienceSuccess(data){
		
		for(var i = 0; i < $scope.audiencesArr.length; i ++){
			if($scope.audiencesArr[i].audienceid == data[0].audienceid){
				$scope.audiencesArr[i] = data[0];
				break;
			}
		}
	}
	function AutomateAudienceError(data){
		console.log(data)
		
	}
	
	
	
	/***************************************** automation / calculation *************************************/
	$scope.getAudienceDetailsFun = function(id){
		
		//$('#audienceAccordion .panel-collapse.in').collapse('hide');
		
		for(var i = 0; i < $scope.audiencesArr.length; i ++){
			if($scope.audiencesArr[i].audienceid == id && !$scope.audiencesArr[i].details){
				var d = {"AudienceId":""+id+""};
				$scope.curveAPI('GetAudienceDetails', {'Token': $rootScope.Token , 'Data' : JSON.stringify(d)}, GetAudienceDetailsSuccess, GetAudienceDetailsError);	
	
			}
		}		
		
	}

	$scope.getAudienceUsersFun = function(id, type){
		
		
		var d = {"id":""+id+"", "type":""+type+""};
		
		// $scope.curveAPI('GetAllUsers', {'Token': $rootScope.Token , 'Data' : JSON.stringify(d)}, GetAudienceCSV, GetAudienceCSVError);	
	
			var downloadLink = document.createElement("a");
				

			var fileUrl = $scope.getAPIURL() + "GetAllUsers?Token="+$rootScope.Token+"&Data="+encodeURIComponent(JSON.stringify(d));
			// var fileUrl = "http://34.231.85.211:8080/Curve/" + "GetAllUsers?Token="+$rootScope.Token+"&Data="+encodeURIComponent(JSON.stringify(d));
				// console.log(uri);
			downloadLink.href = fileUrl;
			//downloadLink.download = title + "_data.csv";
			document.body.appendChild(downloadLink);
			downloadLink.click();
			document.body.removeChild(downloadLink); 
	}

	function GetAudienceCSV(){
		
		
	}
	
	function GetAudienceCSVError(){
		
	}
	
	function GetAudienceDetailsSuccess(data){
		
		/* koko */
		
		/* 
		if(data[0].)
		var ndata = $scope.parseData(data,'a');
		 */
		if(data.length > 0){
			var details = 'details';
			for(var i = 0; i < $scope.audiencesArr.length; i ++){
				if(data[0].audience.audienceid == $scope.audiencesArr[i].audienceid){
					$scope.audiencesArr[i].details = data[0];
					
				}
			}
		}
	}
	
	function GetAudienceDetailsError(data){
		console.log(data)
	}
	
	$scope.getAudienceEventsFun = function(){
		$scope.audienceModalDetails	= [];
		$scope.newAudience = {'name' : '','ConditionType' : '','viewByCondition' : 1}
		$scope.audienceModal = {'step' : 0,'valueArr' : [], 'tempValueArr' : [],"ViewType" : "Action" ,'data' : 
				{	
					"checkEventName" : "",
					"behaviorName" : "", 
					"itemID" : "",
					"Type" : "Action",  ///from user
					"QueryTable":"", ///table_name
					"ColumnName":"",   ///column_name
					"EventName":"",     ///value
					"HwIDColumnName":"",		///userid_column_name
					"Modifier": "",				///modifiers
					"TimingDefinition":"",   ///from user
					"Timing":"",                ///from user
					"TimingPeriod":"",      ///from user
					"Condition":"",          ///from user
					"Value":"",                 ///from user
					"ValueFrom" : "",			///from user
					"ValueTo" : "",				///from user
					"NextCondition":"AND",         ///from user
					"ID" : ""
				}
			};
		
        //34.231.85.211:8080/Curve/GetAudiencesRanksBehaviors?Token=TEST-YZMF8ICL6W8		
		$scope.curveAPI('GetAudiencesRanksBehaviors', {Token: $rootScope.Token}, GetAudiencesRanksBehaviorsSuccess, GetAudiencesRanksBehaviorsError);
	
		$scope.curveAPI('GetCohortsEvents', {Token: $rootScope.Token}, GetCohortsEventsSuccess, GetCohortsEventsError);
	}
	
	function GetAudiencesRanksBehaviorsSuccess(data){
		
		$scope.ranksBehaviorsArr = [];
		if(data.Ranks.length > 0){
			for(var i = 0;i < data.Ranks.length; i ++){
				$scope.ranksBehaviorsArr.push(data.Ranks[i])
			}
		}
		if(data.Behaviors.length > 0){
			for(var i = 0;i < data.Behaviors.length; i ++){
				$scope.ranksBehaviorsArr.push(data.Behaviors[i])
			}
		}
	}
	
	function GetAudiencesRanksBehaviorsError(data){
		console.log(data)
	}
	
	
	function GetCohortsEventsSuccess(data)
	{	
		$scope.eventDescrArr = [];
		$scope.tempEventDescrArr = [];
		$scope.eventsArr = [];
		/* if(data.Behaviors){
			
			$scope.behaviorsArr = data.Behaviors;
		
			var GetBehaviorAudienceIDs = $interval(function (){

				if ($scope.BehaviorAudienceIDs)
				{
					
					var show = "show";
					for(var i = 0;i < $scope.behaviorsArr.length; i ++){
						
						$scope.behaviorsArr[i].show = true;
						for(var j = 0;j < $scope.BehaviorAudienceIDs.length; j ++){
							if($scope.behaviorsArr[i].id == $scope.BehaviorAudienceIDs[j]){
								$scope.behaviorsArr[i].show = false;
							}
						}
					}
				$interval.cancel(GetBehaviorAudienceIDs);			
				}
			}, 300);
		}
		$scope.behaviorsArrShow = true; */
		
 		if(data.CohortsEvents && data.CohortsEvents.length){
			$.each(data.CohortsEvents, function (key,val){
				
				if($scope.eventDescrArr.indexOf(val.description) == -1){
					
					$scope.eventDescrArr.push(val.description);
					$scope.tempEventDescrArr.push(val.description);
					
					var temp = {
						"userid_column_name": val.userid_column_name,
						"description": val.description,
						"table_name": val.table_name,
						"modifiers": val.modifiers,
						"gameid": val.gameid,
						"column_name": val.column_name,
						"valueArr" : []
					}
					$scope.eventsArr.push(temp);
				}
			})
		}
		
		if(data.CohortsEvents && data.CohortsEvents.length && $scope.eventsArr.length){
			$.each(data.CohortsEvents, function (key,val){
				
				if(val.value){
					for(var i = 0; i < $scope.eventsArr.length; i ++){
						if(val.description == $scope.eventsArr[i].description){
							$scope.eventsArr[i].valueArr.push(val.value);
							break;
						}
					}
				}
			})
		}
	};
	
	function GetCohortsEventsError(data)
	{
		$scope.show_error('ERROR! ', data);

	};
	
	$scope.audienceMagnitudeFun = function(id,magnId){
		for(var i = 0; i < $scope.audiencesArr.length; i ++){
			if($scope.audiencesArr[i].audienceid == id){
				$scope.audiencesArr[i].details.magnitudes.value = '';
				break;
			}
		}
		var d = {"AudienceId":id,"MagnitudeId":magnId[0]};
		$scope.curveAPI('GetAudienceMagnitude', {Token: $rootScope.Token,Data : JSON.stringify(d)}, GetAudienceMagnitudeSuccess, GetAudienceMagnitudeError);
		
	}
	
	function GetAudienceMagnitudeSuccess(data){
		
		for(var i = 0; i < $scope.audiencesArr.length; i ++){
			if($scope.audiencesArr[i].audienceid == data.AudienceId){
				
				for(var j = 0; j < $scope.audiencesArr[i].details.magnitudes.Records.length; j ++){
					
					if($scope.audiencesArr[i].details.magnitudes.Records[j][1] == data.MagnitudeName){
						
						$scope.audiencesArr[i].details.magnitudes.value = data.Magnitudes.Records[0][0];
						break;
					}
				}
			}
		}
		
	}
	function GetAudienceMagnitudeError(data){
		
		console.log(data)
	}

	/****************************************************** create new audience *****************************************/
	

	$scope.createAudienceModalFun = function(){
		$scope.tempEventDescrArr = $scope.eventDescrArr;
		$scope.audienceModalDetails	= [];
		$scope.newAudience = {'name' : '','ConditionType' : '','viewByCondition' : 1}
		$scope.audienceModal = {'step' : 0, 'valueArr' : [],'tempValueArr' : [],"ViewType" : "Action" , 'data' : 
			{	
				"checkEventName" : "",
				"behaviorName" : "", 
				"itemID" : "",
				"Type" : "Action",  ///from user
				"QueryTable":"", ///table_name
				"ColumnName":"",   ///column_name
				//"EventName":"",     ///value
				"HwIDColumnName":"",		///userid_column_name
				"Modifier": "",				///modifiers
				"Timing":"",                ///from user
				"TimingDefinition":"",   ///from user
				"TimingPeriod":"",      ///from user
				"Condition":"",          ///from user
				"Value":"",                 ///from user
				"ValueFrom" : "",			///from user
				"ValueTo" : "",				///from user
				"NextCondition":"AND",         ///from user
				"ID" : ""
			}
		};
		
		$('#createAudienceModal').modal('show');
	}
	
	$scope.searchEventFun = function(str){
		
		$scope.newAudience.viewByCondition = 0;
		if(str != ''){			
			$scope.tempEventDescrArr = [];
			$.each($scope.eventDescrArr, function (key,val){
				
				if(val.search(new RegExp(str, "i")) != -1){

					$scope.tempEventDescrArr.push(val);

				}
			})

		}else{
			$scope.tempEventDescrArr = $scope.eventDescrArr;
			$scope.audienceModal.data.Modifier = '';
		}
		
	}
	
	
	$scope.searchValueFun = function(str){
		
		if(str != ''){			
			$scope.audienceModal.tempValueArr = [];
			$.each($scope.audienceModal.valueArr, function (key,val){
				
				if(val.search(new RegExp(str, "i")) != -1){

					$scope.audienceModal.tempValueArr.push(val);

				}
			})

		}else{
			$scope.audienceModal.tempValueArr = $scope.audienceModal.valueArr;
		}
		
	}
	
	
	$scope.checkEventTypeFun = function(name){
		
		$scope.audienceModal.data.checkEventName = name;
		$scope.newAudience.viewByCondition = 1;
		var eventData = $filter('filter')($scope.eventsArr,{description: name})[0];
		$scope.audienceModal.data.QueryTable = eventData.table_name;
		$scope.audienceModal.data.ColumnName = eventData.column_name;
		$scope.audienceModal.data.HwIDColumnName = eventData.userid_column_name;
		$scope.audienceModal.data.Modifier = eventData.modifiers;
		if($scope.audienceModal.data.Modifier == '1'){$scope.newAudience.ConditionType = 'not equals'; $scope.audienceModal.data.Condition = '1001'}
		if($scope.audienceModal.data.Modifier == '2'){$scope.newAudience.ConditionType = 'less than'; $scope.audienceModal.data.Condition = '2001'}
		if($scope.audienceModal.data.Modifier == '3'){$scope.newAudience.ConditionType = 'equals'; $scope.audienceModal.data.Condition = '3001'}
		if($scope.audienceModal.data.Modifier == '4'){$scope.newAudience.ConditionType = 'not equals'; $scope.audienceModal.data.Condition = '4001'}
		$scope.audienceModal.valueArr = eventData.valueArr;
		$scope.audienceModal.tempValueArr = eventData.valueArr;
		$scope.audienceModal.data.EventName = '';
		$scope.audienceModal.data.Timing = '';
		$scope.audienceModal.data.Value = '';
		$scope.audienceModal.data.ValueFrom = '';
		$scope.audienceModal.data.ValueTo = '';
		
	}
	
	$scope.checkBehaviorFun = function(id,type){
		var behaviorData = $filter('filter')($scope.ranksBehaviorsArr,{id: id})[0];
		$scope.audienceModal.data.behaviorName = behaviorData.name;
		$scope.audienceModal.data.ID = behaviorData.id;
		$scope.audienceModal.data.Type = type;
		$scope.audienceModal.data.behaviorDiscFinal = behaviorData.desc_final;
		$scope.audienceModal.data.behaviorDisc = behaviorData.description;
		
	}
	
	$scope.changeConditionFun = function(i,e){
		if($scope.audienceModalDetails[i].NextCondition == 'AND'){
			$scope.audienceModalDetails[i].NextCondition = 'OR';
		}else{
			$scope.audienceModalDetails[i].NextCondition = 'AND';
		}
		
	}
	
	
	$scope.changeTypeFun = function(e){
		$scope.tempEventDescrArr = $scope.eventDescrArr;
		$scope.audienceModal = {'step' : 1, 'valueArr' : [],'tempValueArr' : [],"ViewType" : e, 'data' : 
			{	
				"checkEventName" : "",
				"behaviorName" : "", 
				"itemID" : "",
				"Type" : "",  ///from user
				"QueryTable":"", ///table_name
				"ColumnName":"",   ///column_name
				"EventName":"",     ///value
				"HwIDColumnName":"",		///userid_column_name
				"Modifier": "",				///modifiers
				"Timing":"",                ///from user
				"TimingDefinition":"",   ///from user
				"TimingPeriod":"",      ///from user
				"Condition":"",          ///from user
				"Value":"",                 ///from user
				"ValueFrom" : "",			///from user
				"ValueTo" : "",				///from user
				"NextCondition":"AND",        ///from user
				"ID" : ""
			}
		};
		if(e == 'Action'){$scope.audienceModal.data.Type = e;}
	}
	//console.log('maaaaaaaaaaa311');
	$scope.setItemAudienceModalFun = function(){
		$scope.audienceModal.data.Timing = ""+$scope.audienceModal.data.Timing+"";
		$scope.audienceModal.data.itemID = $scope.audienceModalDetails.length;	
		$scope.audienceModalDetails.push($scope.audienceModal.data);
		
		$scope.audienceModal = {'step' : 0, 'valueArr' : [],'tempValueArr' : [],"ViewType" : "Action", 'data' : 
			{	
				"checkEventName" : "",
				"behaviorName" : "", 
				"itemID" : "",
				"Type" : "Action",  ///from user
				"QueryTable":"", ///table_name
				"ColumnName":"",   ///column_name
				"HwIDColumnName":"",		///userid_column_name
				"Modifier": "",				///modifiers
				"Timing":"",                ///from user
				"TimingDefinition":"",   ///from user
				"TimingPeriod":"",      ///from user
				"Condition":"",          ///from user
				"Value":"",                 ///from user
				"ValueFrom" : "",			///from user
				"ValueTo" : "",				///from user
				"NextCondition":"AND",         ///from user
				"ID" : ""
			}
		};
		
	}
	
	$scope.CreateAudienceFun = function(){
		$scope.statusAudience.showSpinner = true;
		
		for(var i = 0; i < $scope.audienceModalDetails.length; i ++){
			$scope.audienceModalDetails[i].Value = ""+$scope.audienceModalDetails[i].Value+"";
		}
		$scope.audienceModal.data.Timing = ""+$scope.audienceModal.data.Timing+"";
		var d = {
			"Name" : $scope.newAudience.name,  ///from user
			"Description":"",   ///from user
			"Type":"Manual",
			"Props": $scope.audienceModalDetails
		}

		$scope.curveAPI('CreateAudience', {Token: $rootScope.Token,Data : JSON.stringify(d)}, CreateAudienceSuccess, CreateAudienceError);
	}
	
	function CreateAudienceSuccess(data){

		$scope.audiencesArr.unshift(data[0]);
		$scope.statusAudience.showSpinner = false;
	}
	
	function CreateAudienceError(data){
		console.log(data)
		$scope.statusAudience.showSpinner = false;
	}
	
	$scope.audienceEditItemFun = function(id){
		
		for(var i = 0; i < $scope.audienceModalDetails.length; i ++){
			if($scope.audienceModalDetails[i].itemID == id){
				var t = $scope.audienceModalDetails[i];
				var v = $scope.audienceModalDetails[i].Type;
				$scope.audienceModal = {'step' : 2,'valueArr' : [],'tempValueArr' : [],"ViewType" : v , 'data' : t};
				$scope.audienceDelItemFun(id);
				break;
			}
		}
	}
	
	
	$scope.audienceDelItemFun = function(id){
		
		for(var i = 0; i < $scope.audienceModalDetails.length; i ++){
			if($scope.audienceModalDetails[i].itemID == id){
				$scope.audienceModalDetails.splice(i, 1);
				break;
			}
		}
		
	}
	
	
                  /****************************   smart ******************************/

	$scope.activateBehaviorFun = function(data){
		$scope.behaviorsArrShow = false;
		var d = {
			"Name" : data.name,  ///from user
			"Description":"",   ///from user
			"Type":"Behavior",
			"Props": [{"ID" : data.id,"Type" : "Behavior"}]
		}
		
		$scope.curveAPI('CreateAudience', {Token: $rootScope.Token,Data : JSON.stringify(d)}, activateBehaviorSuccess, activateBehaviorError);
		
		function activateBehaviorSuccess(d){
			
			for(var i = 0;i < $scope.behaviorsArr.length; i ++){	
				if($scope.behaviorsArr[i].id == data.id){
					$scope.behaviorsArr[i].show = false;
				}	
			}
			$scope.audiencesArr.unshift(d[0]);
			$scope.behaviorsArrShow = true;
		}
		function activateBehaviorError(data){
			console.log(data)
			$scope.behaviorsArrShow = true;
		}
	}

	
	$scope.editAudienceNameFun = function(data){
		//http://localhost:8080/Curve/EditAudienceName?Token=TEST-YZMF8ICL6W8&Data={"AudienceId":"1","NewName":"Audience Name", "NewDescription":"Audience Description"}
		
		var id = data.audienceid;
		var name = data.newName;
		
		var d = {
			"AudienceId" : data.audienceid,
			"NewName" : data.newName,
			"NewDescription" : data.description
		}
		
		$scope.curveAPI('EditAudienceName', {Token: $rootScope.Token,Data : JSON.stringify(d)}, EditAudienceNameSuccess, EditAudienceNameError);
		
		function EditAudienceNameSuccess(data){
		
		
			for(var i = 0; i < $scope.audiencesArr.length; i ++){
				if($scope.audiencesArr[i].audienceid == id){
					$scope.audiencesArr[i].name = name;
					$scope.audiencesArr[i].newName = name;
					break;
				}
			}
		}
		function EditAudienceNameError(data){
			console.log(data)
		}
		
	}
	
	
	
	$scope.getAudienceListUsersFun = function(id,name){

		if(name == 'audience'){
		
			var getUsers = true;
			for(var i = 0; i < $scope.audiencesArr.length; i ++){
				if($scope.audiencesArr[i].audienceid == id){
					if($scope.audiencesArr[i].usersList){
						getUsers = false;
						$scope.showSampleUsers($scope.audiencesArr[i].audienceid,$scope.audiencesArr[i].name,$scope.audiencesArr[i].usersList);
		
						break;
					}
				}
			}
			
			if(getUsers){
				
				var d = {
					"AudienceId" : id,
				}
				//http://localhost:8080/Curve/GetAudienceSampleUsers?Token=TEST-YZMF8ICL6W8&Data={"AudienceId":"1"}	
				$scope.curveAPI('GetAudienceSampleUsers', {Token: $rootScope.Token,Data : JSON.stringify(d)}, GetAudienceSampleUsersSuccess, GetAudienceSampleUsersError);
				
			}
			
			function GetAudienceSampleUsersSuccess(data){
				
				for(var i = 0; i < $scope.audiencesArr.length; i ++){
					if($scope.audiencesArr[i].audienceid == data.AudienceId){
						
						$scope.audiencesArr[i].usersList = data.Users;
						$scope.showSampleUsers($scope.audiencesArr[i].audienceid,$scope.audiencesArr[i].name,$scope.audiencesArr[i].usersList);
						break;
		
					}
				}
			}
			
			function GetAudienceSampleUsersError(data){
				console.log(data)
			}
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		if(name == 'rank'){
		
			var getUsers = true;
			for(var i = 0; i < $scope.ranksArr.length; i ++){
				if($scope.ranksArr[i].rank.id == id){
					if($scope.ranksArr[i].usersList){
						getUsers = false;
						$scope.showSampleUsers($scope.ranksArr[i].rank.id,$scope.ranksArr[i].rank.name,$scope.ranksArr[i].usersList);
		
						break;
					}
				}
			}
			
			if(getUsers){
				
				var d = {
					"RankId" : id,
				}
				//localhost:8080/Curve/GetRankSampleUsers?Token=TEST-YZMF8ICL6W8&Data={"RankId":"1"}
				$scope.curveAPI('GetRankSampleUsers', {Token: $rootScope.Token,Data : JSON.stringify(d)}, GetRankSampleUsersSuccess, GetRankSampleUsersError);
				
			}
			
			function GetRankSampleUsersSuccess(data){
				
				for(var i = 0; i < $scope.ranksArr.length; i ++){
					if($scope.ranksArr[i].rank.id == data.RankId){
						
						$scope.ranksArr[i].usersList = data.Users;
						$scope.showSampleUsers($scope.ranksArr[i].rank.id,$scope.ranksArr[i].rank.name,$scope.ranksArr[i].usersList);
						break;
		
					}
				}
			}
			
			function GetRankSampleUsersError(data){
				console.log(data)
			}
		}
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		if(name == 'behavior'){

			var getUsers = true;
			for(var i = 0; i < $scope.newBehaviorsArr.length; i ++){
				for(var j = 0; j < $scope.newBehaviorsArr[i].data.length; j ++){
					if($scope.newBehaviorsArr[i].data[j].behavior.behaviorid == id){
						if($scope.newBehaviorsArr[i].data[j].usersList){
							getUsers = false;
							$scope.showSampleUsers($scope.newBehaviorsArr[i].data[j].behavior.behaviorid,$scope.newBehaviorsArr[i].data[j].behavior.name,$scope.newBehaviorsArr[i].data[j].usersList);
			
							break;
						}
					}
				}
			}
			
			if(getUsers){
				
				var d = {
					"BehaviorId" : id,
				}
				//localhost:8080/Curve/GetBehaviorSampleUsers?Token=TEST-YZMF8ICL6W8&Data={"BehaviorId":"1"}
				$scope.curveAPI('GetBehaviorSampleUsers', {Token: $rootScope.Token,Data : JSON.stringify(d)}, GetBehaviorSampleUsersSuccess, GetBehaviorSampleUsersError);
				
			}
			
			function GetBehaviorSampleUsersSuccess(data){
				
				for(var i = 0; i < $scope.newBehaviorsArr.length; i ++){
					for(var j = 0; j < $scope.newBehaviorsArr[i].data.length; j ++){
						if($scope.newBehaviorsArr[i].data[j].behavior.behaviorid == data.BehaviorId){
							
							$scope.newBehaviorsArr[i].data[j].usersList = data.Users;
							$scope.showSampleUsers($scope.newBehaviorsArr[i].data[j].behavior.behaviorid,$scope.newBehaviorsArr[i].data[j].behavior.name,$scope.newBehaviorsArr[i].data[j].usersList);
							break;
							
						}
					}
				}
				
			}
			
			function GetBehaviorSampleUsersError(data){
				console.log(data)
			}
		}
		
		
	}
	
	
	$scope.goToSingleUser = function(id){
		
		$('#sample_users_modal').modal('hide');
		$scope.usersSearch.input = id;
		
		$scope.goToSingleCustomerFun();
		 $('.nav-tabs a[href="#Single_Customer"]').tab('show');
		$scope.reloadSingleTab(id);
	}
	
	
	$scope.goToSingleCustomerFun = function(){
		
		for(var i = 0; i < $scope.dashboards.length; i ++){
			if($scope.dashboards[i].Parent == 'Advanced' && $scope.dashboards[i].Name == 'Single Customer'){
				$scope.setTabFun($scope.dashboards[i].ID,$scope.dashboards[i].Name);
			}
		}
	}
	
	$scope.getAudiencesStatusesFun = function(){

			
		$scope.curveAPI('GetAudiencesStatuses', {Token: $rootScope.Token}, GetAudiencesStatusesSuccess, GetAudiencesStatusesError);
	
	}
	
	function GetAudiencesStatusesSuccess(data){
		
		for(var i = 0; i < data.length; i ++){
			for(var j = 0; j < $scope.audiencesArr.length; j ++){
				if($scope.audiencesArr[j].audienceid == data[i].audienceid){
					
					$scope.audiencesArr[j].status = data[i].status;	
			
				}
			}
		}
		
	}
		
	function GetAudiencesStatusesError(data){
		console.log(data)
	}
	
	$scope.getAudStatIntervalFun = function(){
		
		var intervalAudienceStatus = $interval(function(){
			if($rootScope.dashboardView.active == 5){
				
				$scope.getAudiencesStatusesFun();
				
			}else{
				$interval.cancel(intervalAudienceStatus);
			}
		},30000);
				
	}
	
	
}
























