function DashBrdPrevFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter){

var currentPreviewDivID = "";
var call_id = 0;
var callID = "";
var call = null;

function showPreview()
{
	var divID = -1;
	var other = ['container_preview', 'container_preview_pie', 'container_preview_counter', 'container_preview_funnel'];
	var titles = ['preview_title', 'preview_title_pie', 'title_preview_counter', 'title_preview_funnel'];
	var graphs = ['chart_preview', 'chart_preview_pie', 'chart_preview_counter', 'chart_preview_funnel'];
	
	for(i=0;i<other.length;i++)
	{
		$("#" + graphs[i]).empty();
		$("#" + graphs[i]).append('<div id="preview_loader" style="background: transparent url(&quot;Images/AjaxLoader.gif&quot;) no-repeat scroll center center; width: 100%; height: 100%;"></div>');
	}
	
	var descs = [null, null, 'desc_preview_counter'];
	
	var graphName = document.getElementById('graph_title').value;
	var graphDescription = document.getElementById('graph_description').value;
	
	if(graphType == 'BasicChart' || graphType == 'BarChart' || graphType == 'amBarChart'|| graphType == 'amLineChart')
		divID = 0;
	else if(graphType == 'PieChart'|| graphType == 'amPieChart')
		divID = 1;
	else if(graphType == 'CounterChart')
		divID = 2;
	
	for(var i=0 ; i < other.length ; i++)
	{
		var div = document.getElementById(other[i]);
		var title = document.getElementById(titles[i]);
		var desc = document.getElementById(descs[i]);
		
		if(i == divID)
		{
			currentPreviewDivID = "#" + graphs[i];
			div.style.display = "";
			if(desc == null)
				graphName += " - " + graphDescription;
			else
				desc.innerHTML = graphDescription;
			
			title.innerHTML = graphName;
			
		}
		else
		{
			div.style.display = "none";
		}
	}
	
	var d = getResultData();	
	if(d == null) return;
	
	cancelPreview();	
	callID = "preview_" + (callID++);
	var data = getAPIGlobalParams(d, $('#dashboard-report-range').data('daterangepicker'));
	var call = $scope.curveAPI('GetCustomReportData', data, gotPreviewDataFunc, previewFailed);
	$scope.saveAjaxCall(callID, call);
}

function cancelPreview()
{
	if(callID != "")
		$scope.deleteAjaxCall(callID);
	
	callID = "";
}

function gotPreviewDataFunc(data)
{
	cancelPreview();
	displayGraphData(data);
}

function previewFailed(data)
{
	show_error('Something went wrong', 'Preview could not be presented at current time.');
	cancelPreview();
}

function displayGraphData(data)
{
	var chartData =	parseAllGraphData(graphType, data);

	var options = $.parseJSON(getGraphOptionsPreview(graphType));

	if(options == null)
		options = $scope.getGraphOptions(graphType);

	//if(options != null) $scope.addExtraFormatting(options, graphType);
	
	$scope.drawCustomGraph(currentPreviewDivID, graphType, options, chartData);
}

function getGraphOptionsPreview(type)
{
	if(type == 'PieChart')
		return '{"series":{"pie":{"show":true,"radius":1,"label":{"show":true,"radius":0.6666666666666666,"threshold":0.075}}},"legend":{"show":true}}';
	if(type == 'BasicChart')
		return '{"series":{"lines":{"show":true,"lineWidth":2,"fill":true,"fillColor":{"colors":[{"opacity":0.05},{"opacity":0.01}]}},"points":{"show":true,"fill":true,"radius":3,"lineWidth":1}},"xaxis":{"mode":"time","tickColor":"#eee", "ticks": 11, "minTickSize": [ 1, "day" ]	},"yaxis":{"tickColor":"#eee"},"grid":{"hoverable":true,"tickColor":"#eee","borderColor":"#eee","borderWidth":1},"colors":["#83AC28","#2071B7","#FAB900","#E84C3D","#3EA7EB"]}';
	if(type == 'BarChart')
		return '{"series":{"bars":{"show":true}},"bars":{"barWidth":82800000,"lineWidth":0,"shadowSize":0,"align":"right"},"grid":{"labelMargin":10,"hoverable":true,"clickable":true,"tickColor":"#eee","borderColor":"#eee","borderWidth":1},"xaxis":{"mode":"time","ticks":11,"tickColor":"#eee","minTickSize":[1,"day"]},"yaxis":{"tickColor":"#eee"},"legend":{"backgroundColor":"#eee","labelBoxBorderColor":"none"},"colors":["#83cdff","#add63c","#cccccc"]}';
	
	return null;
}

}