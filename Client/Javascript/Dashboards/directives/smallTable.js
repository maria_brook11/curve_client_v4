
app.directive('smallTable',
	function () {
		return {
			restrict: 'E',
			replace:true,
			scope:true,
			link: function (scope, element, attrs) {
			
			var curID = element.parent().attr("id");
			var curData;
			var options = {'colors' : ["#06789e","#88ce5c","#d1e05c","#afe1c1","#b4d6e2"]};	
			for (var i = 0; i <= scope.activeTab.dashboardActive.Graphs.length; i++) {
		
				if (scope.activeTab.dashboardActive.Graphs[i].ID == curID) {
					 
					curData = scope.activeTab.dashboardActive.Graphs[i].dataGraph;
					break;
				}
			}
		
	var graphData = scope.parseGraphData(curData);
	var table = scope.fillTable(element, graphData, options);
	element.html(table);	
			

			}	
			
		}
	}
);

 app.directive('mltablekoko',
	function () {
		return {
			restrict: 'A',
			replace:true,
			scope:true,
			link: function (scope, element, attrs) {
				
			var options = {'colors' : ["#06789e","#88ce5c","#d1e05c","#afe1c1","#b4d6e2"]};	
			var curData = scope.MLMagnitudeGraph;
			var resp = [];
			function parseData(curData){
				
				
				var labels = [];
				for(var i = 0; i < curData.MetaData.length ; i ++){
					var t = curData.MetaData[i].label;
					labels.push(t)
				}
				var names = [];
				for(var i = 0; i < curData.Data.length ; i ++){
					var t = curData.Data[i].name;
					names.push(t)
				}
				
				var ArrData = [];
				
				 for (var i = 0; i < labels.length; i++)
					{ 
		
						var series1 = {};
						series1.label = labels[i];
						series1.data = [];

						for (var q = 0; q < curData.Data.length ; q++)
							{
								var temp = ["1",curData.Data[q].dataPoints[i].y];
								series1.data.push(temp)		
							}
							
						ArrData.push(series1)
					} 
					
				var series = {};
				series.label = "Segment";
				series.data = [];
				for (var i =0; i < names.length; i++)
					{
						var temp = ["1",names[i]];
						series.data.push(temp);
					}
				
				ArrData.unshift(series) 
								 
				return ArrData;
			}
			
	var graphData = parseData(curData);
	var table = scope.fillTable(element, graphData, options);
	element.html(table);	
			

			}	
			
		}
	}
);