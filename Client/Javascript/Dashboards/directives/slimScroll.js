app.directive('slimscroll', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var h = angular.element(element).css('height');
				
                angular.element(element).slimScroll(
                        {position: 'right', height: h, allowPageScroll: false, disableFadeOut: true, wheelStep: '5'}
                );
            }
        }
    })