app.directive("omnichart", function($compile){
  return{
		restrict: 'A',
		replace:true,
		scope:true,
        link: function(scope, elem, attrs) {
		
		var curID = elem.parent().parent().attr("id");

		var curData;
		for (var i = 0; i < scope.activeTab.dashboardActive.Graphs.length; i++) {
			if (scope.activeTab.dashboardActive.Graphs[i].ID == curID) {
				curData = scope.activeTab.dashboardActive.Graphs[i].dataGraph;
				parseOmniChart();
				break;
			}
		}

	/*y.str={tv:[222] canvas.min.js*/	
	
	function parseOmniChart(){
		
		var data = curData.chartData.ReportDataTable;
		
		scope.audiencesListForOmni = data.Products;
		
		if(data.ROI.current){
			var roi = 'C '+data.ROI.current+'$   O '+data.ROI.optimized+'$   P '+data.ROI.prediction+'$';
		}else{
			var roi = 'C 112086$   O 344905$   P 126101$';
		}
		/* ROI : {
			"current" : "112086",
			"optimized" : "344905",
			"prediction" : "126101"
		} */
		var pricesArr = [
					{ x: 1 , y: 3,indexLabel: ""+data.Marketing.prices.offline+"$" ,markerType: "triangle",markerColor:  'transparent', indexLabelFontColor: "red"},
					{ x: 1 , y: 94,indexLabel: ""+data.Marketing.prices.online+"$" ,markerType: "triangle",markerColor:  'transparent', indexLabelFontColor: "red"},
					{ x: 2 , y: 3,indexLabel: ""+data.Purchase.prices.offline+"$" ,markerType: "triangle",markerColor:  'transparent', indexLabelFontColor: "green"},
					{ x: 2 , y: 94, indexLabel: ""+data.Purchase.prices.online+"$" ,markerType: "triangle",markerColor:  'transparent', indexLabelFontColor: "green"},
					{ x: 3 , y: 3,indexLabel: ""+data.Support.prices.offline+"$" ,markerType: "triangle",markerColor:  'transparent', indexLabelFontColor: "red"},
					{ x: 3 , y: 94,indexLabel: ""+data.Support.prices.online+"$" ,markerType: "triangle",markerColor:  'transparent', indexLabelFontColor: "red"},
					{ x: 4 , y: 3,indexLabel: ""+data.Delivery.prices.offline+"$" ,markerType: "triangle",markerColor:  'transparent', indexLabelFontColor: "red"},
					{ x: 4 , y: 94, indexLabel: ""+data.Delivery.prices.online+"$" ,markerType: "triangle",markerColor:  'transparent', indexLabelFontColor: "red"}
				]
				
		var currentGraph = [
						{ x: 1, y: calcNumber(parseInt(data.Marketing.data.current)),toolTipContent: ""+data.Marketing.data.current+"%" },
						{ x: 2, y: calcNumber(parseInt(data.Purchase.data.current)),toolTipContent: ""+data.Purchase.data.current+"%" },
						{ x: 3, y: calcNumber(parseInt(data.Support.data.current)),toolTipContent: ""+data.Support.data.current+"%" },
						{ x: 4,  y: calcNumber(parseInt(data.Delivery.data.current)) ,toolTipContent: ""+data.Delivery.data.current+"%"}]
						
		var optimizedGraph = [
						{ x: 1, y: calcNumber(parseInt(data.Marketing.data.optimized)),toolTipContent: ""+data.Marketing.data.optimized+"%" },
						{ x: 2, y: calcNumber(parseInt(data.Purchase.data.optimized)),toolTipContent: ""+data.Purchase.data.optimized+"%" },
						{ x: 3, y: calcNumber(parseInt(data.Support.data.optimized)),toolTipContent: ""+data.Support.data.optimized+"%" },
						{ x: 4,  y: calcNumber(parseInt(data.Delivery.data.optimized)) ,toolTipContent: ""+data.Delivery.data.optimized+"%"}]
		
		var predictionGraph = [
						{ x: 1, y: calcNumber(parseInt(data.Marketing.data.prediction)),toolTipContent: ""+data.Marketing.data.prediction+"%" },
						{ x: 2, y: calcNumber(parseInt(data.Purchase.data.prediction)),toolTipContent: ""+data.Purchase.data.prediction+"%" },
						{ x: 3, y: calcNumber(parseInt(data.Support.data.prediction)),toolTipContent: ""+data.Support.data.prediction+"%" },
						{ x: 4,  y: calcNumber(parseInt(data.Delivery.data.prediction)),toolTipContent: ""+data.Delivery.data.prediction+"%" }]
						
		/* var predictionGraph = [
						{ x: 1, y: calcNumber(9) ,toolTipContent: "9%" },
						{ x: 2, y: calcNumber(93),toolTipContent: "93%" },
						{ x: 3, y: calcNumber(0),toolTipContent: "0%" },
						{ x: 4,  y: calcNumber(100),toolTipContent: "100%" }] */
						
		startCanvas(pricesArr,currentGraph,optimizedGraph,predictionGraph,roi);
				
	}
	
	
	function calcNumber (x){
		
		var y = Math.floor((84 / 100) * x + 9);
		return y;
	}
	
	function calcColor(x){
		
		var y = x.indexOf('-');
		if(y == -1){
			z = "green";
		}else{
			z = "red";
		}
		return z;
	}
	
	
	function startCanvas(pricesArr,currentGraph,optimizedGraph,predictionGraph,roi){
		
        var dom = '<div id="chartContainer" style="width: 1100px; height: 535px;" ></div>';
        var compiled = $compile(dom)(scope);
        angular.element($('#container_omnichart')).prepend(compiled);
		
        var chart = new CanvasJS.Chart("chartContainer",   
		{
			zoomEnabled: false,
			animationEnabled: false,
			backgroundColor: "transparent",
			theme: "none",
			title:{
				text: roi,
				fontSize: 20,
				verticalAlign: "top",
				horizontalAlign: "left"
				
			},
			axisY:{
				title: "",
				tickLength: 0,
				lineThickness:0,
				margin:0,
				valueFormatString:" " //comment this to show numeric values
			},
			axisX:{
       
				lineThickness:0,
				margin:0,
				tickLength:0,
				valueFormatString:"" //comment this to show numeric values
			},
			legend:{
				verticalAlign: "top",
				horizontalAlign: "center",
				fontSize: 15
			},
			data: [
				{
					type: "column",
					color: "transparent",
					fillOpacity: 1,
					highlightEnabled: false,
					dataPoints: [
						{ label: " ",x: 1, y: 100 ,toolTipContent: "Marketing"},
						{ label: " ", x: 2, y: 100 ,toolTipContent: "On-Site"},
						{ label: " ", x: 3, y: 100 ,toolTipContent: "Support"},
						{ label: " ",x: 4, y: 100 ,toolTipContent: "Delivery"}]
				},
				{        
					type: "line",
					lineThickness:3,
					lineColor: "#41B7E9",
					markerColor: "#41B7E9",
					showInLegend: true,           
					name: "current",
					dataPoints: currentGraph
				},
				{        
					type: "line",
					lineThickness:3,
					lineColor: "#ffee8c",
					markerColor: "#ffee8c",
					showInLegend: true,           
					name: "optimized",
					dataPoints: optimizedGraph
				},
				{        
					type: "line",
					lineThickness:3,
					lineColor: "#afe1c1",
					markerColor: "#afe1c1",
					showInLegend: true,           
					name: "prediction",
					dataPoints: predictionGraph
				},
				{
				type: "bubble",
				color: "transparent",
				dataPoints: pricesArr
				}
			],
			legend: {
				cursor:"pointer",
				itemclick : function(e) {
					if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
						e.dataSeries.visible = false;
					}else{
						e.dataSeries.visible = true;
					}
					chart.render();
					
				}
			}
        });
		
        chart.render(); 
       
		var images = [];
		var beverages= [];
		
		images.push({url: "Images/cssApp/big/marketing.png"});
		images.push({url: "Images/cssApp/big/onsite.png"});
		images.push({url: "Images/cssApp/big/support.png"});
		images.push({url: "Images/cssApp/big/delivery.png"});
	  

		addImages(chart);

		function addImages(chart){
			for(var i = 0; i < chart.data[0].dataPoints.length; i++){
			  var label = chart.data[0].dataPoints[i].label;
			  
			  if(label){
				beverages.push( $("<img>").attr("src", images[i].url)
							.attr("class", label)
							.css("display", "none").css("z-index", "-10")
							.appendTo($("#container_omnichart #chartContainer>.canvasjs-chart-container"))
						   );        
			  }
			  
			  positionImage(beverages[i], i);
			}    
		}
		  
		function positionImage(beverage, index){ 
			var imageBottom = chart.axisX[0].bounds.y1;     
			var imageCenter = chart.axisX[0].convertValueToPixel(chart.data[0].dataPoints[index].x);
			var yInPixels = chart.axisY[0].convertValueToPixel(chart.data[0].dataPoints[index].y);
			
		   // beverage.height(imageBottom - yInPixels);
		   //beverage.width(beverage.height() * .275);
			beverage.height(440);
			beverage.width(200);
			beverage.css({"position": "absolute", 
					   "display": "block",
					   "top": imageBottom  - beverage.height()-10,
					   "left": imageCenter - beverage.width()/2
					  });
			chart.render();
		}
		  
		$( window ).resize(function() {
			for(var i = 0; i < chart.data[0].dataPoints.length; i++){
				positionImage(beverages[i], i);
			}
		}); 


	}
	  
		
		}
	  }
}); 




app.directive("mlchart", function($compile){
  return{
		restrict: 'A',
		replace:true,
		scope: {
				graphData: "=graphData"
				
			 },
        link: function(scope, elem, attrs) {
		
		
		var divBox = ''+attrs.type+'_'+attrs.grid+'';

		
		/*var curData;
		for (var i = 0; i < scope.activeTab.dashboardActive.Graphs.length; i++) {
			if (scope.activeTab.dashboardActive.Graphs[i].ID == curID) {
				curData = scope.activeTab.dashboardActive.Graphs[i].dataGraph;
				parseMLMagnitudeChart();
				
				break;
			}
		}
		 */
	
		/* var colors = [
		"#FD05AA",
		"#FD05FD",
		"#9F05FD",
		"#0509FD",
		"#0563FD", 
		"#05B2FD",
		"#05DFFD",
		"#05F9FD",
		"#05FDEA",
		"#33FFD1"
		] */
		
 	function parseMLMagnitudeChart(){
		
		//var data = curData.chartData.ReportDataTable;
		//var data = scope.MLMagnitudeGraph;

		var data = scope.graphData;
		var metaData = data.MetaData;
		var BorderLine = data.BorderLine;
		var chartDataObj;
		var colors = [
				"#FF5CFF",
				"#FD05FD",
				"#9F05FD",
				"#5B05FD",
				"#0509FD",
				"#6568FF",
				"#0563FD", 
				"#05B2FD",
				"#05DFFD",
				"#65E4FE" 
				];
		
		var chartData = [
              {        
				type: "line",
                markerType: "none",
                axisXIndex: 1,
                showInLegend: false,
				color: "#f2f2f2",
				dataPoints: metaData					
			},
			  {        
				type: "line",
                markerType: "none",
                axisXIndex: 1,
                showInLegend: false,
				color: "#f2f2f2",
				dataPoints: BorderLine
				/* [
					{x: 1, y:60},     
					{x: 2, y: 60},     
					{x: 3, y: 60},
					{x: 4, y:60},     
					{x: 5, y: 60},     
					{x: 6, y: 60},
					{x: 7, y:60},     
					{x: 8, y: 60},     
					{x: 9, y: 60},
					{x: 10, y:60},     
					{x: 11, y: 60},     
					{x: 12, y: 60},
					{x: 13, y:60},     
					{x: 14, y: 60},
					{x: 15, y: 60},
					{x: 16, y: 60},
					{x: 17, y:60},     
					{x: 18, y: 60},
					{x: 19, y: 60},
					{x: 20, y: 60}				
									
				] */
			}
			
			];
		
		
		for(var i=0; i< data.Data.length;i++){
			var temp = {        
				type: "splineArea",
				name: data.Data[i].name,
              	color: colors[i],
              	showInLegend: true, 
              	markerType: "none",
              	legendMarkerType: "square",
              	axisXIndex: 0,
				dataPoints: data.Data[i].dataPoints
			};
			
			chartData.push(temp);
			
		}
		startMLCanvas(chartData);
		//setTimeout(function(){  }, 3000);
		
	}	
	parseMLMagnitudeChart();

	function startMLCanvas(chartData){

        /*  var dom = '<div id="chartContainer" style="width: 430px; height: 300px;" ></div>';
        var compiled = $compile(dom)(scope); 
        angular.element($('#container_mlchart_'+divBox+'')).prepend(compiled);  */
		
		
		setTimeout(function(){
		
        var chart = new CanvasJS.Chart("chartContainer_"+divBox+"",   
		{
          	zoomEnabled: false,
			animationEnabled: false,
			backgroundColor: "transparent",
			theme: "none",
			title:{
				text: "Statistics Chart"
			},
          	toolTip:{
        		enabled: false, 
        		animationEnabled: false 
      		},
           axisX:{
             
        		gridColor: "#f2f2f2",
				lineColor: "#f2f2f2",
				tickColor: "#f2f2f2",
				labelFontColor: "#333333",
				labelFontSize: 13,
        		gridThickness: 1 ,
				margin:10,
				interval: 1
      		},
          	axisY:{
				lineColor: "#FFFFFF",
				labelFontColor: "#FFFFFF",
				tickLength: 0,
				margin:10,
				minimum: -60,
				maximum: 60,
				valueFormatString:" "
			},
          
			legend: {
				cursor:"pointer",
				fontSize: 11,
				itemclick : function(e) {
					if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
						e.dataSeries.visible = false;
					}else{
						e.dataSeries.visible = true;
					}
					chart.render();
					
				}
			},
          
			data: chartData
		});

        chart.render(); 


	  }, 6000);
			} 
		}
	}
}); 

/* ,
			{        
				type: "splineArea",
				name: "NEW SPENDERS",
              	color: colors[0],
              	showInLegend: true, 
              	markerType: "none",
              	legendMarkerType: "square",
              	axisXIndex: 0,
				dataPoints: [
				{x: 1, y: 0},     
				{x: 2, y: -27},     
				{x: 3, y: -33},
				{x: 4, y: -25},     
				{x: 5, y: -27},     
				{x: 6, y: 33},
				{x: 7, y: 25},     
				{x: 8, y: 27},     
				{x: 9, y: 33},
				{x: 10, y: 25},     
				{x: 11, y: -27},     
				{x: 12, y: -33},
				{x: 13, y: 25},     
				{x: 14, y: -27},     
				{x: 15, y: 33},
				{x: 16, y: 25},     
				{x: 17, y: -27},     
				{x: 18, y: 0}				
				]
			},
            {        
				type: "splineArea",
				name: "LOYAL CUSTOMERS",
                color: colors[1],
                showInLegend: true,
                markerType: "none",
                legendMarkerType: "square",
                axisXIndex: 0,
				dataPoints: [
                {x: 1, y: 0},     
				{x: 2, y: -30},     
				{x: 3, y: -33},
				{x: 4, y: -15},     
				{x: 5, y: 27},     
				{x: 6, y: 33},
				{x: 7, y: 25},     
				{x: 8, y: 27},     
				{x: 9, y: -33},
				{x: 10, y: -40},     
				{x: 11, y: -27},     
				{x: 12, y: 33},
				{x: 13, y: 25},     
				{x: 14, y: 27},     
				{x: 15, y: -13},
				{x: 16, y: -22},     
				{x: 17, y: -27},     
				{x: 18, y: 0}		 
				]
			},
			
			{        
				type: "splineArea",
				name: "BIG SPENDERS",
                color: colors[2],
                showInLegend: true,
                markerType: "none",
                legendMarkerType: "square",
                axisXIndex: 0,
				dataPoints: [
                {x: 1, y: 0},     
				{x: 2, y: 3},     
				{x: 3, y: 13},
				{x: 4, y: 15},     
				{x: 5, y: -2},     
				{x: 6, y: -7},
				{x: 7, y: -10},     
				{x: 8, y: -10},     
				{x: 9, y: -10},
				{x: 10, y: 4},     
				{x: 11, y: 40},     
				{x: 12, y: 45},
				{x: 13, y: 50},     
				{x: 14, y: 45},     
				{x: 15, y: -10},
				{x: 16, y: -2},     
				{x: 17, y: -2},     
				{x: 18, y: 0}		 
				]
			},
			{        
				type: "splineArea",
				name: "LOYAL JOES",
                color: colors[3],
                showInLegend: true,
                markerType: "none",
                legendMarkerType: "square",
                axisXIndex: 0,
				dataPoints: [
                {x: 1, y: 0},     
				{x: 2, y: 15},     
				{x: 3, y: 16},
				{x: 4, y: 17},     
				{x: 5, y: 20},     
				{x: 6, y: 48},
				{x: 7, y: 20},     
				{x: 8, y: 27},     
				{x: 9, y: -35},
				{x: 10, y: -45},     
				{x: 11, y: -29},     
				{x: 12, y: 33},
				{x: 13, y: 25},     
				{x: 14, y: 27},     
				{x: 15, y: -13},
				{x: 16, y: -50},     
				{x: 17, y: -50},     
				{x: 18, y: 0}		 
				]
			},
			{        
				type: "splineArea",
				name: "ALMOST LOST",
                color: colors[4],
                showInLegend: true,
                markerType: "none",
                legendMarkerType: "square",
                axisXIndex: 0,
				dataPoints: [
                {x: 1, y: 0},     
				{x: 2, y: -10},     
				{x: 3, y: -23},
				{x: 4, y: -25},     
				{x: 5, y: -27},     
				{x: 6, y: -33},
				{x: 7, y: 0},     
				{x: 8, y: 7},     
				{x: 9, y: 45},
				{x: 10, y: 40},     
				{x: 11, y: 27},     
				{x: 12, y: 50},
				{x: 13, y: 0},     
				{x: 14, y: -27},     
				{x: 15, y: -13},
				{x: 16, y: -45},     
				{x: 17, y: -20},     
				{x: 18, y: 0}		 
				]
			},
			{        
				type: "splineArea",
				name: "BEST CUSTOMERS",
                color: colors[5],
                showInLegend: true,
                markerType: "none",
                legendMarkerType: "square",
                axisXIndex: 0,
				dataPoints: [
                {x: 1, y: 0},     
				{x: 2, y: -35},     
				{x: 3, y: -3},
				{x: 4, y: -15},     
				{x: 5, y: 27},     
				{x: 6, y: 33},
				{x: 7, y: 45},     
				{x: 8, y: 50},     
				{x: 9, y: -35},
				{x: 10, y: -40},     
				{x: 11, y: -27},     
				{x: 12, y: -33},
				{x: 13, y: 25},     
				{x: 14, y: 27},     
				{x: 15, y: 13},
				{x: 16, y: 22},     
				{x: 17, y: -7},     
				{x: 18, y: 0}		 
				]
			},
			{        
				type: "splineArea",
				name: "LOST CUSTOMERS",
                color: colors[6],
                showInLegend: true,
                markerType: "none",
                legendMarkerType: "square",
                axisXIndex: 0,
				dataPoints: [
                {x: 1, y: 0},     
				{x: 2, y: 3},     
				{x: 3, y: 13},
				{x: 4, y: 15},     
				{x: 5, y: 0},     
				{x: 6, y: 3},
				{x: 7, y: 25},     
				{x: 8, y: 27},     
				{x: 9, y: -3},
				{x: 10, y: -40},     
				{x: 11, y: -27},     
				{x: 12, y: -33},
				{x: 13, y: 0},     
				{x: 14, y: 50},     
				{x: 15, y: 13},
				{x: 16, y: 12},     
				{x: 17, y: 17},     
				{x: 18, y: 0}		 
				]
			},
			{        
				type: "splineArea",
				name: "EARLY ADAPTERS",
                color: colors[7],
                showInLegend: true,
                markerType: "none",
                legendMarkerType: "square",
                axisXIndex: 0,
				dataPoints: [
                {x: 1, y: 0},     
				{x: 2, y: -50},     
				{x: 3, y: -39},
				{x: 4, y: -25},     
				{x: 5, y: -47},     
				{x: 6, y: -33},
				{x: 7, y: -25},     
				{x: 8, y: 7},     
				{x: 9, y: 33},
				{x: 10, y: -40},     
				{x: 11, y: -27},     
				{x: 12, y: -34},
				{x: 13, y: -24},     
				{x: 14, y: -27},     
				{x: 15, y: -3},
				{x: 16, y: -2},     
				{x: 17, y: -7},     
				{x: 18, y: 0}		 
				]
			},
			{        
				type: "splineArea",
				name: "PRICEY",
                color: colors[8],
                showInLegend: true,
                markerType: "none",
                legendMarkerType: "square",
                axisXIndex: 0,
				dataPoints: [
                {x: 1, y: 0},     
				{x: 2, y: 13},     
				{x: 3, y: 22},
				{x: 4, y: 8},     
				{x: 5, y: 17},     
				{x: 6, y: 45},
				{x: 7, y: 25},     
				{x: 8, y: 29},     
				{x: 9, y: -23},
				{x: 10, y: -4},     
				{x: 11, y: -17},     
				{x: 12, y: -13},
				{x: 13, y: 0},     
				{x: 14, y: 27},     
				{x: 15, y: -13},
				{x: 16, y: -42},     
				{x: 17, y: -17},     
				{x: 18, y: 0}		 
				]
			},
			{        
				type: "splineArea",
				name: "CONSERVATIVE",
                color: colors[9],
                showInLegend: true,
                markerType: "none",
                legendMarkerType: "square",
                axisXIndex: 0,
				dataPoints: [
                {x: 1, y: 0},     
				{x: 2, y: 30},     
				{x: 3, y: 33},
				{x: 4, y: 15},     
				{x: 5, y: 27},     
				{x: 6, y: 33},
				{x: 7, y: 25},     
				{x: 8, y: 27},     
				{x: 9, y: 33},
				{x: 10, y: 40},     
				{x: 11, y: 27},     
				{x: 12, y: 33},
				{x: 13, y: 25},     
				{x: 14, y: 27},     
				{x: 15, y: -50},
				{x: 16, y: -22},     
				{x: 17, y: -47},     
				{x: 18, y: 0}		 
				]
			} */

