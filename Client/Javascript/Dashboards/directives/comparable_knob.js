

var ComparableKnob = function(canvas_id, vs_text, options)
{
	var inner_canvas_width_factor = .84;
	var c = document.getElementById(canvas_id);
	var mycanvas = document.createElement("canvas");
	
	if(ComparableKnob.height == -1)
	{
		ComparableKnob.height  = c.offsetHeight*inner_canvas_width_factor;

	}
	
	mycanvas.width = ComparableKnob.height;
	mycanvas.height = ComparableKnob.height;
	
		
	c.appendChild(mycanvas);
	
	/* if(canvas_id == 'canvas_div_id_227')
		console.log('mycanvas: ', canvas_id, mycanvas, c);
	 */
	this.ctx = mycanvas.getContext("2d");
	this.options = options;
	this.vs_text = vs_text;
	this.setDefaultValuesToOptions();
	
	if(this.options.knob_color != null)
		this.precentage_fill = this.options.knob_color;
};

ComparableKnob.height = -1;

ComparableKnob.prototype = 
{
	background_fill: "#fff",
	precentage_fill: "#42b8ea",
	outer_ring_fill: "#e8e8e8",
	inner_ring_fill: "#f7f7f7",
	text_color: "#000",
	text_size: "30px",
	subtext_size: "12px",
	font_name: "Open Sans",
	
	refresh: function()
	{
	
	},
	
	draw: function(precent, value)
	{
		if(this.options.type == "thick")
		{
			this.drawKnobThick(this.ctx, precent, value);
		}
		else
		{
			this.drawKnobThin(this.ctx, precent, value);
		}
	},
	
	drawKnobThick: function (ctx, precent, val)
	{
		var c = ctx.canvas.height*.5;
		
		this.buildRect(ctx, c);
		
		// outer ring
		ctx.beginPath();
		ctx.arc(c,c,c,0, Math.PI*2, false); // outer (filled)
		ctx.arc(c,c,c-c*.1,0,Math.PI*2, true); // outer (filled)
		ctx.fillStyle = this.outer_ring_fill;
		ctx.fill();


		var lineWdt = c - c*.1 - (c*.5 + 5);
		
		// full white precentage ring
		ctx.beginPath();
		ctx.strokeStyle = this.background_fill; // get the color
		ctx.lineWidth = lineWdt;
		ctx.arc(c, c, c - c*.1-(lineWdt*.5), -Math.PI/2, -Math.PI/2 + 2*Math.PI); 
		ctx.stroke();
		
		// precentage ring
		ctx.beginPath();
		ctx.strokeStyle = this.precentage_fill; // get the color
		ctx.lineWidth = lineWdt;
		
		if(precent > 0)
			ctx.arc(c, c, c - c*.1-(lineWdt*.5), -Math.PI/2, -Math.PI/2 + 2*Math.PI*precent/100); 
		else
			ctx.arc(c, c, c - c*.1-(lineWdt*.5), -Math.PI/2 + 2*Math.PI*precent/100, -Math.PI/2); 
		
		ctx.stroke();

		// inner small ring
		ctx.beginPath();
		ctx.fillStyle = this.inner_ring_fill;
		ctx.arc(c,c,c*.5,0,Math.PI*2, false); // outer (filled)
		ctx.arc(c,c,c*.5+5,0,Math.PI*2, true); // outer (unfills it)
		ctx.fill();
		
		// inner fill
		ctx.beginPath();
		ctx.fillStyle = this.background_fill;
		ctx.arc(c,c,c*.73,0,Math.PI*2, false); // outer (filled)
		ctx.fill();

		this.setText(c, precent, val);
	},
	
	drawKnobThin: function (ctx, precent, val)
	{
		var c = ctx.canvas.width*.5;
		var outer_width = 0.2;
		this.buildRect(ctx, c);
		
		// outer ring
		ctx.beginPath();
		ctx.arc(c,c,c,0, Math.PI*2, false); // outer (filled)
		ctx.arc(c,c,c-c*outer_width ,0 ,Math.PI*2 , true); // outer (filled)
		ctx.fillStyle = this.outer_ring_fill;
		ctx.fill();


		var lineWdt = c - c*.1 - (c*.83 + 5);
		
		// full white precentage ring
		ctx.beginPath();
		ctx.strokeStyle = this.background_fill; // get the color
		ctx.lineWidth = lineWdt;
		ctx.arc(c, c, c - c*outer_width - (lineWdt*.5), -Math.PI/2, -Math.PI/2 + 2*Math.PI); 
		ctx.stroke();

		// precentage ring
		ctx.beginPath();
		ctx.strokeStyle = this.precentage_fill; // get the color
		ctx.lineWidth = lineWdt;
		
		if(precent > 0)
			ctx.arc(c, c, c - c*outer_width - (lineWdt*.5), -Math.PI/2, -Math.PI/2 + 2*Math.PI*precent/100); 
		else
			ctx.arc(c, c, c - c*outer_width - (lineWdt*.5), -Math.PI/2 + 2*Math.PI*precent/100, -Math.PI/2); 
		
		
		ctx.stroke();

		// inner small ring
		ctx.beginPath();
		ctx.fillStyle = this.inner_ring_fill;
		ctx.arc(c,c,c*.73,0,Math.PI*2, false); // outer (filled)
		ctx.arc(c,c,c*.73+5,0,Math.PI*2, true); // outer (unfills it)
		ctx.fill();
		
		// inner fill
		ctx.beginPath();
		ctx.fillStyle = this.background_fill;
		ctx.arc(c,c,c*.73,0,Math.PI*2, false); // outer (filled)
		ctx.fill();

		this.setText(c, precent, val);
	},
	
	setText: function (c, precent, val)
	{
		return;
		this.ctx.fillStyle = this.text_color;
		this.ctx.font = this.text_size + " " + this.font_name;
		this.ctx.textAlign = "center";
		this.ctx.fillText(this.formatNumber(val) , c+2, c+9);

		this.ctx.fillStyle = this.text_color;
		this.ctx.font = "lighter " + this.subtext_size + " " + this.font_name;
		this.ctx.textAlign = "center";
		this.ctx.fillText(precent+"%"/*+ " " + this.vs_text*/, c+2, c+30);
	},
	
	buildRect: function(ctx, c)
	{
		if(this.options != null && this.options.show_rect)
		{
			// outer rect
			ctx.beginPath();						
			ctx.rect(c,0,c,c); 
			ctx.fillStyle = this.inner_ring_fill;
			ctx.fill();
		}
	},
	
	formatNumber: function(val)
	{
		try
		{
			var integer = parseFloat(val);
			if(isNaN(integer) || integer == null)
				return val;
				
			var str = integer.toString();
			
			var pattern = /(-?\d+)(\d{3})/;
			while (pattern.test(str))
				str = str.replace(pattern, "$1,$2");
			return str;
			
		}
		catch(e){return val;}
	},
	
	setDefaultValuesToOptions: function()
	{
		if(this.options == null)
			this.options = {type: "thin", show_rect: false};
		else
		{
			if(this.options.type == null)
				this.options.type = "thin";
			if(this.options.show_rect == null)
				this.options.show_rect = false;
			
		}
	}
}