
app.directive('graphmap',['CountryConst',
	function (CountryConst) {
		return {
			restrict: 'A',
			replace:true,
			scope:true,
			link: function (scope, element, attrs) {
			
			var curID = element.parent().attr("id");
			var htmlText = '<div id="map'+curID+'" style="min-width: 310px; height: 300px; margin: 0 auto"></div>'
			element.html(htmlText);
			
			var curData = [];	
			//console.log(curID)
			for (var i = 0; i <= scope.activeTab.dashboardActive.Graphs.length; i++) {
		
				if (scope.activeTab.dashboardActive.Graphs[i].ID == curID) {
					 
					curData = scope.activeTab.dashboardActive.Graphs[i].dataGraph.chartData.ReportDataTable.Records;
					
					break;
				}
			}
	
				// get min and max values
				var minBulletSize = 3;
				var maxBulletSize = 70;
				var min = Infinity;
				var max = -Infinity;
				var total = 0;
				
			
				for ( var i = 0; i < curData.length; i++ ) {
					var value11 = parseInt(curData[i][0]);
					total += value11;
					if ( value11 < min ) {min = value11;}
					if ( value11 > max ) {max = value11;}
				}

				// it's better to use circle square to show difference between values, not a radius
				var maxSquare = maxBulletSize * maxBulletSize * 2 * Math.PI;
				var minSquare = minBulletSize * minBulletSize * 2 * Math.PI;

				// create circle for each country
				var images = [];
				var maxdev = max/total;
					for ( var i = 0; i < curData.length; i++ ) {
						var dataItem = curData[i];
						var value = parseInt(dataItem[0]);
						// calculate size of a bubble
						var square = ( value - min ) / ( max - min ) * ( maxSquare - minSquare ) + minSquare;
						if ( square < minSquare ) {square = minSquare;}
						var size = Math.sqrt( square / ( Math.PI * 2 ) );
						var id = dataItem[1];
						
						var col = getMidColor((value/total/maxdev), '06789e', '5ecff4');
						var color = "#" + col;
						var code = dataItem[1].toUpperCase();
						var usersNum = dataItem[0];
						var CountryName = getCountryName(code);
						var title = ''+CountryName+': '+value+'';
						if (CountryConst.latlong[id])
						{
							images.push({
								"type": "circle",
								"theme": "light",
								"width": size,
								"height": size,
								"color": color,
								"alpha": 0.6,
								"longitude": CountryConst.latlong[id].longitude,
								"latitude": CountryConst.latlong[id].latitude,
								"title": title
								/* "value": value */
							});
						}
						
				}
				
				var chart = false;

				var initChart = function() {
					
					if (chart) chart.destroy();
					
					 chart = AmCharts.makeChart( "map"+curID+"", {
						"type": "map",
						"projection": "eckert6",
						"areasSettings": {
						},
						"dataProvider": {
							"map": "worldLow",
							"images": images
						},
						"export": {
							"enabled": true
						}
					}); 
					
					
/* var chart = AmCharts.makeChart( "chartdiv", {
  "type": "radar",
  "theme": "light",
  "dataProvider": [ {
    "country": "Czech Republic",
    "litres": 156.9
  }, {
    "country": "Ireland",
    "litres": 131.1
  }, {
    "country": "Germany",
    "litres": 115.8
  }, {
    "country": "Australia",
    "litres": 109.9
  }, {
    "country": "Austria",
    "litres": 108.3
  }, {
    "country": "UK",
    "litres": 99
  } ],
  "valueAxes": [ {
    "axisTitleOffset": 20,
    "minimum": 0,
    "axisAlpha": 0.15
  } ],
  "startDuration": 2,
  "graphs": [ {
    "balloonText": "[[value]] litres of beer per year",
    "bullet": "round",
    "lineThickness": 2,
    "valueField": "litres"
  } ],
  "categoryField": "country",
  "export": {
    "enabled": true
  }
} ); */
					
					chart.imagesSettings = {"balloonText": "[[title]]", "alpha": 0.6};

				};
				
				initChart();
		
			function getMidColor(p, forceColorH, forceColorL)
				{
					
						var hex = function(x) {
						
							if (x > 255)
								x = 255;
							if (x<0)
								x = 0;
							x = x.toString(16);
							
							return (x.length == 1) ? '0' + x : x;
						};
						
						var color1;
						if (forceColorH == null)
							color1 =  hex(CountryConst.RGB.hR) + hex(CountryConst.RGB.hG) + hex(CountryConst.RGB.hB);
						else
							color1 = forceColorH;
							
						var color2;
						if (forceColorL == null)
							color2 =  hex(CountryConst.RGB.lR) + hex(CountryConst.RGB.lG) + hex(CountryConst.RGB.lB);
						else
							color2 = forceColorL;


						var ratio = p;
					

						var r = Math.ceil(parseInt(color1.substring(0,2), 16) * ratio + parseInt(color2.substring(0,2), 16) * (1-ratio));
						var g = Math.ceil(parseInt(color1.substring(2,4), 16) * ratio + parseInt(color2.substring(2,4), 16) * (1-ratio));
						var b = Math.ceil(parseInt(color1.substring(4,6), 16) * ratio + parseInt(color2.substring(4,6), 16) * (1-ratio));
						var middle = hex(r) + hex(g) + hex(b);
						
						return middle;
					//var c = 65536 * getMidVal(hR, lR, p) + 256 * getMidVal(hG, lG, p) + getMidVal(hB, lB, p);
					//return c.toString(16);
				}
				function getCountryName(countryCode) {
					var countryName;
					$.each(CountryConst.isoCountries, function(k,v){
						if(k == countryCode)
						{
							countryName = v;
							return false;
						}
					});
					if (countryName != '') {
						return countryName;
					} else {
						return countryCode;
					}
					return countryCode;
				}
			
			}
			
	}
}]);
