

	app.directive('goDiagram', function() {
		  return {
			restrict: 'E',
			template: '<div></div>',  // just an empty DIV element
			replace: true,
			scope: true,
			link: function(scope, element, attrs) {
			

  /********************************* view diagram ************************************/
		var type = element.parent().attr("id");
		var curID = element.parent().parent().attr("id");
		//console.log(curID)
		var $ = go.GraphObject.make; 
		//var id = '31';
		var lightText = 'white';
		
	
					var diagramSequence = $(go.Diagram, element[0],{});
			
		

		function nodeStyle() {
		return [
			new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
			{
				locationSpot: go.Spot.Center
			}
		];
    }
	
	 function makePort(name, spot, output, input) {
		return $(go.Shape, "Circle",
               {
                fill: "transparent",
                stroke: null,  // this is changed to "white" in the showPorts function
                desiredSize: new go.Size(8, 8),
                alignment: spot, alignmentFocus: spot,  // align the port on the main Shape
                portId: name,  // declare this object to be a "port"
                fromSpot: spot, toSpot: spot,  // declare where links may connect at this port
                fromLinkable: output, toLinkable: input  // declare whether the user may draw links to/from here
               //cursor: "pointer"  // show a different cursor to indicate potential link pointks to/from here
               });
	  
    }
  



	diagramSequence.isReadOnly = true;	
    diagramSequence.nodeTemplateMap.add("Start",
      $(go.Node, "Spot", nodeStyle(),
			
        $(go.Panel, "Auto",
          $(go.Shape, "Circle",
            { minSize: new go.Size(40, 40), fill: "#F1F1F1", stroke: null }),
          $(go.TextBlock, "Start",
            { font: "bold 11pt Helvetica, Arial, sans-serif", stroke: "#535353",editable: false },
            new go.Binding("text"))
        ),
       
        makePort("R", go.Spot.Right ,true, false)
      ));

	  
	  
    diagramSequence.nodeTemplateMap.add("End",
      $(go.Node, "Spot", nodeStyle(),
        $(go.Panel, "Auto",
          $(go.Shape, "Circle",
            { minSize: new go.Size(40, 40), fill: "#F1F1F1", stroke: null }),
          $(go.TextBlock, "End",
            { font: "bold 11pt Helvetica, Arial, sans-serif", stroke: "#535353",editable: false },
            new go.Binding("text").makeTwoWay())
        ),
       
        makePort("L", go.Spot.Left, false, true)
       
      ));
 diagramSequence.nodeTemplateMap.add("Description",
      $(go.Node, "Spot", nodeStyle(),
			
        $(go.Panel, "Auto",
          $(go.Shape, "RoundedRectangle",
            { maxSize: new go.Size(0, 0),fill: "#FFFFFF"  }),
          $(go.TextBlock, "Here we can create or edit Squences by drag and drop them to the canvas",
            { font: "9pt proxima-nova", stroke: "#FFFFFF",editable: false },
            new go.Binding("text"))/* ,
			$(go.TextBlock,   // the name of the port
                  { font: "7pt sans-serif" ,text: 'OUT',margin: new go.Margin(25,0,0, 0) }) */
      )//,
        // three named ports, one on each side except the top, all output only:
		/* {fromSpot: go.Spot.BottomSide}, 
       makePort("R", go.Spot.Right ,true, false) */
     )); 
	

if(type == 'template'){
/********************************* Interaction panel (template)*********************************************/	  
    diagramSequence.nodeTemplateMap.add("Interaction",
		$(go.Node, "Auto", nodeStyle(),

			$(go.Shape, "RoundedRectangle",
				{ fill: "#00A9C9", stroke: null },
				new go.Binding("figure", "figure")
			),
			  
			$(go.Panel, "Table",
				{
					minSize: new go.Size(120, NaN),
					maxSize: new go.Size(120, NaN),
					margin: new go.Margin(10)
					
				},
				
				$(go.TextBlock,
					{
						
						row: 0, column: 0, columnSpan: 2,
						textAlign: "center",
						minSize: new go.Size(120, 30),
						editable: false,
						font: "11pt Helvetica, Arial, sans-serif",
						stroke: lightText
						
					},
					new go.Binding("text" , "title").makeTwoWay() 
				),
				$(go.TextBlock,   // the name of the port
					{ 
						row: 1, column: 0,
						font: "7pt sans-serif" ,
						text: 'IN',	
						textAlign : 'left',
						minSize: new go.Size(60, NaN)
					}
				),
				$(go.TextBlock,   // the name of the port
					{ 
						row: 1, column: 1,
						font: "7pt sans-serif" ,
						text: 'OUT',	
						textAlign : 'right',
						minSize: new go.Size(60, NaN)
					}
				),
				$(go.TextBlock,
					{
						
						row: 2, column: 0,columnSpan: 2,
						
						minSize: new go.Size(120, 30),
						
						textAlign: "center",
						editable: false,
						font: "11pt Helvetica, Arial, sans-serif",
						stroke: lightText
						
					},
				 
					new go.Binding("text").makeTwoWay()
				)
				
			),
			
			makePort("L", go.Spot.Left, false, true),
			makePort("R", go.Spot.Right, true, false)
			
		
		)
	);
/********************************* Interaction panel *********************************************/
}else{
		/********************************* Interaction panel (sequence)*********************************************/	  
    diagramSequence.nodeTemplateMap.add("Interaction",
		$(go.Node, "Auto", nodeStyle(),

			$(go.Shape, "RoundedRectangle",
				{ fill: "#00A9C9", stroke: null },
				new go.Binding("figure", "figure")
			),
			  
			$(go.Panel, "Table",
				{
					minSize: new go.Size(120, NaN),
					maxSize: new go.Size(120, NaN),
					margin: new go.Margin(10)
					
				},
				
				$(go.TextBlock,
					{
						
						row: 0, column: 0, columnSpan: 4,
						textAlign: "center",
						minSize: new go.Size(120, 30),
						editable: false,
						font: "11pt Helvetica, Arial, sans-serif",
						stroke: lightText
						
					},
					new go.Binding("text" , "title").makeTwoWay() 
				),
				$(go.TextBlock,   // the name of the port
					{ 
						row: 1, column: 0,
						font: "7pt sans-serif" ,
						text: 'IN',	
						textAlign : 'left',
						minSize: new go.Size(40, NaN)
					}
				),
				/* $(go.TextBlock,
					{ 
						row: 1, column: 3,
						font: "7pt sans-serif" ,
						textAlign : 'left',
						minSize: new go.Size(40, NaN)
					}
				), */
				$(go.TextBlock,   // the name of the port
					{ 
						row: 1, column: 3,
						font: "7pt sans-serif" ,
						text: 'OUT',	
						textAlign : 'right',
						minSize: new go.Size(40, NaN)
					}
				),
				$(go.TextBlock,
					{
						
						row: 2, column: 0,columnSpan: 4,
						
						minSize: new go.Size(120, 30),
						
						textAlign: "center",
						editable: false,
						font: "11pt Helvetica, Arial, sans-serif",
						stroke: lightText
						
					},
				 
					new go.Binding("text").makeTwoWay()
				)
				,
				$(go.Picture,
					{ 
						row: 3, column: 0,
						alignment: go.Spot.Right,
						margin: 3,
						source: "Images/Dashboard/env.png",
						background: "#00A9C9",
						width: 18,
						height: 18 
					}
				),
				$(go.TextBlock,   // the name of the port
					{ 
						row: 3, column: 1,
						alignment: go.Spot.Left,
						//textAlign: "left",
						editable: false,
						font: "11pt Helvetica, Arial, sans-serif",
						minSize: new go.Size(20, 15),
						stroke: lightText
					},
					new go.Binding("text", "env").makeTwoWay()
				),
				$(go.Picture,
					{ 
						row: 3, column: 2,
						alignment: go.Spot.Right,
						margin: 3,
						source: "Images/Dashboard/env-open.png",
						background: "#00A9C9",
						width: 18,
						height: 18 
					}
				),
				$(go.TextBlock,   // the name of the port
					{ 
						row: 3, column: 3,
						alignment: go.Spot.Left,
						//textAlign: "left",
						editable: false,
						font: "11pt Helvetica, Arial, sans-serif",
						//minSize: new go.Size(20, 15),
						stroke: lightText
					},
					new go.Binding("text", "envOpen").makeTwoWay()
				)
			),
			
			makePort("L", go.Spot.Left, false, true),
			makePort("R", go.Spot.Right, true, false)
			
		
		)
	);
/********************************* Interaction panel *********************************************/
	
}

if(type == 'template'){
/********************************* Junction panel (template) *********************************************/

	diagramSequence.nodeTemplateMap.add("Junction",
		$(go.Node, "Auto", nodeStyle(),
			$(go.Shape, "RoundedRectangle",
				{ fill: "#ffe240", stroke: null},
				new go.Binding("figure", "figure")
			),
			  
			$(go.Panel, "Table",
				{
					minSize: new go.Size(120, NaN),
					maxSize: new go.Size(120, NaN),
					margin: new go.Margin(10)
					
				},
				$(go.TextBlock,
					{
						row: 0, column: 0, columnSpan: 2,
						textAlign: "center",
						minSize: new go.Size(120, 30),
						editable: false,
						font: "11pt Helvetica, Arial, sans-serif",
						stroke: '#757575'
						
					},
					new go.Binding("text").makeTwoWay() 
				),
				$(go.TextBlock,   // the name of the port
					{ 
						text: 'IN',	
						row: 1, column: 0,
						font: "7pt sans-serif" ,
						textAlign : 'left',
						minSize: new go.Size(60, NaN)
						
					}
				),
				$(go.TextBlock,   // the name of the port
					{ 
						text: 'YES',
						row: 1, column: 1,
						font: "7pt sans-serif" ,
						textAlign : 'right',
						minSize: new go.Size(60, NaN)
						
					}
				),
				$(go.TextBlock,   // the name of the port
					{ 
						row: 2, column: 0,
						font: "11pt Helvetica, Arial, sans-serif",
						stroke: '#757575',	
						textAlign : 'center',
						minSize: new go.Size(60, 30),
						editable: false
					},
					new go.Binding("text", "number").makeTwoWay()
				),
				$(go.TextBlock,
					{
						text: 'Days',
						row: 2, column: 1, 
						textAlign: "center",
						minSize: new go.Size(60, 30),
						editable: false,
						font: "11pt Helvetica, Arial, sans-serif",
						stroke: '#757575'
						
					},
					new go.Binding("text", "timePeriod").makeTwoWay()
					
				),
				$(go.TextBlock,   // the name of the port
					{ 
						text: 'NO',
						row: 3, column: 0,columnSpan: 2,
						font: "7pt sans-serif" ,	
						textAlign : 'center'
						
					}
				)
				
			),
			
        makePort("L", go.Spot.Left, false, true),
        makePort("R", go.Spot.Right, true, false),
        makePort("B", go.Spot.Bottom, true, false)
		
		)
	);
/********************************* Junction panel *********************************************/
}else{
	
	/********************************* Junction panel (sequence) *********************************************/

	diagramSequence.nodeTemplateMap.add("Junction",
		$(go.Node, "Auto", nodeStyle(),
			$(go.Shape, "RoundedRectangle",
				{ fill: "#ffe240", stroke: null},
				new go.Binding("figure", "figure")
			),
			  
			$(go.Panel, "Table",
				{
					minSize: new go.Size(120, NaN),
					maxSize: new go.Size(120, NaN),
					margin: new go.Margin(10)
					
				},
				$(go.TextBlock,
					{
						row: 0, column: 0, columnSpan: 2,
						textAlign: "center",
						minSize: new go.Size(120, 30),
						editable: false,
						font: "11pt Helvetica, Arial, sans-serif",
						stroke: '#757575'
						
					},
					new go.Binding("text").makeTwoWay() 
				),
				$(go.TextBlock,   // the name of the port
					{ 
						text: 'IN',	
						row: 1, column: 0,
						font: "7pt sans-serif" ,
						textAlign : 'left',
						minSize: new go.Size(60, NaN)
						
					}
				),
				$(go.TextBlock,   // the name of the port
					{ 
						text: 'YES',
						row: 1, column: 1,
						font: "7pt sans-serif" ,
						textAlign : 'right',
						minSize: new go.Size(60, NaN)
						
					}
				),
				$(go.TextBlock,   // the name of the port
					{ 
						row: 2, column: 0,
						font: "11pt Helvetica, Arial, sans-serif",
						stroke: '#757575',	
						textAlign : 'center',
						minSize: new go.Size(60, 30),
						editable: false
					},
					new go.Binding("text", "number").makeTwoWay()
				),
				$(go.TextBlock,
					{
						text: 'Days',
						row: 2, column: 1, 
						textAlign: "center",
						minSize: new go.Size(60, 30),
						editable: false,
						font: "11pt Helvetica, Arial, sans-serif",
						stroke: '#757575'
						
					},
					new go.Binding("text", "timePeriod").makeTwoWay()
					
				),
				$(go.Picture,
					{ 
						row: 3, column: 0,
						alignment: go.Spot.Right,
						source: "Images/Dashboard/users.png",
						width: 20,
						height: 20
					}
				),
				$(go.TextBlock,   // the name of the port
					{ 
						row: 3, column: 1,
						alignment: go.Spot.Left,
						margin: 3,
						font: "11pt Helvetica, Arial, sans-serif",
						stroke: '#757575',	
						//textAlign : 'left',
						//minSize: new go.Size(60, 15),
						editable: false
					},
					new go.Binding("text", "users").makeTwoWay()
				),
				$(go.TextBlock,   // the name of the port
					{ 
						text: 'NO',
						row: 4, column: 0,columnSpan: 2,
						font: "7pt sans-serif" ,	
						textAlign : 'center'
						
					}
				)
				
			),
			
        makePort("L", go.Spot.Left, false, true),
        makePort("R", go.Spot.Right, true, false),
        makePort("B", go.Spot.Bottom, true, false)
		
		)
	);
/********************************* Junction panel *********************************************/
}	
	 // replace the default Link template in the linkTemplateMap
    diagramSequence.linkTemplate =
      $(go.Link,  // the whole link panel
        {
          routing: go.Link.AvoidsNodes,
          curve: go.Link.JumpOver,
          corner: 5, toShortLength: 4,
          relinkableFrom: true,
          relinkableTo: true,
          reshapable: true,
          resegmentable: true,
          // mouse-overs subtly highlight links:
          mouseEnter: function(e, link) { link.findObject("HIGHLIGHT").stroke = "rgba(30,144,255,0.2)"; },
          mouseLeave: function(e, link) { link.findObject("HIGHLIGHT").stroke = "transparent"; }
        },
        new go.Binding("points").makeTwoWay(),
        $(go.Shape,  // the highlight shape, normally transparent
          { isPanelMain: true, strokeWidth: 8, stroke: "transparent", name: "HIGHLIGHT" }),
        $(go.Shape,  // the link path shape
          { isPanelMain: true, stroke: "gray", strokeWidth: 2 }),
        $(go.Shape,  // the arrowhead
          { toArrow: "standard", stroke: null, fill: "gray"}),
        $(go.Panel, "Auto",  // the link label, normally not visible
          { visible: false, name: "LABEL", segmentIndex: 2, segmentFraction: 0.5},
          new go.Binding("visible", "visible").makeTwoWay(),
          $(go.Shape, "RoundedRectangle",  // the label shape
            { fill: "#F8F8F8", stroke: null }),
          $(go.TextBlock, "Yes",  // the label
            {
              textAlign: "center",
              font: "10pt helvetica, arial, sans-serif",
              stroke: "#333333",
              editable: true
            },
            new go.Binding("text").makeTwoWay())
        )
      );
	
	load11();
	
	function load11() {
   
	//console.log(curID)
	if(type == 'template'){
		for(var i = 0; i < scope.templatesArr.length; i ++){
			if(curID == scope.templatesArr[i].sequencetemplateid){
				
					diagramSequence.model = go.Model.fromJson(scope.templatesArr[i].diagramFullData);
				
				
				//console.log(scope.templatesArr[i].diagramFullData)
				break;
			}
		} 
	}else{
		for(var i = 0; i < scope.sequencesArr.length; i ++){
			if(curID == scope.sequencesArr[i].usersequenceid){
				
				diagramSequence.model = go.Model.fromJson(scope.sequencesArr[i].diagramFullData);
				//console.log(scope.sequencesArr[i].diagramFullData)
				break;
			}
		} 
	}
  
  }


			}
		}
	})			