
app.directive('funnelprev',
	function () {
		return {
			restrict: 'E',
			templateUrl: 'Templates/Extern/Funnel.html',
			replace:true,
			scope:true,
			link: function (scope, element, attrs) {
					
				var curID = element.parent().attr("id");
				var curData = scope.funnelModal.prevFunnelData;
				
				if(curData.length > 0){
					
					scope.graphData = parseFunnelSteps(curData);
					
				}
				
				function parseFunnelSteps(d)
				{
					var precision = 10;
					var resp = [];
					if(d != null)
					{
						var ln = d.length;
						
						if(ln > 0)
						{
							var total = parseFloat(d[0][0]);
							if(total == 0)
								total = 1;
							
							for(var i = 0 ; i < ln ; i++)
							{
								var c = d[i];
								var precent = Math.ceil(100 * precision * c[0] / total)/precision;
								if(i != 0){
									var diff = Math.floor(precision*(resp[i-1].precentage - parseFloat(precent)))/precision;
									var divclass = diff == 0 ? 'funnel_connector_none' : diff < 0 ? 'funnel_connector_up' : 'funnel_connector_down';
									var divarrow = diff == 0 ? 'fa-arrows-h' : diff < 0 ? 'fa-arrow-up' : 'fa-arrow-down';
									var temp = Math.abs(diff);
									resp.push({'title': c[1], 'amount': c[0], 'precentage':precent, 'diff' : temp,'divclass' : divclass,'divarrow' : divarrow});
								}else{
									resp.push({'title': c[1], 'amount': c[0], 'precentage':precent, 'diff' : 0,'divclass' : '','divarrow' : ''});
								}
							}
						}
					}
					
					return resp;
				}
	
			}
		}
	
})