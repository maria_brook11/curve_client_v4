app.directive('compcounter',function(){

	function link( scope, element , attr ) {

		function draw() {
        
			element.empty();
			
			var val = attr.value ? JSON.parse(attr.value) : {};
			//console.log(val)
			 
		if(val.dataGraph.chartData.ReportDataTable.Records && val.dataGraph.chartData.ReportDataTable.Records.length > 0)	{
			var proc = 	val.dataGraph.chartData.ReportDataTable.Records[0][0];
			var num = 	val.dataGraph.chartData.ReportDataTable.Records[0][1];
		}else{
			var proc = 	0;
			var num = 	0;
		}
		var color = val.dataGraph.graphColor;
		
		if(scope.dashboardView.active != 4){
			new ComparableKnob("canvas_div_id_"+val.ID+"", val.dataGraph.ComparePeriod, {type: val.dataGraph.type, show_rect: scope.dashboardView.active != 4,knob_color:color}).draw(proc, num);
		}else{
			new ComparableKnob("scanvas_div_id_"+val.ID+"", val.dataGraph.ComparePeriod, {type: val.dataGraph.type, show_rect: scope.dashboardView.active != 4,knob_color:color}).draw(proc, num);
		}
			}

			scope.$watch(function () {
			  return [attr.value];
			}, draw, true);
	}			
	return {
			scope : true,
			priority: 99,
			restrict: 'A',
			link : link
	};
		

	  

})
