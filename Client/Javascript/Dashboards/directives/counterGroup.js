app.directive('easyPieChart11', function(){
    return {
        restrict: 'C',
        scope: {
            percent: '='
        },
        link: function(scope, elem, attrs){
    
            $(elem).easyPieChart({'animate':1000,'barColor':"#6ec4c8",'lineWidth':3,'size':75}).data('easyPieChart').update(scope.percent);   
        }
    } 

})
