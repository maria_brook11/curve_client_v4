
app.directive('graphsparkline',
	function () {
		return {
			restrict: 'E',
			replace:true,
			scope:true,
			link: function (scope, element, attrs) {
				
			var curID = element.parent().attr("id");
			
			var curData;	
			for (var i = 0; i <= scope.activeTab.dashboardActive.Graphs.length; i++) {
		
				if (scope.activeTab.dashboardActive.Graphs[i].ID == curID) {
					 
					curData = scope.activeTab.dashboardActive.Graphs[i].dataGraph.chartData.ReportDataTable.Records;
					break;
				}
			}
			
			var patchData = [];
			for (var k = 0; k < 14; k++){patchData[k] = 0;}
			if(curData != null){
				for (var j=0; j < curData.length; j++){patchData[j] = curData[j][0];}
			}
	
			var options = {
                type: "bar",
                width: "130",
                barWidth: "10",
                height: "85",
                barColor: "#6ec4c8",
                negBarColor: "#6ec4c8",
				zeroColor: "#6ec4c8"
            };

			$(element).sparkline(patchData, options);
			
			}
		}
	}
)
