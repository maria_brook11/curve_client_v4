app.directive('multiselectDropdown', function () {
    return {
        restrict: 'E',
        scope: true,
       template:
           "<div class='btn-group multi' data-ng-class='{open: open}' style='width: 100%;min-height: 45px;'>"+
				"<input type='hidden' class='objValue' value='{{myOptionsArr}}' />"+
				"<div style='width: 100%;border: 2px solid rgb(179, 179, 179);display: inline-block;min-height: 45px;'>"+
					"<div class='dropdown-toggle' id='kokokuku' ng-click='openDropdown(null)' style='width: 38px;float:right;text-align: center;box-shadow: none; height: 34px;cursor:pointer' ><i class='fa fa-plus' style='margin-top: 13px;color:#6ca3ba' aria-hidden='true'></i></div>"+
					"<ul style='padding: 0;' ><li ng-repeat='opt in myOptionsArr' ng-if='opt.add' style='list-style-type: none;display: inline-block;margin: 5px 5px 0 5px' >"+
					"<div class='SelectItem' >{{opt.name}}<i ng-click='delSelectItem(opt)' class='fa fa-times' aria-hidden='true' ></i></div></li></ul>"+
					
				"</div>"+
				"<ul class='dropdown-menu' aria-labelledby='dropdownMenu' style='width: 100%;border: 1px solid #d2d2d2;box-shadow: none;max-height: 200px;overflow-y: auto;overflow-x: hidden;top: 45px;'>"+
				"<li style='cursor:pointer;' ng-repeat='option in myOptionsArr' ng-if='!option.add' ><a ng-click='addSelectItem(option)'><span aria-hidden='true'></span> {{option.name}}</a></li>"+
				"</ul>"+
			"</div>", 

        link: function (scope, element, attrs) {
			
			scope.openGenderFlag = true;
			var t = attrs.arr;
			scope.myOptionsArr = JSON.parse(t);
			
			scope.checkGenderFlag = function(){
				var temp = true;
				for(i = 0; i < scope.myOptionsArr.length; i++) {

					if (scope.myOptionsArr[i].add == false) {
						temp = false;	
						break;
					}

				};
				if(temp){
					scope.openGenderFlag = false;
				}else{
					scope.openGenderFlag = true;
				}
			};
			
			scope.openDropdown = function (e) {
				if(e == 'c'){
					scope.open =  false;
				}else{
					if(scope.openGenderFlag){
						scope.open = !scope.open;
					}
				}
				scope.checkGenderFlag();
			};
	
            scope.selectAll = function () {

                angular.forEach(scope.myOptionsArr, function (val,key) {

                    val.add = true;

                });

            };

            scope.deselectAll = function () {

                angular.forEach(scope.myOptionsArr, function (val,key) {
					
						val.add = false;
					

                });

            };
			
			
			scope.delSelectItem = function (option) {
				angular.forEach(scope.myOptionsArr, function (val,key) {
					if(val.type){
						if(val.id == option.id && val.type == option.type) {

							val.add = false;

						}
					}else{
						if(val.id == option.id) {

							val.add = false;

						}
					}
				});
				scope.checkGenderFlag();
				
			}
			
			scope.addSelectItem = function (option) {

				angular.forEach(scope.myOptionsArr, function (val,key) {
					
					if(val.type){
						
						if (val.id == option.id  && val.type == option.type) {
								
							val.add = true;
							scope.openDropdown(null)
							if(scope.sequenceModal){
								scope.sequenceModal.showMess = false;
							}
						}
					}else{
						
						if (val.id == option.id) {
								
							val.add = true;
							scope.openDropdown(null)
							if(scope.sequenceModal){
								scope.sequenceModal.showMess = false;
							}
						}
					}

				});
				scope.checkGenderFlag();
				
			}
			
			scope.dirClearFilters = function(){

				angular.forEach(scope.myOptionsArr, function (val,key) {
					
						val.add = false;
					
                });
				
				scope.checkGenderFlag();
			};
			
			 scope.$on('dirClearFilters',function(event, data){
				 scope.dirClearFilters()
			 });

			 scope.$on('openDropdown',function(event, data,e){
				 scope.openDropdown(e)
			 });
			 
			  scope.$on('deselectAll',function(event, data){
				 scope.deselectAll()
			 });
		 
        }
    }

});