function CohortFactiryFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst,InsightConst,$compile){


	var events_by_id = {};
	var events_by_desc = {};
	var table_name_mods = {};
	var is_ready = false;
	var table_names = {};
	var ready_func = [];
	var validModifiers = ['>', '<', '=', '>=', '<=', '!='];
	var serverObject = {mod:">", timingDef:"last", timindPeriod:"days"};
	var cohortLineByID = {};
	$scope.cohortData;
	$scope.campaignTitle;
	$scope.mailsData;

$scope.CohortDescriptor = function()
{ 
	$scope.CohortDescriptorLoader = true;
	getCohortsList();
	$scope.curveAPI('GetCohortsEvents', {Token: $rootScope.Token}, onGetCohortsEventsSuccess, onError);
	
};


	
	function invokeReady()
	{
		is_ready = true;
		if(ready_func != null && ready_func.length)
		{
			for(var i = 0 ; i < ready_func.length ; i++)
			{
				var f = ready_func[i];
				if(f != null) f();
			}
			
			ready_func = [];
		}
	};
	
	function ready(f)
	{
		if(is_ready)
		{
			f();
			return;
		}
		
		if(ready_func == null)
			ready_func = [];
		
		ready_func.push(f);
	};
	
	$scope.createNewCohort = function()
	{
		serverObject = {mod:">", timingDef:"last", timindPeriod:"days"};
		resetCohortMakerDisplay();
		$('#create_cohort').modal('show');
	};
	

	
	function onGetCohortsEventsSuccess(data)
	{	
		if(data.length){
			var ln = data.length;
			for(i = 0 ; i < ln ; i++)
			{
				var cur = data[i];
				
				var ev = {};
				
				ev.id = i;
				ev.column_name = cur.column_name;
				ev.description = cur.description;
				ev.modifiers = cur.modifiers == "t";
				ev.table_name = cur.table_name;
				ev.userid_column_name = cur.userid_column_name;
				ev.value = cur.value;
				
				events_by_id[i] = ev;
				
				if(events_by_desc[cur.description] == null)
					events_by_desc[cur.description]  = [];
				events_by_desc[cur.description].push(ev);
				
				if(table_name_mods[cur.table_name] == null)
				{
					table_name_mods[cur.table_name] == 1;
					
					var table = {name: cur.table_name, desc:cur.description, useridcol:ev.userid_column_name};
					table_name_mods[cur.table_name] = table;
				}
			}
			resetCohortMakerDisplay();
		}	
		

	};
	
	function onError(data)
	{
		$scope.show_error('ERROR! ', data);

	};
	
	$scope.selectTimingDef = function(def)
	{
		serverObject.timingDef = def;
		$("#cohort_first_use_desc").empty();
		
		if(def == "within")
			$("#cohort_first_use_desc").append(" of first use.");
		
		$("#cohorts_within_title").empty();
		$("#cohorts_within_title").append(def + "<span class='caret cohort_caret'></span>");
	}; 
	
	$scope.selectTimingPeriod = function(period)
	{
		serverObject.timindPeriod = period;
		$("#cohorts_groupby_title").empty();
		$("#cohorts_groupby_title").append(period + "<span class='caret cohort_caret'></span>");
	}; 
	
	$scope.selectMod = function(mod)
	{
		serverObject.mod = mod;
		
		var code = 0;
		
		switch(mod)
		{
			case ">=":
				code = 8804;
				break;
			case "<=":
				code = 8805;
				break;
			case "!=":
				code = 8800;
				break;
		}
		
		if(code != 0)
			mod = String.fromCharCode(code);
		
		$("#cohorts_condition_title").empty();
		$("#cohorts_condition_title").append(mod + "<span class='caret cohort_caret'></span>");
	};
	
	$scope.selectEvent = function(evIdx)
	{
		var events = events_by_desc[serverObject.category];
		var selectedEvent = events[evIdx];
		serverObject.selectedEvent = selectedEvent;
		
		$("#cohorts_sel_event_title").empty();
		$("#cohorts_sel_event_title").append(selectedEvent.value + "<span class='caret cohort_caret' style='margin-left:10px'></span>");
		
		$("#cohort_events_list").css("display","inline-block");
		$("#cohorts_timing_block").css("display","inline-block");
		
		if(selectedEvent.modifiers)
			$("#cohorts_modifiers_block").css("display","inline-block");
	};
	
	$scope.selectEventCategory = function(category)
	{
		$("#cohorts_action_title").empty();
		$("#cohorts_action_title").append(category + "<span class='caret cohort_caret'></span>");
		
		$("#cohorts_timing_block").css("display","none");
		$("#cohorts_modifiers_block").css("display","none");
		$("#cohort_events_list").css("display","inline-block");
		
		serverObject.category = category;
		
		$.each(events_by_desc, function(key, val){
				if(key == category){
					$scope.lines22 = val;
				}
			});

		$("#cohorts_sel_event_title").empty();
		$("#cohorts_sel_event_title").append("Select Event <span class='caret cohort_caret'></span>");

	};
	
	function resetCohortMakerDisplay()
	{
		//var first = null;
		$scope.lines = events_by_desc;
		
		// for(var k in events_by_desc)
		// {
			// if(first == null)
				// first = k;
			
			// lines += '<li role="presentation"><a role="menuitem" tabindex="-1"  ng-click="selectEventCategory(\'' + k + '\')">' + k + '</a></li>';
		// }
		
		
		// if(first == null)
			// first = 'No events';
		// if(lines == "")
			// lines += '<li role="presentation"><a role="menuitem" tabindex="-1" >No actions availble in this application</a></li>';
		// console.log(lines)
		// $("#cohorts_action").empty();
		// $("#cohorts_action").html(lines);
		cleanModalBox();
	};
	
	function cleanModalBox()
	{
		
			$("#cohorts_timing_block").css("display","none");
			$("#cohorts_modifiers_block").css("display","none");
			$("#cohort_events_list").css("display","none");
			
			$("#cohorts_action_title").empty();
			$("#cohorts_action_title").append("Select Action <span class='caret cohort_caret'></span>");
			
			$("#cohorts_sel_event_title").empty();
			$("#cohorts_sel_event_title").append("Select Event <span class='caret cohort_caret'></span>");
			
			$("#cohorts_condition_title").empty();
			$("#cohorts_condition_title").append("> <span class='caret cohort_caret'></span>");
			
			$("#cohorts_within_title").empty();
			$("#cohorts_within_title").append("last <span class='caret cohort_caret'></span>");
			
			$("#cohorts_groupby_title").empty();
			$("#cohorts_groupby_title").append("days <span class='caret cohort_caret'></span>");
			
			$("#cohorts_amount_input").val(3);
			$("#cohorts_within_amount_input").val(6);
			$("#cohort_name").val("");
		
	};
	
	$scope.createCohortFun = function()
	{
		var data = validate();
		
		if(data.error != null)
		{
			alert(data.error);
			return;
		}

		$scope.curveAPI("CreateNewCohort", {Token: $rootScope.Token, Data:JSON.stringify(data)}, onCreated, onFailedCreating);
	};
	
	function onCreated(data)
	{
		$("#create_cohort").modal('hide');
		$scope.show_success("Successfully created Audience!", "Audience page will now refresh to update view");
		getCohortsList();
	};
	
	function onFailedCreating(data)
	{
		$scope.show_error('ERROR! ', data);
		$("#create_cohort").modal('hide');
	};
	
	function parseCondition(mod)
	{
		if(mod == "=")
			return '2005';
		if(mod == "!=")
			return '2006';
		if(mod == ">")
			return '2002';
		if(mod == "<")
			return '2001';
		if(mod == "<=")
			return '2003';
		if(mod == ">=")
			return '2004';
		
		return null;
	};
	
	function validate()
	{
		var cohortname = $("#cohort_name").val();
		
		if(cohortname == null || cohortname == "")
			return {error:"Must specify a name"};
		
		var ev = serverObject.selectedEvent;
		
		if(ev == null)
			return {error:"Event not selected."};
		
		var event_name = ev.value;
		
		var modifier_str = serverObject.mod;
		var mod = validModifiers.indexOf(modifier_str) == -1 ? null : parseCondition(modifier_str);
		
		var timindPeriod = serverObject.timindPeriod;
		var timingDef = serverObject.timingDef;
		var mod_value = $("#cohorts_amount_input").val();
		var timing_value = $("#cohorts_within_amount_input").val();
		
		if(mod == null)
			return {error:"Modifier not selected."};
		
		if(isNaN(timing_value) || timing_value < 0)
			return {error:"Must specify a value for timing modifier that's not negative."};
		
		var query = {};
		
		if(ev.modifiers)
		{
			if(isNaN(mod_value) || mod_value < 0)
				return {error:"Must specify a value for modifier that's larger than 0."};	
			
			query.Condition = mod;
			query.Value = mod_value;
		}
		
		query.QueryTable = ev.table_name;
		query.ColumnName = ev.column_name;
		query.EventName = event_name;
		query.HwIDColumnName = ev.userid_column_name;
		
		query.TimingDefinition = timingDef;
		query.Timing = timing_value;
		query.TimingPeriod = timindPeriod;
		
		var resp = {}
		resp.Name = cohortname;
		resp.Query = query;
		resp.Description = "";
		resp.GraphType = "Cohort";
		
		return resp;
	};
	
	//*****************************************************************************************
	
	
	$scope.recalculate = function(id)
	{	
		$scope.CohortDescriptorLoader = true;
		var tr = cohortLineByID[id];
		//cohortLineSetLoading(tr, true, null);
		$scope.curveAPI("CalculateCohort", {Token: $rootScope.Token, Data: id}, onCohortsCalculateSuccess, onCohortsCalculateFail);
	};
	
	function onCohortsCalculateSuccess(d)
	{
		var tr = cohortLineByID[d.Id];
		//cohortLineSetLoading(tr, false, d);
		replaceData(d);
		$scope.CohortDescriptorLoader = false;
	};
	
	function onCohortsCalculateFail(d)
	{
		$scope.show_warn("Failed", "Failed to recalculated Audience. Error: " + d);
		getCohortsList();
	};
	
	$scope.showSampleUsers = function(id, name,users)
	{
		var sample_table = $("#cohorts_sample_users_table_div").DataTable();
		
		$("#sample_users_title").html("Audience: '" + name+"'");
		$("#sample_users_modal").modal('show');
		$("#cohorts_sample_users_table_div_filter label input").val('');
		
		sample_table.clear().draw();
		
		if(users == null || users.length == 0)
			sample_table.row.add(['This Audience contains no users.']);
		else
		{
			for(i=0;i<users.length;i++)
				sample_table.row.add([users[i].userid + '<div style="float: right;cursor: pointer;" ng-click="goToSingleUser(\''+users[i].userid+'\')" ><i style="color: #555;" class="fa fa-user" aria-hidden="true"></i></div>']);
		}
		
		sample_table.draw();
		
		var $el = $('#cohort_sample_users');
		$compile($el)($scope);
	
	};
	
	$scope.exportCohortOpen = function(id)
	{
		$scope.cohortCurId = id;
		$("#export_cohort").modal('show');
		$("#export_cohorts_loading").css("display","none");
		
	};
	
	$scope.exportCohort = function()
	{
		$("#export_cohort").modal('hide');
		$scope.curveAPI("GetCohortUsers", {Token: $rootScope.Token, Data: $scope.cohortCurId}, onCohortsUsersSuccess, onCohortsUsersFail);
		
	};

	function onCohortsUsersSuccess(d)
	{
		window.open('data:text/csv;charset=utf-8,' + escape(d.join(",")));
	};

	function onCohortsUsersFail(d)
	{
		//cohortTabLoading(false); 
		$scope.show_warn("Failed", "Failed to retrieve Audience data.");
	};
	
	
	
	$scope.deleteCohort = function(id,e)
	{	
		if(!e){
			$scope.deleteCohortCurId = id;
			promptDelete()
		}else{
			$scope.curveAPI("DeleteCohort", {Token: $rootScope.Token, Data: $scope.deleteCohortCurId}, onCohortsDelSuccess, onCohortsDelFail);
		}
	};
	
	function onCohortsDelSuccess(d)
	{
		$scope.CohortDescriptorLoader = false;
		$scope.show_success("Successfully deleted Audience. ", "Table will now refresh.", true);
		$scope.CohortDescriptor();
	};

	function onCohortsDelFail(d)
	{
		cohortTabLoading(false);
		$scope.show_warn("Failed", "Failed to delete Audience.");
	};
	
	//********************************

	
	function replaceData(data)
	{
		
		if($scope.cohortData == null)
			$scope.cohortData = [];
		
		for(var i = 0 ; i < $scope.cohortData.length ; i++)
		{
			var cur = $scope.cohortData[i];
			if(cur.audienceid == data.Id)
			{
				found = true;
				$scope.cohortData.splice(i, 1);
				break;
			}
		}
		
		$scope.cohortData.push(data);
				
	};
	
	function reloadCohortTable()
	{
		if($scope.cohortData == null)
			return;
		

		var d = $scope.cohortData;
		for(var i = 0 ; i < d.length ; i++)
		{
			var cohort = d[i];
			if(cohort.name != null && cohort.audienceid != null)
				$("#cohort_table_body").append(getCohortLine(cohort));
		}

	};
	//********************************
	
	function cohortTabLoading(show)
	{
		jQuery(document).ready(function()
		{
			//$("#cohorts_table_holder")[0].style.display = !show ? "" : "none";
			//$("#cohorts_table_loading")[0].style.display = show ? "" : "none";
		});
	};
	
	function getCohortsList()
	{
		 //34.231.85.211:8080/Curve/GetAudiencesRanksBehaviors?Token=TEST-YZMF8ICL6W8		
	$scope.curveAPI('GetAudiencesRanksBehaviors', {Token: $rootScope.Token}, AudiencesRanksBehaviorsSuccess, AudiencesRanksBehaviorsError);
	
		$scope.curveAPI('GetAudiences', {'Token': $rootScope.Token}, onGotCohorts, onFailCohorts);
		//$scope.curveAPI("GetCohorts", {Token: $rootScope.Token}, onGotCohorts, onFailCohorts);
		
		//localhost:8080/Curve/GetAudiencesRanksBehaviors?Token=TEST-YZMF8ICL6W8
		//$scope.curveAPI('GetAudiencesRanksBehaviors', {'Token': $rootScope.Token}, onGotCohorts, onFailCohorts);
	};
	
		function AudiencesRanksBehaviorsSuccess(data){
	
		$scope.AudiencesArray = [];
			if(data.Audiences.length > 0){
				for(var i = 0; i < data.Audiences.length; i ++){
					var t = {"type" : data.Audiences[i].type, "id" : data.Audiences[i].id, "name" : data.Audiences[i].name, "add" : false};
					$scope.AudiencesArray.push(t);
				}
			}
			if(data.Behaviors.length > 0){
				for(var i = 0; i < data.Behaviors.length; i ++){
					var t = {"type" : data.Behaviors[i].type, "id" : data.Behaviors[i].id, "name" : data.Behaviors[i].name, "add" : false};
					$scope.AudiencesArray.push(t);
				}
			}
			if(data.Ranks.length > 0){
				for(var i = 0; i < data.Ranks.length; i ++){
					var t = {"type" : data.Ranks[i].type, "id" : data.Ranks[i].id, "name" : data.Ranks[i].name, "add" : false};
					$scope.AudiencesArray.push(t);
				}
			}
			
	}

function AudiencesRanksBehaviorsError(data){console.log(data)}
	
	
	function onGotCohorts(d)
	{	
		
		$scope.CohortDescriptorLoader = false;
		$scope.cohortData = d;
		is_ready = true;
		invokeReady();

	};
	
	function onFailCohorts(d)
	{
		cohortTabLoading(false);

	};
	
	function cohortLineSetLoading(tr, loading, data)
	{
		if(loading)
		{
			tr.find("#refresh_holder").hide();
			tr.find("#users_holder").hide();
			tr.find("#download_holder").hide();
			tr.find("#trash_holder").hide();
			
			tr.find("#refresh_load").show();
			tr.find("#users_load").show();
			tr.find("#download_load").show();
			tr.find("#trash_load").show();
			
			tr.find("#name_holder").empty();
			tr.find("#count_holder").empty();
			tr.find("#last_holder").empty();
			
			tr.find("#name_holder").html("Recalculating, Please wait...");
			tr.find("#count_holder").html("...");
			tr.find("#last_holder").html("...");
		}
		else
		{
			tr.find("#refresh_holder").show();
			tr.find("#users_holder").show();
			tr.find("#download_holder").show();
			tr.find("#trash_holder").show();
			
			tr.find("#refresh_load").hide();
			tr.find("#users_load").hide();
			tr.find("#download_load").hide();
			tr.find("#trash_load").hide();
			
			tr.find("#name_holder").empty();
			tr.find("#count_holder").empty();
			tr.find("#last_holder").empty();
			
			tr.find("#name_holder").html(data.Name);
			tr.find("#count_holder").html(data.Count);
			tr.find("#last_holder").html(data.LastUpdate);
		}
	};
	
	function getCohortLine(d)
	{		
		var getTR = function(name, id)
		{
			if(name == null) name = "N/A";
			var th = $("<th id='"+id+"'></th>");
			th.addClass("cohort_item");
			th.html(name);
			return th;
		};
		
		var getTRIcon = function(icon, onclick, loading_holder, iconColor)
		{
			var th = $("<th></th>");
			th.addClass("cohort_item");
			th.addClass("cohort_icon");
			var icon_color = iconColor == null ? '5a5a5a' : iconColor;
			th.append($('<div id="'+loading_holder+'_load" style="display:none; color: #19baef !important;"><i class="fa fa-cog fa-spin"></i></div><a id="'+loading_holder+'_holder" ng-click="'+onclick+'"><i style="color: #'+icon_color+' !important;" class="fa fa-'+icon+'"></i></a>'));
			return th;
		};
		
		var tr = $("<tr></tr>");
		
		tr.append(getTR(d.Name, 'name_holder'));
		tr.append(getTR(d.LastUpdate == null ? null : d.Count, 'count_holder'));
		tr.append(getTR(d.LastUpdate, 'last_holder'));
		tr.append(getTRIcon("refresh", "recalculate("+d.Id+");", 'refresh'));
		tr.append(getTRIcon("users", "showSampleUsers("+d.Id+", '"+d.Name+"');", 'users'));
		tr.append(getTRIcon("download", "exportCohort("+d.Id+");", 'download'));
		tr.append(getTRIcon("trash-o", "deleteCohort("+d.Id+");", 'trash', 'cc0000'));
		
		cohortLineByID[d.Id] = tr;

		return tr;
	};


function CohortDeletionManager(id_, onAccept_, onCancel_, title, cancelTxt, acceptTxt)
{
	var onAccept = onAccept_;
	var onCancel = onCancel_;
	var id = id_;
	
	if(title != null) $("#delete-prompt-title").html(title);
	if(cancelTxt != null) $("#delete-prompt-cancel").html(cancelTxt);
	if(acceptTxt != null) $("#delete-prompt-accept").html(acceptTxt);
		
	$("#delete-prompt-accept")[0].onclick =  acceptClicked;
	$("#delete-prompt-cancel")[0].onclick = cancelClicked;
	
	CohortDeletionManager.instance = this;
	promptDelete();
};


	function acceptClicked()
	{
		if(CohortDeletionManager.instance.onAccept != null) CohortDeletionManager.instance.onAccept(CohortDeletionManager.instance.id);
	};
	
	function cancelClicked()
	{
		if(CohortDeletionManager.instance.onCancel != null) CohortDeletionManager.instance.onCancel();
	};
	
	function promptDelete()
	{
		$("#delete-prompt").modal('show');
	};
	
	
	$scope.showCreateCohort = function(reportId, campaignTitle, mailsData)
	{
		$scope.interactionTypeOfEmail = "";
		$scope.campaignTitle = campaignTitle;
		$scope.mailsData = mailsData;
		
		$("#choose_segment_dropdown").val([]).trigger("change");
		$("#choose_segment_button").html('Choose Segment <i class="fa fa-angle-down"></i>');
		$("#choose_segment_button")[0].setAttribute("data-report-id", reportId);
		$('#cohorts_lite_loading').css('display' , 'none');
		$('#cohorts_lite_content').css('display' , '');
		$("#lite_cohort_name").val($scope.campaignTitle);
	};
	
	$scope.mailingListsCohortSelected= function(name)
	{
		$("#choose_segment_button").html(name + ' <i class="fa fa-angle-down"></i>');
		$("#lite_cohort_name").val($scope.campaignTitle + " - " + name);
	};
	
	$scope.interactionsCohortSelected= function(name)
	{
		$scope.interactionTypeOfEmail = name;
		$("#choose_segment_interaction_button").html(name + ' <i class="fa fa-angle-down"></i>');
		$scope.Input.interaction = $scope.interactionPopupTitle + " - " + name;
	};
		
	$scope.mailingListsCreateCohort = function()
	{
		var data = validateAndPrepareCohort();
		
		if(data.error != null)
		{
			$scope.show_error("Make sure all fields are properly filled", data.error);
			return;
		}
		
		$('#cohorts_lite_loading').css('display' , '');
		$('#cohorts_lite_content').css('display' , 'none');
		
		$scope.curveAPI("CreateNewExternalCohort", {"Token": $rootScope.Token, Data:JSON.stringify(data)}, onCohortCreated, onFailedCreatingCohort);
	};
	
	
	validateAndPrepareCohort = function()
	{
		var cohortName = $("#lite_cohort_name").val();
		var emailContent = $("#choose_segment_button").text();
		
		if (cohortName == "")
			return {error: "You must specify an Audience name."};
		
		if (emailContent == "Choose Segment ")
			return {error: "You must specify a segment."};
		
		
		var emails = [];
		var reportId = $("#choose_segment_button")[0].getAttribute("data-report-id");
		
		var queryData = {};
		queryData.campaignId = reportId;
		
		if (emailContent == "Opened Emails ")
		{
			emails = $scope.mailsData.openedEmails;
			queryData.cohortType = "openedEmails";
		}
		else if (emailContent == "Non-Opened Emails ")
		{
			emails = $scope.mailsData.notOpenedEmails;
			queryData.cohortType = "notOpenedEmails";
		}
		else if (emailContent == "All Emails ")
		{
			emails = $scope.mailsData.notOpenedEmails.concat($scope.mailsData.openedEmails);
			queryData.cohortType = "allEmails";
		}
		else if (emailContent == "Clicked Link ")
		{
			//emails = $scope.mailsData.notOpenedEmails.concat($scope.mailsData.openedEmails);
			queryData.cohortType = "clicked";
		}
		else if (emailContent == "Complete Signup ")
		{
			//emails = $scope.mailsData.notOpenedEmails.concat($scope.mailsData.openedEmails);
			queryData.cohortType = "signup";
		}
		
		// for test (user exist in big2bonanza)
		// emails.push("abangbukitraya@gmail.com");
		
		return {"CohortName":cohortName, "Emails": emails, "QueryData": queryData};
	};
	
	onCohortCreated = function(data)
	{
		$("#create_cohort_lite").modal('hide');
		if (data == 0)
			$scope.show_message("Audience was not created", "Non of the e-mails found in the mailing list exist in curve users data.");
		else
			$scope.show_success("Successfully created Audience!", "Audience with " + data + " users created.");
	};
	
	onFailedCreatingCohort = function(data)
	{
		$("#create_cohort_lite").modal('hide');
		$scope.show_error('ERROR! ', data);
	};
	
	// sendCampaign: function(campaignID)
	// {
		// curveAPI("SendMailChimpCampaign", {"Token": currentToken, Data:JSON.stringify({"CampaignId":campaignID})}, MailingLists.instance.onSendCampaignData, MailingLists.instance.onSendCampaignError);
	// },
	
	// onSendCampaignData: function(data)
	// {
		// // console.log("onSendCampaignData", data);
		// show_success("Success","Campaign was sent succesfully.");
	// },
	
	// onSendCampaignError: function(data)
	// {
		// console.log("onSendCampaignError", data);
	// },
}