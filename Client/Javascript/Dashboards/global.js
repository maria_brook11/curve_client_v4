function DashBrdGlobalFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter){

$scope.getAPIGlobalParams = function(object, datePicker) 
{	
	if (datePicker != null && datePicker.startDate.format != null && datePicker.startDate.format != 'undefined' ) {
		object.DateFrom = datePicker.startDate.format('X')*1000;
		object.DateTo = datePicker.endDate.format('X')*1000;
		$scope.storeToCache('lastStartDate', getDateFormatForExtern(datePicker.startDate));
		$scope.storeToCache('lastEndDate', getDateFormatForExtern(datePicker.endDate));
	}
	
	return object;
}

$scope.getAPIGlobalParamsSingle = function(object, datePicker) 
{
	if (datePicker != null && datePicker.startDate.format != null && datePicker.startDate.format != 'undefined' ) {
		object.DateFrom = datePicker.startDate.format('X')*1000;
		object.DateTo = datePicker.startDate.format('X')*1000;
		$scope.storeToCache('lastStartDate', getDateFormatForExtern(datePicker.startDate));
		$scope.storeToCache('lastEndDate', getDateFormatForExtern(datePicker.startDate));
	}
	
	return object;
}

function getDateFormatForExtern(date)
{
	return getDateFormatMonthForExtern(date._d.getMonth()) + " " + (date._d.getDate()-1) + "," + date._d.getFullYear();
}

function getDateFormatMonthForExtern(m)
{
	if(m == 0)
		return 'JAN';
	if(m == 1)
		return 'FEB';
	if(m == 2)
		return 'MAR';
	if(m == 3)
		return 'APR';
	if(m == 4)
		return 'MAY';
	if(m == 5)
		return 'JUN';
	if(m == 6)
		return 'JUL';
	if(m == 7)
		return 'AUG';
	if(m == 8)
		return 'SEP';
	if(m == 9)
		return 'OCT';
	if(m == 10)
		return 'NOV';
	if(m == 11)
		return 'DEC';
}

$scope.uiCenterDiv = function(divID)
{
	var parentWidth = $(divID).parent().innerWidth();
	var divWidth = getChildrenWidth(divID, parentWidth*.95);
	var w = (parentWidth - divWidth)/2;
	//$(divID)[0].style.width = divWidth + 'px';
	//$(divID)[0].style.left = w + 'px';
	$(divID)[0].style.position = 'relative';
}

function getChildrenWidth(div, max)
{
	var width = 0;
	$(div).children().each(function() 
	{
		width += $(this).outerWidth( false );
	});
	
	return width > max ? max : width;
}
}
