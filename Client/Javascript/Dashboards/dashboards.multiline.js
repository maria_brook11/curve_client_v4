function DateParserFunction($scope, $rootScope, $http, $location, TConst, URLConst, $filter, CountryConst, InsightConst, $timeout, $interval)
{
	//var CurveDataParser = {};

	$scope.parseMultilineData = function (data)
	{
		if (data == null || data.length == 0)
			return [];

		var dataByDate = {};
		var orderableData = [];
		var columns = [];
		var curData = data[0].data;
		
		var curLabel = data[0].label;
		var valData = data[1].data;
		for (var i = 0; i < curData.length; i++)
		{
			var date = curData[i][0];
			var value = curData[i][1];
			var valueLabel = valData[i][1];

			if (dataByDate[date] == null)
				dataByDate[date] = {};

			dataByDate[date][value] = valueLabel;

			if (columns.indexOf(value) == -1)
				columns.push(value);
		}

		for (k in dataByDate)
		{
			var fulldata =
			{
				date: k
			};
			var dataitem = dataByDate[k];

			for (var i = 0; i < columns.length; i++)
			{
				var cur = dataitem[columns[i]];

				if (cur == null)
					dataitem[columns[i]] = "0";
			}

			fulldata.data = dataitem;
			orderableData.push(fulldata);
		}

		function order(a, b)
		{
			if (parseInt(a.date) > parseInt(b.date))
				return 1;
			return -1;
		}

		orderableData.sort(order);
		return $scope.CurveDataParserRefactor(orderableData, columns);
	}

	$scope.CurveDataParserRefactor = function (d, c)
	{
		var colObject = [];

		for (var i = 0; i < c.length; i++)
			colObject[i] =
			{
				label: c[i],
				data: []
			};

		for (var i = 0; i < d.length; i++)
		{
			for (var j = 0; j < c.length; j++)
			{
				colObject[j].data.push([d[i].date, d[i].data[c[j]]]);
			}
		}

		return colObject;
	}
}
