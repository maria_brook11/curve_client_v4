function DiagramFunction($scope,$rootScope,$timeout,diagramParseService,diagramValidationService,$http){
	$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
    delete $http.defaults.headers.common['X-Requested-With'];	
/********************** load data for page ****************************************/	

	$scope.SequenceData = {'platform' : '', 'interactions': [{'type':'','list':[]}], 'actions':[] };
	
	
	
	$scope.getExtServisesData = function(){
		
		$scope.startSQmodal = [];
		
		if($scope.ExternalAPIsTableData.length > 0){
			for(var i = 0;i < $scope.ExternalAPIsTableData.length;i ++){
				if($scope.ExternalAPIsTableData[i].name == 'MailChimp' /* || $scope.ExternalAPIsTableData[i].type == 'mailListMaropost' */){
					if($scope.ExternalAPIsTableData[i].name == 'MailChimp'){
						var extLink = $rootScope.protocol+"://www.mailchimp.com/templates/";
					}/* else{
						var extLink = $rootScope.protocol+"://www.maropost.com";
					} */
					var n = $scope.ExternalAPIsTableData[i].name;
					if($scope.ExternalAPIsTableData[i].values && $scope.ExternalAPIsTableData[i].values[0] != ''){
						var key = true;
					}else{
						var key = false;
					}
					var temp = {'active' : false, 'APIname' : n, 'data' :  {'icon' : n.toLowerCase()+'.jpg','isKey' : key,'isTempl' : false, 'link' : extLink}};
					$scope.startSQmodal.push(temp);
					$scope.GetSequenceJunctionActionsFun(n)
				
				}
			}
			//console.log($scope.startSQmodal)
			
		}
	}
	
	
	$scope.resetDiagramTemplFun = function(){
		
		$('#templateDetails').collapse("hide");
		$('#sequenceDetails').collapse("hide");
		if($scope.templatesArr){
			
			for(var i = 0; i < $scope.templatesArr.length; i ++){ 
				$scope.templatesArr[i].view = false;
			} 
		} 
		if($scope.sequencesArr){
			
			for(var i =0; i < $scope.sequencesArr.length;i ++){	
				$scope.sequencesArr[i].view = false;
			}
		}
	}
	
	$scope.GetSequenceJunctionActionsFun = function(n){
		
		var d = {'Platform' : n};
		var dstr = JSON.stringify(d);
		//52.20.177.127:8080/Curve/GetSequenceJunctionActions?Token=TEST-YZMF8ICL6W8
		$scope.curveAPI('GetSequenceJunctionActions', {'Token': $rootScope.Token,'Data' : JSON.stringify(d)}, GetSequenceJunctionActionsSuccess, GetSequenceJunctionActionsError);
		
		//52.20.177.127:8080/Curve/GetSequenceInteractions?Token=TEST-YZMF8ICL6W8
		$scope.curveAPI('GetSequenceInteractions', {'Token': $rootScope.Token,'Data' : JSON.stringify(d)}, GetSequenceInteractionsSuccess, GetSequenceInteractionsError);
		
		$scope.reloadSequenceFun(n);
		//$scope.curveAPI('GetSequences', {'Token': $rootScope.Token,'Data' : JSON.stringify(d)}, GetSequencesSuccess, GetSequencesError);
		
	/* var url = $rootScope.httpUrl + 'GetSequences';
	
	var data = $.param({
		'Token' : $rootScope.Token,
		'Data' : dstr
	});
	console.log(data)
	
	$http({
		url : url,
		method:"POST",
		data: data
	}).success(function(data, status, headers, config) {
	console.log(data)
		$scope.sequencesArr = data.Data.Sequences;
		$scope.templatesArr = data.Data.Templates;
		
		 if($scope.sequencesArr.length > 0){
		for(var i =0; i < $scope.sequencesArr.length;i ++){
			var view = "view";
			
			if($scope.sequencesArr[i].total_users > 0 && $scope.sequencesArr[i].total_users_in_dead_ends > 0 ){
				$scope.sequencesArr[i].users_completed = (($scope.sequencesArr[i].total_users_in_dead_ends / $scope.sequencesArr[i].total_users) * 100).toFixed(2);
			}else{
				$scope.sequencesArr[i].users_completed = 0;
			}
			
			
			if($scope.sequencesArr[i].running == '1' && $scope.sequencesArr[i].users_completed < 100 ){
				$scope.sequencesArr[i].active = 'Active';
			}
			else if($scope.sequencesArr[i].running == '0' && $scope.sequencesArr[i].users_completed < 100 ){
				$scope.sequencesArr[i].active = 'Paused';
			}
			else if($scope.sequencesArr[i].users_completed == 100 ){
				$scope.sequencesArr[i].active = 'Done';
			}
			
		}
		}  
		for(var i = 0; i < $scope.startSQmodal.length; i ++){
			if($scope.startSQmodal[i].APIname == data.Platform){
				
				var sequencesArr = 'sequencesArr';
				var templatesArr = 'templatesArr';
				
				$scope.startSQmodal[i].data.sequencesArr = $scope.sequencesArr;
				$scope.startSQmodal[i].data.templatesArr = $scope.templatesArr;
			
			}
		}
		
		$scope.statusPage.creatingSequenceRun = false;
		
	}).error(function(data, status, headers, config) {
		console.log(data);
	}); */
	
	}
	
	$scope.reloadSequenceFun = function(n){
		
		$scope.statusPage.creatingSequenceRun = true;
		$scope.sequencesArr = [];
		
		var d = {'Platform' : n};
		//var dstr = JSON.stringify(d);
		
		//localhost:8080/Curve/GetSequences?Token=TEST-YZMF8ICL6W8
		$scope.curveAPI('GetSequences', {'Token': $rootScope.Token,'Data' : JSON.stringify(d)}, GetSequencesSuccess, GetSequencesError);

	}
	
	
	
	function GetSequencesSuccess(data){
		
		$scope.sequencesArr = data.Sequences;
		$scope.templatesArr = data.Templates;
		
		for(var i =0; i < $scope.sequencesArr.length;i ++){
			
			$scope.sequencesArr[i].view = false;
			
			if($scope.sequencesArr[i].total_users > 0 && $scope.sequencesArr[i].total_users_in_dead_ends > 0 ){
				$scope.sequencesArr[i].users_completed = (($scope.sequencesArr[i].total_users_in_dead_ends / $scope.sequencesArr[i].total_users) * 100).toFixed(2);
			}else{
				$scope.sequencesArr[i].users_completed = 0;
			}
			
			
			if($scope.sequencesArr[i].running == '1' && $scope.sequencesArr[i].users_completed < 100 ){
				$scope.sequencesArr[i].active = 'Active';
			}
			else if($scope.sequencesArr[i].running == '0' && $scope.sequencesArr[i].users_completed < 100 ){
				$scope.sequencesArr[i].active = 'Paused';
			}
			else if($scope.sequencesArr[i].users_completed == 100 ){
				$scope.sequencesArr[i].active = 'Done';
			}
			
		}
		
		for(var i = 0; i < $scope.startSQmodal.length; i ++){
			if($scope.startSQmodal[i].APIname == data.platform){
				
				var sequencesArr = 'sequencesArr';
				var templatesArr = 'templatesArr';
				
				$scope.startSQmodal[i].data.sequencesArr = $scope.sequencesArr;
				$scope.startSQmodal[i].data.templatesArr = $scope.templatesArr;
			
			}
		}
		
		$scope.statusPage.creatingSequenceRun = false;
	}
	function GetSequencesError(data){
		console.log(data)
		$scope.statusPage.creatingSequenceRun = false;
	}
	
	function GetSequenceJunctionActionsSuccess(data){
		
			for(var i = 0; i < $scope.startSQmodal.length; i ++){
				if($scope.startSQmodal[i].APIname == data.platform){
					
					var actions_raw = 'actions_raw';
					var actions = 'actions';
					$scope.startSQmodal[i].data.actions = [];
					
					$scope.startSQmodal[i].data.actions_raw = data.actions;
					
					for(var j = 0; j < data.actions.length; j ++){
						$scope.startSQmodal[i].data.actions.push(data.actions[j].description);	
					}
					
				}
			}
		
	}
	function GetSequenceJunctionActionsError(data){
		console.log(data)
	}
	
	function GetSequenceInteractionsSuccess(data){
		
		if(data.interactions[0] != null && data.interactions[0] != 'undefined'){
			for(var i = 0; i < $scope.startSQmodal.length; i ++){
				if($scope.startSQmodal[i].APIname == data.platform){
					if(data.interactions[0].Templates.templates.length > 0){
						
						var templates = 'templates';
						var type = 'type';
						var list = 'list';
						$scope.startSQmodal[i].data.list = [];
						
						$scope.startSQmodal[i].data.isTempl = true;
						$scope.startSQmodal[i].data.templates = data.interactions[0].Templates;
						$scope.startSQmodal[i].data.type = data.interactions[0].Description;
						for(var j = 0; j < data.interactions[0].Templates.templates.length; j ++){
							$scope.startSQmodal[i].data.list.push(data.interactions[0].Templates.templates[j].name);	
						}
					}
				}
			}
		}
		//{'active' : false, 'APIname' : n, 'data' :  {'icon' : n.toLowerCase()+'.jpg','isKey' : key,'isTempl' : false}};
			/* $timeout(function () {
				console.log($scope.startSQmodal)
				if(data[0].Templates.templates.length > 0){
					if($scope.startSQmodal[0].APIname == 'MailChimp')
						$scope.startSQmodal[0].data.isTempl = true;
					if($scope.startSQmodal[0].data.isKey == true){
						$scope.startSQmodal[0].active = true;
					}
					
					$scope.SequenceData.interactions[0] = {'list' : []};
					$scope.SequenceData.interactions[0].templates = data[0].Templates;
					$scope.SequenceData.interactions[0].type = data[0].Description;
			
					for(var i = 0; i < data[0].Templates.templates.length; i ++){
						$scope.SequenceData.interactions[0].list.push(data[0].Templates.templates[i].name);
					}
				}
			}, 3000); */
			
		
		/* if($scope.startSQmodal.Maropost.MaropostKey == true){
			$scope.SequenceData.MaropostMailArr = ["template 1","template 2","template 3"];
		} */
		//console.log($scope.SequenceData.MaropostMailArr)
	}
	
	function GetSequenceInteractionsError(data){
		console.log(data)
	}

/********************** end load data for page ****************************************/

/********************** details/status/delete sequence funs **************************************/
	
	$scope.viewDiagramDataFun = function(tID){
		
		for(var i = 0; i < $scope.templatesArr.length; i ++){ 
			if($scope.templatesArr[i].sequencetemplateid == tID){
				var diagramData = JSON.parse($scope.templatesArr[i].diagram);
				//console.log(diagramData)
				$scope.templatesArr[i].diagramFullData = diagramData;
				
			}
		}
		//console.log($scope.templatesArr)
	};
	
	$scope.GetSequenceDetailsFun = function(sID,tID){
		
		var getDetailsFlag = false;
		
		for(var k = 0 ;k < $scope.sequencesArr.length; k ++){
			
			if($scope.sequencesArr[k].usersequenceid == sID){
				if(!$scope.sequencesArr[k].diagramFullData || $scope.sequencesArr[k].diagramFullData == null){
					getDetailsFlag = true;
				}
			}
		}
	//localhost:8080/Curve/GetSequenceDetails?Token=TEST-YZMF8ICL6W8&Data={"SequenceId":"39"}
		
		if(getDetailsFlag){
			var d = {"SequenceId" : sID};	
			$scope.curveAPI('GetSequenceDetails', {'Token': $rootScope.Token, 'Data' : JSON.stringify(d)}, GetSequenceDetailsSuccess, GetSequenceDetailsError);
		}
		
		function GetSequenceDetailsSuccess(data){
			
			var diagramData = JSON.parse(data.Diagram[0].diagram);
			 
			for(var k = 0 ;k < $scope.sequencesArr.length; k ++){
				if($scope.sequencesArr[k].usersequenceid == sID){
					
					if(data.Interactions.length > 0){
						for(var j = 0; j < data.Interactions.length; j ++){
							for(var v = 0; v < diagramData.nodeDataArray.length; v ++){
								if(diagramData.nodeDataArray[v].category == "Interaction"){
									if(data.Interactions[j].id == diagramData.nodeDataArray[v].key){
										if(data.Interactions[j].emails){
											diagramData.nodeDataArray[v].env = data.Interactions[j].emails;
										}else{
											diagramData.nodeDataArray[v].env = '0';
										}
										if(data.Interactions[j].open_emails){
											diagramData.nodeDataArray[v].envOpen = data.Interactions[j].open_emails;
										}else{
											diagramData.nodeDataArray[v].envOpen = '0';
										}
									}
								}
							}
						}
					}
					
					if(data.Junctions.length > 0){
						
						for(var j = 0; j < data.Junctions.length; j ++){
							if(data.Junctions[j].junction_id != -1){
								for(var v = 0; v < diagramData.nodeDataArray.length; v ++){
									if(diagramData.nodeDataArray[v].category == "Junction"){
										
										if(data.Junctions[j].junction_id == diagramData.nodeDataArray[v].key){
											if(data.Junctions[j].users){
												diagramData.nodeDataArray[v].users = data.Junctions[j].users;
											}else{
												diagramData.nodeDataArray[v].users = '0';
											}	
											
										}
									}
								}
							}
						}
					}
					var diagramFullData = "diagramFullData";
					$scope.sequencesArr[k].diagramFullData = diagramData;
					break;
					
				}
				
			}
			
			
		}
		
		function GetSequenceDetailsError(data){
			console.log(data)
		}
		
	}
	
		$scope.changeSequenceStatusFun = function(id,e){
		//localhost:8080/Curve/ChangeSequenceStatus?Token=TEST-YZMF8ICL6W8&Data={"Sequence":"1", "Status":"1"}	
		
		var d = {"Sequence": id, "Status": e };
		$scope.curveAPI('ChangeSequenceStatus', {'Token': $rootScope.Token, 'Data' : JSON.stringify(d)}, ChangeSequenceStatusSuccess, ChangeSequenceStatusError);	

		function ChangeSequenceStatusSuccess(data){
			
			for(var i = 0; i < $scope.sequencesArr.length; i ++){
				if($scope.sequencesArr[i].usersequenceid == id){
					$scope.sequencesArr[i].running = e;
					
					if($scope.sequencesArr[i].running == '1' && $scope.sequencesArr[i].users_completed < 100 ){
						$scope.sequencesArr[i].active = 'Active';
					}
					else if($scope.sequencesArr[i].running == '0' && $scope.sequencesArr[i].users_completed < 100 ){
						$scope.sequencesArr[i].active = 'Paused';
					}
					else if($scope.sequencesArr[i].users_completed == 100 ){
						$scope.sequencesArr[i].active = 'Done';
					}
					
					break;
				}
			}
			
		}
		function ChangeSequenceStatusError(data){
			console.log(data)
		}
	}
	
	$scope.deleteSequenceFun = function(){
		//localhost:8080/Curve/DeleteSequence?Token=TEST-YZMF8ICL6W8&Data={"Sequences":["1","2","3"]}	
		//console.log($scope.statusPage.deleteSQid)
		var id = $scope.statusPage.deleteSQid;
		
		var d = {"Sequences":[id]};
		$scope.curveAPI('DeleteSequence', {'Token': $rootScope.Token, 'Data' : JSON.stringify(d)}, DeleteSequenceSuccess, DeleteSequenceError);	

		function DeleteSequenceSuccess(data){
			
			for(var i = 0; i < $scope.sequencesArr.length; i ++){
				if($scope.sequencesArr[i].usersequenceid == id){
					$scope.sequencesArr.splice(i, 1);
					break;
				}
			}
		$scope.statusPage.deleteSQid = "";	
		$scope.statusPage.deleteItem = "";
		}
		function DeleteSequenceError(data){
			console.log(data)
			$scope.statusPage.deleteSQid = "";
			$scope.statusPage.deleteItem = "";
		}
	}

	$scope.deleteTemplateFun = function(){
		//localhost:8080/Curve/DeleteSequenceTemplate?Token=TEST-YZMF8ICL6W8&Data={"TemplateId":"56"}
		var id = $scope.statusPage.deleteSQid;
		var d = {"TemplateId": id};
		$scope.curveAPI('DeleteSequenceTemplate', {'Token': $rootScope.Token, 'Data' : JSON.stringify(d)}, DeleteSequenceTemplateSuccess, DeleteSequenceTemplateError);	

		function DeleteSequenceTemplateSuccess(data){
			
			for(var i = 0; i < $scope.templatesArr.length; i ++){
				if($scope.templatesArr[i].sequencetemplateid == id){
					$scope.templatesArr.splice(i, 1);
					break;
				}
			}
		$scope.statusPage.deleteSQid = "";	
		$scope.statusPage.deleteItem = "";
		}
		function DeleteSequenceTemplateError(data){
			console.log(data)
			$scope.statusPage.deleteSQid = "";
			$scope.statusPage.deleteItem = "";
		}
	}
	
/********************** end details/status/delete sequence funs **************************************/

/********************** start sequence modal funs **************************************/
	
	$scope.setActiveAPI = function(n){
		console.log($scope.startSQmodal)
		for(var i = 0; i < $scope.startSQmodal.length; i++){
			if($scope.startSQmodal[i].APIname == n){
				$scope.startSQmodal[i].active = true;
				if($scope.startSQmodal[i].data.isKey && $scope.startSQmodal[i].data.isTempl){
					$('#startSQmodal').modal('hide');
					
					//$scope.SequenceData.interactions[0] = {'list' : []};
					var list = "list";
					$scope.SequenceData.interactions[0].templates = $scope.startSQmodal[i].data.templates;
					$scope.SequenceData.interactions[0].type = $scope.startSQmodal[i].data.type;
					$scope.SequenceData.interactions[0].list = $scope.startSQmodal[i].data.list;
					
					$scope.SequenceData.actions_raw = $scope.startSQmodal[i].data.actions_raw;
					$scope.SequenceData.actions = $scope.startSQmodal[i].data.actions;
					$scope.startSQmodal.platform = n;
					var t = n.toLowerCase();
					$scope.startSQmodal.platformIcon = "Images/api_icons/"+t+".jpg";
					//console.log($scope.SequenceData)
					$scope.startSequenceModalFun();
				}
			}else{
				$scope.startSQmodal[i].active = false;
			}
		}
		
	};
	
	$scope.statusPage = {};
	$scope.statusPage.creatingSequenceRun = false;
	
	$scope.closeStartSQmodal = function(){
		$scope.clearsequenceModal();
		$('#startSequence').modal('hide');
	};
	
	$scope.showMessFun = function(e){
		if(e == 's'){
			var textMess = "You currently don't have any running sequences to edit & duplicate, please select 'Scratch' to create a new one or start from an existing template.";
			alert(textMess);
		}
		if(e == 't'){
			var textMess = "You currently don't have any templates to edit, please select 'Scratch' to create a new one.";
			alert(textMess);
		}
		
	};

	$scope.saveTmplFun = function(){
		$scope.saveDiagramData();
		$scope.CreateSequenceTemplateFun();
	}
	
	$scope.AddCampaignFun = function(){
		
		$scope.sequenceModal.loading = true;
		$scope.curveAPI('AddCampaign', {Token:$rootScope.Token, Name:$scope.sequenceModal.CampaignName, RedirectLink:$scope.sequenceModal.RedirectLink}, AddCampaignSuccess, AddCampaignError);
		
		function AddCampaignSuccess(data){
			$scope.sequenceModal.loading = false;
			
			var param = '&u=*|EMAIL|*&i=*|CAMPAIGN_UID|*';
			data[0].linkForMC = data[0].trackinglink + param;
			data[0].Name = data[0].name;
			$scope.campLinksArr.unshift(data[0]);
	
		}
		function AddCampaignError(data){
			$scope.sequenceModal.loading = false;
			console.log(data)
		}
	}
	
	$scope.SQModalhide = function(){
		$('#startSQmodal').modal('hide');
	}
	
	$scope.getDefaultMailFun = function(){
		if(!$scope.sequenceModal.fromm || $scope.sequenceModal.fromm == ''){
			$scope.sequenceModal.fromm = 'Name';
		}
		if(!$scope.sequenceModal.replyTo || $scope.sequenceModal.replyTo == ''){
			var email = $scope.loadFromStorage('email');
			$scope.sequenceModal.replyTo = email;
		}
	}
	
	$scope.startSequenceModalFun = function(){
		console.log('startSequenceModalFun')
		$scope.sequenceModal = {'step' : 1,'type' : 0, 'showMess' : false, 'loading' : false, 'saveTempl' : false, 'links' : {}, 'curTplID' : ''};
		$scope.getDefaultMailFun();
		$scope.clearFilters();
		$('#startSequence').modal('show');
		//$scope.curveAPI('GetAudiences', {'Token': $rootScope.Token}, GetAudiencesSuccess, GetAudiencesError);
		
		/* function GetAudiencesSuccess(data){
		console.log('6565656565656565')
			$scope.AudiencesArray = [];
			if(data.length){
				for(var i = 0; i < data.length; i ++){
					var t = {"id" : data[i].audienceid, "name" : data[i].name, "add" : false};
					$scope.AudiencesArray.push(t);
				}
			}
			$('#startSequence').modal('show');
		}
		

		function GetAudiencesError(data){
			console.log('98998988989898')
		}
		 */
	}


	
	$scope.detSQdataFun = function(id){
		$scope.sequenceModal.diagram = '';
		
		/* for(var i = 0; i < $scope.sequencesArr[0].associated_audiences.lenght; i ++){
			for(var j = 0; j < $scope.AudiencesArray.lenght; j ++){
				if($scope.sequencesArr[0].associated_audiences[i] == $scope.AudiencesArray[j].id){
					$scope.AudiencesArray[j].add = true;
				}
			}
		} */
		
		var d = {"SequenceId" : id};	
		$scope.curveAPI('GetSequenceDetails', {'Token': $rootScope.Token, 'Data' : JSON.stringify(d)}, detSQdataSuccess, detSQdataError);
		
		function detSQdataSuccess(data){
			
			var t = data.Diagram[0].diagram;
			
			$scope.sequenceModal.diagram = JSON.parse(t);
			
		}
		function detSQdataError(data){
			console.log(data)
		}
		
	}
	$scope.detTdataFun = function(id){
		
		for(var i = 0; i < $scope.templatesArr.length; i ++){ 
			if($scope.templatesArr[i].sequencetemplateid == id){
				$scope.sequenceModal.diagram = JSON.parse($scope.templatesArr[i].diagram);
				$scope.sequenceModal.diagramData = diagramParseService.getParsedData($scope.sequenceModal.diagram, $scope.SequenceData.interactions[0].templates, $scope.SequenceData.actions_raw);
				
				break;
			}
		}
		
	}
	
	$scope.editSequenceFun = function(){
		
		var temp = [];
		
		var tempStr = $('#AudiencesArray multiselect-dropdown .objValue').val();
		var tempObj = JSON.parse(tempStr);
	
		angular.forEach(tempObj, function (val,key){
			if(val.add){
				temp.push(val.id);
			} 
		})

		if(temp.length > 0 && $scope.sequenceModal.name && $scope.sequenceModal.name != ''){
			
			$scope.statusPage.creatingSequenceRun = true;
			
			var d = {"Platform" : $scope.startSQmodal.platform ,"SequenceId": $scope.sequenceModal.SQid, "Name" : $scope.sequenceModal.name,"From": $scope.sequenceModal.fromm,"ReplyTo" : $scope.sequenceModal.replyTo, "Diagram": JSON.stringify($scope.sequenceModal.diagram), "Data": JSON.stringify($scope.sequenceModal.diagramData), "Audiences": temp, "Status": "1"};
			
			var url = $rootScope.httpUrl + 'EditSequence';
			
			var data = $.param({
				'Token' : $rootScope.Token,
				'Data' : JSON.stringify(d)
			});
				
			$http({
				url : url,
				method:"POST",
				data: data
			}).success(function(data, status, headers, config) {
				
				
				for(var i = 0; i < $scope.sequencesArr.length; i ++){ 
					if($scope.sequencesArr[i].usersequenceid == $scope.sequenceModal.SQid){
						$scope.sequencesArr.splice(i,1);
						break;
					}
				}
				$scope.closeStartSQmodal();
				
				if(data.Data[0].total_users > 0 && data.Data[0].total_users_completed_sequence > 0 ){
					data.Data[0].users_completed = ((data.Data[0].total_users_completed_sequence / data.Data[0].total_users) * 100).toFixed(2);
				}else{
					data.Data[0].users_completed = 0;
				}
				
				if(data.Data[0].running == '1' && data.Data[0].users_completed < 100 ){
					data.Data[0].active = 'Active';
				}
				else if(data.Data[0].running == '0' && data.Data[0].users_completed < 100 ){
					data.Data[0].active = 'Paused';
				}
				else if(data.Data[0].users_completed == 100 ){
					data.Data[0].active = 'Done';
				}
					
				$scope.sequencesArr.unshift(data.Data[0]);
				$scope.statusPage.creatingSequenceRun = false;
				
					
				
			}).error(function(data, status, headers, config) {
				console.log(data);
				$scope.closeStartSQmodal();
				$scope.statusPage.creatingSequenceRun = false;
			});
			
			//localhost:8080/Curve/StartNewSequence?Token=TEST-YZMF8ICL6W8&Data={"SequenceId":"44","Name":"Sequence 1","Description":"CreateSequence test","SequenceTemplateId":"101", "Audiences":"["1","2","3"]","Status":"1"}
			//$scope.curveAPI('EditSequence', {'Token': $rootScope.Token, 'Data' : JSON.stringify(d)}, EditSequenceSuccess, EditSequenceError);
			
	
		}else{
			$scope.sequenceModal.showMess = true;
		}
		
	}
	
	$scope.startSequenceFun = function(){
		
		var temp = [];
		
		var tempStr = $('#AudiencesArray multiselect-dropdown .objValue').val();
		var tempObj = JSON.parse(tempStr);
		var audiencesIDS = [];
		var behaviorsIDS = [];
		var ranksIDS = [];
		angular.forEach(tempObj, function (val,key){
			if(val.add){
				if(val.type == 'audience'){
					audiencesIDS.push(val.id);
				}
				if(val.type == 'behavior'){
					behaviorsIDS.push(val.id);
				}
				if(val.type == 'rank'){
					ranksIDS.push(val.id);
				}
			} 
		})
		
		
		if((audiencesIDS.length > 0 || behaviorsIDS.length > 0 || ranksIDS.length > 0) && $scope.sequenceModal.name && $scope.sequenceModal.name != ''){
			
			if(audiencesIDS.length > 0 && behaviorsIDS.length > 0 && ranksIDS.length > 0){
				
	var d = {"Audiences": audiencesIDS,"Behaviors": behaviorsIDS,"Ranks": ranksIDS,  "Platform" : $scope.startSQmodal.platform ,"Name" : $scope.sequenceModal.name,"From": $scope.sequenceModal.fromm,"ReplyTo" : $scope.sequenceModal.replyTo, "Diagram": JSON.stringify($scope.sequenceModal.diagram), "Data": JSON.stringify($scope.sequenceModal.diagramData), "Status": "1"};
			
			}else{
				if(audiencesIDS.length > 0 && behaviorsIDS.length > 0){
	var d = {"Audiences": audiencesIDS,"Behaviors": behaviorsIDS, "Platform" : $scope.startSQmodal.platform ,"Name" : $scope.sequenceModal.name,"From": $scope.sequenceModal.fromm,"ReplyTo" : $scope.sequenceModal.replyTo, "Diagram": JSON.stringify($scope.sequenceModal.diagram), "Data": JSON.stringify($scope.sequenceModal.diagramData), "Status": "1"};
					
				}
				if(audiencesIDS.length > 0 && ranksIDS.length > 0){
	var d = {"Audiences": audiencesIDS, "Ranks": ranksIDS,  "Platform" : $scope.startSQmodal.platform ,"Name" : $scope.sequenceModal.name,"From": $scope.sequenceModal.fromm,"ReplyTo" : $scope.sequenceModal.replyTo, "Diagram": JSON.stringify($scope.sequenceModal.diagram), "Data": JSON.stringify($scope.sequenceModal.diagramData), "Status": "1"};
					
				}
				if(behaviorsIDS.length > 0 && ranksIDS.length > 0){
	var d = {"Behaviors": behaviorsIDS,"Ranks": ranksIDS,  "Platform" : $scope.startSQmodal.platform ,"Name" : $scope.sequenceModal.name,"From": $scope.sequenceModal.fromm,"ReplyTo" : $scope.sequenceModal.replyTo, "Diagram": JSON.stringify($scope.sequenceModal.diagram), "Data": JSON.stringify($scope.sequenceModal.diagramData), "Status": "1"};
					
				}
				if(audiencesIDS.length > 0 && behaviorsIDS.length == 0 && ranksIDS.length == 0){
	var d = {"Audiences": audiencesIDS, "Platform" : $scope.startSQmodal.platform ,"Name" : $scope.sequenceModal.name,"From": $scope.sequenceModal.fromm,"ReplyTo" : $scope.sequenceModal.replyTo, "Diagram": JSON.stringify($scope.sequenceModal.diagram), "Data": JSON.stringify($scope.sequenceModal.diagramData), "Status": "1"};
					
				}
				if(audiencesIDS.length == 0 && behaviorsIDS.length > 0 && ranksIDS.length == 0){
	var d = {"Behaviors": behaviorsIDS, "Platform" : $scope.startSQmodal.platform ,"Name" : $scope.sequenceModal.name,"From": $scope.sequenceModal.fromm,"ReplyTo" : $scope.sequenceModal.replyTo, "Diagram": JSON.stringify($scope.sequenceModal.diagram), "Data": JSON.stringify($scope.sequenceModal.diagramData), "Status": "1"};
					
				}
				if(audiencesIDS.length == 0 && behaviorsIDS.length == 0 && ranksIDS.length > 0){
	var d = {"Ranks": ranksIDS,  "Platform" : $scope.startSQmodal.platform ,"Name" : $scope.sequenceModal.name,"From": $scope.sequenceModal.fromm,"ReplyTo" : $scope.sequenceModal.replyTo, "Diagram": JSON.stringify($scope.sequenceModal.diagram), "Data": JSON.stringify($scope.sequenceModal.diagramData), "Status": "1"};
					
				}
			}
			
			$scope.statusPage.creatingSequenceRun = true;
			var url = $rootScope.httpUrl + 'StartNewSequence';
			
			var data = $.param({
				'Token' : $rootScope.Token,
				'Data' : JSON.stringify(d)
			});
			
			$scope.closeStartSQmodal();	
			$http({
				url : url,
				method:"POST",
				data: data
			}).success(function(data, status, headers, config) {
					
				if(data.Data[0].total_users > 0 && data.Data[0].total_users_completed_sequence > 0 ){
					data.Data[0].users_completed = ((data.Data[0].total_users_completed_sequence / data.Data[0].total_users) * 100).toFixed(2);
				}else{
					data.Data[0].users_completed = 0;
				}
				
				if(data.Data[0].running == '1' && data.Data[0].users_completed < 100 ){
					data.Data[0].active = 'Active';
				}
				else if(data.Data[0].running == '0' && data.Data[0].users_completed < 100 ){
					data.Data[0].active = 'Paused';
				}
				else if(data.Data[0].users_completed == 100 ){
					data.Data[0].active = 'Done';
				}
				
				$scope.sequencesArr.unshift(data.Data[0]);
				$scope.statusPage.creatingSequenceRun = false;
				
					
				
			}).error(function(data, status, headers, config) {
				console.log(data);
				$scope.closeStartSQmodal();
				$scope.statusPage.creatingSequenceRun = false;
			});
		
			//localhost:8080/Curve/StartNewSequence?Token=TEST-YZMF8ICL6W8&Data={"Name":"Sequence 1","Description":"CreateSequence test","SequenceTemplateId":"101", "Audiences":"["1","2","3"]","Status":"1"}
			//$scope.curvePostAPI('StartNewSequence', {'Token': $rootScope.Token, 'Data' : JSON.stringify(d)}, StartNewSequenceSuccess, StartNewSequenceError);
			
		
		}else{
			$scope.sequenceModal.showMess = true;
		}
		
	}
	
	$scope.newDiagram = {"success" : false};
	
	$scope.CreateSequenceTemplateFun = function(){
		
		saveDiagramProperties();  // do this first, before writing to JSON
	 
		myDiagram.isModified = false;
		var tempD = myDiagram.model.toJson();
		var diagramJson = JSON.parse(tempD);
		//console.log(diagramJson)
		
		for(var i = 0;i < diagramJson.nodeDataArray.length; i ++){
			if(diagramJson.nodeDataArray[i].category == "Junction"){
				if(!diagramJson.nodeDataArray[i].number){
					diagramJson.nodeDataArray[i].number = "1";
				}
			}
		}
		
		$scope.sequenceModal.diagram = diagramJson;
		$scope.sequenceModal.diagramData = diagramParseService.getParsedData(JSON.parse(tempD), $scope.SequenceData.interactions[0].templates, $scope.SequenceData.actions_raw);
		
		
		
		  //localhost:8080/Curve/CreateSequenceTemplate?Token=TEST-YZMF8ICL6W8&Data={"Name":"1", "Description ":"1", "Data":{}}
		var d = {"Platform" : $scope.startSQmodal.platform ,"Name":$scope.newDiagram.templNewName,"Diagram": JSON.stringify($scope.sequenceModal.diagram), "Data": JSON.stringify($scope.sequenceModal.diagramData)};
		
		var url = $rootScope.httpUrl + 'CreateSequenceTemplate';
			
		var data = $.param({
			'Token' : $rootScope.Token,
			'Data' : JSON.stringify(d)
		});
			
		$http({
			url : url,
			method:"POST",
			data: data
		}).success(function(data, status, headers, config) {
			var d = {'Platform' : $scope.startSQmodal.platform};
			$scope.curveAPI('GetSequences', {'Token': $rootScope.Token,'Data' : JSON.stringify(d)}, updateTemplatesSuccess, updateTemplatesError);
			$scope.newDiagram.success = true;
			$timeout(function () {
				$scope.newDiagram = {"success" : false};
			}, 1000);
			$scope.sequenceModal.saveTempl = false;
				
			function updateTemplatesSuccess(data){
				$scope.templatesArr = '';
				$scope.templatesArr = data.Templates;
				
				/* for(var i = 0; i < $scope.templatesArr.length; i ++){ 
					
					var diagramData = JSON.parse($scope.templatesArr[i].diagram);
					$scope.templatesArr[i].diagramFullData = diagramData;
					//$scope.templatesArr[i].view = false;
				} */
				//console.log($scope.templatesArr)
			}
			function updateTemplatesError(data){console.log(data)}
			
		}).error(function(data, status, headers, config) {
			console.log(data);
			$scope.statusPage.creatingSequenceRun = false;
		});
		
		//$scope.curvePostAPI('CreateSequenceTemplate', {'Token': $rootScope.Token, 'Data' : JSON.stringify(d)}, CreateSequenceTemplateSuccess, CreateSequenceTemplateError);	
		
	}
	
	$scope.saveDiagramData = function(step) {
		saveDiagramProperties();  // do this first, before writing to JSON
	 
		myDiagram.isModified = false;
		var tempD = myDiagram.model.toJson();
		var diagramJson = JSON.parse(tempD);
		//console.log(diagramJson)
		for(var i = 0;i < diagramJson.nodeDataArray.length; i ++){
			if(diagramJson.nodeDataArray[i].category == "Junction"){
				if(!diagramJson.nodeDataArray[i].number){
					diagramJson.nodeDataArray[i].number = "1";
				}
			}
		}
		
		$scope.sequenceModal.diagram = diagramJson;
		$scope.sequenceModal.diagramData = diagramParseService.getParsedData(JSON.parse(tempD), $scope.SequenceData.interactions[0].templates, $scope.SequenceData.actions_raw);
		
		var valid = diagramValidationService.diagramValidation($scope.sequenceModal.diagramData);
		
		 if(valid.errors.length > 0){
			
			alert(valid.errors);
		}else{
			
			$scope.sequenceModal.links = {};
			$scope.ifClickAction();
			$scope.sequenceModal.step = step;
		}
		
	}
	
	
	$scope.ifClickAction = function() {
		var tempInterIDarr = [];
		$scope.interactionDataObj = {"idArr" : [],"data" : []};
		
		for(var i = 0 ;i < $scope.sequenceModal.diagramData.junctions.length;i ++){
			if($scope.sequenceModal.diagramData.junctions[i].junction_type == "yesno" && $scope.sequenceModal.diagramData.junctions[i].action_id == "click"){
				for(var j = 0; j < $scope.sequenceModal.diagramData.junctions[i].connected_interaction_ids.length;j ++){
					if(tempInterIDarr.indexOf($scope.sequenceModal.diagramData.junctions[i].connected_interaction_ids[j]) == -1){
						tempInterIDarr.push($scope.sequenceModal.diagramData.junctions[i].connected_interaction_ids[j])
					}
				}
			}
		}
		
		if(tempInterIDarr.length > 0){
			for(var j = 0;j < tempInterIDarr.length;j ++){
				for(var i = 0 ;i < $scope.sequenceModal.diagramData.interactions.length;i ++){
					if(tempInterIDarr[j] ==  $scope.sequenceModal.diagramData.interactions[i].id){
						if($scope.interactionDataObj.idArr.indexOf($scope.sequenceModal.diagramData.interactions[i].interaction_id) == -1){
							$scope.interactionDataObj.idArr.push($scope.sequenceModal.diagramData.interactions[i].interaction_id);
							for(var k = 0 ;k < $scope.SequenceData.interactions[0].templates.templates.length;k ++){
								if($scope.sequenceModal.diagramData.interactions[i].interaction_id == $scope.SequenceData.interactions[0].templates.templates[k].id){
									var t = {"id" : $scope.SequenceData.interactions[0].templates.templates[k].id ,"name" : $scope.SequenceData.interactions[0].templates.templates[k].name,"link" : ""};
									$scope.interactionDataObj.data.push(t);
									
								} 
							}
						}
					}
				}
			}
		}
		$scope.sequenceModal.links = $scope.interactionDataObj;
	}
	
	$scope.mmodel = {};
	
	$scope.setLinkFun = function(){
		for(var i = 0; i < $scope.sequenceModal.links.data.length; i ++){
			if($scope.sequenceModal.links.data[i].id == $scope.sequenceModal.curTplID){
				$scope.sequenceModal.links.data[i].link = $scope.mmodel.selectedLL;
				break;
			}
		}
	}

	
	$scope.clearsequenceModal = function() {
		$scope.sequenceModal = {'step' : 1,'type' : 0, 'showMess' : false, 'loading' : false, 'saveTempl' : false, 'links' : {}, 'curTplID' : ''}
		$scope.getDefaultMailFun();
	}
	
	$scope.clearDiagramData = function() {
		
		if($scope.sequenceModal.type == 3){  
			var diagramEmpty = { "class": "go.GraphLinksModel",
				"linkFromPortIdProperty": "fromPort",
				"linkToPortIdProperty": "toPort",
				"modelData": {"position":"-5 -613"},
				"nodeDataArray": [ 
					{"category":"Description", "text":"Here we can create or edit Squences by drag and drop them to the canvas", "key":-2, "loc":"60 -600"},
					{"category":"Start", "text":"Start", "key":-1, "loc":"-70 -530"},
					{"category":"End", "figure":"Circle", "fill":"#CE0620", "key":-4, "loc":"730 -230"}
				],
				"linkDataArray": []};
			myDiagram.model = go.Model.fromJson(diagramEmpty);
			loadDiagramProperties();  // do this after the Model.modelData has been brought into memory
		}
		
		if($scope.sequenceModal.type == 2){  
			$scope.sequenceModal = {'step' : 2,'type' : 2, 'showMess' : false, 'loading' : false, 'saveTempl' : false, 'links' : {}, 'curTplID' : ''}
		} 
		if($scope.sequenceModal.type == 1){  
			$scope.sequenceModal = {'step' : 2,'type' : 1, 'showMess' : false, 'loading' : false, 'saveTempl' : false, 'links' : {}, 'curTplID' : ''}
		} 
	 
	}
	
	$scope.initDiagram = function(e){
		
		$scope.sequenceModal.step = e;
		
		if($scope.sequenceModal.type == 3){
			if(!$scope.sequenceModal.diagram){
				$scope.sequenceModal.diagram = { "class": "go.GraphLinksModel",
					"linkFromPortIdProperty": "fromPort",
					"linkToPortIdProperty": "toPort",
					"modelData": {"position":"-5 -613"},
					"nodeDataArray": [ 
						{"category":"Description", "text":"Here we can create or edit Squences by drag and drop them to the canvas", "key":-2, "loc":"60 -600"},
						{"category":"Start", "text":"Start", "key":-1, "loc":"-70 -530"},
						{"category":"End", "figure":"Circle", "fill":"#CE0620", "key":-4, "loc":"730 -230"}
					],
					"linkDataArray": []};
			}
			
		}
		
		if($scope.sequenceModal.type == 2){
			
			if(!$scope.sequenceModal.diagram){
				for(var i = 0; i < $scope.templatesArr.length; i ++){ 
					if($scope.templatesArr[i].sequencetemplateid == $scope.sequenceModal.templID){
						$scope.sequenceModal.diagram = JSON.parse($scope.templatesArr[i].diagram);
						
						break;
					}
				}
			}
		}
		
		if($scope.sequenceModal.type == 1){
			if($scope.sequenceModal.diagram){
				for(var i = 0; i < $scope.templatesArr.length; i ++){ 
					if($scope.templatesArr[i].sequencetemplateid == $scope.sequenceModal.templID){
						$scope.sequenceModal.diagram = JSON.parse($scope.templatesArr[i].diagram);
						
						break;
					}
				}
			}else{
				
				var d = {"SequenceId" : $scope.sequenceModal.SQid};	
				$scope.curveAPI('GetSequenceDetails', {'Token': $rootScope.Token, 'Data' : JSON.stringify(d)}, SQdataSuccess);
				
				function SQdataSuccess(data){
					
					var t = data.Diagram[0].diagram;
					
					$scope.sequenceModal.diagram = JSON.parse(t);
					
				}
			}
		} 
		
		
		$timeout(function () {
			if(!document.getElementById("myDiagramDiv")){
				document.getElementById("diagramCanvasBox").innerHTML = '<div id="myDiagramDiv" style="height: 460px;position: relative;cursor: auto;border: 1px solid #dcdcdc;border-radius: 10px;" ></div>';
				startDiagramFun();
			}else{
				
				var diagramEmpty = $scope.sequenceModal.diagram;
				myDiagram.model = go.Model.fromJson(diagramEmpty);
				loadDiagramProperties();  // do this after the Model.modelData has been brought into memory
			}	
		}, 100);

	}

	function load() {
		var diagramEmpty = $scope.sequenceModal.diagram;

		$timeout(function () {
			myDiagram.model = go.Model.fromJson(diagramEmpty);
			loadDiagramProperties();  // do this after the Model.modelData has been brought into memory			
		}, 100);
	
	}

	function saveDiagramProperties() {
		myDiagram.model.modelData.position = go.Point.stringify(myDiagram.position);
	}
	
	function loadDiagramProperties(e) {
    // set Diagram.initialPosition, not Diagram.position, to handle initialization side-effects
		var pos = myDiagram.model.modelData.position;
		if (pos) myDiagram.initialPosition = go.Point.parse(pos);
	}
  
	
/********************** end start sequence modal funs **************************************/

/******************************************** diagram funs ******************************************/

function startDiagramFun(){
	//console.log($scope.SequenceData)
	// init for these samples -- you don't need to call this
	if (window.goSamples){goSamples(); }
	
    var $ = go.GraphObject.make;  // for conciseness in defining templates

    myDiagram =
      $(go.Diagram, "myDiagramDiv",  // must name or refer to the DIV HTML element
        {
          grid: $(go.Panel, "Grid",
					$(go.Shape, "LineH", { stroke: "#efefef", strokeWidth: 0.5 }),
					$(go.Shape, "LineV", { stroke: "#efefef", strokeWidth: 0.5 })
                ),
         "clickCreatingTool.insertPart": function(loc) {  // customize the data for the new node
            this.archetypeNodeData = {
              
				number: "",
				timePeriod : "",
				title : ""
          
            };
            return go.ClickCreatingTool.prototype.insertPart.call(this, loc);
          },
		 
		  allowDrop: true,  // must be true to accept drops from the Palette
          "draggingTool.dragsLink": true,
          "draggingTool.isGridSnapEnabled": true,
          "linkingTool.isUnconnectedLinkValid": false,
          "linkingTool.portGravity": 20,
          "relinkingTool.isUnconnectedLinkValid": true,
          "relinkingTool.portGravity": 20,
          "relinkingTool.fromHandleArchetype":
            $(go.Shape, "Diamond", { segmentIndex: 0, cursor: "pointer", desiredSize: new go.Size(8, 8), fill: "tomato", stroke: "darkred" }),
          "relinkingTool.toHandleArchetype":
            $(go.Shape, "Diamond", { segmentIndex: -1, cursor: "pointer", desiredSize: new go.Size(8, 8), fill: "darkred", stroke: "tomato" }),
          "linkReshapingTool.handleArchetype":
            $(go.Shape, "Diamond", { desiredSize: new go.Size(7, 7), fill: "lightblue", stroke: "deepskyblue" }),
          rotatingTool: $(TopRotatingTool),  // defined below
          "rotatingTool.snapAngleMultiple": 15,
          "rotatingTool.snapAngleEpsilon": 15,
          "undoManager.isEnabled": true
        });

		
			/******************************** select event *****************************/	
 	/* var selectEvent = document.createElement("select");
		var op;
		var list = [$scope.SequenceData.interactions[0].type];
		var l = list.length;
		for (var i = 0; i < l; i++) {
			op = document.createElement("option");
			op.text = list[i];
			op.value = list[i];
			selectEvent.add(op, null);
		}
	
	 selectEvent.onActivate = function() {
		selectEvent.value = selectEvent.textEditingTool.textBlock.text;
		var loc = selectEvent.textEditingTool.textBlock.getDocumentPoint(go.Spot.TopLeft);
		var pos = myDiagram.transformDocToView(loc);
		selectEvent.style.left = pos.x + "px";
		selectEvent.style.top  = pos.y + "px";
		selectEvent.style.visibility = "visible";
		selectEvent.style.width = "120px";
		selectEvent.style.height = "30px";
		selectEvent.style.border = "none";
	}  */
	
	/******************************** select event *****************************/
	
	/******************************** select template mailChimp*****************************/
		
		var list = $scope.SequenceData.interactions[0].list;
		
		var selectTemplate = document.createElement("select");
		var op;
		var l = list.length;
		for (var i = 0; i < l; i++) {
			op = document.createElement("option");
			op.text = list[i];
			op.value = list[i];
			selectTemplate.add(op, null);
		}
	
	 selectTemplate.onActivate = function() {
		selectTemplate.value = selectTemplate.textEditingTool.textBlock.text;
		var loc = selectTemplate.textEditingTool.textBlock.getDocumentPoint(go.Spot.TopLeft);
		var pos = myDiagram.transformDocToView(loc);
		selectTemplate.style.left = pos.x + "px";
		selectTemplate.style.top  = pos.y + "px";
		selectTemplate.style.visibility = "visible";
		selectTemplate.style.width = "120px";
		selectTemplate.style.height = "30px";
		selectTemplate.style.border = "none";
	}
	

		/******************************** select action *****************************/	
		var selectAction = document.createElement("select");
		var op;
		var list = $scope.SequenceData.actions;
		var l = list.length;
		for (var i = 0; i < l; i++) {
			op = document.createElement("option");
			op.text = list[i];
			op.value = list[i];
			selectAction.add(op, null);
		}
	
	 selectAction.onActivate = function() {
		selectAction.value = selectAction.textEditingTool.textBlock.text;
		var loc = selectAction.textEditingTool.textBlock.getDocumentPoint(go.Spot.TopLeft);
		var pos = myDiagram.transformDocToView(loc);
		selectAction.style.left = pos.x + "px";
		selectAction.style.top  = pos.y + "px";
		selectAction.style.visibility = "visible";
		selectAction.style.width = "120px";
		selectAction.style.height = "30px";
		selectAction.style.border = "none";
	}
	
	/******************************** select action *****************************/

		/******************************** select delay  *****************************/	
		var selectDelay = document.createElement("select");
		var op;
		var list = ["Days", "Hours", "Weeks"];
		var l = list.length;
		for (var i = 0; i < l; i++) {
			op = document.createElement("option");
			op.text = list[i];
			op.value = list[i];
			selectDelay.add(op, null);
		}
	
	 selectDelay.onActivate = function() {
		selectDelay.value = selectDelay.textEditingTool.textBlock.text;
		var loc = selectDelay.textEditingTool.textBlock.getDocumentPoint(go.Spot.TopLeft);
		var pos = myDiagram.transformDocToView(loc);
		selectDelay.style.left = pos.x + "px";
		selectDelay.style.top  = pos.y + "px";
		selectDelay.style.visibility = "visible";
		selectDelay.style.width = "60px";
		selectDelay.style.height = "30px";
		selectDelay.style.border = "none";
	}
	
	/******************************** select delay *****************************/
	
    // when the document is modified, add a "*" to the title and enable the "Save" button
    myDiagram.addDiagramListener("Modified", function(e) {
      var button = document.getElementById("SaveButton");
      if (button) button.disabled = !myDiagram.isModified;
      var idx = document.title.indexOf("*");
      if (myDiagram.isModified) {
        if (idx < 0) document.title += "*";
      } else {
        if (idx >= 0) document.title = document.title.substr(0, idx);
      }
    });

    // Define a function for creating a "port" that is normally transparent.
    // The "name" is used as the GraphObject.portId, the "spot" is used to control how links connect
    // and where the port is positioned on the node, and the boolean "output" and "input" arguments
    // control whether the user can draw links from or to the port.
     function makePort(name, spot, output, input) {
	
      // the port is basically just a small transparent square
      return $(go.Shape, "Circle",
               {
				  
                 fromMaxLinks: 1,
                  fill: "transparent",
                  stroke: null,  // this is changed to "white" in the showPorts function
                  desiredSize: new go.Size(12, 12),
                  alignment: spot, alignmentFocus: spot,  // align the port on the main Shape
                  portId: name,  // declare this object to be a "port"
                  fromSpot: spot, toSpot: spot,  // declare where links may connect at this port
                  fromLinkable: output, toLinkable: input,  // declare whether the user may draw links to/from here
                  cursor: "pointer"  // show a different cursor to indicate potential link pointks to/from here
           
               });
	  
    }

	  
	var lightText = 'whitesmoke';
	var selectEvent = document.createElement("input");
	var curPlatform = $scope.SequenceData.interactions[0].type;
	/* selectDelay.onActivate = function() {
		selectEvent.value = curPlatform;
	} */
    myDiagram.nodeTemplateMap.add("Start",
      $(go.Node, "Spot", nodeStyle(),
        $(go.Panel, "Auto",
          $(go.Shape, "Circle",
            { minSize: new go.Size(60, 60), fill: "#F1F1F1", stroke: null},
			new go.Binding("fromLinkable", "from")
			),
			
          $(go.TextBlock, "Start",
            { font: "bold 11pt proxima-nova", stroke: "#535353",editable: false},
            new go.Binding("text"))
        ),
        // three named ports, one on each side except the top, all output only:
		
        makePort("R", go.Spot.Right ,true, false)
      ));

	  
    myDiagram.nodeTemplateMap.add("End",
      $(go.Node, "Spot", nodeStyle(),
        $(go.Panel, "Auto",
          $(go.Shape, "Circle",
            { minSize: new go.Size(60, 60), fill: "#F1F1F1", stroke: null }),
          $(go.TextBlock, "End",
            { font: "bold 11pt proxima-nova", stroke: "#535353",editable: false },
            new go.Binding("text").makeTwoWay())
        ),
        // three named ports, one on each side except the bottom, all input only:
        makePort("L", go.Spot.Left, false, true)
       
      ));
	
	 myDiagram.nodeTemplateMap.add("Description",
      $(go.Node, "Spot", nodeStyle(),
			
        $(go.Panel, "Auto",
          $(go.Shape, "RoundedRectangle",
            { minSize: new go.Size(400, 30),fill: "#F1F1F1", stroke: null }),
          $(go.TextBlock, "Here we can create or edit Squences by drag and drop them to the canvas",
            { font: "9pt proxima-nova", stroke: "#535353",editable: false },
            new go.Binding("text"))
        )
      ));
	  
	
	/********************************* Interaction panel *********************************************/	  
    myDiagram.nodeTemplateMap.add("Interaction",
		$(go.Node, "Auto", nodeStyle(),

			$(go.Shape, "RoundedRectangle",
				{ fill: "#41B7E9", stroke: null },
				new go.Binding("figure", "figure")
			),
			
			$(go.Panel, "Table",
				{
					minSize: new go.Size(120, NaN),
					maxSize: new go.Size(120, NaN),
					margin: new go.Margin(10)
					
				},
				
				$(go.TextBlock,
					{
						text: curPlatform,
						row: 0, column: 0, columnSpan: 2,
						textAlign: "center",
						minSize: new go.Size(120, 30),
						editable: false,
						font: "11pt proxima-nova",
						stroke: lightText//,
						//textEditor: selectEvent
					},
					new go.Binding("text", "title").makeTwoWay()
				),
				$(go.TextBlock,   // the name of the port
					{ 
						row: 1, column: 0,
						font: "7pt proxima-nova" ,
						text: 'IN',	
						textAlign : 'left',
						minSize: new go.Size(60, NaN)
					}
				),
				$(go.TextBlock,   // the name of the port
					{ 
						row: 1, column: 1,
						font: "7pt proxima-nova" ,
						text: 'OUT',	
						textAlign : 'right',
						minSize: new go.Size(60, NaN)
					}
				),
				$(go.TextBlock,
					{
						text: 'Select Template',
						row: 2, column: 0,columnSpan: 2,
						minSize: new go.Size(120, 30),
						textAlign: "center",
						editable: true,
						font: "11pt proxima-nova",
						stroke: lightText,
						textEditor: selectTemplate 
					},
				 
					new go.Binding("text").makeTwoWay()
				)
				
			),
			
			makePort("L", go.Spot.Left, false, true),
			makePort("R", go.Spot.Right, true, false)
			
		
		)
	);
/********************************* Interaction panel *********************************************/

/********************************* Junction panel *********************************************/

	myDiagram.nodeTemplateMap.add("Junction",
		$(go.Node, "Auto", nodeStyle(),
			$(go.Shape, "RoundedRectangle",
				{ fill: "#ffe240", stroke: null},
				new go.Binding("figure", "figure")
			),
			  
			$(go.Panel, "Table",
				{
					minSize: new go.Size(120, NaN),
					maxSize: new go.Size(120, NaN),
					margin: new go.Margin(10)
					
				},
				$(go.TextBlock,
					{
						text: 'Select Action',
						row: 0, column: 0, columnSpan: 2,
						textAlign: "center",
						minSize: new go.Size(120, 30),
						editable: true,
						font: "11pt proxima-nova",
						stroke: '#757575',
						textEditor: selectAction
					},
					new go.Binding("text").makeTwoWay() 
				),
				$(go.TextBlock,   // the name of the port
					{ 
						text: 'IN',	
						row: 1, column: 0,
						font: "7pt proxima-nova" ,
						textAlign : 'left',
						minSize: new go.Size(60, NaN)
					
					}
				),
				$(go.TextBlock,   // the name of the port
					{ 
						text: 'YES',
						row: 1, column: 1,
						font: "7pt proxima-nova" ,
						textAlign : 'right',
						minSize: new go.Size(60, NaN)
						
					}
				),
				$(go.TextBlock,   // the name of the port
					{ 
						text: '1',
						row: 2, column: 0,
						font: "11pt proxima-nova",
						stroke: '#757575',	
						textAlign : 'center',
						minSize: new go.Size(60, 30),
						editable: true
					},
					new go.Binding("text", "number").makeTwoWay()
				),
				$(go.TextBlock,
					{
						text: 'Days',
						row: 2, column: 1, 
						textAlign: "center",
						minSize: new go.Size(60, 30),
						editable: true,
						font: "11pt proxima-nova",
						stroke: '#757575',
						textEditor: selectDelay
					},
					new go.Binding("text", "timePeriod").makeTwoWay()
					
				),
				$(go.TextBlock,   // the name of the port
					{ 
						text: 'NO',
						row: 3, column: 0,columnSpan: 2,
						font: "7pt proxima-nova" ,	
						textAlign : 'center'
					}
				)
				
			),
			
        makePort("L", go.Spot.Left, false, true),
        makePort("R", go.Spot.Right, true, false),
        makePort("B", go.Spot.Bottom, true, false)
		
		)
	);
	  
	   // initialize the Palette that is on the left side of the page
    myPalette =
      $(go.Palette, "myPaletteDiv",  // must name or refer to the DIV HTML element
        {
          maxSelectionCount: 1,
          nodeTemplateMap: myDiagram.nodeTemplateMap,  // share the templates used by myDiagram
          linkTemplate: // simplify the link template, just in this Palette
            $(go.Link,
              { // because the GridLayout.alignment is Location and the nodes have locationSpot == Spot.Center,
                // to line up the Link in the same manner we have to pretend the Link has the same location spot
                locationSpot: go.Spot.Center,
                selectionAdornmentTemplate:
                  $(go.Adornment, "Link",
                    { locationSpot: go.Spot.Center },
                    $(go.Shape,
                      { isPanelMain: true, fill: null, strokeWidth: 0 }),
                    $(go.Shape,  // the arrowhead
                      { toArrow: "Standard", stroke: null })
                  )
              },
              {
                routing: go.Link.AvoidsNodes,
                curve: go.Link.JumpOver,
                corner: 5
              },
              new go.Binding("points"),
              $(go.Shape,  // the link path shape
                { isPanelMain: true }),
              $(go.Shape,  // the arrowhead
                { toArrow: "Standard", stroke: null })
            ),
          model: new go.GraphLinksModel([  // specify the contents of the Palette

            { category: "Interaction",title : curPlatform},
			{ category: "Junction", text: "Junction"}
           
          ])
        }); 
		
	  
	 function nodeStyle() {
      return [
        // The Node.location comes from the "loc" property of the node data,
        // converted by the Point.parse static method.
        // If the Node.location is changed, it updates the "loc" property of the node data,
        // converting back using the Point.stringify static method.
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        {
          // the Node.location is at the center of each node
          locationSpot: go.Spot.Center,
         // handle mouse enter/leave events to show/hide the ports
        mouseEnter: function(e, node) { showSmallPorts(node, true); },
         mouseLeave: function(e, node) { showSmallPorts(node, false); }
		
        }
      ];
    }
	
	 // Make all ports on a node visible when the mouse is over the node
    function showSmallPorts(node, show) {
	 var diagram = node.diagram;
    if (!diagram || diagram.isReadOnly || !diagram.allowLink) return;
      node.ports.each(function(port) {
        if (port.portId !== "") {  // don't change the default port, which is the big shape
          port.fill = show ? "rgba(0,0,0,.3)" : null;
        }
      });
    }

    var linkSelectionAdornmentTemplate =
      $(go.Adornment, "Link",
        $(go.Shape,
          // isPanelMain declares that this Shape shares the Link.geometry
          { isPanelMain: true, fill: null, stroke: "deepskyblue", strokeWidth: 0 })  // use selection object's strokeWidth
      );

    // replace the default Link template in the linkTemplateMap
    myDiagram.linkTemplate =
      $(go.Link,  // the whole link panel
        {
          routing: go.Link.AvoidsNodes,
          curve: go.Link.JumpOver,
          corner: 5, toShortLength: 4,
          relinkableFrom: true,
          relinkableTo: true,
          reshapable: true,
          resegmentable: true,
          mouseEnter: function(e, link) { link.findObject("HIGHLIGHT").stroke = "rgba(30,144,255,0.2)"; },
          mouseLeave: function(e, link) { link.findObject("HIGHLIGHT").stroke = "transparent"; }
        },
        new go.Binding("points").makeTwoWay(),
        $(go.Shape,  // the highlight shape, normally transparent
          { isPanelMain: true, strokeWidth: 8, stroke: "transparent", name: "HIGHLIGHT" }),
        $(go.Shape,  // the link path shape
          { isPanelMain: true, stroke: "gray", strokeWidth: 2 }),
        $(go.Shape,  // the arrowhead
          { toArrow: "standard", stroke: null, fill: "gray"})
      );
	    
    load();  // load an initial diagram from some JSON text

  }


  function TopRotatingTool() {
    go.RotatingTool.call(this);
  }
  go.Diagram.inherit(TopRotatingTool, go.RotatingTool);

  /** @override */
  TopRotatingTool.prototype.updateAdornments = function(part) {
    go.RotatingTool.prototype.updateAdornments.call(this, part);
    var adornment = part.findAdornment("Rotating");
    if (adornment !== null) {
      adornment.location = part.rotateObject.getDocumentPoint(new go.Spot(0.5, 0, 0, -30));  // above middle top
    }
  };

  /** @override */
  TopRotatingTool.prototype.rotate = function(newangle) {
    go.RotatingTool.prototype.rotate.call(this, newangle + 90);
  };
  // end of TopRotatingTool class

  
}