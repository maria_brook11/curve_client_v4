var anim_time = 0;
var anim_ease = "linear";
function onNavigationClick(id)
{
	var complete = function()
	{
		var menu = $("#" + id).find(".curve-sub-menu");
	
		if(menu.hasClass("open"))
		{
			menu.removeClass("open");
			menu.slideUp(anim_time, anim_ease);
		}
		else
		{
			menu.addClass("open");
			menu.slideDown(anim_time, anim_ease);
		}
	}
	closeOthers(id, complete);
}

var navs = ['settings_menu', 'navigation_menu', 'create_menu', 'do_action', 'campaign_menu'];
function closeOthers(thisName, complete)
{
	var closed = false;
	for(var i = 0 ; i < navs.length; i++)
	{
		var name = navs[i];
		if(name != thisName)
		{
			var menu = $("#"+name).find(".curve-sub-menu");;
			if(menu.hasClass("open"))
			{
				menu.removeClass("open");
				menu.slideUp(anim_time, anim_ease, complete);
				closed = true;
			}
		}
	}
	
	if(!closed && complete != null)
		complete();
}

function closeAll()
{
	for(var i = 0 ; i < navs.length; i++)
	{
		var name = navs[i];
		var menu = $("#"+name).find(".curve-sub-menu");
		if(menu.hasClass("open"))
				menu.removeClass("open");
		menu.slideUp(anim_time, anim_ease);
	}
}