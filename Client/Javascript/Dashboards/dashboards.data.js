function DashBrdDataFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter){

$scope.parsePieData = function(d)
{
	if(d == null || d.chartData == null || d.chartData.ReportDataTable == null)
	{
		return [];
	}

	//	var dataTable = d.chartData.ReportDataTable;
	
	
	var graphType = d.chartData.GraphType;
	
	if(graphType == "PieChart" ){
		var dataTable = d.chartData.ReportDataTable;
	}else{
		var dataTable = d.chartData.ReportDataTable.Data;
	}
	
	
	var resp = [];
	
	var ln = dataTable.Records.length;
	if (ln == 0)
		return null;
	for (var j=0; j<ln; j++)
	{
		var currData = dataTable.Records[j];
		resp.push({label:currData[0], data:currData[1]});
	}
	
	return resp;
}

function parseSessionData(d)
{
	// console.log("parseSessionData", d);
	// TODO: remove for test
	// d = JSON.parse('[{"eventtime":"1467200595490","table_type":"SessionData","sessionid":"d4ad4e79-c11a-46cf-b482-8b3dc9d26539"},' +
		// '{"eventtime":"1467200679000","table_type":"TrafficSources","sessionid":"d4ad4e79-c11a-46cf-b482-8b3dc9d26539"},'+
		// '{"eventtime":"1467201728800","table_type":"AppLifeCycle","sessionid":"d4ad4e79-c11a-46cf-b482-8b3dc9d26539"},'+
		// '{"eventtime":"1467200679000","table_type":"TrafficSources","sessionid":"d4ad4e79-c11a-46cf-b482-8b3dc9d26537"},'+
		// '{"eventtime":"1467201728800","table_type":"AppLifeCycle","sessionid":"d4ad4e79-c11a-46cf-b482-8b3dc9d26537"}'+
		// ']');
	
	var dict = {};
	var arr = [];
	for (i=0; i < d.length; i++)
	{
		var curr = d[i];
		if (dict[curr.sessionid] == null)
		{
			dict[curr.sessionid] = [];
			dict[curr.sessionid].sessionMin = curr.eventtime;
			dict[curr.sessionid].sessionMax = curr.eventtime;
			dict[curr.sessionid].sessionid = curr.sessionid;
			arr.push(dict[curr.sessionid]);
		}
		
		dict[curr.sessionid].push(curr);
		
		dict[curr.sessionid].sessionMin = Math.min(dict[curr.sessionid].sessionMin, curr.eventtime);
		dict[curr.sessionid].sessionMax = Math.max(dict[curr.sessionid].sessionMin, curr.eventtime);
		
	}
	
	function compare(a,b) {
	  if (a.sessionMin < b.sessionMin)
		return -1;
	  if (a.sessionMin > b.sessionMin)
		return 1;
	  return 0;
	}
	
	// console.log(arr);
	arr = arr.sort(compare);
	
	function compareET(a,b) {
	  if (a.eventtime < b.eventtime)
		return -1;
	  if (a.eventtime > b.eventtime)
		return 1;
	  return 0;
	}
	
	var getConvertedTimeDiv = function(timeDiff)
	{
		var timediff_units = "Seconds";
		
		
		if (timeDiff == null)
		{
			return "0 " + timediff_units;
		}
		
		timeDiff = timeDiff / 1000;
		if(timeDiff > 60)
		{
			timediff_units = "Minutes";
			timeDiff = timeDiff / 60;
			if (timeDiff < 60)
				return Math.floor(timeDiff) + ":" + Math.floor((timeDiff - Math.floor(timeDiff))*60) + " " + timediff_units;
		}
		
		if(timeDiff > 60)
		{
			timediff_units = "Hours";
			timeDiff = timeDiff / 60;
			if (timeDiff < 24)
				return Math.floor(timeDiff) + ":" + Math.floor((timeDiff - Math.floor(timeDiff))*60) + " " + timediff_units;
		}
		
		if(timeDiff > 24)
		{
			timediff_units = "Days";
			timeDiff = timeDiff / 24;
			return Math.floor(timeDiff) + ":" + Math.floor((timeDiff - Math.floor(timeDiff))*24) + " " + timediff_units;
		}
		
		return timeDiff + " " + timediff_units;
	}
	
	for (j=0; j < arr.length; j++)
	{
		// console.log(arr[j]);
		arr[j] = arr[j].sort(compareET);
		arr[j].sessionLength = arr[j].sessionMax - arr[j].sessionMin;
		arr[j].sessionTiming = getConvertedTimeDiv(arr[j].sessionLength);
		arr[j].numEvents = arr[j].length;
		
	}
	
	return arr;
}

$scope.parseCounterData = function(d)
{
	if (d == null || d.chartData == null)
		return {data: "N/A"};
	
	var dataTable = d.chartData.ReportDataTable;
	return {data: dataTable.Records[0]};
}

$scope.parseMultichoiceTrackingChartData = function(d)
{

	if(d == null || d.chartData == null)
		return [];
	
	var products = d.chartData.ReportDataTable.Products;
	d.chartData.ReportDataTable = d.chartData.ReportDataTable.Data;
	
	var ans = $scope.parseGraphData(d);
	ans.products = products;
	//console.log(ans);
	return ans;
}

function parseSummaryTableData(d)
{
	// console.log("parseSummaryTableData", d);
	if(d == null || d.chartData == null)
		return [];
	
	// TODO: remove for test
	// d.chartData = {"GraphType":"SummaryTable","ReportDataTable":{
		// "Result3":{"ColumnNames":["?column?","?column?","product views","product clicks","product ctr","product add save for later","adds to cart","product checkouts","all unique purchases","all revenue"],"Records":[["Homepage","","50763","0","0","0","0","0","0","0"]]},
		// "Result4":{"ColumnNames":["product name","product brand","product views","product clicks","product ctr","product add save for later","adds to cart","product checkouts","unique purchases","product revenue"],"Records":[["[Search] mono chrome collarless","ANALONDON","264","0","0","0","132","0","132","660"],["[Search] mono cream","ANALONDON","14","0","0","0","2","0","12","600"]]},
		// "Result1":{"ColumnNames":["?column?","?column?","product views","product clicks","product ctr","product add save for later","adds to cart","product checkouts","all unique purchases","all revenue"],"Records":[["","","259656","10","0","50455","423625","0","96710","503545"]]},
		// "Result2":{"ColumnNames":["product name","product brand","product views","product clicks","product ctr","product add save for later","adds to cart","product checkouts","unique purchases","product revenue"],"Records":[["[Category] clothing-plus-size-tops","MISS K","4283","1","0","650","0","0","2171","11610"],["[Category] wedge-shoes","MBB","2749","0","0","487","0","0","1705","8705"],["[Category] clothing-dresses-midi-dresses","HER CRAFT","4166","0","0","581","0","0","1398","7185"],["[Category] clothing-coat-and-jackets-casual-jacket","GUY DARRELLS","1121","0","0","249","0","0","1165","6015"],["[Category] clothing-dresses-skater-dresses","HER CRAFT","2541","0","0","551","0","0","1110","5705"],["[Category] clothing-tops-blouses","ANALONDON","2376","0","0","496","0","0","904","4580"],["[Category] clothing-coat-and-jackets-casual-jacket","HER CRAFT","1384","0","0","286","0","0","876","4445"],["[Category] flat-shoes","JENIKA","608","0","0","241","0","0","857","4410"],["[Category] flat-sandals","MBB","707","0","0","201","0","0","820","4305"],["[Category] sandal-shoes","MBB","513","0","0","222","0","0","813","4150"]]},
		// "Result0":{"ColumnNames":["?column?","?column?","total views","total clicks","avg ctr","total add save for later","total add to cart","total checkout","total unique purchases","total revenue"],"Records":[["","","20448","1","0","3964","0","0","11819","61110"]]}
		// }};
	
	var ans = {};
	ans.header = (d.chartData.ReportDataTable.Result2 == undefined)? ["-","-","-","-","-","-","-","-","-","-"] : d.chartData.ReportDataTable.Result2.ColumnNames;
	ans.headFooter = (d.chartData.ReportDataTable.Result0 == undefined)? ["-","-","-","-","-","-","-","-","-","-"] : d.chartData.ReportDataTable.Result0.Records[0];
	ans.headFooterSmall = (d.chartData.ReportDataTable.Result1 == undefined)? ["-","-","-","-","-","-","-","-","-","-"] : d.chartData.ReportDataTable.Result1.Records[0];
	ans.bodyData = (d.chartData.ReportDataTable.Result2 == undefined)? [["-","-","-","-","-","-","-","-","-","-"]] : d.chartData.ReportDataTable.Result2.Records;
	ans.signs = (d.chartData.ReportDataTable.Result3 == undefined)? ["","","","","","","","","",""] : d.chartData.ReportDataTable.Result3.Records[0];
	
	return ans;
}

function capitalize(str) {
     var splittedEnter = str.split(" ");
     var capitalized;
     var capitalizedResult;
     for (var i = 0 ; i < splittedEnter.length ; i++){
         capitalized = splittedEnter[i].charAt(0).toUpperCase();
         splittedEnter[i] = capitalized + splittedEnter[i].substr(1).toLowerCase();
    }
    return splittedEnter.join(" ");

}

$scope.parseGraphData = function(d)
{ 
	if(d == null || d.chartData == null)
		return [];
	
	if(d.chartData.GraphType != 'MultiChoiceLines' && d.chartData.GraphType != 'MultiChoiceFunnel' && d.chartData.GraphType != 'MultiChoiceFunnel_2' && d.chartData.GraphType != 'Funnel' && d.chartData.GraphType != 'MultiChoicePie'){
		var dataTable = d.chartData.ReportDataTable;
	}else{
		var dataTable = d.chartData.ReportDataTable.Data;
	}
	
	var graphType = d.chartData.GraphType;
	
	/* if(graphType.toLowerCase() == "funnel")
		return dataTable;
	 */
	if(graphType == "userjourney")
		return dataTable;

	var ln = dataTable.ColumnNames.length;
	var resp = [];
	
	if(graphType.toLowerCase() == "sparkline")
	{
		var sparklinedata = [];
		for (var i=0; i< dataTable.Records.length; i++)
			sparklinedata.push(dataTable.Records[i][0]);
		
		return sparklinedata;
	}
	
	if(graphType.toLowerCase() == "countergroup")
	{
		var countergroupdata = [];
		for (var i=0; i< dataTable.Records.length; i++)
			countergroupdata.push(dataTable.Records[i][0]);

		return countergroupdata;
	}
	
	if (ln == 1)
	{
		var seriesEmpty = {};
		seriesEmpty.label =  dataTable.ColumnNames[0];
		seriesEmpty.data = [];
		resp.push(seriesEmpty);
		return resp;
	}
	for (var i=1; i<ln; i++)
	{ 
		var currLn = dataTable.Records.length;
		
		var series = {};
		series.label =  dataTable.ColumnNames[i];
		series.data = [];
		
		for (var j=0; j<currLn; j++)
		{
			var currData = dataTable.Records[j];
			series.data.push([currData[0], currData[i]]);
		}
		
		resp.push(series);
	}

	return resp;
}

}