function CampaignFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst,InsightConst){


var Campaigns = {};

$scope.getGameActiveCampaignsFun = function(){
	$scope.campaignsObjLoader = true;
	$scope.curveAPI('GetGameActiveCampaigns', {Token:$rootScope.Token}, onGotGameActiveCampaigns);
}

function showLoading(show)
{
    if(show)
    {
        $('.loading-page').show();
        $('.page-container').hide();
    }
    else
    {
        $('.loading-page').hide();
        $('.page-container').show();
    }
}




function onGotGameActiveCampaigns(data)
{

	$scope.campaignsObjLoader = false;
	$('#Campaigns_loading').css("display", "none");

	$scope.campaignsObj = data;
	
	$scope.campLinksArr = data.Campaigns;
	var param = '&u=*|EMAIL|*&i=*|CAMPAIGN_UID|*';
	for(var i = 0; i < $scope.campLinksArr.length; i ++){
		$scope.campLinksArr[i].linkForMC = $scope.campLinksArr[i].TrackingLink + param;
	}
	
	jQuery(document).ready(function()
	{
	$('#Campaigns_loading').css("display", "none");
	});
	
}

function getGameHeadline(name, icon)
{
    var title = '<div id="companyImage">';
    title += '<img src="' + icon + '" style="width:54px;height:54px; float:left; margin-right:5px; border-radius: 15px !important;"/></div>';
    title += '<div class="page-title" style="line-height:54px;">' + name + '</div>';
    title += '</div>';

    return title;
}

function getAddButtonSection()
{
    return '<div align="right"><button class="btn green newBtn" onClick="onShowAddCampaignDialog();">+ New Campaign</button></form></div>';
}

$scope.onShowAddCampaignDialog = function()
{
    $("#CampaignName").val('');
    $("#RedirectionLink").val('');
    $("#addCampaignError").val('');
    $('#addCampaignDialog').modal('show');
}

function drawCampaignsTable()
{
    var resp = '';

    resp += '<div class="portlet box yellow"><div class="portlet-title"><div class="caption"> <i class="fa fa-user"></i> Active Campaigns </div></div>';
    resp += '<div class="portlet-body"><table class="table table-striped table-bordered table-hover" id="CampaignsTable">';
    resp += '<thead><tr><th> Name </th>';
    resp += '<th> Redirection Link </th>';
    resp += '<th> Tracking URL <i class="fa fa-info-circle tooltips" style="color:#0266c8" alt="Use in your campaign ad banner for clicks to be tracked by Curve." title="Use in your campaign ad banner for clicks to be tracked by Curve."></i></th>';
    resp += '<th> Hide <i class="fa fa-info-circle tooltips" style="color:#0266c8" alt="Campaign will only disappear from this list, but will be available for users clicks." title="Campaign will only disappear from this list, but will be available for users clicks."></i> </th> </thead>';
    resp += '<tbody>';
    resp += '</tbody></table></div></div>';

    return resp;
}  

function drawCurrentCampaign(campaignID, campaignName, campaignRedirectLink, campaignTrackingLink)
{
    var onHideCampaignFunc = ' onclick="hideCampaign(\'' + campaignID + '\');" ';
    var textToAppend = '<tr class="odd gradeX">';
        textToAppend += '<td> ' + campaignName + ' </td> <td> ' + campaignRedirectLink + ' </td> <td> ' + campaignTrackingLink + ' </td>' ;
        textToAppend += '<td style="text-align: center;"><a class="remove_field" ' + onHideCampaignFunc + '><i class="fa fa-trash-o" style="color:#ff0000;"></i></a></td>';
        textToAppend += '</tr>';

    $('#CampaignsTable').append(textToAppend);
}

function drawAddCampaignDialog()
{
    var resp = '';

    resp += '<div class="form-group"> <table class="table table-striped table-bordered table-hover">';
    resp += '<tr><td><label>Campaign Name</label></td><td><input type="text" name="CampaignName" id="CampaignName" style="width: 100%;"></td></tr>';
    resp += '<tr><td><label>Redirection Link <i class="fa fa-info-circle tooltips" style="color:#0266c8" alt="Paste in your app store or website target page." title="Paste in your app store or website target page."></i></label></td><td><input type="text" name="RedirectionLink" id="RedirectionLink" style="width: 100%;"></td></tr>';
    resp += '</table></div>';
    resp += '<div><label id="addCampaignError" style="color:#ff0000"></label></div>';
    resp += '<div align="center"><button class="btn green newBtn" onClick="onAddCampaign();">Submit</button></form></div>';

    return resp;
}

$scope.hideCampaign = function(campaign)
{
    showLoading(true);
	$scope.campaignsObj.Campaigns.splice($scope.campaignsObj.Campaigns.indexOf(campaign),1);
    $scope.curveAPI('HideCampaign', {Token:$rootScope.Token, CampaignID:campaign.ID}, onHideCampaignSuccess, onHideCampaignFailed);
	onHideCampaignSuccess();
}

function onHideCampaignSuccess()
{
	showLoading(false);
}

function onHideCampaignFailed()
{
	showLoading(false);
}

$scope.onAddCampaign = function()
{
    var name = $('#CampaignName').val();
    if(!name || name == undefined || name.length == 0)
    {
        $('#addCampaignError').text("*Please insert a valid campaign name.");
        return;
    }
    
    var redirectLink = $('#RedirectionLink').val();
    if(!redirectLink || redirectLink == undefined || redirectLink.length == 0)
    {
        $('#addCampaignError').text("*Please insert a valid campaign redirection URL.");
        return;
    }
    
    $scope.curveAPI('AddCampaign', {Token:$rootScope.Token, Name:name, RedirectLink:redirectLink}, onAddCampaignSucceeded, onAddCampaignFailed);
}

function onAddCampaignSucceeded(data)
{	
	$('#addCampaignDialog').modal('hide');
	$scope.show_success("Successfully added campaign!",'Success');
	$scope.campaignsObj.Campaigns.unshift(data)
	
}

function onAddCampaignFailed()
{
    $scope.show_error('Could not add campaign', 'Failed. Please retry.', true);
    //document.location.reload(true);
}

}