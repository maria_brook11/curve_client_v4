function CohortEmailFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst,InsightConst,$interval,dateService){
	

$scope.getInterStatus = function(){
	
	$scope.statusCenterPage = true;
	
	$('#Reports .statusCenterPageBox').html('<div id="statusCenterPage" style="background: url(Images/AjaxLoader.gif) no-repeat center center;width: 100%;height: 250px;"></div>');
	$('#Reports .statusCenterPageBoxData').css('display','none');
	
	var data11 = {"Types":["sms","email","push"]}
	var d = {'Token':$rootScope.Token}
	
	var datepickerid = '#dashboard-report-range';

	if ($(datepickerid).data('daterangepicker'))
	{
		var data = $scope.getAPIGlobalParams(d, $(datepickerid).data('daterangepicker'));

	}else{
		$scope.dateJson = dateService.getDate($scope.singleDate);
		var data =  $scope.getAPIGlobalParams(d, $scope.dateJson);
	} 

	$scope.curveAPI("GetInteractionStatus",data,$scope.GetInteractionStatusCallback,$scope.GetInteractionStatusError);
	
	
}		


	
$scope.GetInteractionStatusCallback = function(data)
{	

	$scope.statusCenterPage = false;
	if(data.interactions){
		$scope.interactionsArr = data.interactions;
		
	}
	if(data.campaigns){
		$scope.campaignsArr = data.campaigns;
		
	}
	if(data.mailchimp.reports){
		$scope.mailChimpArr = data.mailchimp.reports;
		
	}
	if(data.maropost.reports){
		$scope.maropostArr = data.maropost.reports;
		
	}

	$('#statusCenterPage').remove();
	$('#Reports .statusCenterPageBoxData').css('display','block');
	var sent = "sent";
	var notOpenP = "notOpenP";
	
	if($scope.maropostArr && $scope.maropostArr.length > 0){
		$.each($scope.maropostArr, function(key, val)
		{	
			var showDet = "showDet";
			if(val != null){
				val[showDet] = false;
			}
		})
	}
	
	if($scope.mailChimpArr && $scope.mailChimpArr.reports && $scope.mailChimpArr.reports.length > 0){
		$.each($scope.mailChimpArr.reports, function(key, val)
		{	
			var fullDataShow = "fullDataShow";
			if(val != null){
				
				val[fullDataShow] = false;

				if(val.send_time)
				{
					var da = new Date(val.send_time);
					
					var h1 = da.getHours();
					h1 = (h1 < 10) ? ("0" + h1) : h1 ;

					var m1 = da.getMinutes();
					m1 = (m1 < 10) ? ("0" + m1) : m1 ;

					var s1 = da.getSeconds();
					s1 = (s1 < 10) ? ("0" + s1) : s1 ;

					val.send_time = $.datepicker.formatDate("yy-mm-dd ", da) + h1 + ":" + m1 +":" + s1;
					
				}
				else
				{
					val.send_time = "Unknown";
				}
			}
		});
	}
	/* $scope.fullDataShowFun = function(id,e)
	{
		$.each($scope.mailChimpArr.reports, function (key,val)
		{
			
			if(val != null){
				if(val.id == id)
				val.fullDataShow = !e;
			}
		});
	} */
	
	$.each($scope.interactionsArr, function(key, val)
	{
		
		if(val.sentCount > 0)
		{
			if(val.sentCount > 0){
				val.sent = (val.sentCount / val.sentCount) * 100;
			}else{
				val.sent = 0;
			}
			
			var not_opened = val.sentCount - val.openedCount;
			if(not_opened > 0)
			{
				val.openEmails = val.openedCount;
				var not_opened = val.sentCount - val.openedCount;
				val.notOpenP = (not_opened / val.sentCount) * 100;
				val.openP = (val.openEmails / val.sentCount) * 100;
			}else{
				val.openEmails = val.sentCount;
				val.openP =  100;
				val.notOpenP = 0;
			}
			
			
		}else{
			val.sent = 0;
			val.notOpenP = 0;
			val.openEmails = 0;
			val.openP = 0;
			val.openEmails = 0;
		}
	});
	
}

$scope.GetInteractionStatusError = function(data){console.log(data)}

	
	var currentStep = 1;
	var foundService = false;
	
$scope.EmailEditor = function(e,t,a)
{	
	
	$rootScope.$broadcast('deselectAll',{});
	$scope.currentService = e;
	if($scope.currentService == 'mailList'){$scope.currentServiceName = 'MailChimp'}
	if($scope.currentService == 'mailListMaropost' ){$scope.currentServiceName = 'Maropost'}
	if($scope.currentService == 'mailList' || $scope.currentService == 'mailListMaropost' ){
			getmailListTemplates(e);
		
		}
	$("#email_editor_textarea").markdown({
			autofocus:false,
			savable:false,
			resize:"none",
		});
		
	$scope.flagSelect = false;
	if(t != null && a != null){
		var intvGetAud = $interval(function ()
			{
				if ($scope.AudiencesArray && $scope.AudiencesArray.length > 0)
				{	
					for(var i = 0; i < $scope.AudiencesArray.length ; i ++){
						
						for(var j = 0; j < a.length ; j ++){
							
							if($scope.AudiencesArray[i].type == t && $scope.AudiencesArray[i].id == a[j].id){
								if(a[j].checked){
									$scope.AudiencesArray[i].add = true;
								}
							}
						}
					}
					$scope.flagSelect = true;
					$interval.cancel(intvGetAud);
					
				}
			}, 300);
	}else{
		$scope.flagSelect = true;
	}
		onCohortsUpdate();
		onServicesUpdate();
		init();
		
	
};

	$scope.EmailEditorOnComplete = function()
	{
		if ($scope.currentService == "email")
		{
			sendEmail();
		}
		else if ($scope.currentService == "mailList" || $scope.currentService == "mailListMaropost")
		{
			SetMailingList();
		}
	};
	
	function SetMailingList()
	{
		var data = validateAndPrepareMailingList();
		
		if(data.error != null)
		{
			$scope.show_error("Make sure all fields are properly filled", data.error);
			return;
		}
		
		if(data.warn != null)
		{
			$scope.EmailValidatorManager("", sendWithoutValidatingEmail, null, data.warn, "If not, please make sure to fill them properly.", "Yes, Send", "Cancel");
			return;
		}
		
		$('#email_cohort_step_1').css('display' , 'none');
		$('#email_cohort_step_2').css('display' , 'none');
		$('#mailingList_cohort_step_2').css('display' , 'none');
		
		$('#email_cohort_confirm').css('display' , 'none');
		$('#email_cohort_next').css('display' , 'none');
		
		$('#email_main_loader').css('display' , '');
		

		
		$scope.curveAPI("SendMailingListByCohorts", {"Token": $rootScope.Token, Data:JSON.stringify(data)}, onSendMailingListByCohorts, onErrorSendMailingListByCohorts);
	};
	
	function onSendMailingListByCohorts(data)
	{
		
		var message = "";
		if (data.listId != undefined && data.listId != "")
			message += "List with ID number '" + data.listId + "' ";
		if (data.campaignId != undefined && data.campaignId != "")
			message += "and Campaign with ID number '" + data.campaignId + "' ";
		message += "created succesfully. \nStatus, results and additional actions are available under: 'Campaigns' > 'Manage Mailing Lists'.";
		$scope.show_success("Success", message);
		$("#cohort_email_editor").modal('hide');
	};
	
	function onErrorSendMailingListByCohorts(data)
	{
		console.log("ERROR", data);
		$scope.show_error("There was an Error.", data);
		$("#cohort_email_editor").modal('hide');
	};
	
	function sendEmail()
	{
		var data = $scope.validateAndPrepareEmail(true);
		
		if(data.error != null)
		{
			$scope.show_error("Make sure all fields are properly filled", data.error);
			return;
		}
		
		if(data.warn != null)
		{
			$scope.EmailValidatorManager("", sendWithoutValidatingEmail, null, data.warn, "If not, please make sure to fill them properly.", "Yes, Send", "Cancel");
			return;
		}
		
		$('#email_cohort_step_1').css('display' , 'none');
		$('#email_cohort_step_2').css('display' , 'none');
		$('#mailingList_cohort_step_2').css('display' , 'none');
		
		$('#email_cohort_confirm').css('display' , 'none');
		$('#email_cohort_next').css('display' , 'none');
		
		$('#email_main_loader').css('display' , '');
		
		$scope.curveAPI("SendEmailByCohorts", {"Token": $rootScope.Token, Data:JSON.stringify(data)}, onSendEmailByCohorts, onErrorSendEmailByCohorts);
	};
	
	function sendWithoutValidatingEmail(id)
	{
		var data = $scope.validateAndPrepareEmail(false);
		$scope.curveAPI("SendEmailByCohorts", {"Token": $rootScope.Token, Data:JSON.stringify(data)}, onSendEmailByCohorts, onErrorSendEmailByCohorts);
		$("#cohort_email_editor").modal('hide');
	};
	
	function onSendEmailByCohorts(data)
	{
		$scope.show_success("Success","Mail request completed successfully.");
		$("#cohort_email_editor").modal('hide');
		
	};
	
	function onErrorSendEmailByCohorts(data)
	{
		console.log("ERROR", data);
		$scope.show_error("There was an Error sending the mail.", data);
		$("#cohort_email_editor").modal('hide');
	};
	
	function validateAndPrepareMailingList()
	{
		var address = $("#email_editor_address").val();
		var mailingListName = $("#mailingList_name").val();
		var ids = $scope.getCohordIDs('mail');
		var idsExt = getCohordIDsExt();
		var emails = getRecepientsEmails();
		var createCampaign = $("#mailingList_createCampaign_checkBox_val")[0].checked;
		var selectedTemplateID = $("#setup_mailListTemplates_button")[0].getAttribute("data-template-id");
		var fromName = $("#mailList_campaign_sentFrom")[0].value;
		var replyTo = $("#mailList_campaign_replyTo")[0].value;
		var subjectLine = $("#mailList_campaign_subject")[0].value;
		var campaignSettings = {};
		
		var total_recepients = 0;
		if(ids != null)
			total_recepients += 1;
		if(idsExt != null)
			total_recepients += idsExt.length;
		if(emails != null)
			total_recepients += emails.length;
		
		if (total_recepients == 0)
			return {error: "You must specify at least one recepient or Audience."};
		
		if(address == null || address == "")
			return {error: "Please fill in your address."};
		
		
		if(mailingListName == null || mailingListName == "")
			return {error: "Please specify a name for the mailing list."};
		
		if (createCampaign)
		{
			if(fromName == null || fromName == "")
				return {error: "Please fill the 'Sent from' field."};
			if($scope.currentService == 'mailList'){
				campaignSettings.FromName = fromName;
			}
			if($scope.currentService == 'mailListMaropost'){
				campaignSettings.from_name = fromName;
			}
			
			if(replyTo == null || replyTo == "")
				return {error: "Please specify the 'Replies to' field."};
			if($scope.currentService == 'mailList'){
				campaignSettings.ReplyTo = replyTo;
			}
			if($scope.currentService == 'mailListMaropost'){
				campaignSettings.reply_to = replyTo;
			}
			if($scope.currentService == 'mailListMaropost'){
				campaignSettings.from_email = replyTo;
			}
			if(subjectLine == null || subjectLine == "")
				return {error: "Please specify the campaign subject."};
				if($scope.currentService == 'mailList'){
					campaignSettings.SubjectLine = subjectLine;
				}
				if($scope.currentService == 'mailListMaropost'){
					campaignSettings.subject = subjectLine;
				}
			if(selectedTemplateID == null || selectedTemplateID == "")
				return {error: "Please select a Template for the campaign."};
		}
		
		
//Data={"Type":"Maropost","name":"JHA campaign","address":"HaBarzel Street 2, Tel Aviv-Yafo","CohortIds":["2"],"Emails":["vadim.sh@gmail.com"],"CreateCampaign":false,"TemplateID":null,"CampaignSettings":{}}&PacketID=callback_31&_=1492932921974
//Data={"Type":"Maropost","Title":"BIG SPENDERS","CohortIds":["50"],"Emails":["vadim.sht@gmail.com"],"name":"000 Curve name15" ,"address":"address","reply_to_email":"vadim.sht@gmail.com","from_email":"vadim.sht@gmail.com","from_name":"curve","description":"description",
//"CreateCampaign":false}		
		
//Data={"Type":"Maropost","Title":"BIG SPENDERS","CohortIds":["50"],"Emails":["vadim.sht@gmail.com"],"name":"000 Curve name19" ,"address":"address","reply_to_email":"vadim.sht@gmail.com","from_email":"vadim.sht@gmail.com","from_name":"curve","description":"description",
//"CreateCampaign":true,"TemplateID":"2391356","CampaignSettings":{"name":"name","subject":"subject","preheader":"preheader","from_name":"from_name","from_email":"vadim.sht@gmail.com"}}
		
		if($scope.currentService == 'mailList'){
			var temp = {"Type":"MailChimp","Title" : mailingListName, "Emails": emails, "CreateCampaign":createCampaign, "TemplateID":selectedTemplateID, "CampaignSettings": campaignSettings};
			
			angular.forEach(ids, function (val,key) {
				temp[key] = val;
			});
		return temp;
		}
		if($scope.currentService == 'mailListMaropost'){
			var temp = {"Type":"Maropost","name":mailingListName, "address" : address, "MaropostListIds" : idsExt, "Emails": emails, "CreateCampaign":createCampaign, "TemplateID":selectedTemplateID, "CampaignSettings": campaignSettings};
			angular.forEach(ids, function (val,key) {
				temp[key] = val;
			});
		return temp;
		}
	};
	
	$scope.validateAndPrepareEmail = function(validate, checkRecepients)
	{
		var emailSubject = $("#email_editor_subject").val();
		var emailContent = $(".md-preview").html();
		var ids = $scope.getCohordIDs('mail');
		
		var emails = getRecepientsEmails();
		var emailLink = $("#email_editor_link").val();
		
		if (checkRecepients)
		{
			var total_recepients = 0;
			if(ids != null)
				total_recepients += 1;
			if(emails != null)
				total_recepients += emails.length;
			
			if (total_recepients == 0)
				return {error: "You must specify at least one recepient or Audience."};
		}
		
		if (emailContent == undefined)
		{
			$("#email_editor_textarea").data('markdown').showPreview();
			emailContent = $(".md-preview").html();
			$("#email_editor_textarea").data('markdown').hidePreview();
		}
		
		if(validate)
		{
			if(emailSubject == null || emailSubject == "")
				return {warn: "Are you sure you want to send this email without a subject?"};
			
			if(emailContent == null || emailContent == "")
				return {warn: "Are you sure you want to send this email without a body?"};
		}
		/////////////////////////////*   "Id":ids, 555555 */////////////////////////////
		var resp = {"Title":emailSubject, "Text": emailContent,  "Emails": emails};
			
			angular.forEach(ids, function (val,key) {
				resp[key] = val;
			});
		
		if(emailLink != null && emailLink != "")
			resp.Link = emailLink;
		
		return resp;
	};
	
	$scope.getCohordIDs = function(e)
	{
		/* var cohortNames = $("#email_cohort_names_select").val();
		if(cohortNames == null)
			return [];
		
		var ans = [];
		var children = $("#email_cohort_names_select").children();
		
		
		var ln = children.length;
		for (var i=0 ; i < ln ; i++)
		{
			if (cohortNames.indexOf(children[i].getAttribute('value')) != -1)
				ans.push(children[i].getAttribute('data-cohort-id'));
		}
		return ans; */
		if(e == 'mail'){
			var tempStr = $('#AudiencesArrayCenterMail multiselect-dropdown .objValue').val();
		}
		if(e == 'push'){
			var tempStr = $('#AudiencesArrayCenterPush multiselect-dropdown .objValue').val();
		}
		if(e == 'sms'){
			var tempStr = $('#AudiencesArrayCenterSMS multiselect-dropdown .objValue').val();
		}
		var tempObj = JSON.parse(tempStr);
		var audiencesIDS = [];
		var behaviorsIDS = [];
		var ranksIDS = [];
		angular.forEach(tempObj, function (val,key){
			if(val.add){
				if(val.type == 'audience'){
					audiencesIDS.push(val.id);
				}
				if(val.type == 'behavior'){
					behaviorsIDS.push(val.id);
				}
				if(val.type == 'rank'){
					ranksIDS.push(val.id);
				}
			} 
		})
		
		
	if(audiencesIDS.length > 0 || behaviorsIDS.length > 0 || ranksIDS.length > 0){
			
			if(audiencesIDS.length > 0 && behaviorsIDS.length > 0 && ranksIDS.length > 0){
				
	var d = {"Audiences": audiencesIDS,"Behaviors": behaviorsIDS,"Ranks": ranksIDS};
			return d;
			}else{
				if(audiencesIDS.length > 0 && behaviorsIDS.length > 0){
	var d = {"Audiences": audiencesIDS,"Behaviors": behaviorsIDS};
				return d;	
				}
				if(audiencesIDS.length > 0 && ranksIDS.length > 0){
	var d = {"Audiences": audiencesIDS, "Ranks": ranksIDS};
				return d;	
				}
				if(behaviorsIDS.length > 0 && ranksIDS.length > 0){
	var d = {"Behaviors": behaviorsIDS,"Ranks": ranksIDS};
				return d;	
				}
				if(audiencesIDS.length > 0 && behaviorsIDS.length == 0 && ranksIDS.length == 0){
	var d = {"Audiences": audiencesIDS};
				return d;	
				}
				if(audiencesIDS.length == 0 && behaviorsIDS.length > 0 && ranksIDS.length == 0){
	var d = {"Behaviors": behaviorsIDS};
				return d;	
				}
				if(audiencesIDS.length == 0 && behaviorsIDS.length == 0 && ranksIDS.length > 0){
	var d = {"Ranks": ranksIDS};
				return d;	
				}
			}
	}
	return null;
	};
	
	function getCohordIDsExt()
	{
		var cohortNames = $("#email_ext_cohort_names_select").val();
		if(cohortNames == null)
			return [];
		
		var ans = [];
		var children = $("#email_ext_cohort_names_select").children();
		
		
		var ln = children.length;
		for (var i=0 ; i < ln ; i++)
		{
			if (cohortNames.indexOf(children[i].getAttribute('value')) != -1)
				ans.push(children[i].getAttribute('data-cohort-id'));
		}
		return ans;
	};
	
	function getRecepientsEmails()
	{
		var recepients = $("#email_editor_recepients").val();
		
		if(recepients == null || recepients == "")
			return [];
		
		var emails = recepients.replace(/ /g,'');
		return emails != null ? emails.split(",") : [];
	};
	
	function firstStep()
	{
		currentStep = 1;
		$('#cohort_email_editor_title').html("E-mail Manager");
		$('#email_cohort_step_1').css('display' , '');
		$('#email_cohort_step_2').css('display' , 'none');
		$('#mailingList_cohort_step_2').css('display' , 'none');
		
		$('#email_cohort_confirm').css('display' , 'none');
		$('#email_cohort_next').css('display' , '');
		
		$('#email_main_loader').css('display' , 'none');
		
	};
	
	function secondStep()
	{
		currentStep = 2;
		
		$('#email_cohort_step_1').css('display' , 'none');
		$('#email_cohort_next').css('display' , 'none');
		$('#email_cohort_confirm').css('display' , '');
		
		if ($scope.currentService == "email")
		{
			$('#cohort_email_editor_title').html("Send E-mails");
			$('#email_cohort_confirm').html('Send & close');
			$('#email_cohort_step_2').css('display' , '');
		}
		
		if ($scope.currentService == "mailList" || $scope.currentService == "mailListMaropost")
		{
			$('#cohort_email_editor_title').html("Export E-mails to mailList");
			$('#email_cohort_confirm').html('Export & close');
			$('#mailingList_cohort_step_2').css('display' , '');
			
			var name = $("#email_cohort_names_select").val();
			if (name == null || name == "")
				name = $("#email_editor_recepients").val();
			$("#mailingList_name")[0].value = name;
		}
		
	};
	
	function init()
	{
		clearAllForms();
		firstStep();
	};
	
	function clearAllForms()
	{
		// clear cohort field
		$("#email_cohort_names_select").val([]).trigger("change");
		// clear recepient field
		$("#email_editor_recepients").val("");
		//clear service field
		$("#setup_external_apis_content").html('Mail Service <i class="fa fa-angle-down"></i>');
		
		// clear the email forms
		$("#email_editor_subject").val('');
		$("#email_editor_textarea").val('');
		$(".md-preview").html('');
		$("#email_editor_textarea").data('markdown').hidePreview();
		
		// $scope.currentService = "";
	};
	
	$scope.EmailEditorNextStep = function()
	{
		// var service = $("#setup_external_apis_content").text();
		// $scope.currentService = service.substring(0, service.length-1);
		
		if (currentStep == 1 && verifyFirstStep())
		{
			secondStep();
		}
	};
	
	function onServicesUpdate()
	{
		$("#setup_external_apis_content").css('display' , '');
		$("#setup_external_apis_loader").css('display' , 'none');
		
		var data = $scope.ExternalAPIsTableData;
		
		if (data == null || data.length == 0)
			return;
		
		var ln = data.length;
		var lines = '';
		
		for(var i=0 ; i < ln ; i++)
		{
			var service = data[i];
			if (service.type == "email" || service.type == "mailList" || service.type == "mailListMaropost")
			{
				lines+='<li role="presentation" style="width:100%"><a role="menuitem" onclick="serviceSelected(\''+service.uniqueID+'\', \''+service.name+'\')" tabindex="-1">'+service.name+'</a></li>';
				foundService = true;
			}
		}
		
		if ($("#cohort_email_editor")[0].style.display == '' && !foundService)
		{
			$scope.show_warn("No Mail service configured !", "You dont have any Mail service configured. please go to: 'Settings' > 'App Settings' > 'External APIs' to add one.");
			$("#cohort_email_editor").modal('hide');
			return;
		}
		
		$("#setup_external_apis_dropdown").html(lines);
		$("#setup_external_apis_dropdown").val([]).trigger("change");
	};
	
	function serviceSelected(id, name)
	{
		$("#setup_external_apis_content").html(name + ' <i class="fa fa-angle-down"></i>');
	};
	
	function onCohortsUpdate(t,a)
	{ 	
		 var isCohortArr = $interval(function ()
			{

				 if ($scope.cohortData)
				{
					
					 $interval.cancel(isCohortArr);
					$scope.cohortSelect();
				 }
			 }, 300);
	}
	
	$scope.cohortSelect = function(){

	
		var data = $scope.cohortData;
		var ln = data.length;
		var lines = '';
		
		for(var i=0 ; i < ln ; i++)
		{
			var cohort = data[i];
			lines+='<option data-cohort-id='+cohort.audienceid+' value="'+cohort.name+'">'+cohort.name+'</option>';
		}
		
		$("#email_cohort_names_loading").css('display' , 'none');
		$("#email_cohort_names_select").css('display' , '');
		$("#email_cohort_names_select").select2({ placeholder: "Audience names", theme: "classic"});
		$("#email_cohort_names_select").html(lines);
		$("#email_cohort_names_select").val([]).trigger("change");
	};
	
	function verifyFirstStep()
	{
		var ids = $scope.getCohordIDs('mail');
		var idsExt = getCohordIDsExt();
		var emails = getRecepientsEmails();
		
		var total_recepients = 0;
		if(ids != null)
			total_recepients += 1;
		if(idsExt != null)
			total_recepients += idsExt.length;
		if(emails != null)
			total_recepients += emails.length;
		
		if (total_recepients == 0)
			$scope.show_warn("No recepients were specified!", "You must specify at least one recepient or Audience to continue.");
		
		if ($scope.currentService == undefined || $scope.currentService == "" || $scope.currentService == "Mail Service")
		{
			$scope.show_warn("No service selected!", "Please select the service you want to use.");
			return false;
		}
		return total_recepients != 0;	
	};
	
	$scope.EmailEditorOnCreateCampaignChecked = function()
	{
		var checked = $("#mailingList_createCampaign_checkBox_val")[0].checked;
		if (checked)
		{
			$("#setup_mailListTemplates_button").html('Choose Template <i class="fa fa-angle-down"></i>');
			$("#mailListDataContainer").css('display' , '');
			
			
			var email = $scope.loadFromStorage('email');
			var userName = $scope.loadFromStorage('userFirstName')+ " " + $scope.loadFromStorage('userLastName');
			var gameName = $scope.loadFromStorage('loadedGameName');
			
			$("#mailList_campaign_sentFrom").val(userName + " - " + gameName);
			$("#mailList_campaign_subject").val("New Campaign");
			
			if(email != null)
			{
				$("#mailList_campaign_replyTo").val(email);
			}
		}
		else
		{
			$("#mailListDataContainer").css('display' , 'none');
		}
	};
	
	function getmailListTemplates(t){
		
		if(t == 'mailList'){
			var d = {"Type":"MailChimp"};
		}
		if(t == 'mailListMaropost'){
			var d = {"Type":"Maropost"};
			//localhost:8080/Curve/GetExternalLists?Token=YR4H3JGLP5L16UVN&Data={"Platform":"Maropost"}
			var Platform = {"Platform":"Maropost"};
			$scope.curveAPI("GetExternalLists", {"Token": $rootScope.Token, Data:JSON.stringify(Platform)}, GetExternalListsSuccess, GetExternalListsError);
			function GetExternalListsSuccess(data){
				//console.log(data)
				$scope.externalAudienceList = data.lists;
				
				var data11 = $scope.externalAudienceList;
				var ln = data11.length;
				var lines = '';
				
				for(var i=0 ; i < ln ; i++)
				{
					var cohort = data11[i];
					lines+='<option data-cohort-id='+cohort.id+' value="'+cohort.name+'">'+cohort.name+'</option>';
				}
				
				$("#email_ext_cohort_names_loading").css('display' , 'none');
				$("#email_ext_cohort_names_select").css('display' , '');
				$("#email_ext_cohort_names_select").select2({ placeholder: "Audience names", theme: "classic"});
				$("#email_ext_cohort_names_select").html(lines);
				$("#email_ext_cohort_names_select").val([]).trigger("change");
					
					
				}
			function GetExternalListsError(data){console.log(data)}
		}
		
		$scope.curveAPI("GetMailChimpTemplates", {"Token": $rootScope.Token, Data:JSON.stringify(d)}, onGetmailListTemplates, onErrorGetmailListTemplates);
		
		function onGetmailListTemplates(data)
		{	//console.log(data)
			$scope.kaka = 555;
			$("#setup_mailListTemplates_button").css('display' , '');
			$("#setup_mailListTemplates_loader").css('display' , 'none');
			
			if (data == null || data.templates == undefined)
				return;
			
			$scope.mailChimpTemplates = data.templates;
			
			
		};
		
		function onErrorGetmailListTemplates(data)
		{
			console.log(data);
		};
	};
	
	
	
	$scope.templateSelected = function(id, name)
	{
		$("#setup_mailListTemplates_button").html(name + ' <i class="fa fa-angle-down"></i>');
		$("#setup_mailListTemplates_button")[0].setAttribute("data-template-id", id);
	};
	
	
	function onSendChecked()
	{
		var checked = $("#mailingList_send_checkBox_val")[0].checked;
	};
	
}	