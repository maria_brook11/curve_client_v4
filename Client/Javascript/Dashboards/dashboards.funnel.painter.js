function FunnelPainterFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst,InsightConst){

	var precision = 10;
	var max_title_length = 14;

	$scope.drawFunnelData = function(fdata, div)
	{
		$(div)[0].innerHTML = getStepsHTML(parseFunnelSteps(fdata));
		
		if($(div).parent().innerHeight() < $(div)[0].scrollHeight)
			$(div)[0].style["overflow-y"] = "scroll";
		else
			$(div)[0].style["overflow-y"] = "";
		
		$scope.uiCenterDiv(div);
	}

	function getStepsHTML(steps)
	{
		var resp = '';
		
		if(steps == null || steps.length == 0)
		{
			return '<div style="font-size: 30px;color: rgb(206, 206, 206);width: 240px;margin-left: auto;margin-right: auto;margin-top: 100px;"> FUNNEL EMPTY </div>';
		}
		
		for(var i = 0 ; i < steps.length ; i++)
		{
			var step = steps[i];
			resp += getFunnelStep(step.title, step.amount, step.precentage);
			
			if(i+1 < steps.length)
			{
				var diff = Math.floor(precision*(parseFloat(steps[i+1].precentage) - parseFloat(step.precentage)))/precision;
				resp += getFunnelConnector(Math.abs(diff), diff > 0, diff == 0);
			}
		}
		
		return resp;
	}
	function parseFunnelSteps(d)
	{
		var resp = [];
		if(d != null)
		{
			var ln = d.length;
			
			if(ln > 0)
			{
				var total = parseFloat(d[0].actions);
				if(total == 0)
					total = 1;
				
				for(var i = 0 ; i < ln ; i++)
				{
					var c = d[i];
					var precent = Math.ceil(100 * precision * c.actions / total)/precision;
					resp.push({'title': c.action, 'amount': c.actions, 'precentage':precent});
				}
			}
		}
		
		return resp;
	}

	function getFunnelStep(title, amount, precentage)
	{
		var div = '<div class="funnel_step_div" align="center">';
		div += '<div class="funnel_amount_div">' + amount + ' <i class="fa fa-user"></i></div>';
		div += '<div class="funnel_precent_div">' + precentage + '%</div>';
		div += '<div class="funnel_title_div">' + formalizeLength(title) + '</div>';
		div += '</div>';
		return div;
	}

	function formalizeLength(t)
	{
		var temp = t;
		if(t.length > max_title_length)
			t = t.substr(0, max_title_length) + "..";
		
		return '<span title="'+temp+'">' + t + '</span>';
	}

	function getFunnelConnector(precentage, isUp, eq)
	{
		var divclass = eq ? 'funnel_connector_none' : isUp ? 'funnel_connector_up' : 'funnel_connector_down';
		var divarrow = eq ? 'fa-arrows-h' : isUp ? 'fa-arrow-up' : 'fa-arrow-down';
		var div = '<div class="funnel_connector_div" align="center">';
		div += '<i class="fa fa-angle-double-right arrow_right_funnel"></i>';
		div += '<div class="' + divclass + '">' + precentage + '% <i class="fa ' + divarrow +'"></i> </div>';
		div += '</div>';
		return div;
	}
}