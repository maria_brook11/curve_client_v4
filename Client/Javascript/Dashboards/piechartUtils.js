function PieCartDataFunction($scope,$rootScope,$http,$location){


var maxPiechartObjects = 10;

$scope.truncatePiechartData = function(data)
{
	// data = data.sort(compare);
	lastColor = 0;
	if (data == null)
		return [];
	
	// data = data.sort(function(a, b) 
	// {
		// return -1*(parseFloat(a.data) - parseFloat(b.data));
	// });
	
	var ln = data.length;
	var firstColor;
	var lastColor;
	for(var j = 0 ; j < ln ; j++)
	{
		data[j].color = getColor();
		if (j == 0)
			firstColor = data[j].color;
				
		if (j == ln -1)
			lastColor = data[j].color;
	}
	
	if(ln <= maxPiechartObjects)
	{
		for(var i = 0 ; i < ln ; i++)
		{
			data[i].label = PieUtils.truncateLabel(data[i].label);
		}
		
		return data;
	}

	
	var resp = [];
	var othersObject = {label: 'others', data: 0, color:"#80c2d8"};
	for(var i = 0 ; i < ln ; i++)
	{
		if(i < maxPiechartObjects)
		{ 
			data[i].label = PieUtils.truncateLabel(data[i].label);
			resp[i] = data[i];
		}
		else
			othersObject.data = Number(othersObject.data) + Number(data[i].data);
	}

	
	// resp = resp.sort(function(a, b) 
	// {
		//return -1*(parseFloat(a.data) - parseFloat(b.data));
	//});
	
	resp.push(othersObject);
	// console.log(resp);
	return resp;
}

/*var colors = ["#a9d6fb", "#c5e3fc", "#99ebff", "#bbf1ff", "#99ebe7", "#d0edc1", "#dff3d6", "#fbf3b9","#fdfbe8"];*/
var colors = ["#06789e", "#50a0bb", "#19baef", "#5ecff4", "#afc9e1", "#c7d9ea", "#afe1c1", "#c9ebd5", "#88ce5c", "#abdd8d", "#d1e05c","#dfe98d"];

var lastColor = 0;
var PieUtils = {};
PieUtils.truncateLabel = function(txt)
{
	if(txt == null)
		return txt;
	
	if(txt.length > 20)
		return txt.substr(0, 20) + "...";
	
	return txt;
}

function getColor()
{
		lastColor++;
		if(lastColor == colors.length) 
		{
			lastColor = 0;
		}
		return colors[lastColor];
}
 
function compare(a, b) 
{
  if (a.data < b.data) {
    return 1;
  }
  if (a.data > b.data) {
    return -1;
  }
  
  return 0;
}

}