
app.controller('DashbrdCtrl', function($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst,InsightConst,$timeout,$interval,dateService,resetService,saveCSVService,$compile,$document,diagramParseService,diagramValidationService,$mdSidenav) {
	UJdataFunction($scope,$rootScope,$http,$location,$timeout);
	//TopPanelFunction($scope,$rootScope,$http,$location,URLConst,$filter);
	DashboardsFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst,InsightConst,$timeout,$interval,dateService,resetService,saveCSVService);
	AjaxManagerFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter);
	PainterFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter);
	DashBrdGlobalFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter);
	DashBrdDataFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter);
	DashBrdPrevFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter);
	TooltipsFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst);
	PieCartDataFunction($scope,$rootScope,$http,$location);
	DashBrdTableFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter);
	
	DashboardsNotificFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst);
	
	MomentFunction($scope,$rootScope);
	
	CreateGraphFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst,InsightConst);
	FunnelUiFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst,InsightConst);
	FunnelPainterFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst,InsightConst);
	CohortFactiryFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst,InsightConst,$compile);
	
	SetExtAPIsFunction($scope,$rootScope,$http,$location);
	
	CampaignFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst,InsightConst);
	
	CohortEmailFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst,InsightConst,$interval,dateService);
	CohortPushFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst,InsightConst,$interval);
	CohortSmsFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst,InsightConst,$interval);
	
	EmailValidFunction($scope,$rootScope,$http,$location);
	
	UserSearch($scope,$rootScope,$http,$location);
	DateParserFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst,InsightConst,$timeout,$interval);
	PermmissionsFunction($scope,$rootScope,$http,$location,$filter);
	
	AudienceFunction($scope,$rootScope,$http,$location,$filter,$document,$interval,$timeout);
	
	DiagramFunction($scope,$rootScope,$timeout,diagramParseService,diagramValidationService,$http);
    sideNavController($scope,$mdSidenav);
	//DatepickerFunction($scope,$rootScope);
	//DataFilterFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter);
	
	//GlobalPainterFunction($scope,$rootScope,$http,$location);
	//TutorlManagerFunction($scope,$rootScope,$http,$location,TConst);
	//TutorlTextsFunction($scope,$rootScope,$http,$location);
	//TutorlFactoryFunction($scope,$rootScope,$http,$location,TConst);
	
	$rootScope.loginView = false;
	$scope.dashboardStart();


	
	
	
});
