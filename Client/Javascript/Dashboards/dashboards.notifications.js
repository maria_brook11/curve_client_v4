function DashboardsNotificFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst){

$scope.onInteractClick = function(insightId, headingLabel)
	{
		$scope.createAudienceInsightModel = {"id" : insightId, "title" : headingLabel,"actionType" : "none","interaction" : false, "name" : "","subject" : "","UseBatch" : false,"BulkSize" : 100}
		$('#create_interaction_from_insights').modal('show');

	};

$scope.createAudienceInteract = function(){
	$scope.insightPopupID = $scope.createAudienceInsightModel.id;
	var CohortData = {"Name":$scope.createAudienceInsightModel.name,"Id":$scope.createAudienceInsightModel.id,"Description":""};
	
	if($scope.createAudienceInsightModel.actionType == "none"){
		
		var InteractionData = {"Type":"none"};
		
	}else{
		if($scope.createAudienceInsightModel.actionType == "mail"){
			var InteractionData = {"Type":"email", "Title":$scope.createAudienceInsightModel.subject,"Text":$scope.createAudienceInsightModel.text};
		}
		if($scope.createAudienceInsightModel.actionType == "sms"){
			var InteractionData = {"Type":"sms", "FromNumber":$scope.createAudienceInsightModel.subject,"Text":$scope.createAudienceInsightModel.text};
		}
		if($scope.createAudienceInsightModel.actionType == "push"){
			if($scope.createAudienceInsightModel.UseBatch){
				if($scope.createAudienceInsightModel.BulkSize < 1 || $scope.createAudienceInsightModel.BulkSize == ""){
					var bs = "100";
				}else{
					var bs = ""+$scope.createAudienceInsightModel.BulkSize+"";
				}
				var InteractionData = {"Type":"push", "UseBatch":$scope.createAudienceInsightModel.UseBatch,"BulkSize":bs,"Text":$scope.createAudienceInsightModel.text};
			}else{
				var InteractionData = {"Type":"push", "UseBatch":false,"BulkSize":"0","Text":$scope.createAudienceInsightModel.text};
			}
		}
	}
	console.log($scope.createAudienceInsightModel);
	
	
	var data = {"Token": $rootScope.Token, CohortData:JSON.stringify(CohortData), InteractionData:JSON.stringify(InteractionData)};
		
	$scope.curveAPI("CreateCohortPlusInteraction", data, $scope.CreateCohortPlusInteractionSuccess, $scope.CreateCohortPlusInteractionError);
		
}

$scope.CreateCohortPlusInteractionSuccess = function()
{
	$scope.curveAPI("UpdateInsightData", {"Token": $rootScope.Token, "Data": JSON.stringify({"Id": $scope.insightPopupID, "Done": true})}, UpdateInsightDataSuccess ,UpdateInsightDataError);
	$("#create_interaction_from_insights").modal('hide');
};
	
$scope.CreateCohortPlusInteractionError = function()
{
	$("#create_interaction_from_insights").modal('hide');
};

$scope.clearAudienceInsightModelFun = function(){
	$scope.createAudienceInsightModel.subject = "";
	$scope.createAudienceInsightModel.text = "";
	$scope.createAudienceInsightModel.UseBatch = false;
	$scope.createAudienceInsightModel.BulkSize = 100;
}

	
/*********************** crearte audience from reports *****************************/

	$scope.onCreateCohortFromInteractionClick = function(interactionId, headingLabel)
	{
		// console.log("onCreateCohortFromInteractionClick", interactionId, headingLabel);
		$scope.interactionPopupTitle = headingLabel;
		$scope.interactionPopupID = interactionId;

		$scope.audienceModalFRDetails	= [];
		$scope.newAudienceFR = {'name' : '','ConditionType' : '','viewByCondition' : 1}
		$scope.audienceModalFR = {'step' : 0,'valueArr' : [], 'tempValueArr' : [],'data' : 
				{	
					"checkEventName" : "",
					"behaviorName" : "", 
					"itemID" : "",
					"Type" : "Action",  ///from user
					"QueryTable":"", ///table_name
					"ColumnName":"",   ///column_name
					"EventName":"",     ///value
					"HwIDColumnName":"",		///userid_column_name
					"Modifier": "",				///modifiers
					"TimingDefinition":"",   ///from user
					"Timing":"",                ///from user
					"TimingPeriod":"",      ///from user
					"Condition":"",          ///from user
					"Value":"",                 ///from user
					"ValueFrom" : "",			///from user
					"ValueTo" : "",				///from user
					"NextCondition":"AND",         ///from user
					"ID" : ""
				}
			
	
		//$scope.curveAPI('GetCohortsEvents', {Token: $rootScope.Token}, GetCohortsEventsSuccess, GetCohortsEventsError);
	}



	};
	


/*********************************** get notifications data ***************************/
	$scope.Input = {'insight' : ''};
	// $scope.getNotificData = function(){
		// $scope.curveAPI('GetInsights', {'Token': $rootScope.Token}, $scope.onGotInsights, $scope.onGotInsightsError);
	// }
	
	$scope.onGotInsights = function(data){
		
		$scope.insightData = [];
		$scope.insightColor = {
				'positive' : '#3EB7E9',
				'negative' : '#ed6b75',
				'user' : '#2779A1',
				'info' : '#92C362',
				'new' : '#FCE456',
				'action' : '#575757'
			}
		$scope.insightIcon = {
			'positive' : 'fa-thumbs-o-up',
			'negative' : 'fa-thumbs-o-down',
			'user' : 'fa-user',
			'info' : 'fa-info'
		}
		
		for(i = 0 ; i < data.length ; i++)
			{
				try{
					var headerData = JSON.parse(data[i].short_text);
				}
				catch (e)
				{
					//console.log(e);
					continue;
				};
				
				var temp = 	{
								'text' : 	data[i].long_text,
								'heading' : headerData.text,
								'headingLabel' : headerData.label,
								'id' : data[i].id,
								'time' : data[i].last_calculated,
								'seen': data[i].seen,
								'action': data[i].action,
								'color' : $scope.insightColor[headerData.label],
								'icon' : $scope.insightIcon[headerData.label]
							};
								  
			$scope.insightData.push(temp)	
			}
			// console.log("insightData:", $scope.insightData)
	}
	
	$scope.onGotInsightsError = function(){}
	
	function UpdateInsightDataSuccess(){
			$scope.getPickOfTheDay();
		}
		
		function UpdateInsightDataError(){
			
		}
	
	$scope.onCreateCohortClick = function(insightId, headingLabel)
	{
		// console.log("onCreateCohortClick", insightId, headingLabel);
		$scope.insightPopupTitle = headingLabel;
		$scope.insightPopupID = insightId;
		$scope.Input = {'insight' : ''};
		$('#create_cohort_from_insights_loading')[0].style.display = "none";
		$('#create_cohort_from_insights_content')[0].style.display = "";
		$('#create_cohort_from_insights').modal('show');
		
	};
	
	$scope.onCreateCohortFromCampaignClick = function(campaignId, headingLabel)
	{
		// console.log("onCreateCohortFromCampaignClick", campaignId, headingLabel);
		$scope.campaignPopupTitle = headingLabel;
		$scope.campaignPopupID = campaignId;
		$scope.Input = {'campaign' : ''};
		$('#create_cohort_from_campaign_loading')[0].style.display = "none";
		$('#create_cohort_from_campaign_content')[0].style.display = "";
		$('#create_cohort_from_campaign').modal('show');
	};

	
	$scope.onInteractSelected = function(InteractType)
	{
		// console.log("onInteractSelected", InteractType);
		$('#create_interaction_select_action_button').html(InteractType);
		$scope.interactType = InteractType;
	};
	
	$scope.onInteractNext = function()
	{
		// console.log("onInteractNext", $scope.interactType);
		
		$('#create_interaction_loader').css('display' , '');
		
		$('#create_interaction_from_insights_content').css('display' , 'none');
		$('#create_interaction_next_button').css('display' , 'none');
		
		setTimeout(loaderComplete, 3000);
	};
	
	function loaderComplete()
	{
		$('#create_interaction_loader').css('display' , 'none');
		$('#create_interaction_select_action').css('display' , '');
		$('#create_interaction_create_button').css('display' , '');
		$('#create_interaction_interact_button').css('display' , '');
	};
	
	$scope.onInteractCreateAudience = function(InteractType)
	{
		var data = {"Name": $scope.Input.insight, "Id": $scope.insightPopupID, "Description": "this Audience was created from insight " + $scope.insightPopupID};
		$scope.curveAPI("CreateCohortFromInsight", {"Token": $rootScope.Token, "Data": JSON.stringify(data)}, $scope.onCohortCreated, $scope.onFailedCreatingCohort);
	};
	
	$scope.onInteractSelectedConfirm = function()
	{
		$('#create_interaction_select_action').css('display' , 'none');
		$('#create_interaction_create_button').css('display' , 'none');
		$('#create_interaction_interact_button').css('display' , 'none');
		
		if ($scope.interactType =="Send E-mail")
		{
			$("#email_editor_textarea").markdown({
				autofocus:false,
				savable:false,
				resize:"none",
			});
			// clear the email forms
			$("#email_editor_subject").val('');
			$("#email_editor_textarea").val('');
			$(".md-preview").html('');
			$("#email_editor_textarea").data('markdown').hidePreview();
			
			$('#create_interaction_email_step').css('display' , '');
		}
		else if ($scope.interactType =="Send SMS")
		{
			$("#sms_editor_textarea").markdown({
				autofocus:false,
				savable:false,
				resize:"none",
			});
			// clear the sms forms
			$("#sms_editor_textarea").val('');
			$(".md-preview").html('');
			$("#sms_editor_textarea").data('markdown').hidePreview();
			
			$('#create_interaction_sms_step').css('display' , '');
		}
		else if ($scope.interactType =="Send Push")
		{
			$("#push_editor_textarea").markdown({
				autofocus:false,
				savable:false,
				resize:"none",
			});
			// clear the push forms
			$("#push_editor_textarea").val('');
			$(".md-preview").html('');
			$("#push_editor_textarea").data('markdown').hidePreview();
			
			$('#create_interaction_push_step').css('display' , '');
		}
		else
		{
			$scope.show_error("Please choose an action.", "Send E-mail, SMS or Push notification.");
			$('#create_interaction_select_action').css('display' , '');
			$('#create_interaction_create_button').css('display' , '');
			$('#create_interaction_interact_button').css('display' , '');
			return;
		}
		
		$('#create_interaction_send_button').css('display' , '');
	};
	
	$scope.onInteractSendAndCloseClick = function()
	{
		// console.log("onInteractSendAndCloseClick", $scope.interactType);
		
		$('#create_interaction_from_insights_content').css('display' , 'none');
		
		if ($scope.interactType =="Send E-mail")
		{
			sendEmail();
		}
		else if ($scope.interactType =="Send SMS")
		{
			SendSms();
		}
		else if ($scope.interactType =="Send Push")
		{
			SendPush();
		}
	};
	
	function SendSms()
	{
		var interactionData = $scope.validateAndPrepareSmsData(true);
		
		if(interactionData.error != null)
		{
			$scope.show_error("Make sure all fields are properly filled", interactionData.error);
			return;
		}
		
		if(interactionData.warn != null)
		{
			$scope.EmailValidatorManager("", sendWithoutValidatingSms, null, interactionData.warn, "If not, please make sure to fill them properly.", "Yes, Send", "Cancel");
			return;
		}
		
		createCohortPlusInteraction(interactionData);
	};
	
	function sendWithoutValidatingSms(id)
	{
		var interactionData = $scope.validateAndPrepareSmsData(false);
		
		createCohortPlusInteraction(interactionData);
	};
	
	function SendPush()
	{
		var interactionData = $scope.validateAndPreparePushData(true, false);
		
		if(interactionData.error != null)
		{
			$scope.show_error("Make sure all fields are properly filled", interactionData.error);
			return;
		}
		
		if(interactionData.warn != null)
		{
			$scope.EmailValidatorManager("", sendWithoutValidatingPush, null, interactionData.warn, "If not, please make sure to fill them properly.", "Yes, Send", "Cancel");
			return;
		}
		
		// interactionData.CohortIds = [$scope.insightPopupID];
		createCohortPlusInteraction(interactionData);
	};
	
	function sendWithoutValidatingPush(id)
	{
		var interactionData = $scope.validateAndPreparePushData(false, false);
		
		// interactionData.CohortIds = [$scope.insightPopupID];
		createCohortPlusInteraction(interactionData);
	};
	
	function sendEmail()
	{
		var interactionData = $scope.validateAndPrepareEmail(true, false);
		
		// console.log("sendEmail", interactionData);
		
		if(interactionData.error != null)
		{
			$scope.show_error("Make sure all fields are properly filled", interactionData.error);
			return;
		}
		
		if(interactionData.warn != null)
		{
			$scope.EmailValidatorManager("", sendWithoutValidatingEmail, null, interactionData.warn, "If not, please make sure to fill them properly.", "Yes, Send", "Cancel");
			return;
		}
		
		createCohortPlusInteraction(interactionData);
	};
	
	function sendWithoutValidatingEmail()
	{
		var interactionData = $scope.validateAndPrepareEmail(false, false);
		
		createCohortPlusInteraction(interactionData);
	};
	
	function createCohortPlusInteraction(interactionData)
	{
		var cohortData = {"Name": $scope.Input.insight, "Id": $scope.insightPopupID, "Description": "this Audience was created from insight " + $scope.insightPopupID};
		var finalData = {"Token": $rootScope.Token, CohortData:JSON.stringify(cohortData), InteractionData:JSON.stringify(interactionData)};
		
		// console.log("createCohortPlusInteraction final:", finalData);
		$scope.curveAPI("CreateCohortPlusInteraction", finalData, CreateCohortPlusInteractionSuccess, CreateCohortPlusInteractionError);
		
		
	};
	
	$scope.onGotItClicked = function(insightId)
	{
		$scope.curveAPI("UpdateInsightData", {"Token": $rootScope.Token, "Data": JSON.stringify({"Id": insightId, "Done": true})}, UpdateInsightDataSuccess ,UpdateInsightDataError);
	};
	 
	$scope.createCohortFromInsight = function()
	{
		$('#create_cohort_from_insights_loading')[0].style.display = "";
		$('#create_cohort_from_insights_content')[0].style.display = "none";
		var data = {"Name": $scope.Input.insight, "Id": $scope.insightPopupID, "Description": "this Audience was created from insight " + $scope.insightPopupID};
		// console.log(data)
		$scope.curveAPI("CreateCohortFromInsight", {"Token": $rootScope.Token, "Data": JSON.stringify(data)}, $scope.onCohortCreated, $scope.onFailedCreatingCohort);
	};
	
	$scope.createCohortFromCampaign = function()
	{
		$('#create_cohort_from_campaign_loading')[0].style.display = "";
		$('#create_cohort_from_campaign_content')[0].style.display = "none";
		var data = {"Name": $scope.Input.campaign, "Id": $scope.campaignPopupID, "Description": "this Audience was created from campaign " + $scope.campaignPopupID};
		
		// console.log("createCohortFromCampaign", data);
		$scope.curveAPI("CreateCohortFromCampaign", {"Token": $rootScope.Token, "CohortData": JSON.stringify(data)}, $scope.onCohortCreated, $scope.onFailedCreatingCohort);
	};
	
	$scope.createCohortFromInteraction = function()
	{
		$('#create_cohort_from_interaction_loading')[0].style.display = "";
		$('#create_cohort_from_interaction_content')[0].style.display = "none";
		
		var cType = "allEmails"
		
		if($scope.interactionTypeOfEmail == "Non-Opened Emails")
			cType = "notOpenedEmails";
		if($scope.interactionTypeOfEmail == "Complete Signup")
			cType = "signup";
		if($scope.interactionTypeOfEmail == "Clicked Link")
			cType = "clicked";
		if($scope.interactionTypeOfEmail == "Opened Emails")
			cType = "openedEmails";
		
		var data = {"CohortName": $scope.Input.interaction, "InteractionId": $scope.interactionPopupID, "CohortType": cType};
		
		// console.log("createCohortFromInteraction", data);
		
		/////////////////////////////// MOSHE 
		//$scope.curveAPI("CreateCohortByInteractionId", {"Token": $rootScope.Token, "Data": JSON.stringify(data)}, $scope.onCohortCreated, $scope.onFailedCreatingCohort);		
		$scope.curveAPI("CreateCohortByInteractionIdWithMailListType", {"Token": $rootScope.Token, "Data": JSON.stringify(data)}, $scope.onCohortCreated, $scope.onFailedCreatingCohort);
	};
	
	$scope.onCohortCreated = function(data)
	{	
		
		$scope.curveAPI("UpdateInsightData", {"Token": $rootScope.Token, "Data": JSON.stringify({"Id": $scope.insightPopupID, "Done": true})} , UpdateInsightDataSuccess ,UpdateInsightDataError );
		$("#create_cohort_from_insights").modal('hide');
		$("#create_cohort_from_campaign").modal('hide');
		$("#create_cohort_from_interaction").modal('hide');
		$("#create_interaction_from_insights").modal('hide');
		$scope.show_success("Successfully created Audience!", "A new Audience was created.");
	};
	
	$scope.onFailedCreatingCohort = function(data)
	{
		$("#create_cohort_from_insights").modal('hide');
		$("#create_cohort_from_campaign").modal('hide');
		$("#create_cohort_from_interaction").modal('hide');
		$("#create_interaction_from_insights").modal('hide');
		$scope.show_error('ERROR! ', data);
	};
/*********************************** end get notifications data ***************************/
	
}	
	
	
	// $scope.getNotificData = function(){
		// console.log('getNotificData');
		// $scope.curveAPI('GetInsights', {'Token': $rootScope.Token}, $scope.onGotInsights, $scope.onGotInsightsError);
	// }
	
	// $scope.onGotInsights = function(data){
		// console.log(data);
		// for(i = 0 ; i < data.length ; i++)
			// {
				// try{
					// var headerData = JSON.parse(data[i].short_text);
				// }
				// catch (e)
				// {
					// console.log(e);
					// continue;
				// };
				
				// $scope.insightData = {
									// 'text' : 	data[i].long_text,
									// 'heading' : headerData.text,
									// 'headingLabel' : headerData.label,
									// 'id' : data[i].id,
									// 'time' : data[i].last_calculated,
									// 'seen': data[i].seen,
									// 'action': data[i].action
								  // };
								  
				
			// }
			// console.log($scope.insightData)
	// }
	
	// $scope.onGotInsightsError = function(){}
	

// var NotificationsManager = function()
// { 
	// NotificationsManager.instance = this;
// };

// NotificationsManager.prototype = 
// {
	// insightsTabId: 17,
	// gotInsights: false,
	// InsightsData: [],
	// InsightsDataNew: [],
	// InsightsDataOld: [],
	// InsightsHeadingsIDs: [],
	// InsightsContainers: [],
	
	// Colors:{
		// positive : 	'#3EB7E9',
		// negative : 	'#ed6b75',
		// user : 		'#2779A1',
		// info : 		'#92C362',
		// new :		'#FCE456',
		// action:		'#575757',
	// },
	
	// Icons: {
		// positive : 	'<i class="fa fa-thumbs-o-up" style="font-size: 3em;line-height: 1;" aria-hidden="true"></i>',
		// negative : 	'<i class="fa fa-thumbs-o-down" style="font-size: 3em;line-height: 1;" aria-hidden="true"></i>',
		// user : 		'<i class="fa fa-user" style="font-size: 3em;line-height: 1;" aria-hidden="true"></i>',
		// info : 		'<i class="fa fa-info" style="font-size: 3em;line-height: 1;" aria-hidden="true"></i>',
	// },
	
	// init: function (newInsightsTabId) {
		// this.insightsTabId = newInsightsTabId;
		// $scope.curveAPI('GetInsights', {'Token': $rootScope.Token}, NotificationsManager.instance.onGotInsights, NotificationsManager.instance.onGotInsightsError);
	// },
	
	// onGotInsights: function (data) {
		// // console.log("onGotInsights", data);
		// NotificationsManager.instance.InsightsDataNew = [];
		// NotificationsManager.instance.InsightsDataOld = [];
		// NotificationsManager.instance.InsightsData = [];
		
		// if (data.length == 0)
		// {
			// // add new insights
			// NotificationsManager.instance.getStartupInsights();
		// }
		// else
		// {
			// NotificationsManager.instance.parseInsights(data);
		// }
	// },
	
	// parseInsights: function (data) {
		// for(i = 0 ; i < data.length ; i++)
		// {
			// try{
				// var headerData = JSON.parse(data[i].short_text);
			// }
			// catch (e)
			// {
				// console.log(e);
				// continue;
			// };
			
			// var insightData = {
								// text : 	data[i].long_text,
								// heading : headerData.text,
								// headingLabel : headerData.label,
								// id : data[i].id,
								// time : data[i].last_calculated,
								// seen: data[i].seen,
								// action: data[i].action
							  // };
							  
			// NotificationsManager.instance.InsightsData.push(insightData);
			
			// if (insightData.seen == "true")
				// NotificationsManager.instance.InsightsDataOld.push(insightData);
			// else
				// NotificationsManager.instance.InsightsDataNew.push(insightData);
				
			// NotificationsManager.instance.InsightsDataOld = NotificationsManager.instance.sortByDateNotifications(NotificationsManager.instance.InsightsDataOld);
			
		// }
		// NotificationsManager.instance.gotInsights = true;
		// NotificationsManager.instance.showNotifications();
		// NotificationsTopPanelManager.instance.fillInsights(NotificationsManager.instance.InsightsDataNew, NotificationsManager.instance.InsightsDataOld);
		// NotificationsManager.instance.fillInsightsInTables();
	// },
	
	// getStartupInsights: function () {
		
		// $scope.curveAPI('GetIsDataExists', {'Token': $rootScope.Token}, NotificationsManager.instance.onGetIsDataExists, NotificationsManager.instance.onGetIsDataExistsError);
	// },
	
	// onGetIsDataExists: function (data) {
		// // console.log("onGetIsDataExists", data);
		// var insightData = InsightsStartupData;
		// if (data == false)
		// {
			// for(i = 0 ; i < InsightsNoSdkData.length ; i++)
			// {
				// insightData.push(InsightsNoSdkData[i]);
			// }
		// }
		// NotificationsManager.instance.parseInsights(insightData);
	// },
	
	// onGetIsDataExistsError: function (data) {
		// console.log("onGetIsDataExistsError", data);
		// var insightData = InsightsStartupData;
		// NotificationsManager.instance.parseInsights(insightData);
	// },
	
	// showNotifications: function () {
		// var data = NotificationsManager.instance.InsightsDataNew;
		// if(data.length > 0)
		// {	
			// NotificationsManager.instance.showNotification(data[0]);
			
			// if(data.length > 1)
			// {
				// var addtionalInsightsData = {
								// heading : "You have additional " + (data.length - 1) + " new insight.",
							  // };
				// NotificationsManager.instance.showNotification(addtionalInsightsData);
			// }
			
			// var elems = document.getElementsByClassName("curve_notification_click");
			// for(j = 0 ; j < elems.length ; j++)
			// {
				// elems[j].onclick = NotificationsManager.instance.onFloatingNotificationClick;
			// }
			// var elemsh = document.getElementsByClassName("jquery-notific8-heading");
			// for(k = 0 ; k < elemsh.length ; k++)
			// {
				// elemsh[k].innerHTML = elemsh[k].innerHTML + '<i style="color: rgb(200, 200, 200);font-size: 32px;font-weight: bold;position: absolute;right: 10px;bottom: 15px;" class="fa fa-bell-o"></i>';
			// }
		// }
	// },
	
	// addInsightsContainer: function (cont) {
		// NotificationsManager.instance.InsightsContainers.push(cont);
	// },
	
	// fillInsightsInTables: function () {
		// while (NotificationsManager.instance.InsightsContainers.length > 0)
		// {
			// NotificationsManager.instance.fillInsights(NotificationsManager.instance.InsightsContainers.pop());
		// }
	// },
	
	// fillInsights: function (tableId) {
		// $(tableId).empty();
		// var data2 = NotificationsManager.instance.InsightsDataOld;
		// for(i = 0 ; i < data2.length ; i++)
		// {
			// $(tableId).append(NotificationsManager.instance.getNewAccordionNotification(tableId, data2[i]));
		// }
		// var data1 = NotificationsManager.instance.InsightsDataNew;
		// for(j = 0 ; j < data1.length ; j++)
		// {
			// $(tableId).append(NotificationsManager.instance.getNewAccordionNotification(tableId, data1[j]));
			// NotificationsManager.instance.InsightsDataOld.push(data1[j]);
		// }

		// NotificationsManager.instance.InsightsDataOld = NotificationsManager.instance.sortByDateNotifications(NotificationsManager.instance.InsightsDataOld);
		// NotificationsManager.instance.InsightsDataNew = [];
	// },
	
	// onGotInsightsError: function (data) {
		// console.log("onGotInsightsError", data);
	// },
	
	// testId: 0,
	// testNotification: function ()
	// {
		// this.testId++;
		// var insightData = {
							// heading : "short_text",
						  // };
		
		// this.showNotification(insightData);
		
		// var elems = document.getElementsByClassName("curve_notification_click");
		// for(j = 0 ; j < elems.length ; j++)
		// {
			// elems[j].onclick = NotificationsManager.instance.onFloatingNotificationClick;
		// }
		// var elemsh = document.getElementsByClassName("jquery-notific8-heading");
		// for(k = 0 ; k < elemsh.length ; k++)
		// {
			// elemsh[k].innerHTML = elemsh[k].innerHTML + '<i style="color: rgb(200, 200, 200);font-size: 32px;font-weight: bold;position: absolute;right: 10px;bottom: 15px;" class="fa fa-bell-o"></i>';
		// }
	// },
	
	// showNotification: function (insightData) {
		// // console.log("showNotification");
		// var totalLetters = 130;
		// var headingText = insightData.heading;
		// if (headingText.length > totalLetters)
			// headingText = headingText.substring(0,totalLetters) + "...";
		// var params = {
			  // life: "5000",//$('#notific8_life').find('option:selected').val(),
			  // theme: "teal curve_notification_click",//$('#notific8_theme').find('option:selected').val(),
			  // // color: $('#notific8Color').val(), // overrided in components.css - .jquery-notific8-notification
			  // sticky: false,//$('#notific8_sticky').is(':checked'),
			  // horizontalEdge: "bottom",//$('#notific8_pos_hor').find('option:selected').val(),
			  // verticalEdge: "right",//$('#notific8_pos_ver').find('option:selected').val(),
			  // heading: headingText,
			  // // namespace: $('#notific8Namespace').val(),
			  // // height: 100,//$('#notific8Height').val(),
			 
			  // // onInit: function(data) {
				// // if (window.console) {
				  // // console.log('--onInit--');
				  // // console.log('data:');
				  // // console.log(data);
				// // }
			  // // },
			  // // onCreate: function(notification, data) {
				// // if (window.console) {
				  // // console.log('--onCreate--');
				  // // console.log('notification:');
				  // // console.log(notification);
				  // // console.log('data:');
				  // // console.log(data);
				// // }
			  // // },
			  // // onClose: function(notification, data) {
				// // if (window.console) {
				  // // console.log('--onClose--');
				  // // console.log('notification:');
				  // // console.log(notification);
				  // // console.log('data:');
				  // // console.log(data);
				// // }
			  // // }
			// };
			
		  // // $closeText = $('#notific8CloseText'),
		  // // $icon = $('#notific8Icon'),
		  // // $image = $("input[name='notific8Image']:checked"),
		  
		// // if (
		  // // $('#notific8IconOrImage').val() == 'icon' &&
		  // // $.trim($icon.val()) !== ''
		// // ) {
		  // // params.icon = $icon.val();
		// // }
		// // if (
		  // // $('#notific8IconOrImage').val() == 'image' &&
		  // // $image.val() !== undefined
		// // ) {
		  // // params.image = $image.val();
		// // }
		// // if ($.trim($closeText.val()) !== '') {
		  // // params.closeText = $closeText.val();
		// // }
		
		// // show notification
		// $.notific8("", params);
	// },
	
	// onNotificationClick:function(insightDataID)
	// {
		// try{
			// this.goToInsightsTab();
			// $("#" + this.InsightsHeadingsIDs[insightDataID]).click();
		// }
		// catch (e)
		// {
			// console.log("Failed to open insight data " + insightDataID, e);
		// }
	  
	// },
	
	// onFloatingNotificationClick:function()
	// {
		// NotificationsManager.instance.goToInsightsTab();
	// },
	
	// goToInsightsTab: function()
	// {
		// $("#" + this.insightsTabId).click();
	// },
	
	// getNewAccordionNotification: function(parentId, insightData)
	// {
		// var headingID = "insight_heading_" + parentIdClean + '_' + insightData.id;
		
		// NotificationsManager.instance.InsightsHeadingsIDs[insightData.id] = headingID;
		// // console.log("getNewAccordionNotification, ", insightData);
		// var parentIdClean = parentId.substr(1); // remove the #
		// ans = "";
		// ans += '<div class="panel panel-default">';
		// ans += '	<div class="panel-heading feeds">';
		// ans += '		<a class="accordion-toggle" data-toggle="collapse" data-parent="' + parentId + '" id='+ headingID+ ' onclick="NotificationsManager.instance.onAccordionClick(\'' + parentIdClean + '_' + insightData.id + '\');" href="#collapse_' + parentIdClean + '_' + insightData.id + '"> ';
		// ans += '			<li>';
		// ans += '				<div class="col1">';
		// ans += '					<div class="cont" style="padding: 10px;width: 100%;background-color: #ededed;">';
		// ans += '						<div class="cont-col1">';
		
		// // icon
		// ans += '<div class="label label-sm" style="color:' + this.Colors[insightData.headingLabel] + ';">' + this.Icons[insightData.headingLabel] + '</div>';
		
		// ans += '						</div>';
		// ans += '						<div class="cont-col2">';
		// ans += '							<div class="desc" style="font-size: 16px;margin-left: 55px;color: #000000;font-weight: 600;padding-top: 10px; max-width:65%;"> ';
		
		// // header text
		// ans += insightData.heading;	
		// ans += '							</div>';
		// ans += '						</div>';
		// ans += '					</div>';
		// ans += '				</div>';
		
		// // arrow
		// ans += '				<div class="wideCol2 hide-on-medium-screen" style="margin-left: -120px;padding-top: 25px;"><i id="arrow_' + parentIdClean + '_' + insightData.id + '" class="fa fa-chevron-down" style="color:' + this.Colors.action + ';font-size: 1.5em;" aria-hidden="true"></i></div>';
		
		// // date
		// if (insightData.seen == "true")
		// {
			// ans += '				<div class="wideCol2 hide-on-small-screen">';
			// ans += '					<div class="date" style="color:#606060;font-size: 14px;padding-top: 15px;"> ' + insightData.time + ' </div>';
			// ans += '				</div>';
		// }
		// else
		// {
			// ans += '				<div class="wideCol2 hide-on-small-screen">';
			// ans += '					<a class="btn red" style="background-color:' + this.Colors.new + ';margin-top: 15px;margin-left: 25px;color:#000000"> NEW </a>';
			// ans += '				</div>';
		// }
		// ans += '			</li>';
		// ans += '		</a>';
		// ans += '	</div>';
		
		// // body
		// ans += '	<div id="collapse_' + parentIdClean + '_' + insightData.id + '" class="panel-collapse collapse">';
		// ans += '		<div class="panel-body">';
		// ans += '			<p style="margin-left:72px; margin-right:60px;"> ' + insightData.text + ' </p>';
		
		// // call to action from body:
		// ans += this.getButton(insightData);
		
		// ans += '		</div>';
		// ans += '	</div>';
		// ans += '</div>';
		// return ans;
	// },
	
	// getButton: function(insightData)
	// {
		// var ans = "";
		// if (insightData.action == "Cohort")
		// {
			// ans += '				<div class="wideCol2">';
			// ans += '					<a class="btn" onclick="NotificationsManager.instance.onCreateCohortClick(' + insightData.id + ', \'' + insightData.headingLabel + '\');" style="background-color:' + this.Colors.action + ';margin-top: 15px;margin-left: 72px;color:#ffffff"> Create Cohort </a>';
			// ans += '				</div>';
		// }
		// else if (insightData.action == "LinkToSdkInstructions")
		// {
			// ans += '				<div class="wideCol2">';
			// ans += '					<a class="btn" style="background-color:' + this.Colors.action + ';margin-top: 15px;margin-left: 72px;color:#ffffff" href="' + URLConst.CurveSDKConstants.JS_SDK_INSTRUCTIONS_URL + '&AppToken=' + $rootScope.Token + '"  target="_blank">SDK instructions </a>';
			// ans += '				</div>';
		// }
		// else if (insightData.action == "GoToCohortsPanel")
		// {
			// ans += '				<div class="wideCol2">';
			// ans += '					<a class="btn" onclick="$(\'#cohorts_tab_button\').click();" style="background-color:' + this.Colors.action + ';margin-top: 15px;margin-left: 72px;color:#ffffff"> Goto Cohorts </a>';
			// ans += '				</div>';
		// }
		// else if (insightData.action == "ShowUserSegmentsAndCharacteristics")
		// {
			// ans += '				<div class="wideCol2">';
			// ans += '					<a class="btn" onclick="$(\'#user_segments_and_characteristics\').modal(\'show\');" style="background-color:' + this.Colors.action + ';margin-top: 15px;margin-left: 72px;color:#ffffff"> Segments And Characteristics </a>';
			// ans += '				</div>';
		// }
		// return ans;
	// },
	
	// onAccordionClick: function(id)
	// {
		// //TODO : check all the collaped height and change the arrow of the one with height > 0 ?
	// },
	
	// getInsight: function(insightId)
	// {
		// var data = this.InsightsData;
		// for(i = 0 ; i < data.length ; i++)
		// {
			// if (data[i].id == insightId)
				// return data[i];
		// }
	// },
	
	// onCreateCohortClick: function(insightId, headingLabel)
	// {
		// var headingText = this.getInsight(insightId).heading;
		// // console.log("onCreateCohortClick", insightId);
		// $('#create_cohort_from_insights_header')[0].innerHTML = '<span class="label label-sm" style="color:' + this.Colors[headingLabel] + ';">' + this.Icons[headingLabel] + '</span>';
		// $('#create_cohort_from_insights_header').append('<span class="desc" style="font-size: 16px;color: #000000;font-weight: 600;padding-top: 10px;padding-left: 10px;">' + headingText + '</span>');
		// $('#create_cohort_from_insights_loading')[0].style.display = "none";
		// $('#create_cohort_from_insights_content')[0].style.display = "";
		// $('#create_cohort_from_insights').modal('show');
		// $("#create_cohort_from_insights_name")[0].value = "";
		// $("#create_cohort_from_insights_name")[0].setAttribute("data-insight-id", insightId);
	// },
	
	// createCohort()
	// {
		// var data = NotificationsManager.instance.validateAndPrepareCohort();
		
		// if(data.error != null)
		// {
			// show_error("Make sure all fields are properly filled", data.error);
			// return;
		// }
		
		// $('#create_cohort_from_insights_loading')[0].style.display = "";
		// $('#create_cohort_from_insights_content')[0].style.display = "none";
		
		// //console.log(data);
		
		// $scope.curveAPI("CreateCohortFromInsight", {"Token": $rootScope.Token, Data:JSON.stringify(data)}, NotificationsManager.instance.onCohortCreated, NotificationsManager.instance.onFailedCreatingCohort);
		// //http://localhost:8080/Curve/CreateCohortFromInsight?Token=YR4H3JGLP5L16UVN&Data={"Name":"Cohort Test","Id":"13","Description":"This is CreateCohortFromInsight"}
	// },
	
	
	// validateAndPrepareCohort: function()
	// {
		// var cohortName = $("#create_cohort_from_insights_name").val();
		
		// if (cohortName == "")
			// return {error: "You must specify a cohort name."};
		
		// var insightId = $("#create_cohort_from_insights_name")[0].getAttribute("data-insight-id");
		
		// return {"Name": cohortName, "Id": insightId, "Description": "this cohort was created from insight " + insightId};
	// },
	
	// onCohortCreated: function(data)
	// {
		// //console.log("onCohortCreated", data);
		// $("#create_cohort_from_insights").modal('hide');
		// // if (data == 0)
			// // show_message("Cohort was not created", "Non of the e-mails found in the mailing list exist in curve users data.");
		// // else
			// show_success("Successfully created cohort!", "A new cohort was created from this insight.");
	// },
	
	// onFailedCreatingCohort: function(data)
	// {
		// console.log("onFailedCreatingCohort", data);
		// $("#create_cohort_from_insights").modal('hide');
		// show_error('ERROR! ', data);
	// },
	
	// sortByDateNotifications: function(arr)
	// {
			// function compare(a,b) {
			  // if (NotificationsTopPanelManager.instance.getInsightDate(a.time) < NotificationsTopPanelManager.instance.getInsightDate(b.time))
				// return -1;
			  // if (NotificationsTopPanelManager.instance.getInsightDate(a.time) > NotificationsTopPanelManager.instance.getInsightDate(b.time))
				// return 1;
			  // return 0;
			// }

			// //console.log("before sort", arr);
			// arr = arr.sort(compare);
			// //console.log("after sort", arr);
			// return arr;
		
	// }
	
// }

// new NotificationsManager();
// }
