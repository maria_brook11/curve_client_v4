function AjaxManagerFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter){

var currentAjaxCalls = {};

$scope.saveAjaxCall = function(key, call)
{
	currentAjaxCalls[key] = call;
}

$scope.deleteAjaxCall = function(key)
{
	delete currentAjaxCalls[key]; 
}

$scope.abortAjaxCalls = function()
{
	
	for(var key in currentAjaxCalls)
	{
		var xhr = currentAjaxCalls[key];
		if(xhr != null)
		{
			xhr.abort();
			delete currentAjaxCalls[key];
		}
	}
}

function discardAjaxCalls()
{
	currentAjaxCalls = {};
}

}