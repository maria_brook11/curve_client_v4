function DashboardsFunction($scope, $rootScope, $http, $location, TConst, URLConst, $filter, CountryConst, InsightConst, $timeout, $interval,dateService,resetService,saveCSVService)
{
	
$scope.openGenderFlag = true;
$scope.multiGraphwww = '';
$scope.createNewCohortLinkFun = function(){
	$scope.setTabFun(0,'Cohorts');
	//$scope.startAudincesFun();
	$('#audienceTab').tab('show');
}

$rootScope.$on('$locationChangeStart',function(evt, absNewUrl, absOldUrl) {
	
		var urlParameters = $location.search();
		if(urlParameters.settings != null){
			
			$rootScope.dashboardView.active = 7;
			$rootScope.settingsActiveTab = urlParameters.settings;
		}
		if(absOldUrl.indexOf('settings') != -1 && absNewUrl.indexOf('settings') == -1){
				
				$scope.setTabFun($scope.activeTabId, $scope.activeTabName);
				
		}
	});



	var graphID = 1;
	//$scope.dateStatus = {};
	//$scope.dateStatus.singleDate = false;
	//$scope.rangeDate = false;
	$scope.parentAdvanced = false;
	$scope.funVisFalse = false;
	$scope.tabVizible = true;
	$scope.graphVizible = true;
	$scope.currBarInd = 0;
	$scope.graphData = [];
	$scope.filterSelected = {};
	$scope.activeTab = {};
	$scope.pickOfTheDayViewLoad = true;
	//$scope.openGenderFlag = true;
	$scope.SingleCustomerData = false;
	$scope.filtersOBJ = {};
	$scope.filtersOBJflag = false;
	$scope.GraphTypeMLMagnitude = true;
	/* upload app icon */
	/* $scope.resetAppIconFile = function(){
		$('#addIconSelectedFileName').html('No file chosen');
	}
	
	$("#appIconInputFile").live("change",function(){
		$('#addIconSelectedFileName').html('Selected file: '+$("#appIconInputFile").val());
	}); */

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$scope.initSearch = function()
{
	$('#search-user').modal('show');
	hideSearching();
	$('#search_user_result').css('display','none');
	$('#search_field_single_user_fullname').css('display','none');
	$('#search_field_single_user').css('display','');
	$('#search_user_result_panel').html();
	$('#search_user_input').val(''); // Search modal input text
	$scope.searchUsersData = [];
	$scope.viewResStatus = 0;
	
}


$scope.saveGraphFun = function(data){
	
			var title = data.Name;
			var graphdata = data;
			if(graphdata.Options){
				var options = JSON.parse(graphdata.Options);
			}
			var multi = false;
			
			if(graphdata == null)
			{
				$scope.show_warn("Data for this table is empty!", "You cant download data for an empty graph");
				return;
			}
			
			if(options != null &&  options.parseAsMultiline != null && (options.parseAsMultiline == "true" || options.parseAsMultiline == true))
			{
				var temp = $scope.parseGraphData(data.dataGraph);
				var tableData = $scope.parseMultilineData(temp);
				multi = true;
				
			}
			else{
				var tableData = $scope.parseGraphData(data.dataGraph);
				
			}
			
			var rawData = tableData[0].data;
			var ln = rawData.length;
			
			var type = graphdata.GraphType;
			
			if(type == 'SmallTable' || type == 'BigTable')
			{		
				saveCSVService.extractTableData(tableData, title);
				return;
			}
			
			
			if(ln == 0)
			{
				$scope.show_warn("Data for this table is empty!", "You cant download data for an empty graph");
				return;
			}
			
			var myCsv = saveCSVService.parseCsvForPieChart(tableData,title,type,multi);
			
			/* if(multi){
				var myCsv = saveCSVService.parseCsvForMultiGraph(tableData,title,type,multi);
			}else{
				var myCsv = saveCSVService.parseCsvForPieChart(tableData,title,type,multi);
			} */
			
			var uri = 'data:text/csv;charset=utf-8,' + escape(myCsv);

			var downloadLink = document.createElement("a");
			downloadLink.href = uri;
			downloadLink.download = title + "_data.csv";

			document.body.appendChild(downloadLink);
			downloadLink.click();
			document.body.removeChild(downloadLink); 


        //return x.toString(16);
   
}

///////////////////////  dashboard filtering ////////////////////////////////////////////////////////////////////

$scope.filter = {'age':{},'val':{},'gender':[{"id": 1,"name": "Male","add" : false}, {"id": 2, "name": "Female","add" : false}]};

$scope.getPlatformsAndCountries = function(){
	
	if($scope.filterCountryArr && $scope.filterPlatformArr){
		$('#modal_dashboard_filter').modal('show');
		
	}else{
		$('#modal_dashboard_filter').modal('show');
		$scope.curveAPI('GetPlatformsAndCountries', {'Token': $rootScope.Token}, gotFilteringData, failedCountries);
	}
	
	if(Object.keys($scope.filtersOBJ).length == 0){
		$scope.filter = {'age':{},'val':{},'gender':[{"id": 1,"name": "Male","add" : false}, {"id": 2, "name": "Female","add" : false}]};
		$scope.filtersOBJflag = false;
		$rootScope.$broadcast('dirClearFilters',{});
		$scope.filtersOBJ = {};
	}
	
	$('#filterTabs .active').removeClass('active');
	$('#tabContent .active').removeClass('active');
	$('#tabContent .in').removeClass('in');
	$('#filterTabs #firstTab').addClass('active');
	$('#tabContent #Country').addClass('active');
	$('#tabContent #Country').addClass('in');
	//if($('.multi').hasClass('open')){
		
		//$scope.openGenderFlag = true;
		$rootScope.$broadcast('openDropdown',{},'c');
	//}
	
}

function failedCountries(data){$scope.show_error("Failed to retrieve users data", "Please try again at a later time.");}

function gotFilteringData(data)
{
	
	var tempCountries = data.Countries;
	var tempPlatforms = data.Platforms;
	$scope.filterCountryArr = [];
	$scope.filterPlatformArr = [];
	
	angular.forEach(tempCountries, function (val,key){
		var	temp = {"id": key, "name": val.country, "add" : false};	
		$scope.filterCountryArr.push(temp);
	})
	
	angular.forEach(tempPlatforms, function (val,key){
		var	temp = {"id": key, "name": val.platform, "add" : false};	
		$scope.filterPlatformArr.push(temp);
	})
}			



	$scope.clearFilters = function(){
		$scope.filter = {'age':{},'val':{},'gender':[{"id": 1,"name": "Male","add" : false}, {"id": 2, "name": "Female","add" : false}]};
		
		
			$scope.filtersOBJflag = false;
			$rootScope.$broadcast('dirClearFilters',{});
			$scope.filtersOBJ = {};
		if(Object.keys($scope.filtersOBJ).length != 0){	
			if ($scope.activeTab.dashboardActive && $scope.activeTab.dashboardActive.Graphs)
			{	
				$scope.activeTab.dashboardActive = resetService.resetAllGraphsData($scope.activeTab.dashboardActive);
						
			} 
			$scope.activeTab = {};
			$scope.activeTab.dashboardActive = $filter('filter')($scope.dashboards,{ID: $scope.activeTabId})[0];
			var dashboardData = $filter('filter')($scope.dashboards,{ID: $scope.activeTabId})[0];
			reloadDashboard(dashboardData, $scope.graphVizible, null, null);
		}
	};

$scope.commitCountries = function(){
	
	$scope.abortAjaxCalls();
	
	var tempVal = $('#filterInsurance multiselect-dropdown .objValue').val();
	$scope.filter.gender = JSON.parse(tempVal);
	var genderFlug = 0;
	
	if($scope.filter.gender[0].add && $scope.filter.gender[1].add){
		genderFlug = 0;	
	}else if(!$scope.filter.gender[0].add && !$scope.filter.gender[1].add){
		genderFlug = 0;
	}else {
		genderFlug = 1;
		if($scope.filter.gender[0].add){
			var gender = $scope.filter.gender[0].name;
		}else{
			var gender = $scope.filter.gender[1].name;
		}
	}
	
	if($scope.filter.age.min){
		$scope.filtersOBJ.AgeFrom = $scope.filter.age.min;
	}
	if($scope.filter.age.max){
		$scope.filtersOBJ.AgeTo = $scope.filter.age.max;
	}
	if($scope.filter.val.min){
		$scope.filtersOBJ.MoneyFrom = $scope.filter.val.min;
	}
	if($scope.filter.val.max){
		$scope.filtersOBJ.MoneyTo = $scope.filter.val.max;
	}
	if(gender){
		$scope.filtersOBJ.Gender = gender;
	}
	
	if($scope.filtersOBJ.AgeFrom || $scope.filtersOBJ.AgeTo || $scope.filtersOBJ.MoneyFrom || $scope.filtersOBJ.MoneyTo || $scope.filtersOBJ.Gender){
	
		if ($scope.activeTab.dashboardActive && $scope.activeTab.dashboardActive.Graphs)
		{	
			$scope.activeTab.dashboardActive = resetService.resetAllGraphsData($scope.activeTab.dashboardActive);
					
		} 
		$scope.activeTab = {};
		$scope.activeTab.dashboardActive = $filter('filter')($scope.dashboards,{ID: $scope.activeTabId})[0];
		
		var dashboardData = $filter('filter')($scope.dashboards,{ID: $scope.activeTabId})[0];
				
		if(Object.keys($scope.filtersOBJ).length === 0){
			reloadDashboard(dashboardData, $scope.graphVizible, null, null);
		}else{
			$scope.filtersOBJflag = true;
			reloadDashboard(dashboardData, $scope.graphVizible, $scope.filtersOBJ, null);
		}
	}
}


$scope.getDataBySelectedFilter = function(){
	
	$scope.abortAjaxCalls();
	
	var tempStrC = $('#filterCountryArr multiselect-dropdown .objValue').val();
	var tempStrP = $('#filterPlatformArr multiselect-dropdown .objValue').val();
	var tempObjC = JSON.parse(tempStrC);
	var tempObjP = JSON.parse(tempStrP);
	var filterArrC = [];
	var filterArrP = [];
	
	angular.forEach(tempObjC, function (val,key){
		if(val.add){
			filterArrC.push(val.name);
		} 
	})
	
	angular.forEach(tempObjP, function (val,key){
		if(val.add){
			filterArrP.push(val.name);
		} 
	})
	
	if(filterArrC && filterArrC.length > 0){
		$scope.filtersOBJ.Country = filterArrC.toString();
	}
	if(filterArrP && filterArrP.length > 0){
		$scope.filtersOBJ.Platform = filterArrP.toString();
	}
	
	
	if($scope.activeAppLabel == 'Certua' || $scope.activeAppLabel == 'Insurance'){
		var tempVal = $('#filterInsurance multiselect-dropdown .objValue').val();
		$scope.filter.gender = JSON.parse(tempVal);
		var tempGender = [];
		angular.forEach($scope.filter.gender, function (val,key){
			if(val.add){
				tempGender.push(val.name);
			} 
		})
		
		if($scope.filter.age.min){
			$scope.filtersOBJ.AgeFrom = $scope.filter.age.min;
		}
		if($scope.filter.age.max){
			$scope.filtersOBJ.AgeTo = $scope.filter.age.max;
		}
		if($scope.filter.val.min){
			$scope.filtersOBJ.MoneyFrom = $scope.filter.val.min;
		}
		if($scope.filter.val.max){
			$scope.filtersOBJ.MoneyTo = $scope.filter.val.max;
		}
		if(tempGender && tempGender.length > 0){
			$scope.filtersOBJ.Gender = tempGender.toString();
		}
	}
	
	
	
	
	if($scope.filtersOBJ.Country || $scope.filtersOBJ.Platform || $scope.filtersOBJ.AgeFrom || $scope.filtersOBJ.AgeTo || $scope.filtersOBJ.MoneyFrom || $scope.filtersOBJ.MoneyTo || $scope.filtersOBJ.Gender){
	
		if ($scope.activeTab.dashboardActive && $scope.activeTab.dashboardActive.Graphs)
		{	
			$scope.activeTab.dashboardActive = resetService.resetAllGraphsData($scope.activeTab.dashboardActive);
					
		} 
		$scope.activeTab = {};
		$scope.activeTab.dashboardActive = $filter('filter')($scope.dashboards,{ID: $scope.activeTabId})[0];
		
		var dashboardData = $filter('filter')($scope.dashboards,{ID: $scope.activeTabId})[0];
				
		if(Object.keys($scope.filtersOBJ).length === 0){
			reloadDashboard(dashboardData, $scope.graphVizible, null, null);
		}else{
			$scope.filtersOBJflag = true;
			reloadDashboard(dashboardData, $scope.graphVizible, $scope.filtersOBJ, null);
		}
	}
	
	
}

$scope.multiGraph = {};
$scope.multiGraphMFP1 = {};
$scope.multiGraphMFP2 = {};
$scope.multiGraphMFPflag = false;

$scope.multiFilterFun = function(data,title,title2,e){
	
	if(e == 'MultiChoiceLines' || e == 'MultiChoicePie'){
		for(var i = 0; i < data.chartData.ReportDataTable.Products.length; i++){
			if(data.chartData.ReportDataTable.Products[i].title == title){
				var id = data.chartData.ReportDataTable.Products[i].interaction_id;
				var graphID = data.ReportID;
				
			}
		}
		
		if(e == 'MultiChoicePie'){
			for(var j = 0; j < $scope.PieChart.length; j++){
				if( $scope.PieChart[j].ID == graphID){
					$scope.PieChart[j].responceStatus = -1;
				}
			}
		}
		
		if(id != null){
			var opt;
			angular.forEach($scope.activeTab.dashboardActive.Graphs, function (key)
			{
			if(key.dataGraph && key.dataGraph != null && key.dataGraph != 'undefined'){
				if(key.dataGraph.chartData && key.dataGraph.chartData != null && key.dataGraph.chartData != 'undefined'){
					if(key.dataGraph.ReportID == graphID){
							opt = key.Options;
							
						}
				}
			}
			}) 
			
			if(id == 0){
				var temp =
					{
						"Token": $rootScope.Token,
						"GraphOwnerType": data.GraphOwnerType,
						"ReportID": data.ReportID,
						"GraphType": data.chartData.GraphType
					};
			}else{
				var temp =
					{
						"Token": $rootScope.Token,
						"GraphOwnerType": data.GraphOwnerType,
						"ReportID": data.ReportID,
						"GraphType": data.chartData.GraphType,
						"Choise" : id
					};
			}
		
			$scope.reloadGraphByID(graphID,opt,temp);
			$scope.activeTab.dashboardActive = resetService.resetGraphDataByID($scope.activeTab.dashboardActive,graphID);
			
		}
	}
	if(e == 'MultiChoiceFunnel'){
		for(var i = 0; i < data.chartData.ReportDataTable.Products.length; i++){
			if(data.chartData.ReportDataTable.Products[i].title == title){
				var id = data.chartData.ReportDataTable.Products[i].interaction_id;
				var graphID = data.ReportID;
			}
		}
		for(var j = 0; j < $scope.typeFunnel.length; j++){
			if( $scope.typeFunnel[j].ID == graphID){
				$scope.typeFunnel[j].responceStatus = -1;
			}
		}
		var temp =
					{
						"Token": $rootScope.Token,
						"GraphOwnerType": data.GraphOwnerType,
						"ReportID": data.ReportID,
						"GraphType": data.chartData.GraphType,
						"Choise" : id
					};
		$scope.reloadGraphByID(graphID,opt,temp);
		$scope.activeTab.dashboardActive = resetService.resetGraphDataByID($scope.activeTab.dashboardActive,graphID);
	}
	if(e == 'MultiChoiceFunnel_2'){
			
		for(var i = 0; i < data.chartData.ReportDataTable.Products1.length; i++){
			if(data.chartData.ReportDataTable.Products1[i].title == title){
				var id1 = data.chartData.ReportDataTable.Products1[i].interaction_id;
				var graphID = data.ReportID;
			}
		}
		
		for(var j = 0; j < $scope.typeFunnel.length; j++){
			if( $scope.typeFunnel[j].ID == graphID){
				$scope.typeFunnel[j].responceStatus = -1;
			}
		}
		
		var opt;
			angular.forEach($scope.activeTab.dashboardActive.Graphs, function (key)
			{
			if(key.dataGraph && key.dataGraph != null && key.dataGraph != 'undefined'){
				if(key.dataGraph.chartData && key.dataGraph.chartData != null && key.dataGraph.chartData != 'undefined'){
					if(key.dataGraph.ReportID == graphID){
							opt = key.Options;
							//key.dataGraph.responceStatus = -1;
						}
				}
			}
			}) 
		
		
		for(var i = 0; i < data.chartData.ReportDataTable.Products2.length; i++){
			if(data.chartData.ReportDataTable.Products2[i].title == title2){
				var id2 = data.chartData.ReportDataTable.Products2[i].interaction_id;
			}
		}
		
		if(id1 != null && id2 != null){
			
			$scope.multiGraphMFPflag = true;
			
			var opt;
			angular.forEach($scope.activeTab.dashboardActive.Graphs, function (key)
			{
			if(key.dataGraph && key.dataGraph != null && key.dataGraph != 'undefined'){
				if(key.dataGraph.chartData && key.dataGraph.chartData != null && key.dataGraph.chartData != 'undefined'){
					if(key.dataGraph.ReportID == graphID){
							opt = key.Options;
							key.dataGraph.responceStatus = -1;
						}
				}
			}
			}) 
			
			if(id1 == 0 && id2 == 0){
				$scope.multiGraphMFP1 = {};
				$scope.multiGraphMFP2 = {};
				$scope.multiGraphMFPflag = false;
				var temp =
					{
						"Token": $rootScope.Token,
						"GraphOwnerType": data.GraphOwnerType,
						"ReportID": data.ReportID,
						"GraphType": data.chartData.GraphType
					};
			}else{
				var temp =
					{
						"Token": $rootScope.Token,
						"GraphOwnerType": data.GraphOwnerType,
						"ReportID": data.ReportID,
						"GraphType": data.chartData.GraphType,
						"Choise" : id1,
						"Choise2" : id2,
					};
			}
		
			$scope.reloadGraphByID(graphID,opt,temp);
			$scope.activeTab.dashboardActive = resetService.resetGraphDataByID($scope.activeTab.dashboardActive,graphID);
		}
		
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


	
	$scope.vizibleTabFlugFun = function (e)
	{	
		
		if(e == false){
			$scope.tutorialInvokeEvent("tutorial_advancedTab");
			angular.forEach($scope.activeTab.dashboardActive.Graphs, function (key)
					{
			if(key.GraphType == "BarChart" && key.dataGraph && key.dataGraph != null && key.dataGraph != 'undefined'){
						if(key.dataGraph.chartData && key.dataGraph.chartData != null && key.dataGraph.chartData != 'undefined'){
							if(key.dataGraph.chartData.ReportDataTable && key.dataGraph.chartData.ReportDataTable != null && key.dataGraph.chartData.ReportDataTable != 'undefined'){
			$scope.storeToCache('barsData', $scope.activeTab.dashboardActive);
			
			var interv = $interval(function ()
			{

				if ($scope.loadFromStorage('barsData'))
				{
					$scope.activeTab.dashboardActive = resetService.resetBarGraphData($scope.activeTab.dashboardActive);
					$timeout(function () {
						$scope.activeTab.dashboardActive = $scope.loadFromStorage('barsData');
					}, 3000);
					$interval.cancel(interv);
					
				}
			}, 300);
			}}}
					})
		}
		
		$scope.tabVizible = e;
	}
	$scope.setAttrFun = function ($event)
	{
		var el = angular.element('.compcounterAttr');
		el.prop('compcounter', '');
	}

	if ($rootScope.appsArr.length == 0)
	{
		$scope.$parent.applicationsStart();
	}

	$scope.getCountryName = function (e, countryCode)
	{
		if (e == 'color')
		{
			if (CountryConst.isoCountries.hasOwnProperty(countryCode))
			{
				return CountryConst.isoCountries[countryCode];
			}
			else
			{
				return countryCode;
			}
		}
		else
		{
			return CountryConst.latlong[id][e];
		}

	}

	$scope.dashboardStart = function ()
	{
		var ENABLE_ANIMATIONS = false;
		var ANIMATION_TIME = 0.8;
		var forceAm = false;
		var amPrefix = "am";
		$scope.removeEmptyGraphs = false;
		//var currBarInd = 0;
		var urlParameters = $location.search();
		$('.loading-page').hide();
		if (urlParameters.Token != null)
		{
			
			$rootScope.Token = urlParameters.Token;

			$scope.loadExternalServicesList();
			$scope.relaodInappPricingData();
			$scope.GetGameConfigAdmin();
			$scope.myPermissionsFun();
			$scope.permissionManagerFun();
			
			//$scope.GetSequenceJunctionActionsFun();
			
			//if($rootScope.dashboardView.active == 7){
				if(urlParameters.settings != null){
					//$scope.settingsActiveTab = urlParameters.settings;
					$rootScope.dashboardView.active = 7;
					//$location.search('settings', urlParameters.settings);
					$rootScope.settingsActiveTab = urlParameters.settings;
				}/* else{
					$scope.settingsActiveTab = 'apis';
				} */
				
			//}
		}
		
		var dashboardIDs = [];
		var graphDataByID = {};
		var DESIGN_GLOBALS =
		{
			uiColorBlue: "#6ec4c8"
		};
		DESIGN_GLOBALS.counterGroupOptions =
		{
			animate: 1e3,
			size: 75,
			lineWidth: 3,
			barColor: DESIGN_GLOBALS.uiColorBluetpl
		};

		if (urlParameters == null || urlParameters.Token == null)
		{
			$scope.show_error("Something went wrong, going back to Applications webpage");
			$scope.gotoApps();
		};

		if (!String.prototype.startsWith)
		{
			String.prototype.startsWith = function (searchString, position)
			{
				position = position || 0;
				return this.indexOf(searchString, position) === position;
			};
		}

		$scope.curveAPI('GetGameDashboards',
		{
			'Token': $rootScope.Token
		}, getGameDashboardsCallback, getGameDashboardsError);
		
	}

	function getGameDashboardsError(data)
	{
		$scope.show_error('Error fetching dashboards!', data, true);
		
	}

	function getGameDashboardsCallback(data)
	{
		
		$('.loading-page').hide();
		$scope.TopPanelStart();
		$scope.dashboards = data.Dashboards;
		$scope.insights = data.Insights;

		var tplSrc = "tplSrc";
		var imgSrc = "imgSrc";
		var insightsObj = "insightsObj";
		var descrShow = "descrShow";
		
		$scope.homePage = {'id' : '', 'name' : ''};
		$scope.homePage.id = $scope.dashboards[0].ID;
		$scope.homePage.name = $scope.dashboards[0].Name;
		
		$.each($scope.dashboards, function (key, val)
		{
			if ((val.Graphs.length > 0 && val.DashboardType != "Status") || val.DashboardType == "Status")
			{
				var temp = [];
				$.each($scope.insights, function (i, j)
				{
					if (val.Name == j.dashboard_name)
					{
						var obj = JSON.parse(j.short_text);
						j.short_text = obj;
						temp.push(j);
					}
				}
				);

				val.insightsObj = temp;

				if (val.Parent == 'Advanced')
				{
					if(val.Name == 'Single Customer'){
						$scope.SingleCustomerd = val.ID;
					}
					$scope.parentAdvanced = true;
				}
				$.each(val.Graphs, function (k, v)
				{
					var tplName = v.GraphType;

					if (v.GraphType != 'MultichoiceTrackingChart' && v.GraphType != 'CounterChart' && v.GraphType != 'SparkLine' && v.GraphType != 'CounterGroup' && v.GraphType != 'InsightHolder' && v.GraphType != 'userjourney' && v.GraphType != 'ComparableCounter')
					{
						tplName = 'DrawChart';
					}
					v[tplSrc] = 'Templates/Dashboard/GraphsTpl/' + tplName + '.html';

					if (v.GraphType != 'amMap' && v.GraphType != 'ComparableCounter' && v.GraphType != 'PieChart' && v.GraphType != 'CounterChart')
					{
						v[imgSrc] = '../Images/Graph/chart.png';
					}
					else
					{
						v[imgSrc] = '../Images/Graph/' + v.GraphType + '.png';
					}
					if (v.GraphType == 'ComparableCounter')
					{
						v[descrShow] = false;
					}
				}
				);
			}
			
		}
		);
	
	if($scope.dashboardView.active != 7){
		for(var i = 0; i < data.Dashboards.length; i++){
			
			if((data.Dashboards[i].Visible == 0 && data.Dashboards[i].DashboardType == "Status" && $scope.activeAppLabel != 'Certua') || data.Dashboards[i].Visible == 1){
				$scope.setTabFun(data.Dashboards[i].ID, data.Dashboards[i].Name);
				$scope.activeTabId = data.Dashboards[i].ID;
				break;
			}
		}
	}	

		if ($scope.dashboards == null)
		{
			console.log("ERROR: dashboards is null", data);
			return;
		}

		dashboardTypeByID = {};
		for (var i = 0; i < $scope.dashboards.length; i++)
		{
			var dt = $scope.dashboards[i]
				dashboardTypeByID[dt.ID] = dt.DashboardType;
		}

		fillSegmentsAnalysisData();
		
		if($rootScope.Token == 'CURV-FXEH1VYSXGW')
			$scope.tutorialInvokeEvent("ftue1Start1Key");
        	$scope.insightsAll = [];
		
		$.each($scope.insights, function (i, j){
				if (j.done == 0)
				{
					if(j.action == 'Audience' || j.action == 'Interact' ){
						
						$scope.insightsAll.push(j);
					}
					
				}
			});
			
			
	}

	/********************************************** TABS / GET DATA GRAPHS ********************************************/
	
	$scope.activeTab = {};

	

	$scope.settingsFun = function (){
		
		$("#dashboards_nav li").each(function(){
			if ($(this).hasClass('active'))
			$(this).removeClass('active');
		});
		
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();
		$rootScope.dashboardView.active = 7;
 	        if($scope.currTutorial.className == "#ftue6Start6"){
			$timeout(function () {
				
				$scope.tutorialInvokeEvent("ftue6Start6");
				$scope.currTutorial.className = "";				
			}, 100);
			
		}
        	$location.search('settings', 'apis');
		$rootScope.settingsActiveTab = 'apis';
		//$scope.routerPage('settings','tab','apis',$rootScope.Token);
		
	}
	
	$scope.setSettingsTab = function(tab){
		
		$location.search('settings', tab);
		$rootScope.settingsActiveTab = tab;
	}
	
	$scope.myPermissionsFun = function ()
	{
		//$rootScope.dashboardView.active = 3;
		$scope.showPermissions();
	}
	/***************** status page ********************/

	$scope.updateWelcomeText = function ()
	{
		/*
		1. add id="kpi-welcome-div" to:

		<div class="kpi-welcome" id="kpi-welcome-div">

		2. add to function 'cacheLoginData(data)' in 'login.global.js':

		storeToCache("userFirstName", data.userData.FirstName);
		storeToCache("userLastName", data.userData.LastName);

		 */
		var userName = $scope.loadFromStorage('userFirstName');
		var firstTime = $scope.loadFromStorage('firstLogin');

		var currentHour = new Date().getHours();
		var welcomeOptions = [
			"Welcome back ",
			"Ahoi ",
			"Good to see you again ",
			"Good morning ",
			"Hello ",
			"Ready to get growing? ",
		];

		var welcomeText = "";

		if (firstTime)
		{
			welcomeText += "Welcome aboard ";
		}
		else if (currentHour > 6 && currentHour < 10)
		{
			welcomeText += "Good morning ";
		}
		else
		{
			var rand = Math.floor(Math.random() * 6);
			welcomeText += welcomeOptions[rand];
		}
		if (userName != null && userName != 'undefined')

			welcomeText += userName;

		$scope.statusWelcomeTitle = welcomeText;
	}

	$scope.statusRemoveGraph = function (dashboard, graph)
	{

		for (var i = 0; i < $scope.activeTab.dashboardActive.Graphs.length; i++)
		{

			if ($scope.activeTab.dashboardActive.Graphs[i].ID == graph)
			{

				$scope.activeTab.dashboardActive.Graphs[i].Visible = '0';
				break;
			}
		}

		var data =
		[{
			'DashboardId': ''+dashboard+'',
			'GraphId': ''+graph+'',
			'Visible': 'false'
		}];

		$scope.curveAPI('ShowHideGraph',
		{
			'Token': $rootScope.Token,
			'Data': JSON.stringify(data)
		}, $scope.statusRemoveGraphCallback, $scope.statusRemoveGraphError);
	}

	$scope.statusRemoveGraphCallback = function (data)
	{

		console.log('success')

	}

	$scope.statusRemoveGraphError = function (data)
	{
		console.log(data)
	}

	$scope.statusAddGraph = function (dashboard, graph)
	{

		$('.widget-chooser-modal').modal('hide');

		for (var i = 0; i < $scope.activeTab.dashboardActive.Graphs.length; i++)
		{

			if ($scope.activeTab.dashboardActive.Graphs[i].ID == graph)
			{

				$scope.activeTab.dashboardActive.Graphs[i].Visible = '1';
				break;
			}
		}

		var data =
		[{
			'DashboardId': ''+dashboard+'',
			'GraphId':''+graph+'',
			'Visible': 'true'
		}]

		$scope.curveAPI('ShowHideGraph',
		{
			'Token': $rootScope.Token,
			'Data': JSON.stringify(data)
		}, $scope.statusAddGraphCallback, $scope.statusAddGraphError);
	}

	$scope.statusAddGraphCallback = function (data)
	{
		console.log('success')
	}

	$scope.statusAddGraphError = function (data)
	{
		console.log(data)
	}

	var pick_of_the_day_callbacks = 0;
	var current_game_has_data = false;
	$scope.getPickOfTheDay = function ()
	{
		current_game_has_data = false;
		pick_of_the_day_callbacks = 0;
		$scope.curveAPI('GetPickOfTheDay',
		{
			'Token': $rootScope.Token
		}, $scope.GetPickOfTheDayCallback, $scope.GetPickOfTheDayError);
		
		$scope.curveAPI('GetIsDataExists',
		{
			'Token': $rootScope.Token
		}, $scope.GetHasData, $scope.GetHasDataError, null, null, true);

	}

	$scope.GetHasDataError = function (data)
	{
		pick_of_the_day_callbacks++;
		current_game_has_data = true;
		
		if(pick_of_the_day_callbacks >= 2)
			$scope.OnDataReadyPickOfTheDay();
	}
	
	$scope.GetHasData = function (data)
	{
		pick_of_the_day_callbacks++;
		current_game_has_data = data;
		
		if(pick_of_the_day_callbacks >= 2)
			$scope.OnDataReadyPickOfTheDay();
	};
	
	$scope.GetPickOfTheDayCallback = function (data)
	{	
		pick_of_the_day_callbacks++;
		$scope.pickOfTheDayViewLoad = false;
		$scope.pickOfTheDayView = [];
		$scope.pickOfTheDayArr = data;
		
		var userTrial = $scope.loadFromCache("trial");
		if(userTrial != null && userTrial != '' && userTrial)
		{

			var expiration = $scope.loadFromCache("expiration");
			var exp_date = new Date(expiration);
			
			var month = "";
			var m = exp_date.getMonth()+1;
			if(m == 1)
				month = "jan";
			if(m == 2)
				month = "feb";
			if(m == 3)
				month = "mar";
			if(m == 4)
				month = "apr";
			if(m == 5)
				month = "may";
			if(m == 6)
				month = "jun";
			if(m == 7)
				month = "jul";
			if(m == 8)
				month = "aug";
			if(m == 9)
				month = "sep";
			if(m == 10)
				month = "oct";
			if(m == 11)
				month = "nov";
			if(m == 12)
				month = "dec";
			
			var exp_str = "" + exp_date.getDate() + " " +  month + " " + exp_date.getFullYear();
			
			$scope.pickOfTheDayArr.splice(0,0,{
					"dashboard_name": "TrialPeriod",
					"dashboard_category" : "Trial",
					"isgood": "1",
					"action_call": "Contact",
					"done":"0","score":"100","seen":"true",
					"outcome_text": "This is a trial account",
					"long_text": "This is a trial version of Curve. not all features are open and MAU is limited to 5000. To get full access to all featurs, customizations and support please contact our sales department.",
					"action": "EmailSupport",
					"short_text": "{\"text\":\"This account will expire in " + exp_str + ". To upgrade your account, please contact us!\", \"label\":\"info\"}"
				});
				
			if($rootScope.appsArr.length < 2)
			{
				$scope.pickOfTheDayArr.splice(1,0,{
						"dashboard_name": "NewApp",
						"action_call": "+ New App",
						"dashboard_category" : "New App",
						"isgood": "1",
						"outcome_text": "Create New Application",
						"done": 0,
						"seen": false,
						"long_text": "Lets add a new application so you can enjoy everything Curve has to offer!",
						"action": "CreateNewApp",
						"short_text": "{\"text\":\"Lets add a new application so you can enjoy everything Curve has to offer!\", \"label\":\"info\"}"
					});
			}
		}
		
		if(current_game_has_data && $rootScope.Token != 'CURV-FXEH1VYSXGW')
		{
			var IntegrateSDKFlug = true;
			if($scope.pickOfTheDayArr.length > 0){
				for(var i = 0; i < $scope.pickOfTheDayArr.length ;i ++){
					if($scope.pickOfTheDayArr[i].dashboard_name == "IntegrateSDK"){
						IntegrateSDKFlug = false;
					}
				}
			}
			if(IntegrateSDKFlug){
				
				$scope.pickOfTheDayArr.splice(0,0,{
					"dashboard_name": "IntegrateSDK",
					"dashboard_category" : "Integrate",
					"isgood": "1",
					"action_call": "Get Instructions",
					"done":"0","score":"100","seen":"true",
					"outcome_text": "Integrate Our SDK",
					"long_text": "Integrating our SDK is as easy as 3 lines of code! view our easy to use instructions in order to get data flowing into your dashboards.",
					"action": "IntegrateSDK",
					"short_text": "{\"text\":\"Oops, we see you dont have any events reported yet! See how to implement our easy to use SDK.\", \"label\":\"info\"}"
				});
			}
			
		}
		
		if(pick_of_the_day_callbacks >= 2){
			$scope.OnDataReadyPickOfTheDay();	
		}
			
	};

	$scope.OnDataReadyPickOfTheDay = function()
	{
		
		if($scope.pickOfTheDayArr.length > 0)
		{
			var descrShow = 'descrShow';
			
			playAllNotes(0);
			
			function playAllNotes(index)
			{
				if ($scope.pickOfTheDayArr.length > index)
				{
					$timeout(function ()
					{
						
						var obj = JSON.parse($scope.pickOfTheDayArr[index].short_text);
						$scope.pickOfTheDayArr[index].short_text = obj;
						$scope.pickOfTheDayArr[index].descrShow = false;
						$scope.pickOfTheDayView.push($scope.pickOfTheDayArr[index]);
						playAllNotes(++index);
					}, 300);
				}
			}
		}
	}
	
	$scope.refreshSstatusPicksArr = function ()
	{
		$scope.pickOfTheDayView = [];
		pick_of_the_day_callbacks = 0;
	}

	$scope.GetPickOfTheDayError = function (data)  {}

	$scope.descrShowFun = function (i, e)
	{

		$scope.pickOfTheDayArr[i].descrShow = e;
	}

	$scope.getVisFalse = function ()
	{
		if ($scope.funVisFalse == false)
		{
			//$scope.graphVizible = false;
			var dashboardData = $filter('filter')($scope.dashboards,{ID: $scope.activeTabId})[0];
			reloadDashboard(dashboardData, $scope.graphVizible, null, null);
			//$scope.funVisFalse = true;
		}
	}
	/***************** end status page ********************/

	/***************** insights tab ********************/

	$scope.SetInsightFeedbackFun = function (id,e,smart)
	{
		var data =
		{
			'InsightId': id,
			"Feedback": e
		};
		
		if(smart != null && smart != 'undefined' && smart != '' ){
			
			for (var i = 0; i < $scope.insightsAll.length; i++)
			{

				if ($scope.insightsAll[i].id == id)
				{
					
					$scope.insightsAll[i].feedback = e;

					break;
				}
			}
			
		}else{
			
		for (var i = 0; i < $scope.activeTab.dashboardActive.insightsObj.length; i++)
		{

			if ($scope.activeTab.dashboardActive.insightsObj[i].id == id)
			{
				
				$scope.activeTab.dashboardActive.insightsObj[i].feedback = e;

				break;
			}
		}
		}
		 
		$scope.curveAPI('SetInsightFeedback',
		{
			'Token': $rootScope.Token,
			'Data': JSON.stringify(data)
		}, $scope.SetInsightFeedbackCallback, $scope.SetInsightFeedbackError);
	}

	$scope.SetInsightFeedbackCallback = function (data)
	{

	}

	$scope.SetInsightFeedbackError = function (data)  {}

	$scope.compDescShowFun = function (id, e)
	{

		for (var i = 0; i < $scope.activeTab.dashboardActive.Graphs.length; i++)
		{

			if ($scope.activeTab.dashboardActive.Graphs[i].ID == id)
			{

				$scope.activeTab.dashboardActive.Graphs[i].descrShow = e;
				break;
			}
		}
	}
	/***************** end insights tab ********************/

		
	
	$scope.setTabFunRRR = function (id, name)
	{	
		$scope.interaction = {'interaction_id' : id, 'interaction_name' : name};
		$scope.abortAjaxCalls();
		$scope.setTabFun(1, 'Center');
		$('#engmCenterTab').tab('show');
	};

	
	function setNewDB(){
		$scope.activeTab = {};
		$scope.activeTab.dashboardActive = $filter('filter')($scope.dashboards,{ID: $scope.activeTabId})[0];
		for(var i = 0; i < $scope.activeTab.dashboardActive.Graphs.length; i ++){
			$scope.activeTab.dashboardActive.Graphs[i].responceStatus = -1;
		}
		
		var dashboardData = $filter('filter')($scope.dashboards,{ID: $scope.activeTabId})[0];
		return dashboardData;
	}
	
	
	
	$scope.setTabFun = function (tabID, name)
	{	
		$scope.typeCounterChart = [];
		$scope.typeComparableCounter = [];
		$scope.typeFunnel = [];
		$scope.PieChart = [];
		$scope.plotChart = [];
		
		$( "#chartdiv555" ).remove();
		$( "#chartdiv555Div" ).remove();
		$scope.dateStatus = {};
		
		if (name != 'User Journey')
			{
				$scope.dateStatus.singleDate = false;
				
			}
			else
			{
				$scope.dateStatus.singleDate = true;
				
			}
		$scope.abortAjaxCalls();
		$scope.activeTabId = tabID;
		$scope.activeTabName = name;

		$("#dashboards_nav li").each(function(){
			if ($(this).hasClass($scope.activeTabName)){
				
				$(this).addClass('active');
			}else{
				$(this).removeClass('active');
			}
		});
		
        $location.search('settings', null)

		if($scope.currTutorial.className == "#ftue2Start2" && name == 'Center'){
			
			$timeout(function () {
				
				$scope.tutorialInvokeEvent("ftue2Start2");
				$scope.currTutorial.className = "";				
			}, 100);
			
		}
		
		if($scope.currTutorial.className == "#ftue3Start3" && name == 'Status'){
			
			$timeout(function () {
				
				$scope.tutorialInvokeEvent("ftue3Start3");
				$scope.currTutorial.className = "";				
			}, 100);
			
		}
		
		if($scope.currTutorial.className == "#ftue4Start4" && name == 'Traffic'){
			
		
			$timeout(function () {
				$scope.tutorialInvokeEvent("ftue4Start4");
				$scope.currTutorial.className = "";				
			}, 100);
			
		}


		
		if ($scope.activeTab.dashboardActive && $scope.activeTab.dashboardActive.Graphs)
			{	
		
				$scope.activeTab.dashboardActive = resetService.resetAllGraphsData($scope.activeTab.dashboardActive);
					if (name != 'Center' && name != 'Cohorts' && name != 'ml_segmentation')
					var dashboardData = setNewDB();			
			}else{
				if (name != 'Center' && name != 'Cohorts' && name != 'ml_segmentation')
				 var dashboardData = setNewDB();	
			}
			
	if(dashboardData){		
		
		if(name == 'AlgoSOD'){

$timeout(function () {


					var chart = AmCharts.makeChart("chartdiv555", {
    "type": "radar",
    "theme": "light",
   	"dataProvider": [ 
  {
    "subject": "Social (Marketing)",
    "effect": 0.5
  },
  {
    "subject": "TV (Marketing)",
    "effect": 2
  }, {
    "subject": "Cancellation (Delivery)",
    "effect": 1.5
  },
  {
    "subject": "Changed (Delivery)",
    "effect": 2.5
  }, {
    "subject": "Chat  (Support)",
    "effect": 3.9
  },
  {
    "subject": "Email (Support)",
    "effect": 2.5
  }, {
    "subject": "Dynamic UX  (On-Site)",
    "effect": 5
  },
  {
    "subject": "Cash register  (On-Site)",
    "effect": 5
  }
  ],
  "valueAxes": [ {
    "axisTitleOffset": 20,
    "minimum": 0,
    "axisAlpha": 0.15
  } ],
  "startDuration": 2,
  "graphs": [ {
    "balloonText": "[[value]] effect",
    "bullet": "round",
    "lineThickness": 2,
    "valueField": "effect"
  } ],
  "categoryField": "subject",
  "export": {
    "enabled": true
  },
  "startDuration" : 0
  
});	
			}, 500);

			



		}
		if (name != 'Center' && name != 'Cohorts' && name != 'ml_segmentation' && name != 'Single Customer')
		{ 
			
				$scope.dateStatus.singleDate = false;
				//$scope.rangeDate = true;

			
			$scope.funVisFalse = false;

			if (name != 'Status')
			{
				$scope.refreshSstatusPicksArr();
				$rootScope.dashboardView.active = 0;
				if ($scope.activeTab.dashboardActive.DashboardType != 'default')
				{	
					$rootScope.dashboardView.tabs = false;
				}
				else
				{
					$rootScope.dashboardView.tabs = true;
				}

			}
			else
			{
				if (name == 'Status')
				{
					$scope.refreshSstatusPicksArr();
					$scope.pickOfTheDayViewLoad = true;
					$rootScope.dashboardView.active = 4;
					$scope.getPickOfTheDay();
					$scope.updateWelcomeText();
				}

			}
			//alert('get new data')
			reloadDashboard(dashboardData, $scope.graphVizible, null, null);
			
		}

	}
			
			
				//$scope.dateStatus.singleDate = false;
				//$scope.rangeDate = true;

			if (name == 'Center')
			{	
				
				//$scope.activeTab.dashboardActive = '';
				//$scope.activeTab.dashboardActive.DashboardType = 'default';
				//$scope.abortAjaxCalls();
				
				$scope.CohortDescriptor();
				$scope.getInterStatus();
				$('.loading-dashboard').css("display", "block");
				$scope.getGameActiveCampaignsFun();
				$timeout(function () {
					$rootScope.dashboardView.active = 1;
				}, 50);
				
				if ($scope.activeTabId == 1)
				{

					var promise = $interval(function ()
						{

							if ($('#view-reports').is(':visible'))
							{
								$('[data-toggle="tab"][href="#Reports"]').tab('show');
								$interval.cancel(promise);
								$('.loading-dashboard').css("display", "none");
							}
						}, 300);

					$('body').scrollTop(0);
				}
				else
				{
					$('.loading-dashboard').css("display", "none");
				}

			}
			if (name == 'Cohorts')
			{	
				
				$rootScope.dashboardView.active = 5;
				$('.loading-dashboard').css("display", "none");
			}
			if (name == 'ml_segmentation')
			{
				$rootScope.dashboardView.active = 6;
				$('.loading-dashboard').css("display", "none");
			}
			if(name == 'Single Customer'){
					
				var Titles = 'Titles';
				
				for(var i = 0; i < $scope.activeTab.dashboardActive.Graphs.length; i ++){
					if($scope.activeTab.dashboardActive.Graphs[i].GraphType == 'UserCardEntries' && $scope.activeTab.dashboardActive.Graphs[i].Options != ''){
						var temp = JSON.parse($scope.activeTab.dashboardActive.Graphs[i].Options);
						
						var TitleData = [];
						for(var j = 0;j < temp.Titles.length;j ++){
							var temp2 = {"title":temp.Titles[j],"data":""};
							TitleData.push(temp2)
						}
						$scope.activeTab.dashboardActive.Graphs[i].Titles = TitleData;
					}
				}
				
				$scope.createObjForView($scope.activeTab.dashboardActive,0);
			}
		
}
	
	$scope.UserCohortsCallback = function(data){
		
		$scope.UserCohortsList = data;
	}
	
	$scope.reloadSingleTab = function (userID)
	{
		$scope.typeCounterChart = [];
		$scope.typeComparableCounter = [];
		$scope.typeFunnel = [];
		$scope.PieChart = [];
		$scope.plotChart = [];
		
		if ($scope.activeTab.dashboardActive && $scope.activeTab.dashboardActive.Graphs)
			{	
				$scope.activeTab.dashboardActive = resetService.resetAllGraphsData($scope.activeTab.dashboardActive);
						
			} 
			$scope.activeTab = {};
			$scope.activeTab.dashboardActive = $filter('filter')($scope.dashboards,{ID: $scope.activeTabId})[0];
			
		var dashboardData = $filter('filter')($scope.dashboards,{ID: $scope.activeTabId})[0];
		$scope.SingleCustomerData = true;
		reloadDashboard(dashboardData, $scope.graphVizible, null, userID);
		
		//http://localhost:8080/Curve/GetUserCohorts?Token=CURV-FXEH1VYSXGW&UserId=9917fae4-e595-4550-90c6-ef223064f3d1
		var d = {
					"Token": $rootScope.Token,
					"UserId": userID
				};
		$scope.curveAPI('GetUserCohorts', d, $scope.UserCohortsCallback);
	}
	
	$scope.reloadCurrentTab = function ()
	{
		$scope.typeCounterChart = [];
		$scope.typeComparableCounter = [];
		$scope.typeFunnel = [];
		$scope.PieChart = [];
		$scope.plotChart = [];
		
		if ($scope.activeTab.dashboardActive && $scope.activeTab.dashboardActive.Graphs)
			{	
				$scope.activeTab.dashboardActive = resetService.resetAllGraphsData($scope.activeTab.dashboardActive);
						
			} 
			$scope.activeTab = {};
			$scope.activeTab.dashboardActive = $filter('filter')($scope.dashboards,{ID: $scope.activeTabId})[0];
			
			
		var dashboardData = $filter('filter')($scope.dashboards,{ID: $scope.activeTabId})[0];
		reloadDashboard(dashboardData, $scope.graphVizible, null, null);
	}
	
/* 	$scope.reloadAfterCreatedFunnel = function (){
		var dashboardData = $filter('filter')($scope.dashboards,{ID: $scope.activeTabId})[0];
		reloadDashboard(dashboardData, $scope.graphVizible, null, null);
	} */
	
/*********************************************    add / remove graph **************************/	
	
	$scope.modalAllGraphFun = function()
	{
		$scope.dashboardGraphAll = false;
		var tempAll  = {};	
		tempAll = $filter('filter')($scope.dashboards,{ID: $scope.activeTabId})[0];
		var dynamicGraph = angular.copy(tempAll.Graphs);
		$scope.AddRemove = {'tempStatusGraph' : []};
		var tempStatusGraph = 'tempStatusGraph';
		for(i = 0; i < dynamicGraph.length; i ++){
			if(dynamicGraph[i].graphOwnerType == 'SystemGenerated' && dynamicGraph[i].GraphType != 'CounterChart' && dynamicGraph[i].GraphType != 'ComparableCounter'){
				$scope.AddRemove.tempStatusGraph.push(dynamicGraph[i]);
			}
		}
		
	}
	
	$scope.checkAllGraph = function(e)
	{
		
		var t;
		if(e){
			t = '0';
			$scope.dashboardGraphAll = false;
		}else{
			t = '1';
			$scope.dashboardGraphAll = true;
		}
		
		
		for(var i = 0; i < $scope.AddRemove.tempStatusGraph.length; i ++){
			$scope.AddRemove.tempStatusGraph[i].Visible = t;
		}
	}
	
	$scope.checkGraph = function(dashboardID,graphID)
	{
		
			if($scope.activeTab.dashboardActive.ID == dashboardID){
				for(var i = 0; i < $scope.AddRemove.tempStatusGraph.length; i ++){
					if($scope.AddRemove.tempStatusGraph[i].ID == graphID){
						
						if($scope.AddRemove.tempStatusGraph[i].Visible == '0'){
							$scope.AddRemove.tempStatusGraph[i].Visible = '1';
						}else{
							$scope.AddRemove.tempStatusGraph[i].Visible = '0';
						}
						
					}
				}
			}
			
		
	}
	
	$scope.reloadCurrentTabGrahpList = function(){
		$('.loading-page').show();
		var data = [];
		
		for(var i = 0; i < $scope.AddRemove.tempStatusGraph.length; i ++){
			var d =
			[{
				'DashboardId': ''+$scope.activeTab.dashboardActive.ID+'',
				'GraphId': ''+$scope.AddRemove.tempStatusGraph[i].ID+'',
				'Visible': $scope.AddRemove.tempStatusGraph[i].Visible
			}]
			data.push(d);
		}
		
		$scope.curveAPI('ShowHideGraph',
		{
			'Token': $rootScope.Token,
			'Data': JSON.stringify(data)
		}, ShowHideGraphSuccess, ShowHideGraphError);
	}
	
	function ShowHideGraphSuccess(data){
		
		
				for(var i = 0; i < $scope.AddRemove.tempStatusGraph.length; i ++){
					for(var j = 0;j < $scope.activeTab.dashboardActive.Graphs.length; j ++){
						if($scope.AddRemove.tempStatusGraph[i].ID ==  $scope.activeTab.dashboardActive.Graphs[j].ID){
							$scope.activeTab.dashboardActive.Graphs[j].Visible = $scope.AddRemove.tempStatusGraph[i].Visible;
							break;
						}
					}
				}
		
		reloadDashboard($scope.activeTab.dashboardActive, $scope.graphVizible, null, null); 
		$('.loading-page').hide();
	}
	function ShowHideGraphError(data){console.log(data);$('.loading-page').hide();}
	
/*********************************************  end  add / remove graph **************************/	
	$scope.createObjForView = function(arr,e){
		
		var l = arr.Graphs.length;
		for (var i = 0; i < l; i++)
		{ 

			if(arr.Graphs[i].Visible == '1'){
				var temp = arr.Graphs[i];
				
				if(temp.GraphType == 'CounterChart'){
					
					$scope.typeCounterChart.push(temp)
					
				}
				
				if(temp.GraphType == 'ComparableCounter'){
					
					$scope.typeComparableCounter.push(temp)
					
				}
				
				if(temp.GraphType == 'BarChart' || temp.GraphType == 'amMap' || temp.GraphType == 'TrackingChart' || temp.GraphType == 'BasicChart'  || temp.GraphType == 'SmallTable' || temp.GraphType == 'MultiChoiceLines' ){
					
					$scope.plotChart.push(temp)
					
				}
				
				if(temp.GraphType == 'PieChart' || temp.GraphType == 'MultiChoicePie'){
					var responceStatus = 'responceStatus';
					temp.responceStatus = e;
					$scope.PieChart.push(temp)
				}
				
				if(temp.GraphType == 'Funnel' || temp.GraphType == 'MultiChoiceFunnel' || temp.GraphType == 'MultiChoiceFunnel_2'){
					var responceStatus = 'responceStatus';
					temp.responceStatus = e;
					$scope.typeFunnel.push(temp)
				}
				
			}
		}
		
	}
	
	function reloadDashboard(graphsData, v, filter, userID)
	{
		$scope.createObjForView(graphsData,-1);
		
		if (graphsData && graphsData.Name)
		{

			if (graphsData == null)
			{
				return;
			}		
			
			var l = graphsData.Graphs.length;
			for (var i = 0; i < l; i++)
			{	
				var flagData = 'flagData';
				graphsData.Graphs[i].flagData = false;
				var item = graphsData.Graphs[i];
				var curGraphID = '#chart_' + item.graphOwnerType + '_' + graphsData.ID + '_' + item.ID;
				if (item.GraphType == 'MultichoiceTrackingChart')
				{
					$scope.MultiChTrcChart = true;
					$(curGraphID)[0].innerHTML = '<div style="background: transparent url(&quot;Images/AjaxLoader.gif&quot;) no-repeat scroll center center; width: 100%; height: 100%;"></div>';
				}
				if (item.GraphType == 'SummaryTable')
				{
					$(curGraphID + "_loading")[0].style.display = "";
					$(curGraphID)[0].style.display = "none";
				}
				
				if (item.Visible == v)
				{
					$scope.callID = graphsData.ID + item.ID + item.GraphType;

					var d =
					{
						"Token": $rootScope.Token,
						"GraphOwnerType": item.graphOwnerType,
						"ReportID": item.ID,
						"GraphType": item.GraphType
					};
					if(filter != null && filter != 'all'){
						
						angular.forEach(filter, function (val,key) {
							d[key] = val;
						});
		
					}
					if(Object.keys($scope.filtersOBJ).length != 0){
						
						angular.forEach($scope.filtersOBJ, function (val,key) {
							d[key] = val;
						});

					}
					if(userID != null && userID != ''){
						d['UserId'] = userID;
					}
					
					var datepickerid = $scope.dateStatus.singleDate ? '#dashboard-report-single-date' : '#dashboard-report-range';
					if ($(datepickerid).data('daterangepicker'))
					{
						var data = $scope.dateStatus.singleDate ? $scope.getAPIGlobalParamsSingle(d, $(datepickerid).data('daterangepicker')) : $scope.getAPIGlobalParams(d, $(datepickerid).data('daterangepicker'));

					}else{ 
						$scope.dateJson = dateService.getDate($scope.dateStatus.singleDate);
						var data = isUserJourneyDashboard(graphsData.ID) ? $scope.getAPIGlobalParamsSingle(d, $scope.dateJson) : $scope.getAPIGlobalParams(d, $scope.dateJson);
					}
					
						graphData(data,item.Options);
					
				}
				
			}

		}
		/* else
		{
			$scope.singleDate = false;
			//$scope.rangeDate = true;
		} */

	}
	
	$scope.reloadGraphByID = function(id,options,filter)
	{	
		
			var d = filter;
			
			var datepickerid = $scope.dateStatus.singleDate ? '#dashboard-report-single-date' : '#dashboard-report-range';
			if ($(datepickerid).data('daterangepicker'))
			{
				var data = $scope.dateStatus.singleDate ? $scope.getAPIGlobalParamsSingle(d, $(datepickerid).data('daterangepicker')) : $scope.getAPIGlobalParams(d, $(datepickerid).data('daterangepicker'));

			}else{
				$scope.dateJson = dateService.getDate($scope.dateStatus.singleDate);
				var data = isUserJourneyDashboard(graphsData.ID) ? $scope.getAPIGlobalParamsSingle(d, $scope.dateJson) : $scope.getAPIGlobalParams(d, $scope.dateJson);
			} 
		
		graphData(data,options);
	}


	function graphData(data,opt)
	{

		var call = $scope.curveAPI('GetGameGraphData', data, getGraphDataCallback);


		function getComparableColor(precent, isPositiveGood)
		{
			if (precent > 0 && isPositiveGood)
			{
				var tmp = ["#24c364", 1]
				return tmp;
			}
			if (precent > 0 && !isPositiveGood)
			{
				var tmp = ["#ff643f", 0]
				return tmp;
			}
			if (precent < 0 && !isPositiveGood)
			{
				var tmp = ["#24c364", 1]
				return tmp;
			}
			if (precent < 0 && isPositiveGood)
			{
				var tmp = ["#ff643f", 0]
				return tmp;
			}

			return null;
		}
		$('.loading-dashboard').css("display", "none");
		
		function getGraphDataCallback(data)
		{
			//alert(data.ReportID)
			$('.loading-dashboard').css("display", "none");
			var isData = 'isData';
			
			if (data.ReportID)
			{	

				var ReportID = data.ReportID;
				
				if ($scope.activeTab.dashboardActive.Graphs && $scope.activeTab.dashboardActive.Graphs != null && $scope.activeTab.dashboardActive.Graphs != 'undefined')
				{
					var flag = true;
					
					for (var i = 0; i < $scope.activeTab.dashboardActive.Graphs.length; i++)
					{
						if(flag){
							if ($scope.activeTab.dashboardActive.Graphs[i].ID == ReportID)
							{
								
								var dataGraph = "dataGraph";
								$scope.activeTab.dashboardActive.Graphs[i].dataGraph = data;
								$scope.activeTab.dashboardActive.Graphs[i].flagData = true;
								showCharts(data,i);
								flag = false;
								
									
							}
						}
					}
				}
			}
			
			

			
			
			
		}			
			
				function showCharts(data,i){

				var GraphOwnerType = data.GraphOwnerType;
				$scope.curGraphType = data.chartData.GraphType;
							var DashboardID = $scope.activeTab.dashboardActive.ID;
							
							if($scope.activeTab.dashboardActive.Graphs[i].GraphType == 'UserCardEntries' && $scope.activeTab.dashboardActive.Graphs[i].Options != '' && data.chartData.ReportDataTable.Records[0] != null){
								
								for(var j = 0; j < $scope.activeTab.dashboardActive.Graphs[i].Titles.length; j ++){
									var res = $scope.activeTab.dashboardActive.Graphs[i].Titles[j].title.toLowerCase();
									for(var k = 0;k < data.chartData.ReportDataTable.ColumnNames.length;k ++){
										if(res == data.chartData.ReportDataTable.ColumnNames[k]){
											$scope.activeTab.dashboardActive.Graphs[i].Titles[j].data = data.chartData.ReportDataTable.Records[0][k];
											return false;
										}
									}
								}
							}
							
						if($scope.activeTab.dashboardActive.Graphs[i].GraphType != "userjourney"){	
							if (data.chartData != null && !angular.equals({}, data.chartData) && data.chartData.ReportDataTable != null && !angular.equals({}, data.chartData.ReportDataTable) && data.chartData.ReportDataTable.Records != null && data.chartData.ReportDataTable.Records[0] != null && data.chartData.ReportDataTable.Records[0][0] != null)
							{
								if (data.chartData.GraphType == "ComparableCounter")
								{
									
									var ComparePeriod = "ComparePeriod";
									var PositivisGood = "PositivisGood";
									var graphColor = "graphColor";
									var graphStatus = "graphStatus";
									var type = "type";
								
									$scope.tempDDD = JSON.parse($scope.activeTab.dashboardActive.Graphs[i].Options);
									
									var t = $scope.tempDDD.ComparePeriod;
									
									$scope.activeTab.dashboardActive.Graphs[i].dataGraph.ComparePeriod = t;
									
									$scope.activeTab.dashboardActive.Graphs[i].dataGraph['PositivisGood'] = $scope.tempDDD.PositivisGood;
									
									$scope.activeTab.dashboardActive.Graphs[i].dataGraph['type'] = 'thick';
									
									
									if ($scope.activeTab.dashboardActive.Graphs[i].dataGraph.chartData.ReportDataTable.Records[0][0] != 0)
									{
										var color = getComparableColor($scope.activeTab.dashboardActive.Graphs[i].dataGraph.chartData.ReportDataTable.Records[0][0], PositivisGood);
										$scope.activeTab.dashboardActive.Graphs[i].dataGraph.graphColor = color[0];
										$scope.activeTab.dashboardActive.Graphs[i].dataGraph.graphStatus = color[1];
									}
									
									for(var j = 0; j < $scope.typeComparableCounter.length; j ++){
										if($scope.typeComparableCounter[j].ID == $scope.activeTab.dashboardActive.Graphs[i].ID){
											$scope.typeComparableCounter[j].dataGraph = $scope.activeTab.dashboardActive.Graphs[i].dataGraph;
										}
									}
									
								}
								
							
								$scope.activeTab.dashboardActive.Graphs[i].isData = true;
								$scope.activeTab.dashboardActive.Graphs[i].responceStatus = 1;
								isDataFlag(data);
								
							}
							else
							{	
						if(data.chartData.GraphType == 'OmniChart' || data.chartData.GraphType == 'MLMagnitude'){
								if(data.chartData.GraphType == 'OmniChart'){
									$scope.OmniChartData = {};
									if (data.chartData != null)
									{
										for(var i = 0; i < data.chartData.Data.length; i ++){
											
											if(data.chartData.Data[i].type == 'ROI'){
												$scope.OmniChartData.ROI = data.chartData.Data[i].data;
											}
											if(data.chartData.Data[i].type == 'List'){
												$scope.OmniChartData.List = data.chartData.Data[i].data;
											}
											if(data.chartData.Data[i].type == 'Circs'){
												$scope.OmniChartData.Circs = data.chartData.Data[i].data;
											}
										}
										
									}
								}
								if(data.chartData.GraphType == 'MLMagnitude'){
										if (data.chartData.ReportDataTable != null)
										{
											$scope.activeTab.dashboardActive.Graphs[i].isData = true;
											
										}
									}	
						}else{
									
								if(data.chartData.GraphType != 'MultiChoiceLines' && data.chartData.GraphType != 'MultiChoiceFunnel' && data.chartData.GraphType != 'MultiChoiceFunnel_2' && data.chartData.GraphType != 'Funnel' && data.chartData.GraphType != 'MultiChoicePie' ){
									if (data.chartData.ReportDataTable.Records[0] != null && data.chartData.ReportDataTable.Records[0][1] != null)
									{
										
										$scope.activeTab.dashboardActive.Graphs[i].isData = true;
										$scope.activeTab.dashboardActive.Graphs[i].responceStatus = 1;
									}
									else
									{

										$scope.activeTab.dashboardActive.Graphs[i].isData = false;
										$scope.activeTab.dashboardActive.Graphs[i].responceStatus = 0;
									}
								}else{
									if (data.chartData.ReportDataTable.Data.Records[0] != null && data.chartData.ReportDataTable.Data.Records[0][1] != null)
									{
										if(data.chartData.ReportDataTable.Products){
											var temp = {"interaction_id":"0","title":"All"};
											data.chartData.ReportDataTable.Products.unshift(temp);
										}
										if(data.chartData.ReportDataTable.Products1){
											var temp = {"interaction_id":"0","title":"All"};
											data.chartData.ReportDataTable.Products1.unshift(temp);
										}
										if(data.chartData.ReportDataTable.Products2){
											var temp = {"interaction_id":"0","title":"All"};
											data.chartData.ReportDataTable.Products2.unshift(temp);
										}
										
										$scope.activeTab.dashboardActive.Graphs[i].isData = true;
										$scope.activeTab.dashboardActive.Graphs[i].responceStatus = 1;
										
										if(data.chartData.GraphType == 'Funnel' || data.chartData.GraphType == 'MultiChoiceFunnel' || data.chartData.GraphType == 'MultiChoiceFunnel_2'){
											for(var j = 0; j < $scope.typeFunnel.length; j ++){
												if($scope.typeFunnel[j].ID == $scope.activeTab.dashboardActive.Graphs[i].ID){
													$scope.typeFunnel[j].responceStatus = 1;
													$scope.typeFunnel[j].dataGraph = $scope.activeTab.dashboardActive.Graphs[i].dataGraph;
												}
											}
										}
										
										if(data.chartData.GraphType == 'PieChart' || data.chartData.GraphType == 'MultiChoicePie'){
											for(var j = 0; j < $scope.PieChart.length; j ++){
												if($scope.PieChart[j].ID == $scope.activeTab.dashboardActive.Graphs[i].ID){
													$scope.PieChart[j].responceStatus = 1;
													$scope.PieChart[j].dataGraph = $scope.activeTab.dashboardActive.Graphs[i].dataGraph;
												}
											}
										}
									}
									else
									{
										if(data.chartData.GraphType == 'Funnel' || data.chartData.GraphType == 'MultiChoicePie' || data.chartData.GraphType == 'MultiChoiceFunnel_2'){
											for(var j = 0; j < $scope.typeFunnel.length; j ++){
												if($scope.typeFunnel[j].ID == $scope.activeTab.dashboardActive.Graphs[i].ID){
													$scope.typeFunnel[j].responceStatus = 0;
												}
											}
										}
										if(data.chartData.GraphType == 'PieChart' || data.chartData.GraphType == 'MultiChoicePie'){
											for(var j = 0; j < $scope.PieChart.length; j ++){
												if($scope.PieChart[j].ID == $scope.activeTab.dashboardActive.Graphs[i].ID){
													$scope.PieChart[j].responceStatus = 0;
												}
											}
										}
										
										$scope.activeTab.dashboardActive.Graphs[i].isData = false;
										$scope.activeTab.dashboardActive.Graphs[i].responceStatus = 0;
									}
								}
							}}
							
							
						}else if($scope.activeTab.dashboardActive.Graphs[i].dataGraph.chartData.ReportDataTable.length > 0){
								$scope.activeTab.dashboardActive.Graphs[i].isData = true;
								$scope.activeTab.dashboardActive.Graphs[i].responceStatus = 1;
								isDataFlag(data,GraphOwnerType,DashboardID);
							}
							
		
			$scope.deleteAjaxCall($scope.callID);					
							
		}
				
		


			function isDataFlag(data,GraphOwnerType,DashboardID)
			{
				var chartData = parseAllGraphData(data.chartData.GraphType, data);
				var graphID = '#chart_' + GraphOwnerType + '_' + DashboardID + '_' + data.ReportID;
				var containerID = '#container_' + GraphOwnerType + '_' + DashboardID + '_' + data.ReportID;
				var options;
				
				if (opt != null && opt != "" && opt != "null")
					options = $.parseJSON(opt);

				if (options == null)
					options = $scope.getGraphOptions($scope.curGraphType);

				//$scope.addExtraFormatting(options, $scope.curGraphType);

				if ($scope.curGraphType != null && $scope.curGraphType.toLowerCase() != "userjourney")
					storeGraphData(GraphOwnerType, $scope.curGraphType, graphID, chartData);

				if ($scope.curGraphType == "userjourney")
				{
					var ujdata = $scope.activeTab.dashboardActive.Graphs[i].dataGraph.chartData.ReportDataTable;
					var ujGraphId = '#UJchart_'+ $scope.activeTab.dashboardActive.Graphs[i].ID;
					$scope.drawJourneyGraph(ujdata, ujGraphId);
					
					
				}

			}
		
		//$scope.saveAjaxCall($scope.callID, call);
		

	}

	// /**********************************************  end TABS fun ********************************************/

	function isUserJourneyDashboard(DashboardID)
	{
		var type = dashboardTypeByID[DashboardID];

		return type == 'OneDateSelection';
	}

	function parseAllGraphData(GraphType16, data)
	{
		var gtype = GraphType16.toLowerCase();

		if (gtype.indexOf("counterchart") != -1){
			
			return $scope.parseCounterData(data);
		}
		
		else if (gtype.indexOf("summarytable") != -1)
			return parseSummaryTableData(data);
		else
			return $scope.parseGraphData(data);
	}

	function storeGraphData(graphOwnerType, graphType, graphid, data)
	{
		var graphData = $scope.loadFromStorage('graphData');

		if (graphData == null)
			graphData = {};

		graphData[graphid] =
		{
			graphOwnerType: graphOwnerType,
			graphType: graphType,
			graphid: graphid,
			data: data
		};
		$scope.storeToCache('graphData', graphData);
	}

	

	var dashboardTypeByID = {};

	function fillSegmentsAnalysisData()
	{
		$("#user_segments_and_characteristics_table_body").empty();
		var tableData = "";
		for (i = 0; i < InsightConst.SegmentsAnalysisData.length; i++)
		{
			tableData += '<ul>';
			tableData += '	<li class="task-list-item done" style="display: block;">';
			tableData += '		<div class="task-content">';
			tableData += '			<i class="fa fa-user" style="display: inline-block;"></i>';
			tableData += '			<h4 class="uppercase bold" style="display: inline-block;">';
			tableData += '				<div>' + InsightConst.SegmentsAnalysisData[i].name + '</div>';
			tableData += '			</h4>';
			tableData += '			<p style="border-bottom: 1px solid #D1D1D2; padding-bottom: 20px;width: 90%;">';
			tableData += InsightConst.SegmentsAnalysisData[i].analysis;
			tableData += '			</p>';
			tableData += '		</div>';
			tableData += '	</li>';
			tableData += '</ul>';

		}
		$("#user_segments_and_characteristics_table_body").append(tableData);
	}

	$scope.cleanPreviewDivs = function ()
	{
		document.getElementById('container_preview').style.display = 'none';
		document.getElementById('container_preview_pie').style.display = 'none';
		document.getElementById('container_preview_counter').style.display = 'none';
		document.getElementById('container_preview_funnel').style.display = 'none';

		var loading = '<div id="preview_loader" style="background: transparent url(&quot;Images/AjaxLoader.gif&quot;) no-repeat scroll center center; width: 100%; height: 100%;"></div>';
		document.getElementById('chart_preview').innerHTML = loading;
		document.getElementById('chart_preview_funnel').innerHTML = loading;
		document.getElementById('chart_preview_pie').innerHTML = loading;
		document.getElementById('chart_preview_counter').innerHTML = 'N/A';

		var loading_title = "Loading please wait..."
			document.getElementById('preview_title_pie').innerHTML = loading_title;
		document.getElementById('preview_title_funnel').innerHTML = loading_title;
		document.getElementById('preview_title').innerHTML = loading_title;
		document.getElementById('title_preview_counter').innerHTML = "LOADING";
		document.getElementById('desc_preview_counter').innerHTML = 'Please wait...';
	}


	function removeDashboardGraph(dashbordID, graphid)
{
	var graphsData = $scope.loadFromStorage(dashbordID);
	var l = graphsData.Graphs.length;
	for(var i=0 ; i < l ; i++)
{
	var item = graphsData.Graphs[i];
	if(graphid == item.ID)
{
	graphsData.Graphs.splice(i, 1);
	$scope.storeToCache(dashbordID, graphsData);
	return false;
	}
	}
	}



	function buildGraphContainer(globalItem)
{
	var mainGraphsList = "";
	var globalItemID = globalItem.ID;
	var div_id = 'item_'+globalItemID;
	var mainGraphsList = '<div class="tab-pane" id="'+div_id+'">';
	mainGraphsList += '<div class="col-md-12"><h3 class="page-title" style="margin-left:15px">'+globalItem.Name+'</h3></div>';

	var counters = '<div class="col-md-12">';
	var sparkLines = '<div class="col-md-12">';
	var specialGraphsLayer = '<div class="col-md-12">';
	var pie = '';
	var specialPie = '';

	// TODO: remove for test
	// if (globalItem.Name == "Single User")
	// counters += drawHorizontalTimeline(14, 0, "UserGenerated");
	// counters += drawSummaryTable("chart_UserGenerated_14_0");

	var multichoiceTrackingChartFirstGraph = null;

	var ln = globalItem.Graphs.length;
	for(var i = 0 ; i < ln ; i++)
{
	var item = globalItem.Graphs[i];
	var type = item.GraphType;
	var setFullWidth = false;

	if (item.Options != null)
{
	try{
	var options = JSON.parse(item.Options);
	if (options.setFullWidth != null && options.setFullWidth==true)
	setFullWidth = true;
	}
	catch(e){}
	}

	if(type =="HorizontalTimeline")
{
	specialGraphsLayer += drawHorizontalTimeline(globalItemID, item.ID, item.graphOwnerType);
	}
	else if (type =="CounterChart")
{
	counters += drawCounter(item.Name, item.Description, globalItemID, item.ID, item.graphOwnerType);
	}
	else if(type =="InsightHolder")
{
	specialGraphsLayer += drawInsightPanel(globalItemID, item.ID, item.graphOwnerType);
	}
	else if(type =="SummaryTable")
{
	specialGraphsLayer += drawSummaryTable(globalItemID, item.ID, item.graphOwnerType);
	}
	else if(type =="SortableTable")
{
	pie += drawTrafficSourcesPanel(globalItemID, item.ID, item.graphOwnerType);
	}
	else if(type =="CounterGroup")
{
	specialGraphsLayer += drawCounterGroup(item.Name, item.Description, globalItemID, item.ID, item.graphOwnerType);
	}
	else if(type =="SparkLine")
{
	sparkLines += drawSparkLine(item.Name, item.Description, globalItemID, item.ID, item.graphOwnerType);
	}
	else if(type =="Map")
{
	pie += drawMap(type, item.Name, item.Description, globalItemID, item.ID, item.graphOwnerType, div_id);
	}
	else if(type =="userjourney")
{
	pie += drawUserJourney(item.Name, item.Description, globalItemID, item.ID, item.graphOwnerType, div_id);
	}
	else if(type =="PieChart")
{
	pie += drawChart(type, item.Name, item.Description, globalItemID, item.ID, item.graphOwnerType, div_id, item.AxisX, item.AxisY, setFullWidth);
	}
	else if(type =="BigTable" || type =="SmallTable")
{
	pie += drawTable(type, item.Name, item.Description, globalItemID, item.ID, item.graphOwnerType, div_id);
	}
	else if(type =="MultichoiceTrackingChart")
{
	if (multichoiceTrackingChartFirstGraph == null)
{
	multichoiceTrackingChartFirstGraph = drawChart(type, item.Name, item.Description, globalItemID, item.ID, item.graphOwnerType, div_id, item.AxisX, item.AxisY, setFullWidth);
	}
	else
{
	specialPie += drawChartIntoMultichoiceTrackingChart(multichoiceTrackingChartFirstGraph, item.Name, item.Description, globalItemID, item.ID, item.graphOwnerType, div_id, item.AxisX, item.AxisY, setFullWidth);
	}
	}
	else
{
	pie += drawChart(type, item.Name, item.Description, globalItemID, item.ID, item.graphOwnerType, div_id, item.AxisX, item.AxisY, setFullWidth);
	}
	}

	counters += '</div>';
	sparkLines += '</div>';
	specialGraphsLayer += '</div>';
	pie = '<div class="col-md-12">' + specialPie + pie + '</div>';

	mainGraphsList += counters + sparkLines + specialGraphsLayer + pie;
	mainGraphsList += '</div>';

	return mainGraphsList;
	}
	/*




	 */


	//****************    EXPRORT TO EXTERNAL LINK     ****************************************************
$scope.showAlertCopy = function(e){
	//alert(''+e+' has been copied to clipboard');
}
	$scope.exportToMail = function (graph)
	{
		var gameName = $scope.loadFromStorage('loadedGameName');
		var companyName33 = $scope.companyNameByGameName[gameName];
		var iconURL = $scope.gameIconByGameName[gameName];
		var fromDate = typeof ext_from_date !== 'undefined' ? ext_from_date : $scope.loadFromStorage('lastStartDate');
		var toDate = typeof ext_to_date !== 'undefined' ? ext_to_date : $scope.loadFromStorage('lastEndDate');
		var ColumnNames;
		var dataGraphRecords;
		if(graph.GraphType == "MultiChoiceLines" || graph.GraphType == "MultiChoiceFunnel" || graph.GraphType == "MultiChoiceFunnel_2" || graph.GraphType == "Funnel"){
			if(graph.dataGraph.chartData.ReportDataTable.Data.ColumnNames.length > 0){
				ColumnNames = graph.dataGraph.chartData.ReportDataTable.Data.ColumnNames;
			}
		}else if(graph.dataGraph.chartData.ReportDataTable.ColumnNames.length > 0){
				ColumnNames = graph.dataGraph.chartData.ReportDataTable.ColumnNames;
		}
	
		
		
		
	if (graph.GraphType == 'PieChart'){
			$scope.tempGraphRecords = [];
			for (var i = 0; i < graph.dataGraph.chartData.ReportDataTable.Records.length; i++)
			{
				var t =
				{
					'label': graph.dataGraph.chartData.ReportDataTable.Records[i][0],
					'data': graph.dataGraph.chartData.ReportDataTable.Records[i][1]
				}
				$scope.tempGraphRecords.push(t);
			}
			dataGraphRecords = JSON.stringify($scope.tempGraphRecords);
	}else if(graph.GraphType == "MultiChoiceLines"){
		$scope.tempGraphRecords = graph.dataGraph.chartData.ReportDataTable.Data.Records;
		dataGraphRecords = JSON.stringify($scope.tempGraphRecords);
	}else if(graph.GraphType == "MultiChoiceFunnel" || graph.GraphType == "MultiChoiceFunnel_2" || graph.GraphType == "Funnel"){
		$scope.tempGraphRecords = graph.dataGraph.chartData.ReportDataTable;
		dataGraphRecords = JSON.stringify($scope.tempGraphRecords);
	}else{
		$scope.tempGraphRecords = graph.dataGraph.chartData.ReportDataTable.Records;
		dataGraphRecords = JSON.stringify($scope.tempGraphRecords);
	}

		
		var tempOptions = graph.Options;
		var tempOpt = tempOptions.replace(/#/g , "hash");
		var url = $rootScope.protocol+"://curve.beta.curve.tech/#/extern";
		
		url += "?t=" + graph.Name;
		url += getParam("d", graph.Description);
		url += getParam("gn", gameName);
		url += getParam("com", companyName33);
		url += getParam("icon", iconURL);
		//url += getParam("type", graph.graphOwnerType);
		url += getParam("graphtype", graph.GraphType);
		url += getParam("dateFrom", fromDate);
		url += getParam("dateTo", toDate);
		url += getParam("columnNames", ColumnNames);
		url += getParam("data", dataGraphRecords);
		url += getParam("opt", tempOpt);
		url = encodeURI(url);
		exportURL(url);
	}

	function exportURL(text)
	{

		$('#export_url')[0].value = text;
		$('#export_content_modal').modal('show');

		$('#export_content_modal').on('shown.bs.modal', function ()
		{
			$("#export_url")[0].focus();
			$("#export_url")[0].select();
			$('#export_url').scrollTop(0);
			$scope.tutorialInvokeEvent("rawDataExportClicked");
		}
		)
	}

	/* function downloadCurrent()
	{

		var uri = 'data:text/csv;charset=utf-8,' + encodeURIComponent('[InternetShortcut] URL=' + $('#export_url')[0].value);
		var element = document.createElement("a");

		element.href = uri;
		element.download = 'URL';

		document.body.appendChild(element);
		element.click();
		document.body.removeChild(element);
	} */

	function sendEmail(subject, text)
	{

		window.location.href = 'mailto:?subject=Gingee Curve ' + subject + '&body=' + encodeURIComponent(text);
	}

	function getParam(key, val)
	{
		return "&" + key + "=" + val;
	}

}


