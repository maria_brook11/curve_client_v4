var labelType, useGradients, nativeTextSupport, animate;
var nodeWidth = 140;
var nodeHeight = 60;

initUserJourneyParams();

function initUserJourneyParams() 
{
	var ua = navigator.userAgent;
	var iStuff = ua.match(/iPhone/i) || ua.match(/iPad/i);
	var typeOfCanvas = typeof HTMLCanvasElement;
	var nativeCanvasSupport = (typeOfCanvas == 'object' || typeOfCanvas == 'function');
	var textSupport = nativeCanvasSupport && (typeof document.createElement('canvas').getContext('2d').fillText == 'function');
	
	//I'm setting this based on the fact that ExCanvas provides text support for IE
	//and that as of today iPhone/iPad current text support is lame
	labelType = (!nativeCanvasSupport || (textSupport && !iStuff))? 'Native' : 'HTML';
	nativeTextSupport = labelType == 'Native';
	useGradients = nativeCanvasSupport;
	animate = false;//!(iStuff || !nativeCanvasSupport);
}


var most = null;
var least = null;
var start = null;
var st = null;
var json_data = null;
var global_div_id = null;

function mostCommonJourney()
{
	if(json_data == null)
	{
		alert("Data not ready!");
		return;
	}
	
	createPages(most_common_path);
	initUserJourneyData(getClippedMap(most), global_div_id, most);
	// updateButtonsGraphics(1);
	pageSelectedUJ(1);
	
	var desc_id = global_div_id + '_caption';
	$(desc_id)[0].innerHTML = "Showing: Trending Journey. Total sessions: " + json_data.total;
}

function leastCommonJourney()
{
	if(json_data == null)
	{
		alert("Data not ready!");
		return;
	}
	
	createPages(least_common_path);
	initUserJourneyData(getClippedMap(least), global_div_id, least);
	// updateButtonsGraphics(1);
	pageSelectedUJ(1);
	
	var desc_id = global_div_id + '_caption';
	$(desc_id)[0].innerHTML = "Showing: Un-Trending Journey. Total sessions: " + json_data.total;
}

function randomJourney()
{
	if(json_data == null)
	{
		alert("Data not ready!");
		return;
	}
	
	var rand = getRandomPath(json_data);
	createPages(random_path);
	initUserJourneyData(getClippedMap(rand), global_div_id, rand);
	updateButtonsGraphics(num_pages_uj);
	// updateButtonsGraphics(1);
	pageSelectedUJ(1);
	
	var desc_id = global_div_id + '_caption';
	$(desc_id)[0].innerHTML = "Showing: Random Journey. Total sessions: " + json_data.total;
}

function selectPageItemUJ(id)
{
	initUserJourneyData(getClippedMap(id), global_div_id, id);
}

function initRawData(json, div_id)
{
	most_common_path = [];
	least_common_path = [];
	random_path = [];
	
    most = getMostCommonPath(json);
    least = getLeastCommonPath(json);
	global_div_id = div_id;
	json_data = json;
}

function initUserJourneyData(json, div_id, clicked_id)
{
	div_id = div_id.substr(1);
	
	$("#" + div_id)[0].innerHTML = "";
    st = new $jit.ST(
	{
        injectInto: div_id,
        duration: 0,
        transition: $jit.Trans.Quart.easeInOut,
        //set distance between node and its children
        levelDistance: 30,
        //enable panning
        Navigation: 
		{
          enable:true,
          panning:true
        },
        Node: 
		{
            height: nodeHeight,
            width: nodeWidth,
            type: 'rectangle',
            // color: '#ededee',
            color: '#fff',
            overridable: true
        },
        
        Edge: 
		{
            type: 'bezier',
            color: '#5cb8d8',
            lineWidth: 0.8,
            overridable: true
        },
        
        onCreateLabel: function(label, node)
		{
            label.id = node.id;
			var data = node.name.split("__");
			
			var icon = data[3] == "true" ? '<i class="fa fa-hand-pointer-o" style="margin-right:5px;color:#176f01;"></i>' : '<i class="fa fa-desktop" style="margin-right:5px;fontSize:0.7em;color:#5ecff4;"></i>';
			if(data.length > 2)
			{
			    label.innerHTML = '<div class="user_journy_container"><div style="color:#333;fontSize:1em;font-weight:bold" title="'+ data[0] +'">' + icon + formalizeTitle(data[0]) + '</div>' + 
				'<div>Users: ' + data[1] + '%</div>' + 
				'<div>Av Time: ' + data[2] + ' Sec</div></div>';
			}
			else
				label.innerHTML = '<div class="user_journy_container_start"><div style="color:#fff;fontSize:30px;font-weight:bold;font-family: "Open Sans", sans-serif;" title="'+ data[0] +'">' + formalizeTitle(data[0]) + '</div></div>';
           
			label.onclick = function()
			{
                // st.onClick(node.id);
				initUserJourneyData(getClippedMap(node.id), global_div_id, node.id);
            };
			
            //set label styles
            var style = label.style;
            style.width = nodeWidth + 'px';
            style.height = nodeHeight + 'px';
            style.color = '#333';
            style.fontSize = '0.8em';
            style.textAlign= 'center';
			style["font-family"] = "Open Sans";
        },
		
        onBeforePlotLine: function(adj)
		{
            if (adj.nodeFrom.selected && adj.nodeTo.selected)
			{
                adj.data.$color = "#5ecff4";
                adj.data.$lineWidth = 4;
            }
            else 
			{
                delete adj.data.$color;
                delete adj.data.$lineWidth;
            }
        }
    });
	
    //load json data
    st.loadJSON(json);
    //compute node positions and layout
    st.compute();
    //optional: make a translation of the tree
    st.geom.translate(new $jit.Complex(-200, 0), "current");
    //emulate a click on the root node.
	
	if(clicked_id != null)
		st.onClick(clicked_id);
	else if(most == null)
		st.onClick(st.root);
	else
		st.onClick(most);
}

function formalizeTitle(st)
{
	var max = 15;
	if(st.length > max)
		return st.substr(0, max) + "..";
	
	return st;
}
