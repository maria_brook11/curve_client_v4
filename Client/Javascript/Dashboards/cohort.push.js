function CohortPushFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst,InsightConst,$interval){
	
	var currentStep = 1;
	
$scope.PushEditor = function(e,t,a)
{
	$rootScope.$broadcast('deselectAll',{});
		$("#push_editor_textarea").markdown({
			autofocus:false,
			savable:false,
			resize:"none",
		});
		
	$scope.flagSelect = false;
	if(t != null && a != null){
		var intvGetAud = $interval(function ()
			{
				if ($scope.AudiencesArray && $scope.AudiencesArray.length > 0)
				{	
					for(var i = 0; i < $scope.AudiencesArray.length ; i ++){
						
						for(var j = 0; j < a.length ; j ++){
							
							if($scope.AudiencesArray[i].type == t && $scope.AudiencesArray[i].id == a[j].id){
								if(a[j].checked){
									
									$scope.AudiencesArray[i].add = true;
									
								}
							}
						}
					}
					$scope.flagSelect = true;
					$interval.cancel(intvGetAud);
					
				}
			}, 300);
	}else{
		$scope.flagSelect = true;
	}
		
		init();
};

	$scope.PushEditorSendPush = function()
	{
		var data = $scope.validateAndPreparePushData(true);
		
		if(data.error != null)
		{
			$scope.show_error("Make sure all fields are properly filled", data.error);
			return;
		}
		
		if(data.warn != null)
		{
			$scope.EmailValidatorManager("", sendWithoutValidatingPush, null, data.warn, "If not, please make sure to fill them properly.", "Yes, Send", "Cancel");
			return;
		}
		
		$('#push_cohort_step_1').css('display' , 'none');
		$('#push_cohort_step_2').css('display' , 'none');
		
		$('#push_cohort_confirm').css('display' , 'none');
		$('#push_cohort_next').css('display' , 'none');
		$('#push_cohort_back').css('display' , 'none');
		
		$('#push_main_loader').css('display' , '');
		
		$scope.curveAPI("SendPushByCohorts", {"Token": $rootScope.Token, Data:JSON.stringify(data)}, onSendPushByCohorts, onErrorSendPushByCohorts);
	};
	
	function sendWithoutValidatingPush(id)
	{
		var data = $scope.validateAndPreparePushData(false);
		
		$scope.curveAPI("SendPushByCohorts", {"Token": $rootScope.Token, Data:JSON.stringify(data)}, onSendPushByCohorts, onErrorSendPushByCohorts);
		$("#cohort_push_editor").modal('hide');
	};
	
	$scope.PushEditorOnUseBatchChecked = function()
	{
		var checked = $("#push_batch_checkBox")[0].checked;
		if (checked)
		{
			$("#push_batch_number_holder")[0].style.display = "";
			$("#push_batch_number")[0].value = 100;
		}
		else
		{
			$("#push_batch_number_holder")[0].style.display = "none";
		}
		
	};
	
	function onSendPushByCohorts(data)
	{
		//console.log(data);
		if (data && data.Response)
		{
			try{
				var recipients = JSON.parse(data.Response).recipients;
				$scope.show_success("Success","Push notification was sent succesfully to " + recipients + " recipients.");
			}
			catch(e){
				$scope.show_error("There was an Error parsing the push notification data response.", e.toString());
			}
		}
		else if (data && data.ReturnCode && data.ReturnCode=="0")
			$scope.show_success("Success","Push notification request completed successfully.");
		else
			$scope.show_error("There was an Error sending the push notification.", JSON.stringify(data, null, 4));
		
		$("#cohort_push_editor").modal('hide');
		
	};
	
	function onErrorSendPushByCohorts(data)
	{
		console.log("ERROR", data);
		$scope.show_error("There was an Error sending the push notification.", data);
		$("#cohort_push_editor").modal('hide');
	};
	
	$scope.validateAndPreparePushData = function(validate, checkRecepients)
	{
		var pushContent = $(".md-preview").text();
		var ids = $scope.getCohordIDs('push');
		var pushs = getRecepientsPushs();
		
		if (checkRecepients)
		{
			var total_recepients = 0;
			if(ids != null)
				total_recepients += 1;
			if(pushs != null)
				total_recepients += pushs.length;
			
			if (total_recepients == 0)
				return {error: "You must specify at least one recepient or Audience."};
		}
		
		if (pushContent == undefined || pushContent == "")
		{
			$("#push_editor_textarea").data('markdown').showPreview();
			pushContent = $(".md-preview").text();
			$("#push_editor_textarea").data('markdown').hidePreview();
		}
		
		if(validate)
		{
			if(pushContent == null || pushContent == "")
				return {warn: "Are you sure you want to send this push without a body?"};
		}
		
		var batchNumber = $("#push_batch_number")[0].value;
		var useBatch = $("#push_batch_checkBox")[0].checked;
		
		if (useBatch && batchNumber < 1)
			return {error: "You must specify a positive bulk size."};
		
		
		var temp = {"Text": pushContent, "PushIDs": pushs, "UseBatch": useBatch, "BulkSize": batchNumber};
			
			angular.forEach(ids, function (val,key) {
				temp[key] = val;
			});
			
		return temp;
		
	};
	
	/* function getCohordIDs()
	{
		var cohortNames = $("#push_cohort_names_select").val();
		if(cohortNames == null)
			return [];
		
		var ans = [];
		var children = $("#push_cohort_names_select").children();
		
		var ln = children.length;
		for (var i=0 ; i < ln ; i++)
		{
			if (cohortNames.indexOf(children[i].getAttribute('value')) != -1)
				ans.push(children[i].getAttribute('data-cohort-id'));
		}
		return ans;
	}; */
	
	function getRecepientsPushs()
	{
		var recepients = $("#push_editor_recepients").val();
		
		if(recepients == null || recepients == "")
			return [];
		
		var pushs = recepients.replace(/ /g,'');
		return pushs != null ? pushs.split(",") : [];
	};
	
	function hasPushServiceSupport()
	{
		var data = $scope.ExternalAPIsTableData;
		
		if (data == null || data.length == 0)
			return false;
		
		var ln = data.length;
		for(var i=0 ; i < ln ; i++)
		{
			if (data[i].type == "push")
				return true;
		}
	};
	
	function firstStep()
	{
		currentStep = 1;
		$('#cohort_push_editor_title').html("Send Push Notifications to groups (Audiences)");
		$('#push_cohort_step_1').css('display' , '');
		$('#push_cohort_step_2').css('display' , 'none');
		
		$('#push_cohort_confirm').css('display' , 'none');
		$('#push_cohort_next').css('display' , '');
		$('#push_cohort_back').css('display' , 'none');
		
		$('#push_main_loader').css('display' , 'none');
		
		if ($scope.ExternalAPIs_is_ready && !hasPushServiceSupport())
		{
			$scope.show_warn("No Push service configured !", "You dont have any Push service configured. please go to: 'Settings' > 'App Settings' > 'External APIs' to add one.");
			$("#cohort_push_editor").modal('hide');
		}
		// else
			// $("#cohort_push_editor").modal('show');
	};
	
	function secondStep()
	{
		currentStep = 2;
		$('#push_cohort_step_1').css('display' , 'none');
		$('#push_cohort_step_2').css('display' , '');
		
		$('#push_cohort_confirm').css('display' , '');
		$('#push_cohort_next').css('display' , 'none');
		$('#push_cohort_back').css('display' , '');
	};
	
	function init()
	{
		clearAllForms();
		firstStep();
	};
	
	function clearAllForms()
	{
		// clear cohort field
		//$("#push_cohort_names_select").val([]).trigger("change");
		// clear recepient field
		$("#push_editor_recepients").val("");
		
		// clear the push forms
		$("#push_editor_textarea").val('');
		$(".md-preview").html('');
		$("#push_editor_textarea").data('markdown').hidePreview();
	};
	
	$scope.PushEditorNextStep = function()
	{
		if (currentStep == 1 && verifyFirstStep())
			secondStep();
	};
	
	$scope.PushEditorPrevStep = function()
	{
		if (currentStep == 2)
			firstStep();
	};
	
	
	function gotCohorts()
	{
		/* var data = $scope.cohortData;
		var ln = data.length;
		var lines = '';
		
		//console.log("push gotCohorts", data);
		
		for(var i=0 ; i < ln ; i++)
		{
			var cohort = data[i];
			lines+='<option data-cohort-id='+cohort.audienceid+' value="'+cohort.name+'">'+cohort.name+'</option>';
		} */
		
		
		/* $("#push_cohort_names_loading")[0].style.display = "none";
		$("#push_cohort_names_select")[0].style.display = "";
		$("#push_cohort_names_select").select2({ placeholder: "Audience Names", theme: "classic"});
		$("#push_cohort_names_select").html(lines);
		$("#push_cohort_names_select").val([]).trigger("change"); */
	};
	
	function verifyFirstStep()
	{
		var ids = $scope.getCohordIDs('push');
		var pushs = getRecepientsPushs();
		
		var total_recepients = 0;
		if(ids != null)
			total_recepients += 1;
		if(pushs != null)
			total_recepients += pushs.length;
		
		if (total_recepients == 0)
			$scope.show_warn("No recepients were specified!", "You must specify at least one recepient or Audience to continue.");
		
		return total_recepients != 0;	
	};
}