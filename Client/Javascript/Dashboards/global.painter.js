function getCompanyHeadline(name, icon, customer_id)
{
	var title = '<div class="clearfix"></div><div style="height:64px; padding-top:5px; padding-left:5px; margin-top:15px;">';
	title += '<div id="companyImage">';
	title += '<a data-toggle="modal" href="#companyIcon"><img src="' + icon + '" onclick="setCompanyIcon(\'' + customer_id + '\');" style="width:54px;height:54px; float:left; margin-right:5px; border-radius: 15px !important;"/></a>';
	title += '</div><div class="page-title" style="font-size:35px; line-height:54px;">' + name + '</div>';
	title += '</div>';

	return title;
}

function getCompanyIconModalBox()
{
	var resp = '';
	resp += '<div class="modal-dialog">';
	resp += '<div class="modal-content">';
	resp += '<div class="modal-header">';
	resp += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>';
	resp += '<h4 class="modal-title">Update company\'s icon</h4></div>';
	resp += '<div class="modal-body">';
	resp += '<form class="uploadCompanyIcon" action="'+$rootScope.protocol+'://prod.curve.tech/includes/companyUpload.php" method="post" enctype="multipart/form-data">';
	resp += '<h3></h3>';
	resp += '<table>';
	resp += '<tr><td>File:</td><td><input type="file" accept=".png,.jpg,.JPG,.PNG,.jpeg,.GPEG" name="file1" id="file1"></td></tr>';
	resp += '<tr><td>&nbsp;</td><td><input type="submit" value="Upload"></td></tr>';
	resp += '<tr><td><input id="CustomerId" type="hidden" name="CustomerId"/> </td></tr>';
	resp += '</table>';
	resp += '<div class="progress" style="height: 100px;"><div class="percent">0%</div ><div id="comp_status"></div></div>';
	resp += '</form>';
	resp += '<div class="company_icon loading" style="display:none;background: url(Images/AjaxLoader.gif) no-repeat center center;width: 100%;min-height: 300px;"></div>';
	resp += '<script>uploadCompanyImage();</script>';									 
	resp += '</div>';
	resp += '</div></div>';

	return resp;
}

function getUserImageModalBox()
{
	var resp = '<div class="modal-dialog">';
	resp += '<div class="modal-content">';
	resp += '<div class="modal-header">';
	resp += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>';
	resp += '<h4 class="modal-title">Update profile picture</h4></div>';
	resp += '<div class="modal-body">';
	resp += '<form class="uploadImage" action="'+$rootScope.protocol+'://prod.curve.tech/includes/userupload.php" method="post" enctype="multipart/form-data">';
	resp += '<h3></h3>';
	resp += '<table>';
	resp += '<tr><td>File:</td><td><input type="file" accept=".png,.jpg,.JPG,.PNG,.jpeg,.GPEG" name="file1" id="file1"></td></tr>';
	resp += '<tr><td>&nbsp;</td><td><input type="submit" value="Upload"></td></tr>';
	resp += '<tr><td><input id="UserId" type="hidden" name="UserId" /> </td></tr>';
	resp += '</table>';
	resp += '<div class="progress" style="height: 100px;"><div class="percent">0%</div ><div id="status"></div></div>';
	resp += '</form>';
	resp += '<div class="profile_upload loading" style="display:none;background: url(Images/AjaxLoader.gif) no-repeat center center;width: 100%;min-height: 300px;"></div>';
	resp += '<script>uploadUserImage();</script>';									 
	resp += '</div>';
	resp += '</div></div>';

	return resp;
}