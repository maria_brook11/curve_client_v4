var hR = 80;
var hG = 160;
var hB = 187;
// 13507937 // CE1D61


var lR = 143;
var lG = 194;
var lB = 211;
// 4822470 // 4995C6
var maps = [];
function registerMap(id)
{
	if(maps.indexOf(id) == -1)
		maps.push(id);
}

function initMaps()
{
	while(maps.length > 0)
	{
		initMap(maps.pop());
	}
}

function initMap(id) 
{
	id = id + "_map";
	var map = jQuery(id);
	map.vectorMap({
		map: 'world_en',
		backgroundColor: '#fff',
		selectedColor: '#b4d6e2',
		borderColor: '#4995c6',
		borderOpacity: 0.25,
		borderWidth: 1,
		color: '#f4f3f0',
		enableZoom: true,
		hoverOpacity: 0.7,
		regionsSelectable: false,
		showTooltip: true,
		onRegionClick: function(element, code, region)
		{
			var users = countries[code.toLowerCase()];
			if(users == null) users = 0;
			var message = region 
				+ ' has '
				+ users + ' Users.';

			alert(message);
		},
		 onMarkerTipShow: function(e, el, code){
			el.html(el.html()+' test');
		}
		}
	);
	
	var wdt = map.parent().width();
	if(wdt == 0)
		wdt = (jQuery($(document).width()) < 859) ? jQuery($(document).width()) : 859;
	map.width(wdt);
	
	map.height(300);
}

var countries = {};
function setMapData(data, id)
{
	initMaps();
	d = data[0].data;
	
	var total = 0;
	var max = 0;
	for(var i=0 ; i < d.length ; i++)
	{
		var country = d[i];
		if(country[1].length == 2)
		{
			countries[country[1].toLowerCase()] = country[0];
			total += parseInt(country[0]);
			max = Math.max(max, country[0]);
		}
	}
	
	var colors = {};
	var maxdev = max/total;
	for(var i=0 ; i < d.length ; i++)
	{
		var country = d[i];
		if(country[1].length == 2)
		{
			var col = getMidColor(country[0]/total/ maxdev);
			colors[country[1].toLowerCase()] = "#" + col;
		}
	}
	
	$(id+"_map").vectorMap('set', 'colors', colors);
}

function getMidColor(p, forceColorH, forceColorL)
{
		var hex = function(x) {
		
			if (x > 255)
				x = 255;
			if (x<0)
				x = 0;
			x = x.toString(16);
			
			return (x.length == 1) ? '0' + x : x;
		};
		
		var color1;
		if (forceColorH == null)
			color1 =  hex(hR) + hex(hG) + hex(hB);
		else
			color1 = forceColorH;
			
		var color2;
		if (forceColorL == null)
			color2 =  hex(lR) + hex(lG) + hex(lB);
		else
			color2 = forceColorL;


		var ratio = p;
	

		var r = Math.ceil(parseInt(color1.substring(0,2), 16) * ratio + parseInt(color2.substring(0,2), 16) * (1-ratio));
		var g = Math.ceil(parseInt(color1.substring(2,4), 16) * ratio + parseInt(color2.substring(2,4), 16) * (1-ratio));
		var b = Math.ceil(parseInt(color1.substring(4,6), 16) * ratio + parseInt(color2.substring(4,6), 16) * (1-ratio));
		var middle = hex(r) + hex(g) + hex(b);
		
		return middle;
	//var c = 65536 * getMidVal(hR, lR, p) + 256 * getMidVal(hG, lG, p) + getMidVal(hB, lB, p);
	//return c.toString(16);
}

function getMidVal(h, l, p)
{
	var diff = h - l;
	return Math.floor(p*diff + l);
}