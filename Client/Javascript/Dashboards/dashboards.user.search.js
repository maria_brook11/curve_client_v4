function UserSearch($scope,$rootScope,$http,$location){
	
	var searchby = 'userid';
	$scope.viewResStatus = 0;
	$scope.usersSearch = {};
	
	$scope.UserSearchInitSearch = function()
	{
		$('#search-user').modal('show');
		hideSearching();
		$('#search_user_result').css('display','none');
		$('#search_field_single_user_fullname').css('display','none');
		$('#search_field_single_user').css('display','');
		$('#search_user_result_panel')[0].innerHTML = '';
		// $('#users_search_input')[0].value = ''; // In dashboard input text
		$('#search_user_input').val(""); // Search modal input text
	};

	$scope.UserSearchSearchForUser = function()
	{
		var search = $('#search_user_input')[0].value;
		var first = $('#search_user_input_first')[0].value;
		var last = $('#search_user_input_last')[0].value;
		$scope.viewResStatus = 0;
		
		if(searchby == "email")
		{
			if(validateSearchValue(search))
				$scope.curveAPI('Search', {'Token': $rootScope.Token, 'Data': JSON.stringify({"Type":"EmailSearch", "Email":search})}, onResult, onError);
		}
		else if(searchby == "lastname")
		{
			if(validateSearchValue(search))
				$scope.curveAPI('Search', {'Token': $rootScope.Token, 'Data': JSON.stringify({"Type":"LastNameSearch", "LastName":search})}, onResult, onError);
		}
		else if(searchby == "userid")
		{
			if(validateSearchValue(search))
				$scope.curveAPI('Search', {'Token': $rootScope.Token, 'Data': JSON.stringify({"Type":"IDSearch", "UserID":search})}, onResult, onError);
		}
		else if(searchby == "hardwareid")
		{
			if(validateSearchValue(search))
				$scope.curveAPI('Search', {'Token': $rootScope.Token, 'Data': JSON.stringify({"Type":"IDSearch", "UserID":search})}, onResult, onError);
		}
		else if(searchby == "fullname")
		{
			if(validateSearchValue(first, last))
				$scope.curveAPI('Search', {'Token': $rootScope.Token, 'Data': JSON.stringify({"Type":"FullNameSearch", "LastName":last, "FirstName":first})}, onResult, onError);
		}
		
		showSearching();
	};

	validateSearchValue = function(search, search2)
	{
		if(search == null || search.length < 2)
		{
			if(search2 == null || search2.length < 2)
			{
				alert("User search too short!");
				return false;
			}
		}
		
		return true;
	};

	showSearching = function()
	{
		$('#search_user_loading').css('display','');
		$('#search_user_box').css('display','none');
	};

	hideSearching = function()
	{
		$('#search_user_loading').css('display','none');
		$('#search_user_box').css('display','');
	};

	onResult = function(data)
	{	
		$scope.searchUsersData = [];
		hideSearching();
		if(Object.keys(data).length === 0){
			$scope.viewResStatus = 1;
			alert("No user was found...");
		}else{
			
			for(var i = 0 ; i < data.length ; i++){
				if(data[i].userid != null && data[i].userid != 'undefined' &&  data[i].userid != ''){
					$scope.searchUsersData.push(data[i]);
					
				}
			}
		}
	};

	uniqueUserID = function(data)
	{
		var resp = [];
		var ids = {};
		var ln = data.length;
		
		for(var i = 0 ; i < ln ; i++)
		{
			var cur = data[i];
			if(ids[cur.userid] == null)
			{
				ids[cur.userid] = 1;
				resp.push(cur);
			}
		}
		
		return resp;
	};
	
	$(document.body).keydown(function (event) {

		if (event.keyCode == 13) {
			
				if($('#users_search_input').is(':focus')){
					selectUserRefreshDashboards();
				}
				if(event.preventDefault) {
					event.preventDefault();
				}else{
					event.returnValue = false;
				}
			
		}
	});			
	
	$scope.reloadSingleTabFun = function(){
		selectUserRefreshDashboards();
	}
	
	selectUserRefreshDashboards = function()
	{
		if($scope.usersSearch.input != null && $scope.usersSearch.input != ''){
			$scope.reloadSingleTab($scope.usersSearch.input);
		}
	};

	$scope.selectUser = function(user)
	{
		$('#search-user').modal('hide');
		$scope.usersSearch.input = user;
		$scope.reloadSingleTab(user);
	};

	onError = function(data)
	{
		hideSearching();
	};

	$scope.UserSearchSearchBy = function(field)
	{
		if(field == "email")
		{
			searchby = field;
			$('#search_title')[0].innerHTML = 'Email';
			$('#search_field_single_user_fullname').css('display','none');
			$('#search_field_single_user').css('display','');
		}
		else if(field == "lastname")
		{
			searchby = field;
			$('#search_title')[0].innerHTML = 'Last Name';
			$('#search_field_single_user_fullname').css('display','none');
			$('#search_field_single_user').css('display','');
		}
		else if(field == "userid")
		{
			searchby = field;
			$('#search_title')[0].innerHTML = 'User ID';
			$('#search_field_single_user_fullname').css('display','none');
			$('#search_field_single_user').css('display','');
		}
		else if(field == "hardwareid")
		{
			searchby = field;
			$('#search_title')[0].innerHTML = 'Hardware ID';
			$('#search_field_single_user_fullname').css('display','none');
			$('#search_field_single_user').css('display','');
		}
		else if(field == "fullname")
		{
			searchby = field;
			$('#search_title')[0].innerHTML = 'Full Name';
			$('#search_field_single_user_fullname').css('display','');
			$('#search_field_single_user').css('display','none');
		}
	};
}