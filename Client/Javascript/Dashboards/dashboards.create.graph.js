function CreateGraphFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst,InsightConst){
// console.log('CreateGraphFunction')

	var graphType = "";
	var groupBy = "";
	var modifier = "";
	var sel_column = "";
	var dashboardID = "";
	var period = "Daily";
	var graphName = "Daily";

	var filtersByDivs = {};
	var tables = {};
	var columns = {};
	var descToSelectStrObj = {};
	
	var modifiersByColumn = {};
	var filtersByColumn = {};
	var groupByColumn = {};
	
	var global_emails = [];
	
	$scope.isGetCreateGraphData = false;
	$scope.isGetCustomTablesData = false;
	
$scope.getCreateGraphData = function(){
	
	$scope.curveAPI('GetCreateGraphData', {'Token': $rootScope.Token}, $scope.gotCreationData);
	$scope.curveAPI('GetCustomTablesData', {'Token': $rootScope.Token}, $scope.gotCustomTablesData);
	

}
// // ******************** GET DATA *****************************************************

$scope.gotCustomTablesData = function(data)
{	
	$scope.isGetCustomTablesData = true;

	var ln = data.length;
	for(var i = 0 ; i < ln ; i++)
	{
		var cur = data[i];
		var src = cur.SourceTable;
				
		if(tables[src] == null)
			tables[src] = [];
		
		tables[src].push(cur);
		
		var filter = cur.Filter_column_name;
		var selectStr = cur.Measure;
		var filterOpt = cur.Filter_Options;
		var desc = cur.Measure_Description;
		var measureField = cur.GroupByDate;
		
		var cols = desc.split(" ");
		var len = cols.length;
		
		if(len > 0)
		{
			var str = "";
			for(var j = 0 ; j < len - 1 ; j++)
			{
				var mod = cols[j];
				if(j != 0) str += " ";
				str += mod;
			}
						
			var col = cols[len-1];
			if(columns[col] == null)
			{
				columns[col] = {};
				columns[col].filters = {};
				columns[col].modifiers = {};
			}
			
			if(columns[col].filters[filter] == null)
				columns[col].filters[filter] = {'f':filter, 'o':filterOpt};
			
			if(columns[col].modifiers[str] == null)
				columns[col].modifiers[str] = str;
		}
		
		descToSelectStrObj[desc] = {'table': src, 'selectStr': selectStr, 'MeasureField':measureField};
	}
	
	onDataReady();
}

function onDataReady()
{
	// console.log("onDataReady", columns);
	var elements_code = '<li role="presentation" class="divider"></li>';
	
	for (var col in columns)
	{
		var modifiers_elements_code = '<li role="presentation" class="divider"></li>';
		var groupby_elements_code = '<li role="presentation" class="divider"></li>';
		var filter_elements_code = '<li role="presentation" class="divider"></li>';
		elements_code += getColumnElement(col);
		// console.log("elements_code", elements_code);
		
		var currnetColumn = columns[col];
		
		for (var mod in currnetColumn.modifiers)
		{
			modifiers_elements_code += getModifierElement(mod);
		}
		
		for (var filt in currnetColumn.filters)
		{
			groupby_elements_code += getGroupByElement(filt);
			filter_elements_code += getFilterByElement(filt, currnetColumn.filters[filt].o);
		}
		
		modifiersByColumn[col] = modifiers_elements_code;
		groupByColumn[col] = groupby_elements_code;
		filtersByColumn[col] = filter_elements_code;
	}
	
	jQuery(document).ready(function()
	{
		var div = document.getElementById('custom_events_dropdown');
		div.innerHTML = elements_code;
	});
}

// // ******************** PREPARE DATA FOR SENDING *************************************

function sendGraph()
{
	var data = getResultData();	
	if(data == null) return;
	
	$('.loading-page').show();
	$scope.curveAPI('CreateUserGeneratedGraph', data, graphRegistered, graphNotRegistered);
}

function graphRegistered(data)
{
	$('.loading-page').hide();
	show_message("Success", "Successfully created your new graph! Page will now relaod to display changes.", true);
	location.reload();
}

function graphNotRegistered(data)
{
	$('.loading-page').hide();
	$scope.show_error("FAILED", "For some reason your graph did not create. ERROR: " + data);
}

$scope.gotCreationData = function(data)
{	
	$scope.isGetCreateGraphData = true;
	jQuery(document).ready(function()
	{ 
		gotDashboardNames(data.Dashboards);
		gotGameUsers(data.Users);
	});
}

function getResultData()
{
	var graphDescription = document.getElementById('graph_description').value;
	
	var error = updateButtonStateAndValidateData();
	if(error != null)
	{
		$scope.show_error("Data invalid: " + error);
		return null;
	}
	
	var queryObject = getQueryObject();
	var users = getUsers();
	
	var data = {'Name' : graphName,
				'Dashboardid' : dashboardID,
				'Description' : graphDescription, 
				'GraphType' : graphType,
				'Query' : queryObject,
				'Users' : users
				};
	
	return {'Token': $rootScope.Token, 'Data': JSON.stringify(data)};
}

function getUsers()
{
	var st = $("#permissions_div").val();
	if(st == null)
		return [];
	
	return st.split(',');
}

function updateButtonStateAndValidateData()
{
	var error = validateGlobalData();
	
	if(error != null)
	{
		document.getElementById('conmfirm_btn').style.display = "none";
		document.getElementById('preview_btn').style.display = "none";		
	}
	else
	{
		document.getElementById('conmfirm_btn').style.display = "";
		document.getElementById('preview_btn').style.display = "";
	}
	// console.log('error', error)
	return error;
}

function validateGlobalData()
{
	if(sel_column == null || sel_column == "")
		return "You did not select the 'Event Name'.";
	if(modifier == null || modifier == "")
		return "'Modifier' not selected.";
	if(graphName == null || graphName == "")
		return "You did not provide the 'Graph Title'.";
	if(graphType == null || graphType == "")
		return "You did not select 'Graph Type'.";
	if(dashboardID == null || dashboardID == "")
		return "You did not select a 'Dashboard Name'.";
	
	if(graphType == 'BasicChart' || graphType == 'BarChart')
	{
		if(period == null || period == "")
			return "You did not select a 'Measure Period'.";
	}
	else if(graphType == 'PieChart')
	{
		if(groupBy == null || groupBy == "")
			return "You did not select 'Group By'.";
	}
	
	return null;
}

function getQueryObject()
{	
	var desc = modifier + " " + sel_column;
	var d = descToSelectStrObj[desc];
	var filters = getFilters();
	
	desc = desc.replace(/ /g,"_");
	
	var resp = {
			'SelectStr' : d.selectStr,
			'Description' : desc,
			'Table' : d.table,
			'Filters' : filters,
			'MeasureField' : d.MeasureField
	};
	
	if(graphType == 'BarChart' || graphType == 'BasicChart' || graphType == "amBarChart" || graphType == 'amLineChart')
	{
		resp.MeasurePeriod = period;
	}
	else if(graphType == 'PieChart' || graphType == 'amPieChart')
	{
		resp.GroupBy = groupBy;	
	}	
	
	return resp;
}

function getFilters()
{
	var definitions = [];
	
	for(var id in filtersByDivs)
	{
		var fltr = filtersByDivs[id];
		var data = getValidatedData(fltr);
		if(data != null) definitions.push(data);
	}
	
	return definitions;
}

function getValidatedData(fltr)
{
	if(fltr.functionlity == null || fltr.functionlity == "")
		return null;
	
	if(fltr.event == null || fltr.event == "")
		return null;
	
	if(fltr.freeValue == null || fltr.freeValue.length == 0)
		return null;
		
	return {'FilterFunctionality': fltr.functionlity, 'FilterField': fltr.event, 'FilterValues': fltr.freeValue};
}





// ******************************* ON DATA SELECTED ********************************

function titleChanged(grpName)
{
	graphName = grpName;
	updateButtonStateAndValidateData();
}

function groupbySelected(grpby)
{
	groupBy = grpby;
	
	var div = document.getElementById('group_by_title');
	div.innerHTML = groupBy + ' <i class="fa fa-angle-down">';
	updateButtonStateAndValidateData();
}

function columnSelected(column)
{
	sel_column = column;
	
	document.getElementById('stage_3').style.display = "";
	var div = document.getElementById('event_title');
	div.innerHTML = column + ' <i class="fa fa-angle-down">';
	resetModAndGrouping();
		
	var div = document.getElementById('custom_values_dropdown');
	div.innerHTML = modifiersByColumn[column];
	
	var div = document.getElementById('groupby_values_dropdown');
	div.innerHTML = groupByColumn[column];
		
	var div = document.getElementById('filter_by_event_dropdown');
	div.innerHTML = filtersByColumn[column];
	updateButtonStateAndValidateData();
}

function modifierSelected(mod)
{
	modifier = mod;
	var div = document.getElementById('values_title');
	div.innerHTML = mod + ' <i class="fa fa-angle-down">';
	updateButtonStateAndValidateData();
}

function resetModAndGrouping()
{
	groupBy = "";
	modifier = ""
	var div = document.getElementById('values_title');
	div.innerHTML = 'None <i class="fa fa-angle-down">';
	
	var div = document.getElementById('group_by_title');
	div.innerHTML = 'None <i class="fa fa-angle-down">';
}

$scope.graphTypeSelected = function(name, type)
{
	graphType = type;
	document.getElementById('stage_2').style.display = "";
	if(graphType == 'BarChart' || graphType == 'BasicChart')
	{
		document.getElementById('group_by').style.display = "none";
		document.getElementById('measure_period').style.display = "";
	}
	else if(graphType == 'PieChart')
	{
		document.getElementById('measure_period').style.display = "none";
		document.getElementById('group_by').style.display = "";
	}
	else if(graphType == 'CounterChart')
	{
		document.getElementById('measure_period').style.display = "none";
		document.getElementById('group_by').style.display = "none";
	}
	
	var div = document.getElementById('graph_type_title');
	div.innerHTML = name + ' <i class="fa fa-angle-down">';
	updateButtonStateAndValidateData();
}

function periodSelected(prd)
{
	period = prd;
	
	var div = document.getElementById('period_title');
	div.innerHTML = prd + ' <i class="fa fa-angle-down">';
	updateButtonStateAndValidateData();
}

function dashboardSelected(name, id)
{
	dashboardID = id;
	
	var div = document.getElementById('dashboard_title');
	div.innerHTML = name + ' <i class="fa fa-angle-down">';
	updateButtonStateAndValidateData();
}

// ***************** FILTER MODAL HANDLERS ***************************************

var filterTempEvent = "";
var filterTempOption = "";
var tempFilterValue = "";
var filterTempID = "";
var filterTempName = "";
var filterLineID = 0;

function filterEventSelected(name, opt)
{
	filterTempEvent = name;
	filterTempOption = opt;
	
	var div = document.getElementById('filter_event_title');
	div.innerHTML = name + ' <i class="fa fa-angle-down">';
}

function filterFunctionalitySelected(name, id, numFreeValues)
{
	filterTempID = name == "None" ? "" : id;
	filterTempName = name == "None" ? "" : name;
	var div = document.getElementById('filter_functionality_title');
	div.innerHTML = name + ' <i class="fa fa-angle-down">';
	
	showValueFields(numFreeValues);
}

function resetValueFields()
{
	document.getElementById('free_value_0').value = "";
	document.getElementById('free_value_1').value = "";
	document.getElementById('free_value_2').value = "";
	document.getElementById('filter_free_values_1_and_eq_val').checked = true;
	document.getElementById('filter_free_values_2_and_eq_val').checked = true;
}

function showValueFields(num)
{
	resetValueFields();
	document.getElementById('filter_free_values_0').style.display = 'none';
	document.getElementById('filter_free_values_1').style.display = 'none';
	document.getElementById('filter_free_values_2').style.display = 'none';
	
	if(num == 0) document.getElementById('filter_free_values_0').style.display = '';
	if(num > 0) document.getElementById('filter_free_values_1').style.display = '';	
	if(num > 1)	document.getElementById('filter_free_values_2').style.display = '';	
}

function createFilterLine(name, modifier, val)
{
	var mod = modifier == "" ? "" : '(Mod: "' + modifier + '" | Val: [' + val + '])';
	var div = document.createElement("div");
	div.className = "filter_line";
	div.id = "filter_line_"	+ (filterLineID++);
	div.innerHTML = '<div class="filter_delete" onclick="removeLine(this);"><i class="fa fa-times"></i></div><div class="filter_title">'
	+ name + '</div><div class="filter_title">' + mod + '</div>';
	
	return div;
}

function addFilterLine()
{
	var val0 = document.getElementById('free_value_0').value;
	var val1 = document.getElementById('free_value_1').value;
	var val2 = document.getElementById('free_value_2').value;
	
	var data_val0 = (val0 == null || val0 == "") ? null : {Value:val0};
	var data_val1 = (val1 == null || val1 == "") ? null : {Equals:document.getElementById('filter_free_values_1_and_eq_val').checked, Value:val1};
	var data_val2 = (val2 == null || val2 == "") ? null : {Equals:document.getElementById('filter_free_values_2_and_eq_val').checked, Value:val2};
	
	var freeval_data = [];
	if(data_val0 != null) freeval_data.push(data_val0);
	if(data_val1 != null) freeval_data.push(data_val1);
	if(data_val2 != null) freeval_data.push(data_val2);
	
	if(!verifyFilterData(freeval_data))
	{
		$scope.show_warn("Data for filter is not complete. Filter cretion aborted.");
		return;
	}
	
	var val = "";
	if(data_val0 != null)
	{
		val = val0;
	}
	else if(data_val1 != null)
	{
		if(data_val2 != null)
		{
			val = val1+"," + val2;
		}
		else
		{
			val = val1;
		}
	}
	
	var div = document.getElementById('filters_scroll');
	var line = createFilterLine(filterTempEvent, filterTempName, val);
	filtersByDivs[line.id] = {div:line, event:filterTempEvent, functionlity:filterTempID, freeValue:freeval_data, options:filterTempOption};
	div.appendChild(line);
	resetFilterModal();
}

function verifyFilterData(val)
{
	if(filterTempEvent == null || filterTempEvent == "")
		return false;
	
	if(filterTempID == null || filterTempID == "")
		return false;
		
	if(val == null || val == "" || val.length == 0)
		return false;
	
	return true;
}

// **********************************************************************************

function removeLine(dv)
{
	// console.log(filtersByDivs);
	// console.log(dv);
	delete filtersByDivs[dv.id];
	// console.log(filtersByDivs);
	dv.parentNode.remove();
}

// ****************************** LOADING VISUAL HANDLER

function showMainModalLoading()
{
}

function hideMainModalLoading()
{
}

function showFilterModalLoading()
{
}

function hideFilterModalLoading()
{
}

// ****************************** CLEANUPS *****************************************************

$scope.resetMainModal = function()
{	
	if($scope.isGetCreateGraphData === false || $scope.isGetCustomTablesData === false){
		$scope.getCreateGraphData();
	}
	graphType = "";
	dashboardID = "";
	graphType = "";
	groupBy = "";
	modifier = "";
	sel_column = "";
	
	var div = document.getElementById('stage_2');
	div.style.display = "none";

	var div = document.getElementById('stage_3');
	div.style.display = "none";

	document.getElementById('graph_title').value = "";
	document.getElementById('graph_description').value = "";
	
	var div = document.getElementById('values_title').innerHTML = 'None <i class="fa fa-angle-down">';
	var div = document.getElementById('event_title').innerHTML = 'None <i class="fa fa-angle-down">';
	var div = document.getElementById('graph_type_title').innerHTML = 'None <i class="fa fa-angle-down">';
	var div = document.getElementById('group_by_title').innerHTML = 'None <i class="fa fa-angle-down">';
	var div = document.getElementById('dashboard_title').innerHTML = 'None <i class="fa fa-angle-down">';
	
	resetFilterModal();	
	clearFilterLines();
	updateButtonStateAndValidateData();
	$("#permissions_div").val([]).trigger("change");
}

function resetFilterModal()
{
	filterTempEvent = "";
	filterTempOption = "";
	filterTempCategory = "";
	filterTempID = "";
	filterTempName = "";
	
	var div = document.getElementById('filter_event_title').innerHTML = 'None <i class="fa fa-angle-down">';	
	var div = document.getElementById('filter_functionality_title').innerHTML = 'None <i class="fa fa-angle-down">';
	// var div = document.getElementById('filter_value_title').innerHTML = 'None <i class="fa fa-angle-down">';
	resetValueFields();
	showValueFields(-1);
	
	var div = document.getElementById('stage_2_filters');
	div.style.display = "none";
}

function clearFilterLines()
{
	for(var id in filtersByDivs)
	{
		var filter = filtersByDivs[id];
		if(filter.div != null)
			filter.div.remove();
	}
	
	filtersByDivs = {};
}

// ********************* PAINTERS -- GET ELEMENTS ***************************************************

/* function getDashboardLine(name, id)
{
	return '<li role="presentation" style="width:100%"><a role="menuitem" onclick="dashboardSelected(\'' + name + '\', \'' + id + '\');" tabindex="-1">' + name + '</a></li>';
} */

function getColumnElement(name)
{
	return '<li role="presentation" style="width:100%"><a role="menuitem" tabindex="-1" onclick="columnSelected(\'' + name + '\');">' + name + '</a></li>';
}

function getModifierElement(name)
{
	return '<li role="presentation" style="width:100%"><a role="menuitem" tabindex="-1" onclick="modifierSelected(\'' + name + '\');">' + name + '</a></li>';
}

function getGroupByElement(name)
{
	return '<li role="presentation" style="width:100%"><a role="menuitem" tabindex="-1" onclick="groupbySelected(\'' + name + '\');">' + name + '</a></li>';
}

function getFilterByElement(name, opt)
{
	return '<li role="presentation" style="width:100%"><a role="menuitem" tabindex="-1" onclick="filterEventSelected(\'' + name + '\', \'' + opt + '\');">' + name + '</a></li>';
}


}