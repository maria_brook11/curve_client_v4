function FunnelUiFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst,InsightConst){

	/********************************* popup create funnel **************************************/
	
	$scope.isGetFunnelEvents = false;
	
	$scope.createFunnelModalOpenFun = function()
	{	
		$scope.funnelModal = {'step' : 1, 'actionArr' : [],'checkEvent' : [], 'dashboarDatadArr' : [],'DashboardName' : '', 'userDataArr' : [],'data' : 
				{
					'Name':'',
					'Description':'',
					'Dashboardid':'',
					'Users':[],
					'GraphType':"Funnel",
					'Query':{
						'FirstAction':'',
						'Actions':[]
						}
				}
			};
			
		if($scope.isGetFunnelEvents === false){
			$scope.curveAPI('GetFunnelEvents', {'Token': $rootScope.Token}, $scope.gotFunnelEventsData);
		}else{
			$scope.funnelModal.eventDataArr = $scope.eventDataArr;
		}
		$scope.curveAPI('GetCreateGraphData', {'Token': $rootScope.Token}, $scope.GetCreateGraphDataSuccess);
		
	}
	
	$scope.GetCreateGraphDataSuccess = function(data){
		
		$scope.dashboarDatadArr = [];
		for(var i=0 ; i < data.Dashboards.length ; i++){
			var temp = {'name' : data.Dashboards[i].name, 'id' : data.Dashboards[i].id };
			$scope.dashboarDatadArr.push(temp);
		}
		
		var myemail = $scope.loadFromCache("email");
		$scope.userDataArr = [];
		for(var i=0 ; i < data.Users.length ; i++){

			if(data.Users[i].email != myemail)
			{	
				var temp = {"perm": data.Users[i].permission, "id": data.Users[i].email,"name": data.Users[i].email,"add" : false};
				$scope.userDataArr.push(temp);
			}
		}
		$scope.funnelModal.dashboarDatadArr = $scope.dashboarDatadArr;
		$scope.funnelModal.userDataArr = $scope.userDataArr;
	}
	
	$scope.gotFunnelEventsData = function(data)
	{	
		
		$scope.isGetFunnelEvents = true;
		
		//data = [{"event":"Clicks","action":"MenuBtn"},{"event":"Clicks","action":"UpgradeHat"},{"event":"Clicks","action":"GameOptions"},{"event":"Clicks","action":"ShowMap"},{"event":"Clicks","action":"Help"},{"event":"Clicks","action":"NotificationsSettings"},{"event":"Clicks","action":"BuyPackage_500"},{"event":"Clicks","action":"LocaleSelect"},{"event":"Clicks","action":"ShareResult"},{"event":"Clicks","action":"BuildWearhouse"},{"event":"Clicks","action":"UpdateStatus"},{"event":"Clicks","action":"BuyPackage_50"},{"event":"Clicks","action":"SendGift"},{"event":"Clicks","action":"SetUserID"},{"event":"Clicks","action":"SimilarItems"},{"event":"Clicks","action":"SoundOn"},{"event":"Clicks","action":"NewItems"},{"event":"Clicks","action":"UpgradeShirt"},{"event":"Clicks","action":"SetUserModel"},{"event":"Clicks","action":"NextBtn"},{"event":"Clicks","action":"Shoot"},{"event":"Clicks","action":"SoundOff"},{"event":"Clicks","action":"EditProfile"},{"event":"Clicks","action":"SelectItem"},{"event":"Clicks","action":"UndoPrevAction"},{"event":"Clicks","action":"SendGiftOptions"},{"event":"Clicks","action":"UploadImage"},{"event":"CustomEvent","action":"GiftRecieved"},{"event":"CustomEvent","action":"LevelStart"},{"event":"CustomEvent","action":"ServerDisconnected"},{"event":"CustomEvent","action":"LevelReached"},{"event":"Install","action":"Install"},{"event":"Login","action":"Login"},{"event":"Purchase","action":"Purchase"},{"event":"Screens","action":"AdditionalSettingsScreen"},{"event":"Screens","action":"LevelScreen"},{"event":"Screens","action":"InviteUserPopup"},{"event":"Screens","action":"MainMenuScreen"},{"event":"Screens","action":"RegistrationScreen"},{"event":"Screens","action":"CashierScreen"},{"event":"Screens","action":"SettingsScreen"},{"event":"Screens","action":"TutorialScreen"},{"event":"Screens","action":"GameLobbyScreen"},{"event":"Screens","action":"ErrorPopup"},{"event":"Screens","action":"SupportPopup"},{"event":"Screens","action":"SelectItemPopup"},{"event":"Screens","action":"MyInfoScreen"}];
		
		$scope.eventDataArr = [];
		var keys = [data[0].event];
		
		for(var k = 1 ; k < data.length ; k++){
			if(keys.indexOf(data[k].event) == -1){
				keys.push(data[k].event);
			}
		}
		
		for(var a = 0 ; a < keys.length ; a++){
			$scope.eventDataArr.push({'event' : keys[a] , 'actions' : []})
		}
		
		for(var i = 0 ; i < data.length ; i++){
			for(var j = 0 ; j < $scope.eventDataArr.length ; j++){
				if($scope.eventDataArr[j].event == data[i].event){
					$scope.eventDataArr[j].actions.push(data[i].action);
				}
			}		
		}
		
		$scope.funnelModal.eventDataArr = $scope.eventDataArr;
	
	}
	
	$scope.funnelModalSaveUsersArrFun = function(){
		
		var allUsers = $('#funnelModalStep2 multiselect-dropdown .objValue').val();
		if(allUsers != 'undefined' && allUsers != null ){
			
			var allUsersArr = JSON.parse(allUsers);
			
			if(allUsersArr.length > 0){
				
				var u = [];
				angular.forEach(allUsersArr, function (val,key){
					if(val.add){
						u.push(val.name);
					} 
				})
				
				$scope.funnelModal.data.Users = u;
			}
			
		}
	}
	
	$scope.addEventFun = function(e,a){
		var temp = {'label' : e ,'act' : a};
		$scope.funnelModal.checkEvent.push(temp);
		$scope.funnelModal.checkType = '';
		$scope.funnelModal.checkAction = '';
	}
	
	$scope.delEventFun = function(i){
		
		$scope.funnelModal.checkEvent.splice(i, 1);

	}
	
	$scope.previewFunnelFun = function(){
		
		$scope.funnelModal.data.Query.FirstAction = $scope.funnelModal.checkEvent[0].act;
		for(var i = 1 ; i < $scope.funnelModal.checkEvent.length ; i++){
			$scope.funnelModal.data.Query.Actions.push($scope.funnelModal.checkEvent[i].act);
		}
		
		var data = {'Token': $rootScope.Token, 'Data': JSON.stringify($scope.funnelModal.data)};
		
		$scope.curveAPI('GetFunnelPreview', {'Token': $rootScope.Token, 'Data': JSON.stringify(data)}, GetFunnelPreviewSuccess, GetFunnelPreviewError);
		
	}
	
	function GetFunnelPreviewSuccess(data){
		
		$scope.funnelModal.prevFunnelData = data.chartData.ReportDataTable.Data.Records;
		
	}
	
	function GetFunnelPreviewError(data){
		
		alert(data)
	}
	
	$scope.createFunelFun = function(){
		if($scope.funnelModal.data.Query.FirstAction == ''){
			$scope.funnelModal.data.Query.FirstAction = $scope.funnelModal.checkEvent[0].act;
			for(var i = 1 ; i < $scope.funnelModal.checkEvent.length ; i++){
				$scope.funnelModal.data.Query.Actions.push($scope.funnelModal.checkEvent[i].act);
			}
		}
		
		var data = {'Token': $rootScope.Token, 'Data': JSON.stringify($scope.funnelModal.data)};
		
		$scope.curveAPI('CreateUserFunnel', data, CreateUserFunnelSuccess, CreateUserFunnelError);
		$('#createFunnelModal').modal('hide');
	}

	function CreateUserFunnelError(data)
	{
		//$('.loading-page').hide();
		$scope.show_error("FAILED", "For some reason your funnel did not create. ERROR: " + data);
	}

	function CreateUserFunnelSuccess(data)
	{
		$scope.show_message("Success", "Successfully created your new funnel! Page will now relaod to display changes.", true);
		$scope.dashboardStart();
	}
	/********************************* end popup create funnel **************************************/

	var MAX_KPI_LENGTH = 10;
	$scope.eventsByActions = {};
	$scope.funnelEventsActions = [];
	$scope.funnelEvents = [];
	$scope.funnelSelectedAction = [];
	$scope.funnelSelectedEvent = [];
	$scope.funnelDashboardNames;
	var dashboardID;
	

	$scope.dashboardFunnelSelected = function(name, id)
	{
		dashboardID = id;
		
		var div = document.getElementById('dashboard_funnel_title');
		div.innerHTML = name + ' <i class="fa fa-angle-down">';
		updateFunnelButtonStateAndValidateData();
	}

	$scope.updateFunnelPermissions = function(lines)
	{
		document.getElementById('permissions_div_funnel').innerHTML = lines;
		$("#permissions_div_funnel").val([]).trigger("change");
	}
	
	function getFunnelUsers()
	{
		var tempVal = $('#newFunnelUsersArr multiselect-dropdown .objValue').val();
		var tempArr = JSON.parse(tempVal);
		var uMails = [];
		for(var i = 0; i < tempArr.length; i ++){
			if(tempArr[i].add == true){
				uMails.push(tempArr[i].name);
			}
		}
		return uMails;
		
	}
	
	$scope.resetFunnelEventModal = function()
	{
		
		funnel_edited_div = null;
		funnel_edited_index = -1;
		$scope.funnelSelectedAction = '';
		$scope.funnelSelectedEvent = '';
		document.getElementById('funnel_event_type_title').innerHTML = 'Funnel Event Type <i class="fa fa-angle-down"></i>';
		document.getElementById('funnel_event_title').innerHTML = 'Select Event <i class="fa fa-angle-down"></i>';
		
		var ln = $scope.funnelEvents.length;
		
		if(ln >= MAX_KPI_LENGTH)
		{
			alert("Maximum step count " +MAX_KPI_LENGTH+" exceeded. No event was added.");
			return;
		}
		
		$('#add_funnel_events').modal('show');
	}

	$scope.funnelActionTypeSelected = function(name)
	{
		
		$scope.funnelSelectedAction = name;
		document.getElementById('funnel_event_type_title').innerHTML = name + ' <i class="fa fa-angle-down"></i>';
		
		var events = $scope.eventsByActions[name];
		
		document.getElementById('funnel_event_title').innerHTML = 'Select Event <i class="fa fa-angle-down"></i>';
		
		if(events.length == 1)	
			$scope.funnelEventSelected(events[0]);
	}

	$scope.funnelEventSelected = function(name)
	{
		$scope.funnelSelectedEvent = name;
		
		document.getElementById('funnel_event_title').innerHTML = truncateFunnelEvent(name) + ' <i class="fa fa-angle-down"></i>';
	}

	$scope.addFunnelEvent = function()
	{
		if($scope.funnelSelectedAction == "" || $scope.funnelSelectedEvent == "")
		{
			$scope.show_error("No event was selected", "No event was selected, so no event will be added to funnel.")
			return;
		}
		
		// Handle editing
		if(funnel_edited_div != null && funnel_edited_index != -1)
		{	
			funnel_edited_div.parentNode.innerHTML = createFunnelEventLine($scope.funnelSelectedAction, $scope.funnelSelectedEvent).innerHTML;
			$scope.funnelEvents[funnel_edited_index].action = $scope.funnelSelectedAction;
			$scope.funnelEvents[funnel_edited_index].event = $scope.funnelSelectedEvent;
			return;
		}
		
		var ln = $scope.funnelEvents.length;
		
		if(ln >= MAX_KPI_LENGTH)
		{
			alert("Maximum step count " +MAX_KPI_LENGTH+" exceeded. Adding event was discarded.");
			return;
		}
		
		for(var i = 0 ; i < ln ; i++)
		{
			if($scope.funnelEvents[i].event == $scope.funnelSelectedEvent)
			{
				alert("Event: '" + $scope.funnelSelectedEvent + "' already exists on this funnel. No event was added.");
				return;
			}
		}
		
		var div = document.getElementById('funnel_events_scroll');
		var line = createFunnelEventLine($scope.funnelSelectedAction, $scope.funnelSelectedEvent);
		$scope.funnelEvents.push({action:$scope.funnelSelectedAction, event:$scope.funnelSelectedEvent, id: line.id});
		div.appendChild(line);
	}

	$scope.gotFunnelDashboardNames = function(data)
	{	
		$scope.funnelDashboardNames = data;
		
	}

	var funnel_edited_index = -1;
	var funnel_edited_div = null;

	function editFunnelLine(dv)
	{
		var res = -1;
		for(var i = 0 ; i < $scope.funnelEvents.length ; i++)
		{
			if($scope.funnelEvents[i].id == dv.parentNode.id)
			{
				res = i;
				break;
			}
		}
		
		if(res != -1)
		{
			resetFunnelEventModal();
			funnel_edited_index = res;
			funnel_edited_div = dv;
			var type = $scope.funnelEvents[res].action;
			var ev = $scope.funnelEvents[res].event;
			funnelActionTypeSelected(type);
			$scope.funnelEventSelected(ev);
			$('#add_funnel_events').modal('show');
		}
		else
		{
			alert("Funnel step maleformed. It will be deleted.");
			removeFunnelLine(dv);
		}
	}

	function removeFunnelLine(dv)
	{
		var res = -1;
		for(var i = 0 ; i < $scope.funnelEvents.length ; i++)
		{
			if($scope.funnelEvents[i].id == dv.parentNode.id)
			{
				res = i;
				break;
			}
		}
		
		if(res != -1)
			$scope.funnelEvents.splice(res, 1);
		
		dv.parentNode.remove();
	}

	// .................. PREVIEW / SUBMIT DATA ................................

	$scope.sendFunnel = function()
	{
		var data = getFunnelData('sendFunnel');	
		
		if(data == null)
		{
			var error = updateFunnelButtonStateAndValidateData();
			$scope.show_error("Data not valid", error);
			return;
		}
		
		$('#custom-funnel').modal('hide');
		$('.loading-page').show();
		$scope.curveAPI('CreateUserFunnel', data, funnelRegistered, funnelNotRegistered);
	}

	function funnelNotRegistered(data)
	{
		$('.loading-page').hide();
		$scope.show_error("FAILED", "For some reason your funnel did not create. ERROR: " + data);
	}

	function funnelRegistered(data)
	{
		$scope.show_message("Success", "Successfully created your new funnel! Page will now relaod to display changes.", true);
		$scope.dashboardStart();
	}

	// ......... PREVIEW ...............

	$scope.previewFunnel = function()
	{
		var data = getFunnelData('previewFunnel');
		
		if(data == null)
		{
			$scope.show_error("Data not valid", "you must select two funnel events or more!");
			return;
		}
		
		//$scope.cleanPreviewDivs();
		//document.getElementById('container_preview_funnel').style.display = '';
		
		$('#preview-graph').modal('show');
		$scope.curveAPI('GetFunnelPreview', {'Token': $rootScope.Token, 'Data': JSON.stringify(data)}, previewFunnelComplete, previewFunnelFailed);
	}

	function previewFunnelFailed(data)
	{
		$scope.show_error("Could not show preview", data);
	}

	function previewFunnelComplete(data)
	{
		
		 $scope.prevFunnelName = document.getElementById('funnel_title').value;
		 $scope.prevFunnelDescription = document.getElementById('funnel_description').value;
		
		if($scope.prevFunnelName == null || $scope.prevFunnelName == '')
			$scope.prevFunnelName = 'No Title';
		
		if($scope.prevFunnelDescription == null || $scope.prevFunnelDescription == '')
			$scope.prevFunnelDescription = 'No Description';
		
		$scope.prevFunnelData = data;
		/*document.getElementById('preview_title_funnel').innerHTML = funnelName + " - " + funnelDescription;
		
		$scope.drawFunnelData(data.chartData.ReportDataTable, '#chart_preview_funnel'); */
	}

	function getFunnelData(e)
	{	
		var funnelName = document.getElementById('funnel_title').value;
		var funnelDescription = document.getElementById('funnel_description').value;
		if(funnelDescription == null)
			funnelDescription = '';
		if(e != 'previewFunnel'){
			var error = updateFunnelButtonStateAndValidateData();
			if(error != null)
			{
				$scope.show_error("Data invalid: " + error);
				return null;
			}
		}
		var queryObject = getFunnelQueryObject();
		var users = getFunnelUsers();
		
		var data = {'Name' : funnelName,
					'Dashboardid' : dashboardID,
					'Users' : users,
					'Description' : funnelDescription, 
					'GraphType' : 'Funnel',
					'Query' : queryObject,
					};
		
		return {'Token': $rootScope.Token, 'Data': JSON.stringify(data)};
	}

	

	function getFunnelQueryObject()
	{	
		if($scope.funnelEvents.length > 1)
		{
			var resp = {'FirstAction': $scope.funnelEvents[0].event, 'Actions':[]};
			for(var i = 1 ; i < $scope.funnelEvents.length ; i++)
			{
				resp.Actions.push($scope.funnelEvents[i].event);
			}
			
			if(resp.FirstAction != null && resp.FirstAction != '' && resp.Actions.length > 0 && resp.Actions[0] != null && resp.Actions[0] != '')
				return resp;
		}
		
		return null;
	}

	// .................. PREVIEW / SUBMIT DATA .........................
	// ...................... VALIDATIONS ...............................

	function updateFunnelButtonStateAndValidateData()
	{
		return validateFunnelGlobalData();
		// var error = validateFunnelGlobalData();
		
		// if(error != null)
		// {
			// document.getElementById('funnel_confirm').style.display = "none";
			// document.getElementById('funnel_preview').style.display = "none";		
		// }
		// else
		// {
			// document.getElementById('funnel_confirm').style.display = "";
			// document.getElementById('funnel_preview').style.display = "";
		// }
		
		// return error;
	}

	function validateFunnelGlobalData()
	{
		var data = getFunnelQueryObject();
		
		if(document.getElementById('funnel_title').value == null || document.getElementById('funnel_title').value == "")
			return "You did not provide the 'Funnel Title'.";
		if(dashboardID == null || dashboardID == "")
			return "You did not select a 'Dashboard Name'.";
		if(data == null)
			return "Data not valid, you must select two funnel events or more!";
		
		
		return null;
	}

	// ...................... VALIDATIONS ...............................

	// ********************* PAINTERS -- GET ELEMENTS ***************************************************


	function truncateFunnelEvent(name)
	{	
		if(name.length > 24)
			return name.substr(0, 24) + "..";
		
		return name;
	}

	function truncateFunnelEventMax(name, max)
	{
		if(name.length > max)
			return name.substr(0, max) + "..";
		
		return name;
	}

	var funnelLineID = 0;
	function createFunnelEventLine(action, event)
	{
		var data = action + ': "' + truncateFunnelEventMax(event, 35) + '"';
		var title = action + ": '" + event + "'";
		var div = document.createElement("div");
		div.className = "filter_line";
		div.id = "funnel_line_"	+ (funnelLineID++);
		div.innerHTML = '<div class="filter_delete" title="Delete funnel step" onclick="removeFunnelLine(this);"><i class="fa fa-times"></i></div><div class="filter_delete" title="Edit funnel step" onclick="editFunnelLine(this);"><i class="fa fa-pencil"></i></div><div class="filter_title" title="'+title+'">'
		+ name + ' </div><div class="filter_title">' + data + '</div>';
		
		return div;
	}

	// ...................... FUNNEL STEPS ...............................

	var currentFunnelStep = 1;

	function resetFunnelSteps()
	{
		currentFunnelStep = 1;
		firstFunnelStep();
	}

	$scope.backFunnelStep = function()
	{
		if(currentFunnelStep <= 1)
			return
		
		// Select next step
		currentFunnelStep--;
		
		// Go to next step
		if(currentFunnelStep == 1)
			firstFunnelStep();
		if(currentFunnelStep == 2)
			secondFunnelStep();
		if(currentFunnelStep == 3)
			thirdFunnelStep();
		if(currentFunnelStep == 4)
			fourthFunnelStep();
	}

	$scope.nextFunnelStep = function()
	{
		// Verify current step
		if(currentFunnelStep == 1 && !verifyFirstFunnelStep())
			return;
		if(currentFunnelStep == 2 && !verifySecondFunnelStep())
			return;
		if(currentFunnelStep == 3 && !verifyThirdFunnelStep())
			return;
		if(currentFunnelStep == 4 && !verifyFourthFunnelStep())
			return;
		
		// Select next step
		currentFunnelStep++;
		
		// Go to next step
		if(currentFunnelStep == 1)
			firstFunnelStep();
		if(currentFunnelStep == 2)
			secondFunnelStep();
		if(currentFunnelStep == 3)
			thirdFunnelStep();
		if(currentFunnelStep == 4)
			fourthFunnelStep();
	}

	function verifyFirstFunnelStep()
	{
		if(document.getElementById('funnel_title').value == null || document.getElementById('funnel_title').value == "")
		{
			$scope.show_error("Cant go to next step", "You did not provide the 'Funnel Title'.");
			return false;
		}
		
		
		return true;	
	}

	function verifySecondFunnelStep()
	{
		var data = getFunnelQueryObject();
		if(data == null)
		{
			$scope.show_error("Cant go to next step", "Data not valid, you must select two funnel events or more!");
			return false;
		}
		
		
		return true;
	}

	function verifyThirdFunnelStep()
	{
		if(dashboardID == null || dashboardID == "")
		{
			$scope.show_error("Cant go to next step", "You did not select a 'Dashboard Name'.");
			return false;
		}
		
		return true;
	}

	function verifyFourthFunnelStep()
	{
		return true;
	}

	function firstFunnelStep()
	{
		//document.getElementById('funnel_title').value = "";
		//document.getElementById('funnel_description').value = "";
		document.getElementById('funnel_modal_title').innerHTML = "Step 1: Give a title";
		document.getElementById('funnel_modal_desc').innerHTML = "Select the title and description for user funnel. Title is mandatory.";
		document.getElementById('funnel_step_1').style.display = '';
		document.getElementById('funnel_step_2').style.display = 'none';
		document.getElementById('funnel_step_3').style.display = 'none';
		document.getElementById('funnel_step_4').style.display = 'none';
		
		document.getElementById('funnel_confirm').style.display = 'none';
		document.getElementById('funnel_preview').style.display = 'none';
		document.getElementById('funnel_next').style.display = '';
		document.getElementById('funnel_back').style.display = 'none';
	}

	function secondFunnelStep()
	{
		document.getElementById('funnel_modal_title').innerHTML = "Step 2: Choose KPIs";
		document.getElementById('funnel_modal_desc').innerHTML = "Select your funnel steps. must add at least 2 steps.";
		document.getElementById('funnel_step_1').style.display = 'none';
		document.getElementById('funnel_step_2').style.display = '';
		document.getElementById('funnel_step_3').style.display = 'none';
		document.getElementById('funnel_step_4').style.display = 'none';
		
		
		document.getElementById('funnel_confirm').style.display = 'none';
		document.getElementById('funnel_preview').style.display = 'none';
		document.getElementById('funnel_next').style.display = '';
		document.getElementById('funnel_back').style.display = '';
	}

	function thirdFunnelStep()
	{
		document.getElementById('funnel_modal_title').innerHTML = "Step 3: Choose funnel location";
		document.getElementById('funnel_modal_desc').innerHTML = "Select the dashboard this graph will be stored into. This action CANT be changed later.";
		document.getElementById('funnel_step_1').style.display = 'none';
		document.getElementById('funnel_step_2').style.display = 'none';
		document.getElementById('funnel_step_3').style.display = '';
		document.getElementById('funnel_step_4').style.display = 'none';
		
		document.getElementById('funnel_preview').style.display = '';
		document.getElementById('funnel_confirm').style.display = '';
		document.getElementById('funnel_next').style.display = '';
		document.getElementById('funnel_back').style.display = '';
	}

	function fourthFunnelStep()
	{
		document.getElementById('funnel_modal_title').innerHTML = "Step 4: Give permissions";
		document.getElementById('funnel_modal_desc').innerHTML = "Select which users will be able to see your funnel";
		document.getElementById('funnel_step_1').style.display = 'none';
		document.getElementById('funnel_step_2').style.display = 'none';
		document.getElementById('funnel_step_3').style.display = 'none';
		document.getElementById('funnel_step_4').style.display = '';
		
		document.getElementById('funnel_preview').style.display = '';
		document.getElementById('funnel_confirm').style.display = '';
		document.getElementById('funnel_next').style.display = 'none';
		document.getElementById('funnel_back').style.display = '';
	}

}