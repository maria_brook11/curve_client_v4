app.directive('calendar', function () {
            return {
				restrict: 'A',
                scope: true,
                link: function (scope, el, attr) {
	
	var curID = el.parent().attr("id");
	var date_today = moment.utc().subtract(moment().utcOffset(), 'minutes').format('MM/DD/YYYY');	
	var date_2_days_ago = moment.utc().subtract(2, 'days').subtract(moment().utcOffset(), 'minutes');	
		
	/* if( $('#dashboard-report-range') != null)
	{ */	
		
		$('#'+curID+' .thin').html(getDefaultDatePickerRange());
		$('#'+curID+' div').show();
		
		$('#'+curID+' div').daterangepicker(
			
			{
				opens: 'left',
				startDate: moment.utc().subtract(29, 'days').startOf('day').subtract(moment().utcOffset(), 'minutes'),
				endDate: moment.utc().subtract(moment().utcOffset(), 'minutes'),
				minDate: '01/01/2012',
				maxDate: date_today,
				dateLimit: { days: 120 },
				showDropdowns: false,
				showWeekNumbers: true,
				timePicker: false,
				timePickerIncrement: 1,
				timePicker12Hour: true,
				ranges: {
					'Yesterday': [moment.utc().startOf('day').subtract(moment().utcOffset(), 'minutes'), moment.utc().subtract(1, 'days').hour(23).minutes(59).seconds(59).subtract(moment().utcOffset(), 'minutes')],
					'Last 7 Days': [moment.utc().subtract(6, 'days').startOf('day').subtract(moment().utcOffset(), 'minutes'), moment.utc().subtract(moment().utcOffset(), 'minutes')],
					'Last 30 Days': [moment.utc().subtract(29, 'days').startOf('day').subtract(moment().utcOffset(), 'minutes'), moment.utc().subtract(moment().utcOffset(), 'minutes')],
					'This Month': [moment.utc().startOf('month').subtract(moment().utcOffset(), 'minutes'), moment.utc().endOf('month').subtract(moment().utcOffset(), 'minutes')],
					'Last Month': [moment.utc().subtract(1, 'month').startOf('month').subtract(moment().utcOffset(), 'minutes'), moment.utc().subtract(1, 'month').endOf('month').subtract(moment().utcOffset(), 'minutes')]
				},
				buttonClasses: ['btn btn-sm'],
				applyClass: ' blue',
				cancelClass: 'default',
				format: 'MM/DD/YYYY',
				separator: ' to ',
				locale: {
					applyLabel: 'Apply',
					fromLabel: 'From',
					toLabel: 'To',
					customRangeLabel: 'Custom Range',
					daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
					monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
					firstDay: 1
				}
			},
			function (start, end) {
				$('#dashboard-report-range span').html(start.format('MMM D') + ' - ' + end.format('MMM D, YYYY'));
				if(scope.dashboardView.active == 1){
					scope.singleDate = false;
					scope.rangeDate = true;
					scope.getInterStatus();
				}else{
					scope.setTabFun(scope.activeTabId,scope.activeTabName);
				}
			}
					
		);
		$('.daterangepicker').append( "<p style='font-size: 13px;padding-top: 10px;color: #666666;' >Maximum period 4 months</p>" );
		
	//}
	
	/* if( $('#dashboard-report-single-date') != null)
	{
		$('#'+curID+' .thin').html(getDefaultDate());
		$('#'+curID+' div').show();
		
		$('#'+curID+' div').daterangepicker(
		{
				opens: 'left',
				showDropdowns: false,
				showWeekNumbers: true,
				timePicker: false,
				timePickerIncrement: 1,
				timePicker12Hour: true,
				buttonClasses: ['btn btn-sm'],
				applyClass: ' blue',
				cancelClass: 'default',
				singleDatePicker: true,
				format: 'MM/DD/YY',
				startDate: date_2_days_ago,
				maxDate: date_today,
				locale: {
							applyLabel: 'Apply',
							fromLabel: 'From',
							toLabel: 'To',
							customRangeLabel: 'Custom Range',
							daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
							monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
							firstDay: 1
						}
			},
			function (date) {
				$('#dashboard-report-single-date span').html(date.format('MMM D, YYYY'));
				scope.setTabFun(scope.activeTabId,'graph');
			});
	}	 */
	
    function getDefaultDatePickerRange() 
	{	
        return moment.utc().subtract(29, 'days').subtract(moment().utcOffset(), 'minutes').format('MMM D') + ' - ' + moment.utc().subtract(moment().utcOffset(), 'minutes').format('MMM D, YYYY');
    }
	
	function getDefaultDate()
	{
        return date_2_days_ago.format('MMM D, YYYY') ;
    }
	

}}})


app.directive('audiencedate', function () {
            return {
				restrict: 'A',
                scope: true,
                link: function (scope, el, attr) {
	
	var curID = el.parent().attr("id");
	var date_today = moment.utc().subtract(moment().utcOffset(), 'minutes').format('MM/DD/YYYY');
	var date_for_audience = moment.utc().subtract(moment().utcOffset(), 'minutes').format('YYYY-MM-DD');
	var date_2_days_ago = moment.utc().subtract(2, 'days').subtract(moment().utcOffset(), 'minutes');	
	
	if(curID == 'audienceDate'){
		scope.audienceModal.data.Value = date_for_audience;
	}

		$('#'+curID+' .thin').html(getDefaultDate());
		$('#'+curID+' div').show();
		
		$('#'+curID+' div').daterangepicker({
				opens: 'left',
				showDropdowns: false,
				showWeekNumbers: true,
				timePicker: false,
				timePickerIncrement: 1,
				timePicker12Hour: true,
				buttonClasses: ['btn btn-sm'],
				applyClass: ' blue',
				cancelClass: 'default',
				singleDatePicker: true,
				format: 'MM/DD/YY',
				startDate: date_2_days_ago,
				maxDate: date_today,
				locale: {
							applyLabel: 'Apply',
							fromLabel: 'From',
							toLabel: 'To',
							customRangeLabel: 'Custom Range',
							daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
							monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
							firstDay: 1
						}
			},
			function (date) {
				$('#'+curID+' .thin').html(date.format('MMM D, YYYY'));
				var audienceDate = 	$('#'+curID+' div').data('daterangepicker');
				scope.audienceDateDef = $('#'+curID+' div').data('daterangepicker');
				if(curID == 'audienceDate'){
					scope.audienceModal.data.Value = audienceDate.endDate.format('YYYY-MM-DD');
				}
				/* if(curID == 'audienceDateFrom'){
					scope.audienceModal.data.ValueFrom = audienceDate.endDate.format('X')*1000;
				}
				if(curID == 'audienceDateTo'){
					scope.audienceModal.data.ValueTo = audienceDate.endDate.format('X')*1000;
				} */
				if(curID == 'automationStartDate'){
					scope.automationModal.StartDate = audienceDate.endDate.format('X')*1000;
				}
			});
	
	
	function getDefaultDate()
	{
        return date_2_days_ago.format('MMM D, YYYY') ;
    }
	

}}})