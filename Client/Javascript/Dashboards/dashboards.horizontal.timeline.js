var HorizontalTimelineCreator = function()
{
	HorizontalTimelineCreator.instance = this;
};

HorizontalTimelineCreator.prototype = 
{
	curGraphID: "",
	SessionsHistory: null,
	
	init: function (newCurGraphID, data) {
		HorizontalTimelineCreator.instance.curGraphID = newCurGraphID;
		// console.log("HorizontalTimelineCreator", data);
		$scope.curveAPI('GetSessionsHistory', data, HorizontalTimelineCreator.instance.onGotSessionsHistory, HorizontalTimelineCreator.instance.onGotSessionsHistoryError);
	},
	
	onGotSessionsHistory: function (data) {
		// console.log("onGotSessionsHistory", data);
		if (data != null && data.length >0)
		{
			HorizontalTimelineCreator.instance.fillHorizontalTimeline(parseSessionData(data));
		}
		else{
			$(HorizontalTimelineCreator.instance.curGraphID + "_body")[0].style.display = "none";
			$(HorizontalTimelineCreator.instance.curGraphID + "_loading")[0].style.display = "none";
			$(HorizontalTimelineCreator.instance.curGraphID + "_no_data")[0].style.display = "";
		}
	},
	
	onGotSessionsHistoryError: function (data) {
		// console.log("onGotSessionsHistoryError", data);
		$(HorizontalTimelineCreator.instance.curGraphID + "_body")[0].style.display = "none";
		$(HorizontalTimelineCreator.instance.curGraphID + "_loading")[0].style.display = "none";
		$(HorizontalTimelineCreator.instance.curGraphID + "_no_data")[0].style.display = "";
	},
	
	fillHorizontalTimeline: function (data) {
		// console.log("fillHorizontalTimeline", data);
		HorizontalTimelineCreator.instance.SessionsHistory = data;
		
		// var orgData =  '[{"eventtime":"1467200595490","table_type":"SessionData","sessionid":"d4ad4e79-c11a-46cf-b482-8b3dc9d26539"},' +
						// '{"eventtime":"1467200679000","table_type":"TrafficSources","sessionid":"d4ad4e79-c11a-46cf-b482-8b3dc9d26539"},'+
						// '{"eventtime":"1467201728800","table_type":"AppLifeCycle","sessionid":"d4ad4e79-c11a-46cf-b482-8b3dc9d26539"}'+
					   // ']';
		// var data = JSON.parse(orgData);
		// console.log("drawHorizontalTimeline data:", data);
		
		$(HorizontalTimelineCreator.instance.curGraphID + "_header").empty();
		$(HorizontalTimelineCreator.instance.curGraphID + "_data").empty();
		
		var headerStr = "";
		var dataStr = "";
		
		var minTime = parseFloat(data[0].sessionMin);
		var maxTime = parseFloat(data[0].sessionMin);
		var dataLength = data.length;
		for (i=0; i< dataLength; i++)
		{
			var sessionStartTime = parseFloat(data[i].sessionMin);
			minTime = Math.min(minTime, sessionStartTime);
			maxTime = Math.max(maxTime, sessionStartTime);
			
			
			var d = new Date(sessionStartTime);
			var dateStr = d.getDate() + "/" + (d.getMonth()+1) + "/" + d.getFullYear() + "T" + d.getHours() + ":" + (d.getMinutes()<10?'0':'') + d.getMinutes()+ '_' + i ;
			var dateNiceStr = d.getDate() + "/" + (d.getMonth()+1) + "/" + d.getFullYear() + " </br> " + d.getHours() + ":" + (d.getMinutes()<10?'0':'') + d.getMinutes();
			var selectedStr = '';
			if (i==0)
				selectedStr = 'selected';
			
			headerStr += '	<li>';
			headerStr += '		<a data-date="' + dateStr + '" style="width: 65px;" class="border-after-red bg-after-red ' + selectedStr;
			headerStr += '		" >' + dateNiceStr + '</a>';
			headerStr += '	</li>';
			
			dataStr += '<li class="' + selectedStr + '" data-date="' + dateStr + '">';
			dataStr += '<div style="background: #ededee;">';
			dataStr += '	<br/>';
			dataStr += '	<div class="mt-title">';
			dataStr += '		<ul style="float: left;">';
			dataStr += '			<li><h2 class="mt-content-title">Session Details ' + (i+1) + '/' + dataLength + '</h2></li>';
			dataStr += '		</ul>';
			dataStr += '	</div>';
			dataStr += '	<div class="clearfix"></div>';
			dataStr += '	<div class="mt-content border-grey-steel">';
			dataStr += '		<ul style="float: left;">';
			dataStr += '			<li>Session id: <span style="font-weight: bold;">' + data[i].sessionid + '</span></li>';
			dataStr += '			<li>Session started on <span style="font-weight: bold;">' + d.getDate() + "/" + (d.getMonth()+1) + "/" + d.getFullYear() + '</span> at <span style="font-weight: bold;">' + d.getHours() + ":" + (d.getMinutes()<10?'0':'') + d.getMinutes() + '</span></li>';
			dataStr += '			<li>Total session length is <span style="font-weight: bold;">' + data[i].sessionTiming + '</span></li>';
			dataStr += '			<li>Number of events during this session: <span style="font-weight: bold;">' + data[i].numEvents + '</span></li>';
			dataStr += '		</ul>';
			dataStr += '		<button class="btn" style="background-color:#575757;display: inline-block; color: #fff;margin: 55px;" onclick="HorizontalTimelineCreator.instance.showSessionEvents(' + i + ')">';
			dataStr += '			Session Events';
			dataStr += '		</button>';
			dataStr += '	</div>';
			dataStr += '</div>';
			dataStr += '</li>';
		}
		
		$(HorizontalTimelineCreator.instance.curGraphID + "_header").append(headerStr);
		$(HorizontalTimelineCreator.instance.curGraphID + "_data").append(dataStr);
		
		$(HorizontalTimelineCreator.instance.curGraphID + "_loading")[0].style.display = "none";
		$(HorizontalTimelineCreator.instance.curGraphID + "_body")[0].style.display = "";
		
		var timeDiff = maxTime - minTime;
		
		// console.log("session timeDiff = ", timeDiff, HorizontalTimelineCreator.instance.getTimeFactor(timeDiff));
		
		horizontalTimelineInit(HorizontalTimelineCreator.instance.getTimeFactor(timeDiff));
		
	},
	
	getTimeFactor: function(timeDiff)
	{
		var ans = 1000;
		
		if (timeDiff == null)
		{
			return ans;
		}
		
		timeDiff = timeDiff / 1000;
		if(timeDiff > 60)
		{
			ans = 100;
			timeDiff = timeDiff / 60;
			if (timeDiff < 60)
				return ans;
		}
		
		if(timeDiff > 60)
		{
			ans = 10;
			timeDiff = timeDiff / 60;
			if (timeDiff < 24)
				return ans;
		}
		
		if(timeDiff > 24)
		{
			ans = .01;
			return ans;
		}
		
		return ans;
	},
	
	showSessionEvents: function (index) {
		var sessionData = HorizontalTimelineCreator.instance.SessionsHistory[index];
		// console.log("showSessionEvents", i, sessionData);
		for (i=0; i<sessionData.length; i++)
		{
			var d = new Date(parseFloat(sessionData[i].eventtime));
			$('#session_data_table_body').append('<tr><td> ' + (i+1) + ' </td><td> ' + sessionData[i].table_type + ' </td><td> ' + d + ' </td></tr>');
		}
		$('#session_data').modal('show');
	},
}

new HorizontalTimelineCreator();