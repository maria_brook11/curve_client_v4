function APICallsFunction($scope,$rootScope,$http,$location){

	/* var mu = $location.host();
	$rootScope.protocol = $location.protocol();
	if($rootScope.protocol == 'http'){
		var port = '8080';
	}else{
		var port = '8443';
	}
	$rootScope.curentUrl = $rootScope.protocol+'://'+ mu +'/';
	$rootScope.mainUrl = $rootScope.protocol+'://'+ mu +'/#/';
	
	if($rootScope.mainUrl == $rootScope.protocol+'://curve.tech/#/'){
		
		//$rootScope.host = '52.1.71.61'; // old Production
		
		$rootScope.host = '52.20.177.127'; // Production
		$rootScope.production = true;
		
	}else{
		$rootScope.host = '34.231.85.211'; // QA
			
	} */
	
	
	var mu = $location.host();

	$rootScope.protocol = $location.protocol();
	if($rootScope.protocol == 'http'){
		var port = '8080';
	}else{
		var port = '8443';
	}
	$rootScope.curentUrl = $rootScope.protocol+'://'+ mu +'/';
	$rootScope.mainUrl = $rootScope.protocol+'://'+ mu +'/#/';
	if(mu == 'prod.curve.tech'){
				
		$rootScope.host = mu; // Production
		$rootScope.production = true;
		
	}else{
		if(mu ==  '10.0.0.8' || mu ==  'dev.curve.tech'){
			$rootScope.host = '34.231.85.211';
		}
		else{
			$rootScope.host = mu;
		}
			
	}
	
	
//prod.curve.tech 52.1.71.61
//integration.curve.tech 52.20.177.127
//

	
// **************** HOSTS *******************
// var host = '52.1.71.61'; // Production
// var host = '54.174.202.109'; // test
// var host = '52.20.177.127'; // QA
// var host = '34.231.85.211'; // Integration
// var host = 'curve-test-1956099999.us-east-1.elb.amazonaws.com'; // test
// var host = 'localhost';

// ****************** API PATHS *******************
// var api_path = 'API';
var api_path = 'Curve';
// var api_path = 'API_Dev';
// var api_path = 'API_AUTH';

var APIURL = $rootScope.protocol+'://' + $rootScope.host + ':'+port+'/' + api_path + '/';
$rootScope.httpUrl = $rootScope.protocol+'://' + $rootScope.host + ':'+port+'/' + api_path + '/';
var complete = {};
var fail = {};
var redirects = {};
var isloginpagereq = {};
var global_disable_halt = {};
var ajaxcalls = {};

$scope.getAPIURL = function()
{
	return APIURL;
}

$scope.curveAPI = function(servlet, data, completeCallback, failCallback, redirect, isLoginPR, disable_halt)
{
	return $scope.curveGetAPI(servlet, data, completeCallback, failCallback, redirect, isLoginPR, disable_halt);	
}

$scope.curveGetAPI = function(servlet, data, completeCallback, failCallback, redirect, isLoginPR, disable_halt)
{
	return ajaxCommand('GET', servlet, data, completeCallback, failCallback, redirect, isLoginPR, disable_halt);
}

$scope.curvePostAPI = function(servlet, data, completeCallback, failCallback, redirect, isLoginPR, disable_halt)
{
	return ajaxCommand('POST', servlet, data, completeCallback, failCallback, redirect, isLoginPR, disable_halt);
}

function ajaxCommand(type, servlet, data, completeCallback, failCallback, redirect, isLoginPR, disable_halt)
{
	if(redirect == null)
		redirect = true;
	if(isLoginPR == null)
		isLoginPR = false;
	if(disable_halt == null)
		disable_halt = false;

	// store completeCallback function
	$scope.packetid++;

	if(data == null)
		data = {};

	var pid = 'callback_' + $scope.packetid;
	data.PacketID = pid;
	complete[pid] = completeCallback;
	
	if(failCallback != null) 
		fail[pid] = failCallback;

	redirects[pid] = redirect;
	isloginpagereq[pid] = isLoginPR;
	global_disable_halt[pid] = disable_halt;
	

	// build url
	var api = APIURL + servlet;
	
	let request = {
						url: api,
						type: type,
						dataType: 'jsonp',
						xhrFields: { withCredentials: true },
						data: data,
						cache: false
					};
					
	if(type == "POST")
	{
		request = 	{
						url: api,
						type: type,
						dataType: 'application/json',
						xhrFields: { withCredentials: true },
						data: data,
						complete: function (req, textStatus) 
						{
							if(textStatus == "parsererror")
							{

								var resp = req.responseText;
								$scope.parseCurveResponse(JSON.parse(resp));
							}
						}
					};
	}
	
	var ajax = $.ajax(request);
					
	ajaxcalls[pid] = ajax;
	
	return ajax;
	
}


$scope.parseCurveResponse = function(data)
{

	/*if(data.indexOf("parseCurveResponse") != -1)
	{
		var newStr = data.substring(data.indexOf("parseCurveResponse")+"parseCurveResponse".length+1,data.indexOf('});')+1);
		var newJson = jQuery.parseJSON(newStr);
	}
	
	return newJson;*/
	//var data = getResponseJson(result.responseText);
		
		
	var packetid = data.PacketID;

	var completeCallback = complete[packetid];
	var failCallback = fail[packetid];
	var redirect = redirects[packetid];
	var isLoginPR = isloginpagereq[packetid];
	var disable_halt = global_disable_halt[packetid];
	if(validateResponse(data, redirect, isLoginPR, disable_halt))
	{
		if(completeCallback != null)
			completeCallback(data.Data);
	}
	else
	{
		if(failCallback != null)
			failCallback(data.ResultText);
	}
    
	disposeCall(packetid);
}

function disposeCall(packetid)
{
	delete ajaxcalls[packetid];
	delete fail[packetid];
	delete redirects[packetid];
	delete complete[packetid];
}

function clearPendingCalls()
{
	for(var id in ajaxcalls)
	{
		var ajax = ajaxcalls[id];
		ajax.abort();
		disposeCall(id);
	}
	
}

var status = "normal";

$scope.logoutMessage = function(redirect)
{
	clearPendingCalls();
			
	if(status == "normal")
	{
		status = "alert";
		$scope.purgeCache();
		if(redirect) 
			window.location = "/?m=Your session has timed out, please re-login to continue";
		
		status = "normal";
	}
}

function validateResponse(curveResponse, redirect, isLoginPR, disable_halt)
{
	//storeToCache('curveResponse', curveResponse);

	if(curveResponse.Result != 0)
	{	
		if (curveResponse.Result == "1004" && !isLoginPR)
		{
			$scope.logoutMessage(redirect);
		}
		else if(redirect)
		{
			if(disable_halt == true) return;
			
			var hult = true;

			if(curveResponse.Result == 1){

				hult = false;

                if($rootScope.loginTpl != 0) {

					$scope.show_error("Server Error", 'Code: ' + curveResponse.Result + '. Message: ' + curveResponse.ResultText, hult);
                }
			}
		}
		
		return false;
	}
	else
	{
		return true;
	}
}

	$scope.logoutFun = function(){
		$('.loading-page').show();
		$scope.curvePostAPI('Logout', {}, $scope.onCompleteLogout);
		
	}
	$scope.onCompleteLogout = function(data)
	{	
		$rootScope.appsArr = [];
		$scope.purgeCache();
		window.location = '/#/';
		$rootScope.Token = '';
		
	}

}