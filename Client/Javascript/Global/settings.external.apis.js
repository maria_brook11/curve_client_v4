function SetExtAPIsFunction($scope,$rootScope,$http,$location){

	var MAX_STR_LENGTH = 1000;
	// var externalServices = null;
	var uniqueIDCounter = 0;
	var ready_func = [];
	
	$scope.ExternalAPIs_is_ready = false;
	$scope.changeData = {"ExternalAPIsList" : false, "PurchasePricesArr" : false,"ConfigAdmin" : false}
	$scope.ExternalAPIsTableData = {};
	$scope.ExternalAPIsList = {};
	$scope.curCustID = $scope.loadFromStorage('currentCustomerID');
	
$scope.loadExternalServicesList = function()
{
	// http://localhost:8080/Curve/GetExternalServicesList?Token=YR4H3JGLP5L16UVN
	//parseCurveResponse({"PacketID":"","Result":0,"ResultText":"Success","Data":[{"name":"MailGun","type":"email"}]}); 
	// showLoading(true);
	$scope.curveAPI("GetExternalServicesList", {"Token": $rootScope.Token}, onGetExternalServicesList, onErrorGetExternalServicesList);
};
	
function onGetExternalServicesList(data)
{	
	$scope.ExternalAPIsList = data;
	$scope.ExternalAPIs_relaodExternalAPIsData();
};

function onErrorGetExternalServicesList(data)
{
	console.log("ERROR", data);
};
	
// $scope.ExternalAPIs = function()
// {
	

	function ready(f)
	{
		if($scope.ExternalAPIs_is_ready)
		{
			f();
			return;
		}
		
		if(ready_func == null)
			ready_func = [];
		
		ready_func.push(f);
	};
	
	function invokeReady()
	{
		$scope.ExternalAPIs_is_ready = true;
		if(ready_func != null)
		{
			for(var i = 0 ; i < ready_func.length ; i++)
			{
				var f = ready_func[i];
				if(f != null) try{f();}catch(e){console.log(e);};
			}
			
			ready_func = [];
		}
	};
	
	$scope.ExternalAPIs_relaodExternalAPIsData = function()
	{
		// http://localhost:8080/Curve/GetExternalServices?Token=YR4H3JGLP5L16UVN
		// showLoading(true);
		$scope.curveAPI("GetExternalServices", {"Token": $rootScope.Token}, onGetExternalServices, onErrorGetExternalServices);
	};
	
	$scope.updateExternalAPIsData = function()
	{
		$scope.changeData.ExternalAPIsList = false;
		var res = [];
		
		for (i = 0; i < $scope.ExternalAPIsTableData.length; i++)
		{
			if(!$scope.ExternalAPIsTableData[i].deleteItem){
				
				var temp = [];
				for(var j = 0; j < $scope.ExternalAPIsTableData[i].data.length; j ++){
					temp.push($scope.ExternalAPIsTableData[i].data[j].val);
					
				}
				$scope.ExternalAPIsTableData[i].metadataObj.configData = temp;
				var t = JSON.stringify($scope.ExternalAPIsTableData[i].metadataObj);
				res.push({
							metadata: t,
							name: $scope.ExternalAPIsTableData[i].name,
							type: $scope.ExternalAPIsTableData[i].type
						});
			}
		}
		
		$scope.curveAPI("SetExternalServices", {"Token": $rootScope.Token, Data:JSON.stringify(res)}, onSetExternalServices, onErrorSetExternalServices);
	};
	
	function onGetExternalServices(data)
	{
		$scope.ExternalAPIsTableData = data;
		
		addUniqueIdToData();
		invokeReady();
		
	};
	
	function onSetExternalServices(data)
	{
		$scope.show_success("Saved data successfully", "");
		// showLoading(false);
	};
	
	function onErrorGetExternalServices(data)
	{
		console.log("ERROR", data);
		alert("There was an Error loading the data.");
		// showLoading(false);
	};
	
	function onErrorSetExternalServices(data)
	{
		console.log("ERROR", data);
		alert("There was an Error saving the data.");
		// showLoading(false);
	};
	
	function addUniqueIdToData()
	{
		for (i = 0; i < $scope.ExternalAPIsTableData.length; i++)
		{	var data = "data";
			var lineData = $scope.ExternalAPIsTableData[i];
			if (lineData.metadata != "")
			{
				
				for(var j=0; j < $scope.ExternalAPIsList.length; j++)
				{
					if ($scope.ExternalAPIsList[j].name == lineData.name)
					{
						var tempArr = [];
						
						$scope.ExternalAPIsTableData[i].metadataObj = JSON.parse(lineData.metadata);
						$scope.ExternalAPIsTableData[i].keys = JSON.parse($scope.ExternalAPIsList[j].props);
						$scope.ExternalAPIsTableData[i].values = JSON.parse(lineData.metadata).configData;
						for(var k = 0; k < $scope.ExternalAPIsTableData[i].keys.length; k ++){
							var temp = {"key" : $scope.ExternalAPIsTableData[i].keys[k],"val" : $scope.ExternalAPIsTableData[i].values[k],"newVal" : $scope.ExternalAPIsTableData[i].values[k],"edit" : false}
							tempArr.push(temp);
						}
						$scope.ExternalAPIsTableData[i].data = tempArr;
					}
				}
			}
			$scope.ExternalAPIsTableData[i].uniqueID = "u_" + lineData.name + "_" + lineData.type;
			$scope.ExternalAPIsTableData[i].deleteItem = false;
		}
		
	};
	
	// function showLoading(show)
	// {
		// $("#setup_external_apis_loader")[0].style.display = show ? "" : "none";
		// $("#setup_external_apis_content")[0].style.display = show ? "none" : "";
	// };
	
	function addNewData(data)
	{
		
		$scope.ExternalAPIsTableData.push(data);
		
	};
	
	function updateConfigData(dataIndex, newData)
	{
		
		var data = $scope.ExternalAPIsTableData[dataIndex];
		var orgMetadata = JSON.parse(data.metadata);
		orgMetadata.configData = JSON.parse(newData.metadata).configData;
		data.values = orgMetadata.configData;
		data.metadata = JSON.stringify(orgMetadata);
		
		
		$scope.ExternalAPIsTableData[dataIndex] = data;
		
	};
	
	function getDataIndex(uniqueID)
	{
		
		for (i = 0; i < $scope.ExternalAPIsTableData.length; i++)
		{
			if ($scope.ExternalAPIsTableData[i].uniqueID == uniqueID)
				return i;
		}
		return -1;
	};
	
	/* $scope.ExternalAPIs_removeData = function(uniqueID)
	{
		// console.log("ExternalAPIs_removeData", uniqueID);
		$scope.ExternalAPIsTableData.splice(getDataIndex(uniqueID), 1);
	}; */
	
	/* $scope.ExternalAPIs_saveAndExit = function()
	{
		// send the $scope.ExternalAPIsTableData to the server
		updateExternalAPIsData(true);
	}; */
	
	$scope.ExternalAPIs_updateAndRedrawEditLine = function(uniqueID)
	{
		var curr = $scope.ExternalAPIsTableData[getDataIndex(uniqueID)];
		$scope.ExternalAPIsEditLineData = $scope.ExternalAPIsTableData[getDataIndex(uniqueID)];
		$scope.ExternalAPIsEditLineData.newTitle = curr.name;
		$scope.ExternalAPIsEditLineData.isNewLine = false;
		
		$scope.ExternalAPIs_onServiceSelected(curr.name, false, uniqueID);
		
		$('#edit_service_token').modal('show');
	};
	
	$scope.ExternalAPIs_createNewEditLine = function()
	{
		var uniqueID = "newData_" + uniqueIDCounter++;
		
		$scope.ExternalAPIsEditLineData = {name:"", uniqueID:uniqueID, type:"", token:""};
		$scope.ExternalAPIsEditLineData.newTitle = "Select Service";
		$scope.ExternalAPIsEditLineData.isNewLine = true;
		
		$("#select_service_title").empty();
		$("#select_service_title").append("Select Service <span class='caret cohort_caret'></span>");
		$("#edit_line_type").empty();
		$("#service_data_edit_fields").empty();
		
		$("#edit_service_token").modal("show");
	};
	
	$scope.ExternalAPIs_onServiceSelected = function(sName, isNewLine, uniqueID)
	{
		if (isNewLine)
		{
			for(var i=0; i < $scope.ExternalAPIsList.length; i++)
			{
				if ($scope.ExternalAPIsList[i].name == sName)
				{
					$("#select_service_title").empty();
					$("#select_service_title").append(sName + "<span class='caret cohort_caret'></span>");
					$("#select_service_title")[0].setAttribute("data-service-title", sName);
					$("#select_service_title")[0].setAttribute("data-uniqueID", uniqueID);
					$("#select_service_title")[0].setAttribute("data-isNewLine", isNewLine);
					$("#edit_line_type").val($scope.ExternalAPIsList[i].type);
					
					$("#service_data_edit_fields").empty();
					var keys = JSON.parse($scope.ExternalAPIsList[i].props);
					for(j=0; j < keys.length; j++)
					{
						$("#service_data_edit_fields").append('<tr class="odd gradeX"><td><span style="width: 150px;display:inline-block;">' + keys[j] + '</span><input class="service_token_line_type" style="width:300px;" id="edit_line_metadata_' + j + '" type="text" value=""/></td></tr>');
					}
				}
			}
		}
		else
		{
			for(var i=0; i < $scope.ExternalAPIsTableData.length; i++)
			{
				if ($scope.ExternalAPIsTableData[i].name == sName)
				{
					$("#select_service_title").empty();
					$("#select_service_title").append(sName + "<span class='caret cohort_caret'></span>");
					$("#select_service_title")[0].setAttribute("data-service-title", sName);
					$("#select_service_title")[0].setAttribute("data-uniqueID", uniqueID);
					$("#select_service_title")[0].setAttribute("data-isNewLine", isNewLine);
					$("#edit_line_type").val($scope.ExternalAPIsTableData[i].type);
					
					$("#service_data_edit_fields").empty();
					var keys = $scope.ExternalAPIsTableData[i].keys;
					var values = [];
					if (!isNewLine && $scope.ExternalAPIsTableData[i] != undefined)
						values = $scope.ExternalAPIsTableData[i].values;
					for(j=0; j < keys.length; j++)
					{
						var value = values[j]==undefined ? "" : values[j];
						$("#service_data_edit_fields").append('<tr class="odd gradeX"><td><span style="width: 150px;display:inline-block;">' + keys[j] + '</span><input class="service_token_line_type" style="width:300px;" id="edit_line_metadata_' + j + '" type="text" value="' + value + '"/></td></tr>');
					}
				}
			}
		}
	};
	
	$scope.ExternalAPIs_onCompleteEditLine = function()
	{
		var newName = $("#select_service_title").attr("data-service-title");
		var uniqueID = $("#select_service_title").attr("data-uniqueID");
		var isNewLine = $("#select_service_title").attr("data-isNewLine");
		var newType = $("#edit_line_type").val();
		var configData = [];
		var keys;
		var count = 0;
		for(var i=0; i < $scope.ExternalAPIsList.length; i++)
		{
			if ($scope.ExternalAPIsList[i].name == newName)
			{
				keys = JSON.parse($scope.ExternalAPIsList[i].props);
				for(j=0; j < keys.length; j++)
				{	
					if($("#edit_line_metadata_" + j).val() != ''){
						configData.push( $("#edit_line_metadata_" + j).val());
						count ++;
					}
				}
				
			}
		}
		if(count == keys.length){
			$scope.changeData.ExternalAPIsList = true;
			var metadata = {};
			metadata.configData = configData;

			if (validateString(newName, "Service") && validateString(newType, "Type"))
			{
				var newData = {name:newName, uniqueID:uniqueID, type:newType, metadata:JSON.stringify(metadata)};
				
				if (isNewLine == 'true' && serviceExistInTable(newData))
				{
					$scope.show_warn("Service duplication", "Service \'" + newName + "\' of Type '" + newType + "\' already exist in table.");
					return;
				}
				var dataIndex = getDataIndex(uniqueID);
				
				if (dataIndex == -1)
				{
					var orgMetadata = JSON.parse(newData.metadata);
					newData.values = orgMetadata.configData;
					newData.keys = keys;
					var tempArr = [];
					
					newData.metadataObj = orgMetadata;
					
					for(var k = 0; k < newData.keys.length; k ++){
						var temp = {"key" : newData.keys[k],"val" : newData.values[k],"newVal" : newData.values[k],"edit" : false}
						tempArr.push(temp);
					}
					newData.data = tempArr;
					addNewData(newData);
				}
				else
				{
					updateConfigData(dataIndex, newData);
				}
				$("#edit_service_token").modal("hide");
			}
		}else{
			alert('Please fill out all required fields')
		}
	};
	
	function serviceExistInTable(data)
	{
		for (i = 0; i < $scope.ExternalAPIsTableData.length; i++)
		{
			if ($scope.ExternalAPIsTableData[i].name == data.name && $scope.ExternalAPIsTableData[i].type == data.type)
				return true;
		}
		return false;
	};
	
	function validateString(str, name)
	{
		if (str == null || str == "")
		{
			alert("Field \'" + name + "\' must be filled out.");
			return false;
		}
		if (str.length > MAX_STR_LENGTH)
		{
			alert("Field \'" + name + "\' must be not longer than " + MAX_STR_LENGTH + ".");
			return false;
		}
		return true;
	};
// }

/*************************InAppPricing***********************/
	$scope.InAppPricing_MAX_STR_LENGTH = 100;
	$scope.InAppPricing_tableData = null;
	$scope.InAppPricing_uniqueIDCounter = 0;
	$scope.PurchasePricesArr = [];
	
	//v.val = ExternalAPIsTableData[lineData].data[k].newVal;v.newVal = ExternalAPIsTableData[lineData].data[k].newVal; v.edit = false; changeData.isChange = true;
	
	$scope.PurchasePricesEdit = function(d,i){
		$scope.changeData.PurchasePricesArr = true;
		$scope.PurchasePricesArr[i].value = JSON.parse(JSON.stringify(d.newVal));
		$scope.PurchasePricesArr[i].newVal = JSON.parse(JSON.stringify(d.newVal));
		$scope.PurchasePricesArr[i].edit = false;
					//changeData.isChange = true;

	}
	
	$scope.PurchasePricesCancel = function(d,i){
		//console.log(d)
		$scope.PurchasePricesArr[i].newVal = JSON.parse(JSON.stringify(d.value));
		$scope.PurchasePricesArr[i].edit = false;
	}
	
	$scope.relaodInappPricingData = function()
	{
		
		$scope.curveAPI("GetPurchasePrices", {"Token": $rootScope.Token}, onGetPurchasePrices, onErrorGetPurchasePrices);
	};
	
	function onGetPurchasePrices(data)
	{
		//console.log(data)
		
		if(data.length > 0){
			
			for(var i = 0 ; i < data.length; i ++){
				var newObject = JSON.parse(JSON.stringify(data[i]));
				var temp = {"value" : data[i],"newVal" : newObject,"edit" : false, "deleteItem" : false}
				$scope.PurchasePricesArr.push(temp)
			}
			
		}
		
	};
	
	function onErrorGetPurchasePrices(data)
	{
		console.log("ERROR", data);
		alert("There was an Error loading the data.");
		//InAppPricing.instance.showLoading(false);
	};
	
	
	
	
	$scope.InAppPricing_onCompleteEditLine = function()
	{
		var newId = $("#inapp_edit_line_id").val();
		var uniqueID = $("#inapp_edit_line_id").attr("data-uniqueID");
		var newPackagename = $("#inapp_edit_line_packagename").val();
		var newCost = $("#inapp_edit_line_cost").val();
		
		if (validateString(newId, "ID") && validateString(newPackagename, "Package Name") && $scope.isNumeric(newCost))
		{	
			var newData = {"package_id" : newId, "package_name" : newPackagename, "package_cost" : newCost, "package_currency" : $("#inapp_edit_line_currency").val(), "package_version" : $("#inapp_edit_line_version").val()};
			var newDataCopy = JSON.parse(JSON.stringify(newData));
			var temp = {"value" : newData,"newVal" : newDataCopy,"edit" : false, "deleteItem" : false}
			
			$scope.PurchasePricesArr.push(temp)
			$scope.changeData.PurchasePricesArr = true;
			$("#edit_Inapp_pricing").modal("hide");
		}
	};
	
	$scope.validateString = function(str, name)
	{
		if (str == null || str == "")
		{
			alert("Field \'" + name + "\' must be filled out.");
			return false;
		}
		if (str.length > this.MAX_STR_LENGTH)
		{
			alert("Field \'" + name + "\' must be not longer than " + this.MAX_STR_LENGTH + ".");
			return false;
		}
		return true;
	};
	
	$scope.isNumeric = function(n)
	{
		return !isNaN(parseFloat(n)) && isFinite(n);
	};
	
	$scope.updatePurchasePricesData = function(){
		
		$scope.changeData.PurchasePricesArr = false;
		var res = [];
		
		for (i = 0; i < $scope.PurchasePricesArr.length; i++)
		{
			if(!$scope.PurchasePricesArr[i].deleteItem){
				
	var newData = {"id" : $scope.PurchasePricesArr[i].value.package_id, "packagename" : $scope.PurchasePricesArr[i].value.package_name, "cost" : $scope.PurchasePricesArr[i].value.package_cost, "currency" : $scope.PurchasePricesArr[i].value.package_currency, "version" : $scope.PurchasePricesArr[i].value.package_version};
				
				res.push(newData);
			}
		}
		$scope.updateInappPricingData(res);
	}
	
	$scope.updateInappPricingData = function(data)
	{
		
		$scope.curveAPI("SetPurchasePrices", {"Token": $rootScope.Token, Data:JSON.stringify(data)}, onSetPurchasePrices, onErrorSetPurchasePrices);
	};
	
	
	function onSetPurchasePrices(data)
	{
		$scope.show_success("Saved data successfully", "");
		
	};
	
	function onErrorSetPurchasePrices(data)
	{
		console.log("ERROR", data);
		
	};
	
	
	$scope.InAppPricing_createNewEditLine = function(){
		
		var uniqueID = "newPricing_" + $scope.InAppPricing_uniqueIDCounter++;
		var curr = {id:"", uniqueID:uniqueID, packagename:"", cost:0, currency:"", version:""};
		// var curr = {package_id:"", uniqueID:uniqueID, package_name:"", package_cost:0, currency:"", package_version:""};
		var tableRows = drawEditLine(curr);
		
		$("#edit_inapp_pricing_table").empty();
		$("#edit_inapp_pricing_table").append(tableRows);
		
		$("#edit_Inapp_pricing").modal("show");
		
	}
	
	function drawEditLine(lineData)
	{
		if (lineData.currency==null) lineData.currency = '';
		if (lineData.version==null) lineData.version = '';
		// if (lineData.package_version==null) lineData.package_version = '';
		
		var ans = '<tr class="odd gradeX">' +
				  '<td><input style="width:150px;" id="inapp_edit_line_id" data-uniqueID="' + lineData.uniqueID + '" type="text" value="' + lineData.id + '"/></td>' +
				  '<td><input style="width:150px;" id="inapp_edit_line_packagename" type="text" value="' + lineData.packagename + '"/></td>' +
				  '<td><input style="width:80px;" id="inapp_edit_line_cost" type="number" value="' + lineData.cost + '"/></td>' +
				  '<td><input style="width:80px;" id="inapp_edit_line_currency" type="text" value="' + lineData.currency + '"/></td>' +
				  '<td><input style="width:80px;" id="inapp_edit_line_version" type="text" value="' + lineData.version + '"/></td>' +
				  '</tr>';
		// var ans = '<tr class="odd gradeX">' +
				  // '<td><input style="width:150px;" id="inapp_edit_line_id" data-uniqueID="' + lineData.uniqueID + '" type="text" value="' + lineData.package_id + '"/></td>' +
				  // '<td><input style="width:150px;" id="inapp_edit_line_packagename" type="text" value="' + lineData.package_name + '"/></td>' +
				  // '<td><input style="width:80px;" id="inapp_edit_line_cost" type="number" value="' + lineData.package_cost + '"/></td>' +
				  // '<td><input style="width:80px;" id="inapp_edit_line_currency" type="text" value="' + lineData.currency + '"/></td>' +
				  // '<td><input style="width:80px;" id="inapp_edit_line_version" type="text" value="' + lineData.package_version + '"/></td>' +
				  // '</tr>';
		return ans;
	}

	/*********************** reports ***********************/
	$scope.GetGameConfigAdmin = function(){
		$scope.curveAPI('GetGameConfigAdmin', {"Token": $rootScope.Token}, gameConfCallback);
	}
	
	function gameConfCallback(data){
		
		$scope.GameConfigAdminArr = data.webConfigData;
		//console.log($scope.GameConfigAdminArr)
	}
	
	$scope.updateGameConfigAdmin = function(){
		
		var temp = '{';
		$.each($scope.GameConfigAdminArr.Flags, function (key, val){
			temp += '"' + val.FlagName + '" : ' + val.Enabled + ',';
			
		})
		var t = temp.slice(0, -1);
		t += '}';
		var dataToSend = t;
		$scope.curveAPI('SetGameConfig', {"Token": $rootScope.Token, "Data": dataToSend}, setConfigCallback, failedToConfig);
	}
	
	function setConfigCallback(data){
		$scope.show_success("Saved data successfully", "");
	}
	
	function failedToConfig(data){
		console.log(data)
	}

}