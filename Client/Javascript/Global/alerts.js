function AletrsFunction($scope,$rootScope,$http,$location){
	
	function show_custom(title, msg, header, color)
	{
		resetModal(color, 'close', header, title, msg, '#00aa00', null, null);
	}

	$scope.show_message = function(title, msg, hang)
	{
		if(!hang) resetModal('gray', 'close', 'Message', title, msg, '#aaaaaa');
		
		if(msg == null)
			msg = "";
		else 
			msg = ": " + msg;
		if(hang) alert("[MESSAGE]" + title + msg);
	}

	$scope.show_success = function(title, msg, hang, redirectLink, redirectTitle)
	{
		if(!hang) resetModal('green', 'close', 'SUCCESS!', title, msg, '#00aa00', redirectLink, redirectTitle);
		
		if(msg == null)
			msg = "";
		else 
			msg = ": " + msg;
		if(hang) alert("[SUCCESS]" + title + msg);
	}

	$scope.show_warn = function(title, msg, hang)
	{	
		if(!hang) resetModal('red', 'close', 'WARNNING!', title, msg, '#aa0000');
		
		if(msg == null)
			msg = "";
		else 
			msg = ": " + msg;
		if(hang) alert("[WARNNING]" + title + msg);
	}

	$scope.show_error = function(title, msg, hang)
	{
		if(!hang) resetModal('red', 'close', 'ERROR!', title, msg, '#aa0000');
		
		if(msg == null)
			msg = "";
		else 
			msg = ": " + msg;
		
		if(hang) alert("[ERROR]: " + title + msg);
	}

	function redirectTo(url)
	{
		window.location = url;
	}

	function resetModal(btnClass, btnText, ttl, msg1, msg2, color, redirect, redirectTitle)
	{
		$('#notify-box').modal('show');
		$('#notify-box').on('hidden.bs.modal', function () {if (typeof(forceCloseTutorialOnClosePopup) !== 'undefined') forceCloseTutorialOnClosePopup();});
		document.getElementById('notification_msg_1').innerHTML = msg1;
		document.getElementById('notification_msg_2').innerHTML = msg2;

		var title = document.getElementById('notification_title');
		title.innerHTML = ttl;
		if(color != null)
			title.style.color = color;
		
		document.getElementById('notify-right-btn').className = "btn "+btnClass;
		document.getElementById('notify-right-btn').innerHTML = btnText;
		
		if(redirectTitle != null && redirectTitle != "" && document.getElementById('notify-left-btn') != null)
		{
			document.getElementById('notify-left-btn').innerHTML = redirectTitle;
			document.getElementById('notify-left-btn').onclick = function() { redirectTo(redirect) };
		}
	}
}