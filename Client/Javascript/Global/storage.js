function StorageFunction($scope,$rootScope,$http,$location){

	var storageIdentefier = 'gingeecurve.storage';

	$scope.storeToCache = function(key, val)
	{
		var gingeeObject = loadFromLS();
		
		if(gingeeObject == null)
			gingeeObject = {};
		
		var vald = JSON.stringify(val);
		gingeeObject[key] = vald;
		saveToLS(gingeeObject);
	}

	$scope.loadFromCache = function(key)
	{
		return $scope.loadFromStorage(key);
	}

	$scope.loadFromStorage = function(key)
	{
		var gingeeObject = loadFromLS();

		if(gingeeObject == null)
			return null;
		
		var retrievedObject = gingeeObject[key];
		if(retrievedObject == null)
			return null;

		return JSON.parse(retrievedObject);
	}
	
	$scope.deleteCache = function(key)
	{
		var gingeeObject = loadFromLS();

		if (gingeeObject)
		{
			delete gingeeObject[key];
			saveToLS(gingeeObject);
		}
	}

	$scope.purgeCache = function()
	{
		
		localStorage.removeItem(storageIdentefier);
		
	}

	function saveToLS(obj)
	{	
		localStorage.setItem(storageIdentefier, JSON.stringify(obj));
	}

	function loadFromLS()
	{
		var ls = localStorage.getItem(storageIdentefier);

		if(ls == null)
			return null;
		
		return JSON.parse(ls);
	}
}