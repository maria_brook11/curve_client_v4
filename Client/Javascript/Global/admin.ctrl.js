app.controller('AdminCtrl', function($scope,$rootScope,$http,$location,TConst,URLConst,$filter,CountryConst,InsightConst,$timeout,$interval,dateService,resetService,saveCSVService,$compile,$document,diagramParseService,diagramValidationService) {
	
	$('.loading-page').hide();
	$scope.curApp = {'icon' : 'Images/icons/default.png','label' : 'Select App'};
	
	
	/************************************  update Tendency icon *************************************/
	
	$scope.TendencyIcon = {'TendencyID' : '','loader' : false};
	
	$scope.deleteTendencyIconFun = function(){
		$scope.TendencyIcon.loader = true;
		console.log($scope.TendencyIcon.TendencyID)
	}
	
	$scope.updateModelTNDiconFun = function(){
		
		$scope.TendencyIcon.loader = false;
		$scope.modelUploadImg = { 
			code: '',
			image: '',
			statusImgMes : '',
			imgFor : ''
		};
		$scope.imgFor = '';
		
	}
	
	$scope.profPicReadSRCFun = function(input) {
		
		$scope.modelUploadImg.image = input;
      if (input) {
            
			var reader = new FileReader();

            reader.onload = function (e) {

               $scope.modelUploadImg.code = e.target.result;
			   
            }
    
            reader.readAsDataURL(input);
			
        }else{
			$scope.modelUploadImg.statusImgMes = "Max size 20MB";
		}
    }

	$scope.profPicRemoveFun = function(){
		delete $scope.modelUploadImg;
		$scope.modelUploadImg = { 
			code: '',
			image: '',
			statusImgMes : '',
			imgFor : $scope.imgFor
		};
	}
	
 	$scope.profPicUploadFun = function(){
		
		
	/* 	if($scope.modelUploadImg.imgFor == 'user'){
			var fileName = 'userupload.php';
			var y = $scope.loadFromStorage("userid");
			var uploadData = {file1: $scope.modelUploadImg.image, 'UserId': y};
			
		}
		if($scope.modelUploadImg.imgFor == 'app'){
			var fileName = 'upload.php';
			var custID = $scope.loadFromStorage('currentCustomerID');
			var uploadData = {file1: $scope.modelUploadImg.image, 'CustomerId': custID, 'AppToken' : $rootScope.Token };
			
		} */
		
		console.log($scope.modelUploadImg.image)
		
		/* if($scope.modelUploadImg.image != ''){
		
			Upload.upload({
				url: ''+$rootScope.curentUrl+'includes/'+fileName,
				method: "POST",
				data: uploadData
			}).then(
			
			function suc(response){
				
				if(response.data.status == "success"){
					
					
					if($scope.modelUploadImg.imgFor == 'user'){
						
						var url = ''+$rootScope.curentUrl+'includes' + response.data.path;
						$scope.modelUploadImg.statusImgMes = "Successfully uploaded";
					
						$timeout(function () {
							setUserIcon(url);
						}, 1000);
						
					}
					if($scope.modelUploadImg.imgFor == 'app'){
						
						var url = ''+$rootScope.curentUrl+'includes' + response.data.path;
						$scope.modelUploadImg.statusImgMes = "Successfully uploaded";
						
						$timeout(function () {
							sendAppIcon(url);
						}, 1000);
					}
				} 
				if(response.data.status == "fail"){
					//alert("Error : "+response.data.message);
					$scope.modelUploadImg.statusImgMes = "Error : "+response.data.message;
				}
				$scope.getAppDataFun($scope.curApp.token);
			},

			function err(response){
				alert("Couldn't upload file");
				$scope.getAppDataFun($scope.curApp.token);
			},

			function prg(e) {
				var progressPercentage = parseInt(100.0 * e.loaded / e.total);
				//console.log('Progress: ' + progressPercentage + '%');
			}
		);
		
		} */
	};

function setUserIcon(iconUrl){
		$scope.email = $scope.loadFromStorage('email');
		$scope.curvePostAPI('SetUserImageURL', {"Email": $scope.email, "ImageURL": iconUrl}, setUserIconCallback);

		function setUserIconCallback() 
		{
			$scope.storeToCache("updateImg", iconUrl);
			//location.reload();
			var random = Math.floor(Math.random() * 1000) + 1;
			$scope.userImage  = iconUrl+'?r='+random;
			$scope.$apply('userImage', function(){})
			$('#updateImg').modal('hide');
		}
	}

function sendAppIcon(iconUrl) 
{
	$scope.curvePostAPI('SetGameIcon', {"Token": $rootScope.Token, "Icon": iconUrl}, setAppIconCallback);
	
	function setAppIconCallback() 
	{
        var random = Math.floor(Math.random() * 1000) + 1;
		$scope.activeAppImg  = {'icon':iconUrl+'?r='+random};
		$scope.$apply('activeAppImg', function(){})
		$('#updateImg').modal('hide');
    }
}


/************************************ end Tendency icon *************************************/
	
	
	/******************************** add app ***********************************************/
	$scope.addAppModalOpenFun = function(){
		$scope.addAppModal = { 
			CompanyName: '',
			FirstName: '',
			LastName : '',
			Email : '',
			Password : '',
			Trial : false,
			Cupon : '',
			TrialDays : '',
			loader : false,
			mSuccess : '',
			mError : ''
		};
		
		$('#addApp').modal('show');
		
	}
//AddCustomerByAdmin?&Data={"Email":"test3@permissions.com","Password":"3536","FirstName":"Moshe", "LastName":"Maman", "CompanyName":"Test001","Trial":"true", "Cupon":"09876", "TrialDays":"90"} 'Token': $rootScope.Token,
	
		/************************************************************************************/
	 $scope.ranksdata = {data:[], labels:[]};
	//$scope.options = {legend: {display: true}};
	$scope.options = {
    responsive: true,
    legend: {
		display: true,
      position: 'top',
    },

    animation: {
      animateScale: true,
      animateRotate: true
    },
    tooltips: {
      callbacks: {
        label: function(tooltipItem, data) {
        var dataset = data.datasets[tooltipItem.datasetIndex];
          var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
            return previousValue + currentValue;
          });
          var currentValue = dataset.data[tooltipItem.index];
          var precentage = ((currentValue/total) * 100).toFixedDown(2);        
          return precentage + "%";
        }
      }
    }
  }
/************************************************************************************/

	$scope.addAppfun = function(){
		$scope.addAppModal.loader = true;
		if($scope.addAppModal.Cupon == null)$scope.addAppModal.Cupon = '';
		if($scope.addAppModal.TrialDays == null)$scope.addAppModal.TrialDays = '';
		var data = { 
			CompanyName: $scope.addAppModal.CompanyName,
			FirstName: $scope.addAppModal.FirstName,
			LastName : $scope.addAppModal.LastName,
			Email : $scope.addAppModal.Email,
			Password : $scope.addAppModal.Password,
			Trial : ''+$scope.addAppModal.Trial+'',
			Cupon : $scope.addAppModal.Cupon,
			TrialDays : $scope.addAppModal.TrialDays
		};
		
		$scope.curveAPI('AddCustomerByAdmin', { 'Data' : JSON.stringify(data)}, AddCustomerByAdminSuccess, AddCustomerByAdminError);
		
		function AddCustomerByAdminSuccess(data){
			console.log(data);
			$scope.addAppModal.loader = false;
			$scope.addAppModal.mSuccess = 'Success';
			$timeout(function () {
				$('#addApp').modal('hide');
			}, 3000);
			
			$scope.startAdnimPanel();
		}
		function AddCustomerByAdminError(data){
			console.log(data);
			$scope.addAppModal.loader = false;
			$scope.addAppModal.mError = data;
			$timeout(function () {
				$scope.addAppModal.mError = '';
			}, 3000);
			
			//$('#addApp').modal('hide');
		}
	}
	

/******************************** end add app ***********************************************/

	Number.prototype.toFixedDown = function(digits) {
    var re = new RegExp("(\\d+\\.\\d{" + digits + "})(\\d)"),
        m = this.toString().match(re);
    return m ? parseFloat(m[1]) : this.valueOf();
	};

	$scope.startAdnimPanel = function(){
		$scope.curveAPI('GetUserCompanies', null, $scope.GetUserCompaniesSuccess);
		
	}
	
	$scope.GetUserCompaniesSuccess = function(data){ 
		$scope.applicationsArr = [];
		$.each(data.Customers, function(i, value){
			
			if(value.Games.length > 0){
				
				$.each(value.Games, function(j, app){
					
					$scope.applicationsArr.push(app);
						
				});
			}
		});
		
		
	
	}
	
	$scope.getAppDataFun = function(token){
		
		$('.loading-page').show();
		
		$scope.ranksArr = [];
		$scope.newBehaviorsArr = null;
		$scope.behaviorsArr = [];
		$scope.eventDescrArr = [];
		$scope.tempEventDescrArr = [];
		$scope.eventsArr = [];
		
		var temp = $filter('filter')($scope.applicationsArr,{Token: token})[0];	
		$scope.curApp = {'icon' : temp.Icon ,'label': temp.GameName ,'token' : token};
		
		//localhost:8080/Curve/GetRanks?Token=TEST-YZMF8ICL6W8
		$scope.curveAPI('GetRanks', {Token: token}, GetRanksSuccess, GetRanksError);
		
		//localhost:8080/Curve/GetBehaviors?Token=TEST-YZMF8ICL6W8
		//$scope.curveAPI('GetBehaviors', {Token: token}, GetBehaviorsSuccess, GetBehaviorsError);
		
		$scope.curveAPI('GetCohortsEvents', {Token: token}, GetCohortsEventsSuccess, GetCohortsEventsError);
		
		$scope.getBehaviorTypesFun();
		
		$('.loading-page').hide();
	}
	
		$scope.recalculetedBehaviorFun = function(id){
		
			//console.log("recalculetedBehaviorFun");
			for(var i = 0; i < $scope.newBehaviorsArr.length; i ++){
			for(var j = 0; j < $scope.newBehaviorsArr[i].data.length; j ++){
				if($scope.newBehaviorsArr[i].data[j].behavior.behaviorid == id){
					if ($scope.newBehaviorsArr[i].data[j].behavior.isactive=='1')
					{
						$scope.newBehaviorsArr[i].data[j].behavior.recalculetedNOW = true;
						break;
					}
					else
							return;
					
					
					
				}
			}
		}
		
		//console.log("recalculetedBehaviorFun2");
		$scope.curveAPI('CalculateBehavior', {'Token': $scope.curApp.token, 'Data' : id}, CalculateBehaviorSuccess, CalculateBehaviorError);

		//Calculate
		//http://localhost:8080/Curve/CalculateBehavior?Token=TEST-YZMF8ICL6W8&Data=8
	}
	function CalculateBehaviorSuccess(data){
	
		for(var i = 0; i < $scope.newBehaviorsArr.length; i ++){
			for(var j = 0; j < $scope.newBehaviorsArr[i].data.length; j ++){
				if($scope.newBehaviorsArr[i].data[j].behavior.behaviorid == data[0].behaviorid){
					$scope.newBehaviorsArr[i].data[j].behavior = data[0];
					$scope.newBehaviorsArr[i].data[j].behavior.recalculetedNOW = false;
					recaluculateBehaviorPieData($scope.newBehaviorsArr[i]);
					break;
					
				}
			}
		}
		
	}
	function CalculateBehaviorError(data){
		console.log(data)
	}
	function recaluculateBehaviorPieData(tendency){
		
		tendency.piedata.data = [];
		tendency.piedata.labels = [];
	
			for(var i = 0 ; i < tendency.data.length ; i++){
				
					if (tendency.data[i].behavior.isactive=='1' && tendency.data[i].behavior.size !=null && tendency.data[i].behavior.size != undefined)
					{
						tendency.piedata.data.push(parseInt(tendency.data[i].behavior.size));
						tendency.piedata.labels.push(tendency.data[i].behavior.name);
					}
							
				
			
			}	

			

	}
	$scope.calculatedBehaviorShowHideFun = function(id){
		for(var i = 0; i < $scope.audiencesArr.length; i ++){
			if($scope.audiencesArr[i].audienceid == id){
				$scope.audiencesArr[i].calculatedShow = !$scope.audiencesArr[i].calculatedShow;
			}else{
				$scope.audiencesArr[i].calculatedShow = false;
			}
		}
	}
	
		
	function GetRanksSuccess(data){

		$scope.ranksArr = data;
		$scope.ranksdata.data = [];
		$scope.ranksdata.labels = [];
		
		$.each($scope.ranksArr, function (key,val){
			//if (val != null && val.rank != null && val.rank.length > 0)
			//{
				var n = val.rank.name;
				val.rank.newName = n;
				val.rank.editName = false;
				$scope.ranksdata.data.push(parseInt(val.rank.size));
		
				$scope.ranksdata.labels.push(n);
			//}
				
			});
		
	}
	function GetRanksError(data){
		console.log(data)
	}
	
	function GetBehaviorsSuccess(data){
	
		//$scope.behaviorsArr = data;
		

		$scope.newBehaviorsArr = [];
		
		/*var types = [data[0].behavior.subtype];
		
		for(var k = 1 ; k < data.length ; k++){
			if(types.indexOf(data[k].behavior.subtype) == -1){
				types.push(data[k].behavior.subtype);
			}
		}*/
		
		var types = [];		
		for(var m = 0 ; m < $scope.behaviorTypes.length ; m++){
			types.push($scope.behaviorTypes[m].typeid);
		}
		
			
		for(var a = 0 ; a < types.length ; a++){
			$scope.newBehaviorsArr.push({'subtype' : types[a] , 'data' : [], 'piedata':{'data':[],'labels':[]}});
		}
		
			
		for(var i = 0 ; i < data.length ; i++){
			for(var j = 0 ; j < $scope.newBehaviorsArr.length ; j++){
				if($scope.newBehaviorsArr[j].subtype == data[i].behavior.subtype){
					$scope.newBehaviorsArr[j].data.push(data[i]);
					if (data[i].behavior.isactive=='1' && data[i].behavior.size !=null && data[i].behavior.size != undefined)
					{

						$scope.newBehaviorsArr[j].piedata.data.push(parseInt(data[i].behavior.size));
						$scope.newBehaviorsArr[j].piedata.labels.push(data[i].behavior.name);
					}
					
					
				}
			
			}		
		}
	
	
					
		var bType = $interval(function ()
			{
				if ($scope.behaviorTypes)
				{
					SetbehaviorTypeFun();
					$interval.cancel(bType);	
				}
			}, 200);
			
				
	}
	function GetBehaviorsError(data){
		console.log(data)
	}
	
	 function SetbehaviorTypeFun(){
		
		 for(var i = 0 ; i < $scope.newBehaviorsArr.length ; i++){
			for(var j = 0 ; j < $scope.behaviorTypes.length ; j++){
				if($scope.newBehaviorsArr[i].subtype == $scope.behaviorTypes[j].typeid ){
					$scope.newBehaviorsArr[i].typeName = $scope.behaviorTypes[j].name;
					$scope.newBehaviorsArr[i].typeDescr = $scope.behaviorTypes[j].description;
					
					$scope.newBehaviorsArr[i].typeid = $scope.behaviorTypes[j].typeid;
					$scope.newBehaviorsArr[i].isactive = $scope.behaviorTypes[j].isactive;
			
				}
			}
	
			for(var k = 0 ; k < $scope.newBehaviorsArr[i].data.length ; k++){
				var n = $scope.newBehaviorsArr[i].data[k].behavior.name;
					$scope.newBehaviorsArr[i].data[k].behavior.newName = n;
					$scope.newBehaviorsArr[i].data[k].behavior.editName = false;
					$scope.newBehaviorsArr[i].data[k].behavior.recalculetedNOW = false;
				}
					
		}

		
	}
	
	function GetCohortsEventsSuccess(data)
	{	
		
		/* if(data.Behaviors){
		
			var GetBehaviorAudienceIDs = $interval(function (){

				if ($scope.BehaviorAudienceIDs)
				{
					
					var show = "show";
					for(var i = 0;i < $scope.behaviorsArr.length; i ++){
						
						$scope.behaviorsArr[i].show = true;
						for(var j = 0;j < $scope.BehaviorAudienceIDs.length; j ++){
							if($scope.behaviorsArr[i].id == $scope.BehaviorAudienceIDs[j]){
								$scope.behaviorsArr[i].show = false;
							}
						}
					}
				$interval.cancel(GetBehaviorAudienceIDs);			
				}
			}, 300);
		}
		$scope.behaviorsArrShow = true; */
		
 		if(data.CohortsEvents && data.CohortsEvents.length){
			$.each(data.CohortsEvents, function (key,val){
				
				if($scope.eventDescrArr.indexOf(val.description) == -1){
					
					$scope.eventDescrArr.push(val.description);
					$scope.tempEventDescrArr.push(val.description);
					
					var temp = {
						"userid_column_name": val.userid_column_name,
						"description": val.description,
						"table_name": val.table_name,
						"modifiers": val.modifiers,
						"gameid": val.gameid,
						"column_name": val.column_name,
						"valueArr" : []
					}
					$scope.eventsArr.push(temp);
				}
			})
		}
		
		if(data.CohortsEvents && data.CohortsEvents.length && $scope.eventsArr.length){
			$.each(data.CohortsEvents, function (key,val){
				
				if(val.value){
					for(var i = 0; i < $scope.eventsArr.length; i ++){
						if(val.description == $scope.eventsArr[i].description){
							$scope.eventsArr[i].valueArr.push(val.value);
							break;
						}
					}
				}
			})
		}
		
	};
	
	function GetCohortsEventsError(data)
	{
		console.log('ERROR! ', data);

	};
	
	
	$scope.editRankNameFun = function(data){
		
		var d = {
			"Rank" : data.id,
			"Name" : data.newName
		}
		
		//localhost:8080/Curve/RenameRank?Token=TEST-YZMF8ICL6W8&Data={"Name":"Audience 1", "Rank":"1"}]}
		
		$scope.curveAPI('RenameRank', {Token: $scope.curApp.token,Data : JSON.stringify(d)}, RenameRankSuccess, RenameRankError);
		
		function RenameRankSuccess(){
		
		
			 for(var i = 0; i < $scope.ranksArr.length; i ++){
				if($scope.ranksArr[i].rank.id == data.id){
					$scope.ranksArr[i].rank.name = data.newName;
					$scope.ranksArr[i].rank.newName = data.newName;
					break;
				}
			}
		}
		function RenameRankError(data){
			console.log(data)
		}
		
	}
	
	$scope.editBehaviorNameFun = function(data){
		
		var d = {
			"Behaviorid" : data.behaviorid,
			"Name" : data.newName
		}
		
	//localhost:8080/Curve/RenameBehavior?Token=TEST-YZMF8ICL6W8&Data={"Name":"behaviorid 1", "Behaviorid":"1"}
	
		$scope.curveAPI('RenameBehavior', {Token: $scope.curApp.token,Data : JSON.stringify(d)}, RenameBehaviorSuccess, RenameBehaviorError);
		
		function RenameBehaviorSuccess(){
		
		
			 for(var i = 0; i < $scope.behaviorsArr.length; i ++){
				if($scope.behaviorsArr[i].behavior.behaviorid == data.behaviorid){
					$scope.behaviorsArr[i].behavior.name = data.newName;
					$scope.behaviorsArr[i].behavior.newName = data.newName;
					break;
				}
			}
		}
		function RenameBehaviorError(data){
			console.log(data)
		}
		
	}
	

	$scope.activateTendencyFun = function(data,e){
		
		var d = {
			"Typeid" : data.typeid,
			"Isactive" : e
		}
		
	//localhost:8080/Curve/ActivateBehavior?Token=TEST-YZMF8ICL6W8&Data={"Isactive":"0", "Behaviorid":"1"}
	
		$scope.curveAPI('ActivateBehaviorType', {Token: $scope.curApp.token,Data : JSON.stringify(d)}, ActivateTendencySuccess, ActivateTendencyError);
		
		function ActivateTendencySuccess(){
		
		//console.log(data)
		
			//for(var i = 0; i < $scope.newBehaviorsArr.length; i ++){
				//if($scope.newBehaviorsArr[i].typeid == data.typeid){
			//	$scope.newBehaviorsArr[i].isactive = e;
			//	break;
			//	}
		//	}
			$scope.getAppDataFun($scope.curApp.token);
			
		}
		function ActivateTendencyError(data){
			//console.log(data)
			$scope.getAppDataFun($scope.curApp.token);
		}
		
	}
	
	$scope.activateBehaviorFun = function(data,e){
		
		var d = {
			"Behaviorid" : data.behaviorid,
			"Isactive" : e
		}
		
	//localhost:8080/Curve/ActivateBehavior?Token=TEST-YZMF8ICL6W8&Data={"Isactive":"0", "Behaviorid":"1"}
	
		$scope.curveAPI('ActivateBehavior', {Token: $scope.curApp.token,Data : JSON.stringify(d)}, ActivateBehaviorSuccess, ActivateBehaviorError);
		
		function ActivateBehaviorSuccess(){
		
		//console.log(data)
		//	for(var i = 0; i < $scope.behaviorsArr.length; i ++){
			//	if($scope.behaviorsArr[i].behavior.behaviorid == data.behaviorid){
				//	$scope.behaviorsArr[i].behavior.isactive = e;
					//break;
				//}
			//}
			$scope.getAppDataFun($scope.curApp.token);
		}
		function ActivateBehaviorError(data){
		//	console.log(data)
			//for(var i = 0; i < $scope.behaviorsArr.length; i ++){
				//if($scope.behaviorsArr[i].behavior.behaviorid == data.behaviorid){
					//$scope.behaviorsArr[i].behavior.isactive = e;
					//break;
				//}
			//}
			$scope.getAppDataFun($scope.curApp.token);
		}
		
	}
	
	
	/********************************************* add new Behavior type*****************************************/
	
	$scope.addBehaviorTypeModalFun = function(){
		$scope.updateModelTNDiconFun();
		$scope.newAudienceType = {'name' : '','descr' : ''};
		$('#addNewBehavioralType').modal('show');
	}
	
	$scope.addBehaviorTypeFun = function(){
		
		var d = {
			"Name" : $scope.newAudienceType.name,  ///from user
			"Description": $scope.newAudienceType.descr   ///from user
		}

		//localhost:8080/Curve/AddAudienceType?Token=A1K4WKUW6YTPQVPY&Data={"Name":"test3@permissions.com","Description":"Read"}
		$scope.curveAPI('AddBehaviorType', {Token: $scope.curApp.token,Data : JSON.stringify(d)}, AddBehaviorTypeSuccess, AddBehaviorTypeError);
	}
	function AddBehaviorTypeSuccess(data){
		$('#addNewBehavioralType').modal('hide');
		$scope.newAudienceType = {'name' : '','descr' : ''};
		
		if($scope.modelUploadImg.image != ''){
			$scope.profPicUploadFun();
		}else{
			$scope.getAppDataFun($scope.curApp.token);
		}
			
	}
	function AddBehaviorTypeError(data){$scope.newAudienceType = {'name' : '','descr' : ''};console.log(data)	}
	
	/********************************************* end add new Behavior type*****************************************/
	
	$scope.getBehaviorTypesFun = function(){
	
		//localhost:8080/Curve/GetAudienceTypes?Token=A1K4WKUW6YTPQVPY
		$scope.curveAPI('GetBehaviorTypes', {'Token': $scope.curApp.token}, GetBehaviorTypesSuccess, GetBehaviorTypesError);
	}
	function GetBehaviorTypesSuccess(data){
		
		$scope.behaviorTypes = data;
		$scope.curveAPI('GetBehaviors', {Token: $scope.curApp.token}, GetBehaviorsSuccess, GetBehaviorsError);
		$scope.audienceTypeArr = [];
		$scope.tempAudienceTypeArr = [];
		$scope.foolAudienceTypeArr = [];
		
		$.each(data, function (key,val){
				
				if($scope.audienceTypeArr.indexOf(val.name) == -1){
					
					$scope.audienceTypeArr.push(val.name);
					$scope.tempAudienceTypeArr.push(val.name);
					
					
				}
			})
		$scope.foolAudienceTypeArr = data;
		//console.log($scope.foolAudienceTypeArr)	
	}
	function GetBehaviorTypesError(data){console.log(data)	}
	
	/****************************************************** create new Behavioral fun *****************************************/
	

	$scope.createBehaviorModalFun = function(){
		
		//$scope.getBehaviorTypesFun();
		$scope.tempEventDescrArr = $scope.eventDescrArr;
		$scope.audienceModalDetails	= [];
		$scope.newAudience = {'name' : '', 'typeName' : '', 'type': '','description' : '', 'ConditionType' : '','viewByCondition' : 1}
		$scope.audienceModal = {'step' : 1, 'valueArr' : [],'tempValueArr' : [], 'checkAudienceTypeName' : '', 'data' : 
			{	
				"checkEventName" : "",
				"behaviorName" : "", 
				"itemID" : "",
				"Type" : "Action",  ///from user
				"QueryTable":"", ///table_name
				"ColumnName":"",   ///column_name
				"HwIDColumnName":"",		///userid_column_name
				"Modifier": "",				///modifiers
				"Timing":"",                ///from user
				"TimingDefinition":"",   ///from user
				"TimingPeriod":"",      ///from user
				"Condition":"",          ///from user
				"Value":"",                 ///from user
				"ValueFrom" : "",			///from user
				"ValueTo" : "",				///from user
				"NextCondition":"AND",         ///from user
				"ID" : ""
			}
		};
		
		$('#createAudienceModal').modal('show');
	}
	
	$scope.searchAudTypeFun = function(str){
		
		if(str != ''){			
			$scope.tempAudienceTypeArr = [];
			$.each($scope.audienceTypeArr, function (key,val){
				
				if(val.search(new RegExp(str, "i")) != -1){

					$scope.tempAudienceTypeArr.push(val);
					
				}
			})

		}else{
			$scope.tempAudienceTypeArr = $scope.audienceTypeArr;
		}
		
	}
	

	$scope.checkAudienceTypeFun = function(k){
	
		for(var i = 0; i < $scope.foolAudienceTypeArr.length; i ++){
			if($scope.foolAudienceTypeArr[i].name == k){
				$scope.newAudience.typeName = $scope.foolAudienceTypeArr[i].name;
				$scope.newAudience.type =  $scope.foolAudienceTypeArr[i].typeid;
			}
		}
		
		
	}
	
	
	$scope.searchEventFun = function(str){
		
		$scope.newAudience.viewByCondition = 0;
		if(str != ''){			
			$scope.tempEventDescrArr = [];
			$.each($scope.eventDescrArr, function (key,val){
				
				if(val.search(new RegExp(str, "i")) != -1){

					$scope.tempEventDescrArr.push(val);

				}
			})

		}else{
			$scope.tempEventDescrArr = $scope.eventDescrArr;
			$scope.audienceModal.data.Modifier = '';
		}
		
	}
	
	
	$scope.searchValueFun = function(str){
		
		if(str != ''){			
			$scope.audienceModal.tempValueArr = [];
			$.each($scope.audienceModal.valueArr, function (key,val){
				
				if(val.search(new RegExp(str, "i")) != -1){

					$scope.audienceModal.tempValueArr.push(val);

				}
			})

		}else{
			$scope.audienceModal.tempValueArr = $scope.audienceModal.valueArr;
		}
		
	}
	
	
	$scope.checkEventTypeFun = function(name){
		
		$scope.audienceModal.data.checkEventName = name;
		$scope.newAudience.viewByCondition = 1;
		var eventData = $filter('filter')($scope.eventsArr,{description: name})[0];
		$scope.audienceModal.data.QueryTable = eventData.table_name;
		$scope.audienceModal.data.ColumnName = eventData.column_name;
		$scope.audienceModal.data.HwIDColumnName = eventData.userid_column_name;
		$scope.audienceModal.data.Modifier = eventData.modifiers;
		if($scope.audienceModal.data.Modifier == '1'){$scope.newAudience.ConditionType = 'not equals'; $scope.audienceModal.data.Condition = '1001'}
		if($scope.audienceModal.data.Modifier == '2'){$scope.newAudience.ConditionType = 'less than'; $scope.audienceModal.data.Condition = '2001'}
		if($scope.audienceModal.data.Modifier == '3'){$scope.newAudience.ConditionType = 'equals'; $scope.audienceModal.data.Condition = '3001'}
		if($scope.audienceModal.data.Modifier == '4'){$scope.newAudience.ConditionType = 'not equals'; $scope.audienceModal.data.Condition = '4001'}
		$scope.audienceModal.valueArr = eventData.valueArr;
		$scope.audienceModal.tempValueArr = eventData.valueArr;
		$scope.audienceModal.data.EventName = '';
		$scope.audienceModal.data.Timing = '';
		$scope.audienceModal.data.Value = '';
		$scope.audienceModal.data.ValueFrom = '';
		$scope.audienceModal.data.ValueTo = '';
		
	}

	$scope.changeConditionFun = function(i,e){
		if($scope.audienceModalDetails[i].NextCondition == 'AND'){
			$scope.audienceModalDetails[i].NextCondition = 'OR';
		}else{
			$scope.audienceModalDetails[i].NextCondition = 'AND';
		}
		
	}
	
	$scope.setItemAudienceModalFun = function(){
		$scope.audienceModal.data.Timing = ""+$scope.audienceModal.data.Timing+"";
		$scope.audienceModal.data.itemID = $scope.audienceModalDetails.length;	
		$scope.audienceModalDetails.push($scope.audienceModal.data);
		$scope.audienceModal = {'step' : 0, 'valueArr' : [],'tempValueArr' : [], 'checkAudienceTypeName' : '','data' : 
			{	
				"checkEventName" : "",
				"behaviorName" : "", 
				"itemID" : "",
				"Type" : "Action",  ///from user
				"QueryTable":"", ///table_name
				"ColumnName":"",   ///column_name
				"HwIDColumnName":"",		///userid_column_name
				"Modifier": "",				///modifiers
				"Timing":"",                ///from user
				"TimingDefinition":"",   ///from user
				"TimingPeriod":"",      ///from user
				"Condition":"",          ///from user
				"Value":"",                 ///from user
				"ValueFrom" : "",			///from user
				"ValueTo" : "",				///from user
				"NextCondition":"AND",         ///from user
				"ID" : ""
			}
		};
		
	}
	
	$scope.CreateAudienceFun = function(){
		
		for(var i = 0; i < $scope.audienceModalDetails.length; i ++){
			$scope.audienceModalDetails[i].Value = ""+$scope.audienceModalDetails[i].Value+"";
		}
		$scope.audienceModal.data.Timing = ""+$scope.audienceModal.data.Timing+"";
		var d = {
			"Name" : $scope.newAudience.name,  ///from user
			"Description": $scope.newAudience.description,   ///from user
			"Type": $scope.newAudience.type,
			"Props": $scope.audienceModalDetails
		}
		$scope.newBehaviorsArr = null;
		//console.log(d)
		$scope.curveAPI('CreateBehavior', {Token: $scope.curApp.token,Data : JSON.stringify(d)}, CreateBehaviorSuccess, CreateBehaviorError);
	}
	
	function CreateBehaviorSuccess(data){
		$scope.curveAPI('GetBehaviors', {Token: $scope.curApp.token}, GetBehaviorsSuccess, GetBehaviorsError);
	}
	
	function CreateBehaviorError(data){
		console.log(data)
		alert(data)
	}
	
	$scope.audienceEditItemFun = function(id){
		
		for(var i = 0; i < $scope.audienceModalDetails.length; i ++){
			if($scope.audienceModalDetails[i].itemID == id){
				var t = $scope.audienceModalDetails[i];
				$scope.audienceModal = {'step' : 2,'valueArr' : [],'tempValueArr' : [], 'data' : t};
				$scope.audienceDelItemFun(id);
				break;
			}
		}
	}
	
	
	$scope.audienceDelItemFun = function(id){
		
		for(var i = 0; i < $scope.audienceModalDetails.length; i ++){
			if($scope.audienceModalDetails[i].itemID == id){
				$scope.audienceModalDetails.splice(i, 1);
				break;
			}
		}
		
	}
	
	/***********************************  end create Behavioral fun *********************************/
	
	$scope.startAdnimPanel();
	
	
	
})