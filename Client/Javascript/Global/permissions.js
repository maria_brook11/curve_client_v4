function PermmissionsFunction($scope,$rootScope,$http,$location,$filter){

$scope.selectedAppName = 'Select App';

$scope.permissionManagerFun = function ()
	{
		$scope.curveAPI('GetUserCompaniesUsersPermissions', null, onGotCompanyUsersPermissions);
	}

function onGotCompanyUsersPermissions(data){
	
	$scope.UsersPermissionsArr = data;
	
	if($scope.UsersPermissionsArr.length > 0){
		
		$scope.permissionsAppSelected($scope.UsersPermissionsArr[0].gameid);
	}
}

$scope.permissionsAppSelected = function(i){
	
	if($scope.UsersPermissionsArr.length > 0){
		$scope.permissionCurApp = $filter('filter')($scope.UsersPermissionsArr,{gameid: i})[0];
		$scope.selectedAppName = $scope.permissionCurApp.gamename;
		$scope.selectedAppID = $scope.permissionCurApp.gameid;
	}
	
	if($scope.permissionCurApp.users.length > 0){
		for(var i = 0;i < $scope.permissionCurApp.users.length; i ++){
		
			$scope.permissionCurApp.users[i].deleteItem = false;
			$scope.permissionCurApp.users[i].edit = 0;
		}
	}
	if($scope.permissionCurApp.tempusers.length > 0){
		for(var i = 0;i < $scope.permissionCurApp.tempusers.length; i ++){
		
			$scope.permissionCurApp.tempusers[i].deleteItem = false;
			$scope.permissionCurApp.tempusers[i].edit = 0;
		}
	}
	
};



$scope.userPermissionEdit = function(d,e){
	
	if(e == 'r'){
		var temp = {"UserID" : d.userid, "Email" : d.email,"GamesPermissions":[{"GameID" : $scope.selectedAppID, "Permission" : d.newPermission}]};
		var data = {'Data' : JSON.stringify(temp)};
		$scope.curveAPI('SetRegisteredUserPermissions',data, RegUserPermSuccess);
		
		
	}else{
		var temp = {"Email" : d.email,"GamesPermissions":[{"GameID" : $scope.selectedAppID, "Permission" : d.newPermission}]};
		var data = {'Data' : JSON.stringify(temp)};
		$scope.curveAPI('SetTempUserPermissions', data, TempUserPermSuccess);
		
		
	}
	
	function RegUserPermSuccess(){
		
		for(var i = 0;i < $scope.permissionCurApp.users.length; i ++){
			if($scope.permissionCurApp.users[i].userid == d.userid){
				
				$scope.permissionCurApp.users[i].edit = 0;
			}
		}
	}
	function TempUserPermSuccess(){
		for(var i = 0;i < $scope.permissionCurApp.tempusers.length; i ++){
			if($scope.permissionCurApp.tempusers[i].userid == d.userid){
				
				$scope.permissionCurApp.tempusers[i].edit = 0;
			}
		}
	
	}	
}






$scope.userPermDelete = function(d){
		
		var data = 'Email='+d.email+'';
		$scope.curveAPI('DeleteUser', data, userPermDeleteSuccess);	
	
		function userPermDeleteSuccess(){
	
			//console.log('5656565656565')
		}
} 

	
$scope.showPermissions = function()
{
	$('.loading-page').show();
	$scope.curveAPI('GetUserPersonalPermissions', {}, gotUserPermissions, getUserInvitationsFail);
}

$scope.onAddClick = function()
{	
	$("#emailAlready").hide();
    var newMail = $("#newUserEmail").val().toLowerCase().replace(/\s/g, "");
   
    if(!validateEmail(newMail))
    {
        $scope.show_error(newMail + ' is not a valid email address!');
        $("#newUserEmail").val("");
        return;
    }
	
	var Already = false;
	var newInvitation = false;
    for(var i = 0;i < $scope.permissionCurApp.users.length; i ++){
		if($scope.permissionCurApp.users[i].email == newMail){
			
			Already = true;
			break;
		}
	}
	
	for(var i = 0;i < $scope.permissionCurApp.tempusers.length; i ++){
		if($scope.permissionCurApp.tempusers[i].email == newMail){
			
			newInvitation = true;
			break;
		}
	}
	
        if(Already)
        {
            $("#emailAlready").show();
            return;
        }
		
	if(newInvitation){
		var temp = "Email="+ newMail ;
		$scope.curveAPI('ResendInvitationEmail', temp, ResendInvitationSuccess);
			
		function ResendInvitationSuccess(){
			
			alert('Successfully sent Invitation');
		}
	}else{
		var newPerm = $("#newUserPermission option:selected").val();
		
		var temp = {"Email" : newMail,"GamesPermissions":[{"GameID" : $scope.selectedAppID, "Permission" : newPerm}]};
			var data = {'Data' : JSON.stringify(temp)};
			$scope.curveAPI('SetTempUserPermissions', data, newUserPermSuccess);
			
		function newUserPermSuccess(){
			
			var t = {"deleteItem":false,"edit":0,"email":newMail,"permission":newPerm}
			$scope.permissionCurApp.tempusers.push(t);
		}
	}	
}



function validateEmail(email) 
{ 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


function gotUserPermissions(data)
{
	var comaniesListed = {};
	
	

	var ln = data.length;
	$scope.games = [];
	var companies = [];
	
	for(var i = 0 ; i < ln ; i++)
	{
		var perm = data[i];
		var gamename = perm.gamename;
		
		if(gamename != null)
			$scope.games.push(perm);
		else
			companies.push(perm);
	}
	//console.log($scope.games)

}

function getUserInvitationsFail(data)
{
	$('.loading-page').hide();
	$scope.show_error('Somthing went wrong', 'Could not retrieve user invitations.');
}

function getInvitationLine(companyname, companyID)
{
	var onClickFunc = 'onclick="approveCompany(\'' + companyID + '\');"';
	var ans = '<tr>';
	ans += '<td class="highlight">' + companyname + '</td>';
	ans += '<td><a href="#" ' + onClickFunc + 'class="btn default btn-xs green"><i class="fa fa-edit"></i> Accept </a></td>';
	ans += '</tr>';
	return ans;
}

}