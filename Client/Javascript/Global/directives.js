app.directive('Metronic', function(){
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			$(element).Metronic(scope.$eval(attrs.Metronic));
		}
	};
});