var app = angular.module("Main", ['ngRoute','ngCookies','ngAnimate','rzModule','ngclipboard','angular-toArrayFilter','angular-intro','ngFileUpload','chart.js','ngSanitize','ngTouch', 'ngMaterial']);

var generalPath = {};
	generalPath["old"] = "";
	generalPath["new"] = "";

angular.element(document).ready(function() {

      angular.bootstrap(document, ['Main']);

   });

   function parseCurveResponse(data) {
    var scope = angular.element(document.getElementById("MainWrap")).scope();
    scope.$apply(function () {
    scope.parseCurveResponse(data);
    });
}
   
app.run(function ($rootScope, $location,$route,$window) {
	
	$rootScope.loginView = false;
	$rootScope.loginTpl = 0;
	$rootScope.viewForm = 0;
	$rootScope.startStatus;
	$rootScope.appsArr = [];
	$rootScope.Token = '';
	$rootScope.extern = false;
	$rootScope.protocol = '';
	$rootScope.mainUrl = '';
	$rootScope.curentUrl = '';
	$rootScope.production = false;
	$rootScope.host = '';
	$rootScope.dashboardView = {};
	$rootScope.httpUrl = '';
	
	

	
	
	$rootScope.$on('$routeChangeStart', function (event, scope, newRoute) {
	
		$rootScope.locPath = $location.path();
	
		if($rootScope.locPath == '/'){
			$rootScope.loginTpl = 0;
			$rootScope.viewForm = 0;
		}
		if($rootScope.locPath.indexOf('register-trial') != -1){
			$rootScope.loginTpl = 1;
			$rootScope.viewForm = 2;
		}
		if($rootScope.locPath.indexOf('start-trial') != -1){
			$rootScope.loginTpl = 2;
			$rootScope.viewForm = 3;
			$rootScope.startStatus = 0;
		}
		if($rootScope.locPath.indexOf('invitation') != -1){
			$rootScope.loginTpl = 2;
			$rootScope.viewForm = 3;
			$rootScope.startStatus = 1;
		}
		if($rootScope.locPath.indexOf('resetpassword') != -1){
			$rootScope.loginTpl = 3;
			$rootScope.viewForm = 4;
		}
		if($rootScope.locPath.indexOf('extern') != -1){
			$rootScope.extern = true;
		}
		/* if($rootScope.locPath.indexOf('settings') != -1){
			$rootScope.dashboardView.active = 7;
		} */
		
	});
	
});


app.config(['$routeProvider','$locationProvider','$httpProvider',
	function($routeProvider,$locationProvider,$httpProvider) {
	
	$routeProvider.
		when('/', {
			templateUrl: 'Templates/Login/login.html',
			controller: 'LoginCtrl'
			}).
		when('/admin', {
			templateUrl: 'Templates/Dashboard/admin.html',
			controller: 'AdminCtrl'
			}).
		when('/start-trial/:name?/:companyName?/:token?/:email?', {
			templateUrl: 'Templates/Login/login.html',
			controller: 'LoginCtrl'
			}).
		when('/invitation/:CustomerID?/:Email?/:CompanyName?', {
			templateUrl: 'Templates/Login/login.html',
			controller: 'LoginCtrl'
			}).
		when('/resetpassword/:Token?/:Email?', {
			templateUrl: 'Templates/Login/login.html',
			controller: 'LoginCtrl'
			}).
		when('/register-trial/:Coupon?', {
			templateUrl: 'Templates/Login/login.html',
			controller: 'LoginCtrl'
			}).
		when('/dashboard/:Token?/:settings?', {
			reloadOnSearch: false,
			templateUrl: 'Templates/Dashboard/dashboard.html',
			controller: 'DashbrdCtrl'
			}).
		/* when('/settings/:Token?/:tab?', {
			reloadOnSearch: false,
			templateUrl: 'Templates/Dashboard/dashboard.html',
			controller: 'DashbrdCtrl'
			}).	 */
		when('/extern/:t?/:d?/:gn?/:com?/:icon?/:graphtype?/:dateFrom?/:dateTo?/:data?/:opt?', {
			templateUrl: 'Templates/Extern/extern.html',
			controller: 'ExternCtrl'
			}).
		when('/onboarding/:Platform?/:AppToken?/:appsData?', {
			templateUrl: 'Templates/Onboarding/onboarding.html',
			controller: 'OnboardingCtrl'
			}).
		otherwise({
			redirectTo: '/'
			});
	  
	//$locationProvider.html5Mode({enabled:true, requireBase:false});
	//$locationProvider.hashPrefix('!');
/* $httpProvider.defaults.withCredentials = true;
$httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
 delete $httpProvider.defaults.headers.post['Content-type']; */	  
	/* $httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
	$httpProvider.defaults.useXDomain = true;
	delete $httpProvider.defaults.headers.common['X-Requested-With'];
	 */
	$httpProvider.defaults.withCredentials = true;
		
}]);
