function DashbGMFunction($scope,$rootScope,$http,$location){

$scope.GameModeManager = {};

$scope.GameModeManager.setGameMode = function(token, mode)
{
	$scope.GameModeManager.modeByToken = JSON.parse($scope.loadFromStorage('$scope.GameModeManager.modeByToken'));
	if($scope.GameModeManager.modeByToken == null) $scope.GameModeManager.modeByToken = {};
	$scope.GameModeManager.modeByToken[token] = mode;
	$scope.storeToCache('$scope.GameModeManager.modeByToken', JSON.stringify($scope.GameModeManager.modeByToken));
}

$scope.GameModeManager.setDashboardsMode = function(token)
{
	$scope.GameModeManager.modeByToken = JSON.parse($scope.loadFromStorage('$scope.GameModeManager.modeByToken'));
	if($scope.GameModeManager.modeByToken == null) $scope.GameModeManager.modeByToken = {};
	
	var mode = $scope.GameModeManager.modeByToken[token] == null ? "" : $scope.GameModeManager.modeByToken[token];
	
	jQuery(document).ready(function()
	{
		
		if(mode.toLowerCase() == "install")
		{
			$(".game_mode").each(function(t, s){$(s).addClass("not_in_mode");});
		}
		else
		{
			$(".game_mode").each(function(t, s){$(s).removeClass("not_in_mode");});
		}
	});
}

}