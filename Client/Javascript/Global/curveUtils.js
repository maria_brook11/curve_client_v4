function CurveUtilsFunction($scope,$rootScope,$http,$location){

	$scope.getURLParams = function(url)
	{
		var paramsString = url.split('?')[1];

		if(paramsString == null)
			return null;

		var params = paramsString.split('&');

		if(params == null)
			return null;

		var resp = {};
		for(var i = 0 ; i < params.length ; i++)
		{
			var p = params[i].split('=');
			resp[p[0]] = p[1];
		}

		return resp;
	}
	
	$scope.isGlobalFirstLogin = function()
	{
		return $scope.loadFromCache("globalFirstLogin");
	}
}