function TopPanelFunction($scope,$rootScope,$http,$location,URLConst,$filter,Upload,$timeout){	

/************************************  update prof pic *************************************/
	$scope.updateImageModalOpenFun = function(e){
		$scope.modelUploadImg = { 
			code: '',
			image: '',
			statusImgMes : '',
			imgFor : e
		};
		$scope.imgFor = e;
		$('#updateImg').modal('show');
		
	}
	
	$scope.profPicReadSRCFun = function(input) {
		
		$scope.modelUploadImg.image = input;
      if (input) {
            
			var reader = new FileReader();

            reader.onload = function (e) {

               $scope.modelUploadImg.code = e.target.result;
			   
            }
    
            reader.readAsDataURL(input);
			
        }else{
			$scope.modelUploadImg.statusImgMes = "Max size 20MB";
		}
    }

	$scope.profPicRemoveFun = function(){
		delete $scope.modelUploadImg;
		$scope.modelUploadImg = { 
			code: '',
			image: '',
			statusImgMes : '',
			imgFor : $scope.imgFor
		};
	}
	
 	$scope.profPicUploadFun = function(){
		
		
		if($scope.modelUploadImg.imgFor == 'user'){
			var fileName = 'userupload.php';
			var y = $scope.loadFromStorage("userid");
			var uploadData = {file1: $scope.modelUploadImg.image, 'UserId': y};
			
		}
		if($scope.modelUploadImg.imgFor == 'app'){
			var fileName = 'upload.php';
			var custID = $scope.loadFromStorage('currentCustomerID');
			var uploadData = {file1: $scope.modelUploadImg.image, 'CustomerId': custID, 'AppToken' : $rootScope.Token };
			
		}
		
		
		
		Upload.upload({
			url: ''+$rootScope.curentUrl+'includes/'+fileName,
			method: "POST",
			data: uploadData
		}).then(
			
			function suc(response){
				
				if(response.data.status == "success"){
					
					
					if($scope.modelUploadImg.imgFor == 'user'){
						
						var url = ''+$rootScope.curentUrl+'includes' + response.data.path;
						$scope.modelUploadImg.statusImgMes = "Successfully uploaded";
					
						$timeout(function () {
							setUserIcon(url);
						}, 1000);
						
					}
					if($scope.modelUploadImg.imgFor == 'app'){
						
						var url = ''+$rootScope.curentUrl+'includes' + response.data.path;
						$scope.modelUploadImg.statusImgMes = "Successfully uploaded";
						
						$timeout(function () {
							sendAppIcon(url);
						}, 1000);
					}
				} 
				if(response.data.status == "fail"){
					//alert("Error : "+response.data.message);
					$scope.modelUploadImg.statusImgMes = "Error : "+response.data.message;
				}
				
			},

			function err(response){
				alert("Couldn't upload file");
			},

			function prg(e) {
				var progressPercentage = parseInt(100.0 * e.loaded / e.total);
				//console.log('Progress: ' + progressPercentage + '%');
			}
		);
	};

function setUserIcon(iconUrl){
		$scope.email = $scope.loadFromStorage('email');
		$scope.curvePostAPI('SetUserImageURL', {"Email": $scope.email, "ImageURL": iconUrl}, setUserIconCallback);

		function setUserIconCallback() 
		{
			$scope.storeToCache("updateImg", iconUrl);
			//location.reload();
			var random = Math.floor(Math.random() * 1000) + 1;
			$scope.userImage  = iconUrl+'?r='+random;
			$scope.$apply('userImage', function(){})
			$('#updateImg').modal('hide');
		}
	}

function sendAppIcon(iconUrl) 
{
	$scope.curvePostAPI('SetGameIcon', {"Token": $rootScope.Token, "Icon": iconUrl}, setAppIconCallback);
	
	function setAppIconCallback() 
	{
        var random = Math.floor(Math.random() * 1000) + 1;
		$scope.activeAppImg  = {'icon':iconUrl+'?r='+random};
		$scope.$apply('activeAppImg', function(){})
		$('#updateImg').modal('hide');
    }
}


/************************************ end update prof pic *************************************/
 
 
$scope.TopPanelStart = function(){
	 Metronic.initAjax();
	
		$scope.email = $scope.loadFromStorage('email');
		$scope.curUserEmail = $scope.loadFromStorage('email');
		
		
		var curApp = $filter('filter')($rootScope.appsArr,{Token: $rootScope.Token})[0];
		if(curApp.Logo){
				$scope.cssApp = {"logo" : curApp.Logo,"bgColor" : curApp.BgColor,"bgImg" : curApp.BgImage};
		}else{
				$scope.cssApp = {"logo" : "Images/cssApp/logoDefault.svg","bgColor" : "#41B7E9;","bgImg" : "Images/cssApp/bgDefault.png"};		
		}

		if($scope.email == null)
			$scope.email = "user unknown";
		
		if($scope.email.length > 15)
			$scope.email = $scope.email.substring(0, 15)+"...";

		$scope.userImage = $scope.loadFromStorage('userImage');

};


function debugInsights()
{
//////////
				//DEBUG
			       
	
				var arrNew = [{id:"test1", seen:"false", time:"2016-06-22 11:33:05", text : "long_text", heading : "short_text"},
							  {id:"test2", seen:"false", time:"2016-06-22 05:33:05", text : "long_text1", heading : "short_text1"}]
					

				var arrRead = [{id:"test21", seen:"true", time:"2016-06-22 01:10:00", text : "long_text21", heading : "short_text21"},
								{id:"test22", seen:"true", time:"2016-06-20 11:33:05", text : "long_text22", heading : "short_text22"},
								{id:"test23", seen:"true", time:"2016-06-19 03:13:55", text : "long_text23", heading : "short_text23"},
								{id:"test24", seen:"true", time:"2016-06-01 11:33:05", text : "long_text24", heading : "short_text24"},
								{id:"test25", seen:"true", time:"2016-05-10 12:00:00", text : "long_text25", heading : "short_text25"},
								{id:"test26", seen:"true", time:"2015-06-20 11:33:05", text : "long_text26", heading : "short_text26"},
							  {id:"test27", seen:"true", time:"2011-06-19 05:33:05", text : "long_text27", heading : "short_text27"}]					
				fillInsights(arrNew, arrRead);
				
				
				///////////
		}
		
		function debugReadInsights()
{
//////////
				//DEBUG
			       
	
				var arrNew = []
					

				var arrRead = [
				{id:"test1", seen:"true", time:"2016-06-22 11:33:05", text : "long_text", heading : "short_text"},
							  {id:"test2", seen:"true", time:"2016-06-22 05:33:05", text : "long_text1", heading : "short_text1"},
							  {id:"test21", seen:"true", time:"2016-06-22 01:10:00", text : "long_text21", heading : "short_text21"},
								{id:"test22", seen:"true", time:"2016-06-20 11:33:05", text : "long_text22", heading : "short_text22"},
								{id:"test23", seen:"true", time:"2016-06-19 03:13:55", text : "long_text23", heading : "short_text23"},
								{id:"test24", seen:"true", time:"2016-06-01 11:33:05", text : "long_text24", heading : "short_text24"},
								{id:"test25", seen:"true", time:"2016-05-10 12:00:00", text : "long_text25", heading : "short_text25"},
								{id:"test26", seen:"true", time:"2015-06-20 11:33:05", text : "long_text26", heading : "short_text26"},
							  {id:"test27", seen:"true", time:"2011-06-19 05:33:05", text : "long_text27", heading : "short_text27"}]					
				fillInsights(arrNew, arrRead);
				
				
				///////////
		}
function getUserInvitationsCallback(data)
{
	var invitations = data.UserInvitations;
	var ln = invitations.length;
	
	if(ln != 0)
	{
		$('div#img-circle-note').append(ln);
		$('div#img-circle-note')[0].style.display = "";
		
		$('#inbox_text_holder')[0].innerHTML = "(" + ln + ")";
	}
	else
	{
		$('#inbox_text_holder')[0].innerHTML = "";
		$('div#img-circle-note')[0].style.display = "none";
	}
}

function getUserInvitationsFail(data)
{
}

	
var NotificationsTopPanelManager = function()
{ 
	NotificationsTopPanelManager.instance = this;

};
	
NotificationsTopPanelManager.prototype = 
{
	clearInsights: function () {
		 NotificationsTopPanelManager.instance.hideInsightsBadge();
		$("#top_panel_notification_dropdown_list").empty();
	},
	
	fillInsights: function(arrNew, arrRead)
	{
		NotificationsTopPanelManager.instance.clearInsights();
		var insights_str = "";
		for (i=0; i<arrNew.length ;i++)
		{ 
			insights_str+= NotificationsTopPanelManager.instance.fillInsightRow(arrNew[i]);
		}
		
		for (j=0; j<arrRead.length ;j++)
		{ 
			insights_str+= NotificationsTopPanelManager.instance.fillInsightRow(arrRead[j]);
		}
		
		if (arrNew.length > 0)
		{
			 NotificationsTopPanelManager.instance.showInsightsBadge(arrNew.length);
		}
		else
		{
			
		}
		
		$("#top_panel_notification_dropdown_list").prepend(insights_str);
		
		
	}, 
	
	notificationHolderCreator: function ()
	{
		
		var top_menu_append_str = "";

		top_menu_append_str +='<ul class="nav navbar-nav pull-right">';
		top_menu_append_str +='	<li class="dropdown dropdown-extended dropdown-notification" id="top_panel_notification_bar">';
		top_menu_append_str +='		<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" onclick="$(\'#top_panel_notification_badge\').empty();" data-hover="dropdown" data-close-others="true">';
		top_menu_append_str +='			<i class="fa fa-bell-o" style="color:#fff; font-size:26px;"></i>';
		// top_menu_append_str +='		<i class="fa fa-bell" style="color:#fff; font-size:26px;"></i>';
		top_menu_append_str +='			<span class="badge badge-default badge-m" id="top_panel_notification_badge"></span>';
		top_menu_append_str +='		</a>';
		top_menu_append_str +='		<ul class="dropdown-menu dropdown-m" id="top_panel_notification_dropdown">';
		top_menu_append_str +='			<li class="external-m">';
		top_menu_append_str +=' 			<h3 class="dropdown-h" id="top_panel_notification_dropdown_header">';
		top_menu_append_str +='				<span class="bold" id="top_panel_notification_dropdown_header_text">0 new</span> insights</h3>';
		// top_menu_append_str +='			<a class="a-m" href="page_user_profile_1.html">view all</a>';
		top_menu_append_str +=' 		</li>';
		top_menu_append_str +='			<li>';
		top_menu_append_str +=' 			<div style="position: relative; overflow: hidden; width: auto; height: 250px;" class="slimScrollDiv">'
		top_menu_append_str +='					<div data-initialized="1" class="dropdown-menu-list  scroller" id="top_panel_notification_dropdown_list" style="overflow-y: scroll;height: 250px;" ';
		top_menu_append_str +='						style="height: 250px; overflow: hidden; width: auto;" data-handle-color="#637283" >';
		top_menu_append_str +='					</div>';
		top_menu_append_str +='					<div style="background: rgb(99, 114, 131) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 75px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 121.359px;" class="slimScrollBar"></div><div style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(234, 234, 234) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;" class="slimScrollRail"></div></div>';
		top_menu_append_str +='			</li>';
		top_menu_append_str +='		</ul>';
		top_menu_append_str +='	</li>'
		top_menu_append_str +='</ul>';
		return top_menu_append_str;
	},


	getInsightDate: function(timestr)
	{
		var times = timestr.split(" ");
		var yyyymmdd = times[0].replace(/-/g,"");
		var hhmmss = times[1];
		
		return new Date( timestr);
	},
	
	getInsightTime: function(insightData)
	{
		var times = insightData.time.split(" ");
		var yyyymmdd = times[0].replace(/-/g,"");
		var hhmmss = times[1];
		var dateStr = yyyymmdd+":"+hhmmss;
		var momentStr = moment(dateStr, "YYYYMMDD:hh:mm:ss").fromNow();
		momentStr = momentStr.replace("minutes", "min");
		momentStr = momentStr.replace("minute", "min");
		momentStr = momentStr.replace("hours", "hrs");
		momentStr = momentStr.replace("hour", "hrs");
		momentStr = momentStr.replace("seconds", "secs");
		momentStr = momentStr.replace("second", "secs");
		momentStr = momentStr.replace("ago", "");
		momentStr = momentStr.replace("in", "");
		momentStr = momentStr.replace("an ", "1 ");
		momentStr = momentStr.replace("a ", "1 ");
		
		return momentStr;
	},
	
	clickOnInsight: function(insightID)
	{
		
		NotificationsManager.instance.onNotificationClick(insightID);
	},
	
	fillInsightRow: function(insightData)
	{
		var insight_str = "";
		
		if (insightData.seen == 'false')
			insight_str +='<li class="lim" onClick="NotificationsTopPanelManager.instance.clickOnInsight('+insightData.id+')">';
		else
			insight_str +='<li class="lim limUnread" onClick="NotificationsTopPanelManager.instance.clickOnInsight('+insightData.id+')">';
			
        insight_str +='<a href="javascript:;">';
		
		if (insightData.seen == 'false')
			 insight_str +='<span class="new-m">NEW</span>';
		else
			 insight_str +='<span class="time-m">' + NotificationsTopPanelManager.instance.getInsightTime(insightData) + '</span>';
			
       
        insight_str +='<span class="details">';
		//insight_str +='<span class="label label-sm label-icon label-success">';
		insight_str += insightData.heading;
		insight_str +='</a>';
		insight_str +='</li>';


		return insight_str;
	},

	showInsightsBadge: function(num)
	{
		$("#top_panel_notification_badge").prepend(num);
		$("#top_panel_notification_dropdown_header_text").empty();
		$("#top_panel_notification_dropdown_header_text").prepend(num + " new");
		
		
	},
	 
	hideInsightsBadge: function()
	{
		$("#top_panel_notification_badge").empty();
		$("#top_panel_notification_dropdown_header_text").empty();
		$("#top_panel_notification_dropdown_header_text").prepend(0 + " new");
	}
}

new NotificationsTopPanelManager();

}