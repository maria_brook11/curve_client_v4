app.controller('MainCtrl', function($scope,$rootScope,$http,$location,$cookies,$parse,TConst,URLConst,$timeout,$filter,Upload) {
	
	APICallsFunction($scope,$rootScope,$http,$location);
	CurveUtilsFunction($scope,$rootScope,$http,$location);
	StorageFunction($scope,$rootScope,$http,$location);
	AletrsFunction($scope,$rootScope,$http,$location);
	DashbGMFunction($scope,$rootScope,$http,$location);
	
	//CurveConstants($scope,$rootScope,$http,$location);
	ApplicationsFunction($scope,$rootScope,$http,$location,TConst,URLConst,$filter);
	//DashboardsFunction($scope,$rootScope,$http,$location);
	TopPanelFunction($scope,$rootScope,$http,$location,URLConst,$filter,Upload,$timeout);
	//GlobalPainterFunction($scope,$rootScope,$http,$location);
	TutorlManagerFunction($scope,$rootScope,$http,$location,TConst,$timeout);
	//TutorlTextsFunction($scope,$rootScope,$http,$location);
	TutorlFactoryFunction($scope,$rootScope,$http,$location,TConst);
	SendEmailFunction($scope,$rootScope,$http,$location);
	//EmailValidFunction($scope,$rootScope,$http,$location);
	
	
	$scope.packetid = 0;
	$(document.body).keydown(function (event) {

		if (event.keyCode == 13) {
			
			if($rootScope.loginView){
				if($rootScope.loginTpl == 0 && $rootScope.viewForm == 0){
					
					//$("#loginSubmitBtn").trigger('click');
				}
				if($rootScope.loginTpl == 0 && $rootScope.viewForm == 1) {
					$("#forgetSubmitBtn").trigger('click');
				}
				if($rootScope.loginTpl == 1 && $rootScope.viewForm == 2) {
					$("#regSubmitBtn").trigger('click');
				}
				if($rootScope.loginTpl == 2 && $rootScope.viewForm == 3) {
					$("#startSubmitBtn").trigger('click');
				}
				if($rootScope.loginTpl == 3 && $rootScope.viewForm == 4) {
					$("#resPaswSubmitBtn").trigger('click');
				}
				if(event.preventDefault) {
					event.preventDefault();
				}else{
					event.returnValue = false;
				}
			}
		}
	});			
	

	
});
app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });
 
                event.preventDefault();
            }
        });
    };
});
