app.directive('tooltip', function ($document, $compile) {
  return {
    restrict: 'A',
    scope: true,
    link: function (scope, element, attrs) {

      var tip = $compile('<div ng-class="tipClass">{{ text }}<div class="tooltipD-arrow"></div></div>')(scope),
          tipClassName = 'tooltipD',
          tipActiveClassName = 'tooltipD-show';
		  
		  scope.tipClass = [tipClassName];
      scope.text = attrs.tooltip;
      
      if(attrs.tooltipPosition) {
        scope.tipClass.push('tooltipD-' + attrs.tooltipPosition);
      }
      else {
       scope.tipClass.push('tooltipD-down'); 
      }
      
      element.bind('mouseover', function (e) {
	  
      $document.find('body').append(tip);  
		  
		  
        tip.addClass(tipActiveClassName);
        
		var topW  = window.pageYOffset || document.documentElement.scrollTop;
		
        var pos = e.target.getBoundingClientRect(),
            offset = tip.offset(),
            tipHeight = tip.outerHeight(),
            tipWidth = tip.outerWidth(),
            elWidth = pos.width || pos.right - pos.left,
            elHeight = pos.height || pos.bottom - pos.top,
            tipOffset = 10;
        
        if(tip.hasClass('tooltipD-right')) {
          offset.top = pos.top - (tipHeight / 2) + (elHeight / 2);
          offset.left = pos.right + tipOffset;
        }
        else if(tip.hasClass('tooltipD-left')) {
          offset.top = pos.top - (tipHeight / 2) + (elHeight / 2);
          offset.left = pos.left - tipWidth - tipOffset;
        }
        else if(tip.hasClass('tooltipD-down')) {
          offset.top = pos.top + elHeight + tipOffset + topW;
          offset.left = pos.left - (tipWidth / 2) + (elWidth / 2);
        }
        else {
          offset.top = pos.top - tipHeight - tipOffset + topW;
          offset.left = pos.left - (tipWidth / 2) + (elWidth / 2);
        }

        tip.offset(offset);
      });
      
      element.bind('mouseout', function () {
        tip.removeClass(tipActiveClassName);
      });

      tip.bind('mouseover', function () {
        tip.addClass(tipActiveClassName);
      });

      tip.bind('mouseout', function () {
        tip.removeClass(tipActiveClassName);
      });

      
    }
  }
});